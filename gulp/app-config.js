var gulp = require('gulp');
var rename = require('gulp-rename');
var ngConstant = require('gulp-ng-constant');

gulp.task('config', function () {
  var myConfig = require('../config/app.config.json');
  var envConfig = myConfig[process.env.NODE_ENV || 'development'];

  return ngConstant({
    constants: envConfig,
    name: 'adminPanel.constants',
    deps: [],
    stream: true
  })
    .pipe(rename("app.config.js"))
    .pipe(gulp.dest('src/app/'));
});
