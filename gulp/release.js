'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');

var connect = require('gulp-connect');
var runSequence = require('run-sequence');

var util = require('util');


gulp.task('webserver', function() {
  connect.server({
    port: 3000,
    host: '0.0.0.0',
    livereload: false,
    root: ['dist','.tmp']
  });
});



gulp.task('release', function(callback) {
  runSequence('clean',
    'build',
    'webserver',
    callback);
});
