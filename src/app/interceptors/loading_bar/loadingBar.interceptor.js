(function (app) {
  app.factory('loadingBarInterceptor', ['ngProgressLite', '$q', function (ngProgressLite, $q) {
    var completeProgress,working;
    working = false;

    completeProgress = function() {
      if (working) {
        ngProgressLite.done();
        return working = false;
      }
    };

    return {
      request: function(request) {
        if (!working) {
          ngProgressLite.set(0);
          ngProgressLite.start();
          working = true;
        }
        return request;
      },
      requestError: function(request) {
        completeProgress();
        return $q.reject(request);
      },
      response: function(response) {
        completeProgress();
        return response;
      },
      responseError: function(response) {
        completeProgress();
        return $q.reject(response);
      }
    }
  }]);

  app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('loadingBarInterceptor');
  });

})(angular.module('adminPanel.interceptors.loadingBar', []));


