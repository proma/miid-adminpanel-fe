describe('Interceptors', function(){

  describe('loadingBar', function() {
    var element,
      $httpBackend,
      $http,
      Interceptor,
      ngProgress,
      $rootScope;

    beforeEach(function () {
      ngProgressLite = {
        start: function () {},
        done: function () {},
        set: function () {}
      };

      module(function($provide) {
        $provide.value('ngProgressLite', ngProgressLite);
      });
    });

    beforeEach(module('adminPanel.interceptors.loadingBar'));

    beforeEach(inject(function($injector) {
      $rootScope = $injector.get('$rootScope');
      $httpBackend = $injector.get('$httpBackend');
      $http = $injector.get('$http');
      Interceptor = $injector.get('loadingBarInterceptor', {
        '$rootScope': $rootScope
      });
    }));

    it('should be defined', function () {
      expect(Interceptor).toBeDefined();
    });


    it('should start the progress bar on a request', function () {
      spyOn(ngProgressLite, 'start');
      $httpBackend.expect('GET', '12345').respond(200, {});
      $http.get('12345');
      $httpBackend.flush();
      expect(ngProgressLite.start).toHaveBeenCalled();
    });

    it('should stop the progress bar on response', function () {
      spyOn(ngProgressLite, 'done');
      $httpBackend.expect('GET', '12345').respond(200, {});
      $http.get('12345');
      $httpBackend.flush();
      expect(ngProgressLite.done).toHaveBeenCalled();
    });


    it('should call reset if double requests occur', function () {
      spyOn(ngProgressLite, 'set');
      $httpBackend.expect('GET', 'bar').respond(200, {});
      $httpBackend.expect('GET', 'foo').respond(200, {});
      $http.get('bar');
      $http.get('foo');
      $httpBackend.flush();
      expect(ngProgressLite.set).toHaveBeenCalled();
    });
  });

});
