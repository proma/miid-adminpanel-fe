(function (app) {
})(angular.module('adminPanel.interceptors', [
  'adminPanel.interceptors.login',
  'adminPanel.interceptors.loadingBar',
  'adminPanel.interceptors.unauthorized'
]));
