(function (app) {

  app.config(['$httpProvider', 'jwtInterceptorProvider', function ($httpProvider, jwtInterceptorProvider) {

    jwtInterceptorProvider.tokenGetter = ['jwtHelper', 'config', '$http', '$state', '$localStorage', 'MIID_API',
      function (jwtHelper, config, $http, $state, $localStorage, MIID_API) {
        var currentToken = $localStorage.jwt;
        // there's not token stored, and url is not login page -> redirect user to login
        if (!currentToken && !config.url.endsWith('login.html')) {
          $state.go('login');
          return null;
        }

        // url is not for API call, jwt not needed
        if (!config.url.startsWith(MIID_API.base_url)) {
          return null;
        }

        var refreshToken = $localStorage.refreshToken;
        var HOUR_IN_MILLIS = 1000 * 60 * 60;
        var date = jwtHelper.getTokenExpirationDate(currentToken);

        // token is expired... send to login
        if (date.getTime() < Date.now()) {
          $state.go('login');
          return null;
        }

        // less than an hour for token to expire, let's refresh it
        if (date.getTime() - Date.now() < HOUR_IN_MILLIS && !config.nointercept) {
          return $http({
            url: MIID_API.base_url + '/auth/refresh',
            method: 'POST',
            data: {
              refreshToken: refreshToken
            },
            nointercept: true
          }).then(function (response) {
            var newToken = response.data['miidapi-jwt'];
            $localStorage.jwt = newToken;
            return newToken;
          });
        } else {
          return currentToken;
        }
      }];

    $httpProvider.interceptors.push('jwtInterceptor');
  }]);

})(angular.module('adminPanel.interceptors.login', []));
