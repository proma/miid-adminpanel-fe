describe('Interceptors', function(){

  describe('unauthorized', function() {
    var element,
      $httpBackend,
      $http,
      Interceptor,
      $state,
      $rootScope;

    beforeEach(function () {
      $state = {
        go: function () {}
      };

      module(function($provide) {
        $provide.value('$state', $state);
      });
    });
    beforeEach(module('adminPanel.interceptors.unauthorized'));

    beforeEach(inject(function($injector) {
      $rootScope = $injector.get('$rootScope');
      $state = $injector.get('$state');
      $rootScope.$state = $state;
      $httpBackend = $injector.get('$httpBackend');
      $http = $injector.get('$http');
      Interceptor = $injector.get('unauthorizedInterceptor', {
        '$rootScope': $rootScope,
        '$q': $injector.get('$q')
      });
    }));

    it('should be defined', function () {
      expect(Interceptor).toBeDefined();
    });

    it('should redirect to login state when unauthorized', function () {
      spyOn($rootScope.$state, 'go');
      $httpBackend.expect('GET', '12345').respond(401, {status: 401, headers: {}});
      $http.get('12345');
      $httpBackend.flush();
      expect($state.go).toHaveBeenCalled();
    });

    it('should not redirect to login state when request is ok', function () {
      spyOn($rootScope.$state, 'go');
      $httpBackend.expect('GET', '12345').respond(200, {});
      $http.get('12345');
      $httpBackend.flush();
      expect($state.go).not.toHaveBeenCalled();
    });

  });

});
