(function (app) {
  app.factory('unauthorizedInterceptor', ['$q', '$rootScope', function ($q, $rootScope) {
    return {
      responseError: function(rejection) {
        if (rejection.status === 401) {
          $rootScope.$state.go('login');
        }
        return $q.reject(rejection);
      },
      requestError: function (error) {
        return $q.reject(error);
      }
    }
  }]);

  app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('unauthorizedInterceptor');
  });
})(angular.module('adminPanel.interceptors.unauthorized', []));


