(function() {
  'use strict';

  var app = angular
    .module('adminPanel',
      [
        'base64',
        'ngCookies',
        'ngTouch',
        'ngSanitize',
        'ngMessages',
        'ngAria',
        'ngResource',
        'ngAnimate',
        'ngFileUpload',
        'ui.router',
        'ui.bootstrap',
        'permission',
        'angles',
        'ngProgressLite',
        'datePicker',
        'toastr',
        'pascalprecht.translate',
        'ngStorage',
        'ngJsTree',
        'oitozero.ngSweetAlert',
        'summernote',
        'dndLists',
        'angular-jwt',
        'rzModule',
        'angularSpectrumColorpicker',
        'slugifier',
        'lrInfiniteScroll',
        'angular-peity',
        'ngHighlight',
        'adminPanel.filters',
        'adminPanel.directives',
        'adminPanel.services',
        'adminPanel.constants',
        'adminPanel.api',
        'adminPanel.interceptors',
        'adminPanel.home',
        'adminPanel.login',
        'adminPanel.customer_management',
        'adminPanel.ecommerce',
        'adminPanel.marketing',
        'adminPanel.product',
        'adminPanel.cms',
        'adminPanel.personalize',
        'adminPanel.notFound',
        'adminPanel.settings',
        'adminPanel.support'
      ]);

  app.controller('AppController',function($scope, $rootScope, $localStorage, uibPaginationConfig, $translate){

    $rootScope.changeLanguage = function (lang) {
      console.log(lang);
      $translate.use(lang);
    };

    $rootScope.logout = function () {
      $localStorage.$reset();
    };

    console.log('called');
    $scope.$on('$translationChangeSuccess', function () {
      console.log('success');
       $translate('pagination.previous').then(function (text) {
        uibPaginationConfig.previousText = text;
      });
       $translate('pagination.next').then(function (text) {
        uibPaginationConfig.nextText = text;
      });
    });

     $translate('pagination.next').then(function (text) {
      uibPaginationConfig.nextText = text;
    });
     $translate('pagination.previous').then(function (text) {
      uibPaginationConfig.previousText = text;
    });

    $rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams) {
      $rootScope.previousState = from.name;
    });
  });

})();
