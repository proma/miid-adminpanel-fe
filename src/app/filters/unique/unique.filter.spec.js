'use strict';


describe('Filters', function () {
  describe('unique', function () {
    var unique;

    beforeEach(module('adminPanel'));
    beforeEach(module('adminPanel.filters.unique'));

    beforeEach(inject(function($filter){
      unique = $filter('unique');
    }));

    it('should get array with unique values',function (){
      var testArr = [
        {name:'John', age: 23},
        {name:'Sam', age: 23},
        {name:'James', age: 24}
      ];
      expect(unique(testArr,'age').length).toEqual(2);
      expect(unique(testArr, 'age')[0]).toEqual(testArr[0]);
      expect(unique(testArr, 'age')[1]).toEqual(testArr[2]);
    });

  });
});
