(function (app) {

  app.filter('unique', function () {
    return function (arr, field) {
      return _.uniq(arr, function (a) {
        return a[field];

      });
    };
  });

})(angular.module('adminPanel.filters.unique', []));
