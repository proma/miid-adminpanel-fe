(function (app) {

  app.filter('productCount', productCount);

  function productCount() {

    return function (obj) {

      var count = 0;

      if(obj.sections === undefined) {


      } else {

        if(obj.sections.length > 0) {

          obj.sections.forEach(function (section) {

            count += section.products.length;

          });
        }
      }

      return count;
    }

  }

})(angular.module('adminPanel.filters.collectionProductsCount',[]));
