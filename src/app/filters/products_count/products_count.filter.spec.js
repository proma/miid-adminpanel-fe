'use strict';

describe('Filters', function () {

  describe('products_count', function () {

    var products_count;

    beforeEach(module('adminPanel'));
    beforeEach(module('adminPanel.filters.collectionProductsCount'));

    beforeEach(inject(function($filter){
      products_count = $filter('productCount');
    }));

    it('should count products in a collection',function (){
      var testCollection =
      {
        "_id": "12345",
        "title": "test collection",
        "sections": [
          {
            "_id": '54456',
            "products": ['12','13']
          },
          {
            "_id": '9876',
            "products": ['45', '89']
          }
        ]
      };
      expect(products_count(testCollection)).toEqual(4);

    });

  });

});
