'use strict';
fdescribe('Filters', function () {
  fdescribe('fileSize', function () {
    var size;

    beforeEach(module('adminPanel'));
    beforeEach(module('adminPanel.filters.fileSize'));

    beforeEach(inject(function($filter){
      size = $filter('size');
    }));

    fit('should convert 23 to bytes',function (){
      expect(size(23)).toBe('23.0 bytes');
    });

    fit('should get undefined at 0', function () {
      expect(size(0)).toBe('NaN undefined');
    })
  });
});
