(function(app){
  'use strict'

  app.factory('NavigationFactory', NavigationFactory);

  NavigationFactory.$inject = ['$resource', '$q','URL'];


  function NavigationFactory($resource, $q , URL) {

    var service = {
      getAll: getNavigations,
      get: getNavigation,
      getBySlug: getbyslug,
      edit: editNavigation,
      create: createNavigation,
      delete: deleteNavigation
    };

    /*
     * Resource has the following methods
     * - save ( create new - POST )
     * - delete ( delete - DELETE )
     * - query ( gets all - GET )
     * - get ( gets by id - GET )
     * - edit ( edit - PUT )
     * - slug ( get by slug - GET   )
     * */

    var url = URL.NAVIGATIONS + '/:id';


    var resource = $resource(url , {} , {
      'query': {
        method: 'GET',
        isArray: false
      },
      edit: {
        method: 'PUT'
      },
      slug: {
        method: 'GET',
        url: URL.NAVIGATIONS + '/slug/:slug'
      }
    });


    // get List of Navigations

    function getNavigations(param) {

      var params = {};

      // apply limit  if available
      if(!_.isUndefined(param.limit) && !_.isNull(param.limit) ){
        params.limit = param.limit;
      }
      // apply page if available
      if(!_.isUndefined(param.page) && !_.isNull(param.page) ){
        params.page = param.page;
      }
      // apply sort_order if available
      if(!_.isUndefined(param.sortOrder) && !_.isNull(param.sortOrder) ){
        params.sort_order = param.sortOrder;
      }
      // apply sort_by if available
      if(!_.isUndefined(param.sortBy) && !_.isNull(param.sortBy) ){
        params.sort_by = param.sortBy;
      }
      // apply status filter if available
      if(!_.isUndefined(param.status) && !_.isNull(param.status) ){
        params.status = param.status;
      }

      // apply from data filter if available
      if(!_.isUndefined(param.serachText) && !_.isNull(param.searchText) ){
        params.title = param.searchText;
      }

      var deferred = $q.defer();

      resource.query(params, function (result) {
        URL.resolve(result, deferred);
      }, function (error) {
        deferred.reject(error);
      });

      return deferred.promise;
    }


    // get By Slug

    function getbyslug(slug) {
      // Query params
      var params = {
        'slug': slug
      };
      var deferred = $q.defer();

      resource.slug(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // get Navigation by ID

    function getNavigation(id) {

      // Query params
      var params = {
        id: id
      };
      var deferred = $q.defer();

      resource.get(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;

    }

    // delete Navigation by ID

    function deleteNavigation(id) {

      // Query params
      var params = {
        id: id
      };
      var deferred = $q.defer();

      resource.delete(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;

    }

    // edit Navigation by ID

    function editNavigation(id, obj) {
      // Query params
      var params = {id: id};

      var deferred = $q.defer();

      resource.edit(params,obj, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // create Navigation

    function createNavigation(params) {
      // Query params
      params = params || {};

      var deferred = $q.defer();

      resource.save(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;

    }

    return service;

  }

})(angular.module('adminPanel.api.navigation', []));
