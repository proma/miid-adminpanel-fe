(function(app){
  'use strict';

  app.factory('BrandsFactory', BrandsFactory);

  BrandsFactory.$inject = [ '$resource', '$q' , 'URL'];

  function BrandsFactory($resource, $q, URL){
    var service = {
      getAll: getBrands,
      get: getBrand,
      updateStatus: updateStatus,
      save: save,
      edit: editBrand,
      editContact: editContact,
      createContact: createContact,
      getShippingOptions: getShippingOptions,
      updateShippingOptions: updateShippingOptions
    };
    var singleURL = URL.BRANDS + '/admin/:id';
    var brandUrl = URL.BRANDS + '/:id';
    var contactURL = URL.BRANDS + '/:id/contacts/:contactID';

    var shippingOptionsURL = URL.BRANDS + '/:id/shipping_options';

    /*
     * Resource has the following methods
     * - query ( gets all - GET )
     * - get ( gets by id - GET )
     * - save ( adds a new brand - POST)
     * */

    var brandResource = $resource(brandUrl,null, {
      'query': {
        method: 'GET',
        isArray: false
      },
      'updateStatus': {
        method: 'PUT',
        url: brandUrl + '/status'
      },
      'get': {
        method: 'GET',
        url: singleURL
      },
      'edit': {
        method: 'PUT'
      },
      'editContact': {
        method: 'PUT',
        url: contactURL
      },
      'createContact': {
        method: 'POST',
        url: contactURL
      },
      'getShippingOptions': {
        method: 'GET',
        url: shippingOptionsURL,
        isArray: true
      },
      'updateShippingOptions': {
        method: 'PUT',
        url: shippingOptionsURL
      }
    });

    // Create Brand
    function save(params){
      // API prepared payload
      params = params || {};

      var deferred = $q.defer();

      brandResource.save(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });
      return deferred.promise;
    }

    // Get List of Brands

    function getBrands(limit, page, listType, status, search){
      // Query params
      var params = {};
      // set limit param if available
      if(!_.isUndefined(limit) && !_.isNull(limit)){
        params.limit = limit;
      }

      // set page param if available
      if(!_.isUndefined(page) && !_.isNull(page)){
        params.page = page;
      }

      // set list values if available
      if(!_.isUndefined(listType) && !_.isNull(listType)){
        params.list_type = listType;
      }

      if(!_.isUndefined(status) && !_.isNull(status)){
        params.status = status;
      }

      if(!_.isUndefined(search) && !_.isNull(search) && !_.isEmpty(search)){
        params.search = search;
      }

      var deferred = $q.defer();

      brandResource.query(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // Get Brand By ID

    function getBrand(id){
      // Query params
      var params = {
        id: id
      };
      var deferred = $q.defer();

      brandResource.get(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // edit Brand

    function editBrand(id, obj){
      // Query params
      var params = {
        id: id
      };
      var deferred = $q.defer();

      brandResource.edit(params, obj || {}, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    //edit contact of brand by id

    function editContact(brandID, contactID, obj){
      // Query params
      var params = {
        id: brandID,
        contactID: contactID
      };
      var deferred = $q.defer();

      brandResource.editContact(params, obj || {}, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    //create contact of brand by id

    function createContact(brandID, obj){
      // Query params
      var params = {
        id: brandID
      };
      var deferred = $q.defer();

      brandResource.createContact(params, obj || {}, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    function updateStatus(id, status) {
      var params = {
        id: id
      };
      var payload = {
        status: status
      };
      var deferred = $q.defer();
      brandResource.updateStatus(params, payload, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    function getShippingOptions(id){

      var params = {
        id: id
      };

      var deferred = $q.defer();

      brandResource.getShippingOptions(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    function updateShippingOptions(id, obj_arr){
      // Query params
      var params = {
        id: id
      };

      var payload  = {
        shipping_options: obj_arr
      };

      var deferred = $q.defer();

      brandResource.updateShippingOptions(params, payload, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    return service;
  }

})(angular.module('adminPanel.api.brands',[]));
