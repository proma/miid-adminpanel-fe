'use strict';

describe('Factories', function () {
  describe('BrandsFactory', function () {

    var data = [
      {
        name: 'brand1',
        id: 'brand1_id'
      },
      {
        name: 'brand2',
        id: 'brand2_id'
      },
      {
        name: 'brand3',
        id: 'brand3_id'
      },
    ];

    var BrandsFactory,
      $httpBackend,
      $rootScope,
      $http,
      $q,
      URL;

    // load the dependencies module
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.api.apiurl'));
    beforeEach(module('adminPanel.api.brands'));


    //Inject modules
    beforeEach(inject(function ($injector) {
      $httpBackend = $injector.get('$httpBackend');
      $rootScope = $injector.get('$rootScope');
      BrandsFactory = $injector.get('BrandsFactory');
      URL = $injector.get('URL');
      $http = $injector.get('$http');
      $q = $injector.get('$q');
    }));


    describe('MA-117: List brands', function() {

      it('should get all Brands', function () {
        $httpBackend.expect('GET', URL.BRANDS).respond(200, data);
        var brands = {};
        BrandsFactory.getAll().then(function (data) {
          brands = data;
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data);
        deferred.promise.then(function (data) {
          $httpBackend.flush();
          expect(brands).toEqual(data);
        })
      });

    });

    describe('MBA-192: List brands', function() {

      it('should update brand status', function () {
        $httpBackend.expect('PUT', URL.BRANDS + '/' + data[0].id + '/status').respond(200, {result: 'success'});
        var brands = {};
        BrandsFactory.updateStatus(data[0].id, 'ACTIVE').then(function (response) {
          $httpBackend.flush();
          expect(response.result).toEqual('success');
        });
      });

    });

    describe('MA-191: Add brand', function() {

      it('should create a brand',function(){

        $httpBackend.expect('POST', URL.BRANDS).respond(200, data[0]);
        var brand = {};
        BrandsFactory.save(data[0]).then(function(data){
          brand = data; //created data
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(brand).toEqual(data);
        })
      });

    });

  });
});
