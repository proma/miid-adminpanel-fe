'use strict';

describe('Factories', function () {
  describe('UIFactory', function () {

    var data = {
      id: 'product1_id',
      name: 'product1',
      steps: [
        {
          name: 'step1',
          id: 'step1_id',
          groups:
            [
              {
                name: 'group1',
                id: 'group1_id',
                ui:[
                  {
                    id: 'ui1_id'
                  },
                  {
                    id: 'ui2_id'
                  }
                ]
              }
            ]
        }
      ]
    };

    var UIFactory,
      $httpBackend,
      $rootScope,
      $http,
      $q,
      URL,
      uiURL;

    // load the dependencies module
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.api.apiurl'));
    beforeEach(module('adminPanel.api.ui'));


    //Inject modules
    beforeEach(inject(function ($injector) {
      $httpBackend = $injector.get('$httpBackend');
      $rootScope = $injector.get('$rootScope');
      UIFactory = $injector.get('UIFactory');
      URL = $injector.get('URL');
      $http = $injector.get('$http');
      $q = $injector.get('$q');
      uiURL = URL.UI.replace(':productId', data.id);
      uiURL = uiURL.replace(':stepId', data.steps[0].id);
      uiURL = uiURL.replace(':groupId', data.steps[0].groups[0].id);
    }));


    describe('MA-77: Product UI', function() {

      it('should get all UI', function () {
        $httpBackend.expect('GET', uiURL).respond(200, data.steps[0].groups[0].ui);
        var ui = {};
        UIFactory.query(data.id, data.steps[0].id, data.steps[0].groups[0].id).then(function (data) {
          ui = data;
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.steps[0].groups[0].ui);
        deferred.promise.then(function (data) {
          $httpBackend.flush();
          expect(ui).toEqual(data);
        })
      });

      it('should get one ui',function(){
        $httpBackend.expect('GET', uiURL + '/' + data.steps[0].groups[0].ui[0].id).respond(200, data.steps[0].groups[0].ui[0]);
        var ui = {};
        UIFactory.get(data.id, data.steps[0].id, data.steps[0].groups[0].id, data.steps[0].groups[0].ui[0].id).then(function(data){
          ui = data;
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.steps[0].groups[0].ui[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(ui.id).toEqual(data.id);
        })
      });

      it('should delete a ui',function(){
        $httpBackend.expect('DELETE', uiURL + '/' + data.steps[0].groups[0].ui[0].id ).respond(200, data.steps[0].groups[0].ui[0]);
        var ui = {};
        UIFactory.remove(data.id, data.steps[0].id, data.steps[0].groups[0].id, data.steps[0].groups[0].ui[0].id).then(function(data){
          ui = data; //deleted data
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.steps[0].groups[0].ui[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(ui.id).toEqual(data.id);
        })
      });

      it('should create a ui',function(){

        $httpBackend.expect('POST', uiURL).respond(200, data.steps[0].groups[0].ui[0]);
        var ui = {};
        UIFactory.save(data.id, data.steps[0].id, data.steps[0].groups[0].id, data.steps[0].groups[0].ui[0]).then(function(data){
          ui = data; //created data
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.steps[0].groups[0].ui[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(ui).toEqual(data);
        })
      });

      it('should edit ui',function(){

        $httpBackend.expect('PUT', uiURL + '/' + data.steps[0].groups[0].ui[0].id).respond(200, data.steps[0].groups[0].ui[0]);
        var ui = {};
        UIFactory.edit(data.id, data.steps[0].id, data.steps[0].groups[0].id, data.steps[0].groups[0].ui[0].id, data.steps[0].groups[0].ui[0]).then(function(data){
          ui = data; //edited data
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.steps[0].groups[0].ui[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(ui.id).toEqual(data.id);
        })
      });

    });

  });
});
