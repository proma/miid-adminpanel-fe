(function(app){
  'use strict'

  app.factory('UIFactory', UIFactory);

  UIFactory.$inject = [ '$resource', '$q' , 'URL']

  function UIFactory($resource, $q, URL){

    /*
     * This factory is for UI resource
     * - save ( create new UI)
     * - get ( get existing UI )
     * - query ( to get all existing UI )
     * - remove ( delete an existing UI )
     * - edit ( update existing UI )
     * */

    // cached model data for UI resource
    var model = {};

    var service = {
      model: model,
      save: save,
      get: get,
      remove: remove,
      query: query,
      edit: edit
    };
    var uiURL = URL.UI;
    var singleUIURL = URL.UI + '/:uiId';

     // save UI
    function save(productId,stepId,groupId,params){

      var uiResource = $resource(uiURL, {productId: productId, stepId: stepId, groupId: groupId});

      // API prepared payload
      params = params || {};

      var deferred = $q.defer();

      uiResource.save(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // get UI
    function get(productId,stepId,groupId,uiId){

      var uiResource = $resource(singleUIURL
        , {productId: productId, stepId: stepId, groupId: groupId, uiId: uiId });

      var deferred = $q.defer();

      uiResource.get( function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // remove UI
    function remove(productId,stepId,groupId,uiId){

      var uiResource = $resource(singleUIURL
        , {productId: productId, stepId: stepId, groupId: groupId, uiId: uiId });

      var deferred = $q.defer();

      uiResource.delete(function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // edit UI
    function edit(productId,stepId,groupId,uiId,params){

      var uiResource = $resource(singleUIURL
        , {productId: productId, stepId: stepId, groupId: groupId, uiId: uiId }
        , {'edit': { method:'PUT' }
      });

      var deferred = $q.defer();

      uiResource.edit(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // query all UIs
    function query(productId,stepId,groupId){

      var uiResource = $resource(uiURL, {productId: productId, stepId: stepId, groupId: groupId});

      var deferred = $q.defer();

      uiResource.query(function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }
    // return service object
    return service;
  }

})(angular.module('adminPanel.api.ui',[]));
