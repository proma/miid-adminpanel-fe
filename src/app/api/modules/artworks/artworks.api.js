(function (app) {

  app.service('ArtworksFactory', ArtworksFactory);

  ArtworksFactory.$inject = ['$resource', '$q' , 'URL'];

  function ArtworksFactory ($resource, $q, URL) {
    var service = {
      query: getArtworks,
      get: getArtwork,
      delete: removeArtwork,
      edit: editArtwork,
      create: createArtwork
    };

    var url = URL.ARTWORKS + '/:id';

    /*
     * Resource has the following methods
     * - query ( gets all - GET )
     * - get ( get one {id} - GET )
     * - delete ( delete one {id} - DELETE )
     * - edit ( edit by id with data - PUT )
     * - create ( save new obj - POST )
     * */

    var resource = $resource(url , {} , {
      'query': {
        method: 'GET',
        isArray: false
      },
      edit: {
        method: 'PUT'
      }
    });

    //Get ALL
    function getArtworks (params) {
      var page = params.page;
      params = _.pick(params, _.identity); //remove null keys
      params.page = page;

      var deferred = $q.defer();

      resource.query(params, function(result){
        deferred.resolve(result);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    //get one
    function getArtwork (id) {
      var params = {'id': id};

      var deferred = $q.defer();

      resource.get(params, function(result){
        deferred.resolve(result);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // Delete one
    function removeArtwork (id) {
      var params = {'id': id};

      var deferred = $q.defer();

      resource.delete(params, function(result){
        deferred.resolve(result);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    //edit one
    function editArtwork (id, data) {
      var params = {'id': id};

      var deferred = $q.defer();

      resource.edit(params, data, function(result){
        deferred.resolve(result);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    //save one
    function createArtwork (data) {
      var deferred = $q.defer();

      resource.save(data, function(result){
        deferred.resolve(result);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    return service;
  }

})(angular.module('adminPanel.api.artworks', []));
