(function(app){
  'use strict'

  app.factory('ScenesFactory', ScenesFactory);

  ScenesFactory.$inject = [ '$resource', '$q' , 'URL']

  function ScenesFactory($resource, $q, URL){

    /*
     * This factory is for scenes resource
     * - save ( create new Scene)
     * - query ( to get all existing Scene )
     * - remove ( delete an existing Scene )
     * - edit ( update existing Scene )
     * */

    var service = {
      save: save,
      remove: remove,
      query: query,
      edit: edit
    };
    var sceneURL = URL.SCENES;
    var singleSceneURL = URL.SCENES + '/:sceneId';

     // save Scene
    function save(productId,params){

      var scenesResource = $resource(sceneURL
        ,{productId: productId}
        ,{'save': { method:'PUT' }}
      );

      // API prepared payload
      params = params || {};

      var deferred = $q.defer();

      scenesResource.save(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // remove Scene
    function remove(productId, sceneId){

      var sceneResource = $resource(singleSceneURL
        , {productId: productId, sceneId: sceneId});

      var deferred = $q.defer();

      sceneResource.delete(function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // edit Scene
    function edit(productId, sceneId, params){

      var sceneResource = $resource(singleSceneURL
        , {productId: productId, sceneId: sceneId}
        , {'edit': { method:'PUT' }
      });

      var deferred = $q.defer();

      sceneResource.edit(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // query all Scenes
    function query(productId){

      var sceneResource = $resource(sceneURL, {productId: productId});

      var deferred = $q.defer();

      sceneResource.query(function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }
    // return service object
    return service;
  }

})(angular.module('adminPanel.api.scenes',[]));
