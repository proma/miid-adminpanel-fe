'use strict';

describe('Factories', function () {
  describe('ScenesFactory', function () {

    var data = {
      id: 'product1_id',
      name: 'product1',
      scenes:
      [
        {
          name: 'scene1',
          id: 'scene1_id'
        },
        {
          name: 'scene1',
          id: 'scene1_id'
        }
      ]
    };

    var ScenesFactory,
      $httpBackend,
      $rootScope,
      $http,
      $q,
      URL,
      scenesURL;

    // load the dependencies module
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.api.apiurl'));
    beforeEach(module('adminPanel.api.scenes'));


    //Inject modules
    beforeEach(inject(function ($injector) {
      $httpBackend = $injector.get('$httpBackend');
      $rootScope = $injector.get('$rootScope');
      ScenesFactory = $injector.get('ScenesFactory');
      URL = $injector.get('URL');
      $http = $injector.get('$http');
      $q = $injector.get('$q');
      scenesURL = URL.SCENES.replace(':productId', data.id);
    }));


    describe('MBA-303: Product scenes', function() {

      it('should get all scenes', function () {
        $httpBackend.expect('GET', scenesURL).respond(200, data.scenes);
        var scenes = {};
        ScenesFactory.query(data.id).then(function (data) {
          scenes = data;
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.scenes);
        deferred.promise.then(function (data) {
          $httpBackend.flush();
          expect(scenes).toEqual(data);
        })
      });

      it('should delete a scene',function(){
        $httpBackend.expect('DELETE', scenesURL + '/' + data.scenes[0].id ).respond(200, data.scenes[0]);
        var scene = {};
        ScenesFactory.remove(data.id, data.scenes[0].id).then(function(data){
          scene = data; //deleted data
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.scenes[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(scene.id).toEqual(data.id);
        })
      });

      it('should create a scene',function(){

        $httpBackend.expect('PUT', scenesURL).respond(200, data.scenes[0]);
        var scene = {};
        ScenesFactory.save(data.id, data.scenes[0]).then(function(data){
          scene = data; //created data
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.scenes[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(scene).toEqual(data);
        })
      });

      it('should edit scene',function(){

        $httpBackend.expect('PUT', scenesURL + '/' + data.scenes[0].id).respond(200, data.scenes[0]);
        var scene = {};
        ScenesFactory.edit(data.id, data.scenes[0].id,data.scenes[0]).then(function(data){
          scene = data; //edited data
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.scenes[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(scene.id).toEqual(data.id);
        })
      });

    });

  });
});
