(function(app){
  'use strict'

  app.factory('GroupsFactory', GroupsFactory);

  GroupsFactory.$inject = [ '$resource', '$q' , 'URL']

  function GroupsFactory($resource, $q, URL){

    /*
     * This factory is for Groups resource
     * - save ( create new Group)
     * - get ( get existing Group )
     * - query ( to get all existing Groups )
     * - remove ( delete an existing Group )
     * - edit ( update existing Group )
     * */

    // cached model data for Groups resource
    var model = {};

    var service = {
      model: model,
      save: save,
      get: get,
      remove: remove,
      query: query,
      edit: edit
    };
    var groupsURL = URL.GROUPS;
    var singleGroupURL = URL.GROUPS + '/:groupId';

     // save Group
    function save(productId,stepId,params){

      var groupResource = $resource(groupsURL, {productId: productId, stepId: stepId});

      // API prepared payload
      params = params || {};

      var deferred = $q.defer();

      groupResource.save(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // get Group
    function get(productId,stepId,groupId){

      var groupResource = $resource(singleGroupURL
        , {productId: productId, stepId: stepId, groupId: groupId});

      var deferred = $q.defer();

      groupResource.get( function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // remove Group
    function remove(productId,stepId,groupId){

      var groupResource = $resource(singleGroupURL
        , {productId: productId, stepId: stepId, groupId: groupId});

      var deferred = $q.defer();

      groupResource.delete(function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // edit Step
    function edit(productId,stepId,groupId,params){

      var groupResource = $resource(singleGroupURL
        , {productId: productId, stepId: stepId, groupId: groupId}
        , {'edit': { method:'PUT' }
      });

      var deferred = $q.defer();

      groupResource.edit(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // query all Groups
    function query(productId,stepId){

      var groupResource = $resource(groupsURL, {productId: productId, stepId: stepId});

      var deferred = $q.defer();

      groupResource.query(function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }
    // return service object
    return service;
  }

})(angular.module('adminPanel.api.groups',[]));
