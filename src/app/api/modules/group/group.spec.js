'use strict';

describe('Factories', function () {
  describe('GroupsFactory', function () {

    var data = {
      id: 'product1_id',
      name: 'product1',
      steps: [
        {
          name: 'step1',
          id: 'step1_id',
          groups:
            [
              {
                name: 'group1',
                id: 'group1_id'
              },
              {
                name: 'group2',
                id: 'group2_id'
              }
            ]
        }
      ]
    };

    var GroupsFactory,
      $httpBackend,
      $rootScope,
      $http,
      $q,
      URL,
      groupsURL;

    // load the dependencies module
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.api.apiurl'));
    beforeEach(module('adminPanel.api.groups'));


    //Inject modules
    beforeEach(inject(function ($injector) {
      $httpBackend = $injector.get('$httpBackend');
      $rootScope = $injector.get('$rootScope');
      GroupsFactory = $injector.get('GroupsFactory');
      URL = $injector.get('URL');
      $http = $injector.get('$http');
      $q = $injector.get('$q');
      groupsURL = URL.GROUPS.replace(':productId', data.id);
      groupsURL = groupsURL.replace(':stepId', data.steps[0].id);
    }));


    describe('MA-77: Product Groups', function() {

      it('should get all groups', function () {
        $httpBackend.expect('GET', groupsURL).respond(200, data.steps[0].groups);
        var group = {};
        GroupsFactory.query(data.id, data.steps[0].id).then(function (data) {
          group = data;
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.steps[0].groups);
        deferred.promise.then(function (data) {
          $httpBackend.flush();
          expect(group).toEqual(data);
        })
      });

      it('should get one group',function(){
        $httpBackend.expect('GET', groupsURL + '/' + data.steps[0].groups[0].id).respond(200, data.steps[0].groups[0]);
        var group = {};
        GroupsFactory.get(data.id, data.steps[0].id, data.steps[0].groups[0].id).then(function(data){
          group = data;
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.steps[0].groups[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(group.id).toEqual(data.id);
        })
      });

      it('should delete a group',function(){
        $httpBackend.expect('DELETE', groupsURL + '/' + data.steps[0].groups[0].id ).respond(200, data.steps[0].groups[0]);
        var group = {};
        GroupsFactory.remove(data.id, data.steps[0].id, data.steps[0].groups[0].id).then(function(data){
          group = data; //deleted data
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.steps[0].groups[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(group.id).toEqual(data.id);
        })
      });

      it('should create a group',function(){

        $httpBackend.expect('POST', groupsURL).respond(200, data.steps[0].groups[0]);
        var group = {};
        GroupsFactory.save(data.id, data.steps[0].id, data.steps[0].groups[0]).then(function(data){
          group = data; //created data
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.steps[0].groups[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(group).toEqual(data);
        })
      });

      it('should edit group',function(){

        $httpBackend.expect('PUT', groupsURL + '/' + data.steps[0].groups[0].id).respond(200, data.steps[0].groups[0]);
        var group = {};
        GroupsFactory.edit(data.id, data.steps[0].id, data.steps[0].groups[0].id,data.steps[0].groups[0]).then(function(data){
          group = data; //edited data
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.steps[0].groups[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(group.id).toEqual(data.id);
        })
      });

    });

  });
});
