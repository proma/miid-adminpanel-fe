(function(app){
  'use strict';

  app.factory('PagesFactory', PagesFactory);

  PagesFactory.$inject = ['$resource', '$q' , 'URL'];

  function PagesFactory( $resource, $q, URL){

    var service = {
      getAll: getPages,
      get: getPage,
      getBySlug: getbyslug,
      delete: removePage,
      edit: editPage,
      create: createPage
    };
    var url = URL.PAGES + '/:id';

    /*
     * Resource has the following methods
     * - save ( create new - POST )
     * - delete ( delete - DELETE )
     * - query ( gets all - GET )
     * - get ( gets by id - GET )
     * - edit ( edit - PUT )
     * */

    var resource = $resource(url , {} , {
      'query': {
        method: 'GET',
        isArray: false
      },
      edit: {
        method: 'PUT'
      },
      slug: {
        method: 'GET',
        url: URL.PAGES + '/slug/:slug'
      }
    });

    // Get List of Pages

    function getPages(param){
      // Query params
      var params = {};

      // apply limit  if available
      if(!_.isUndefined(param.limit) && !_.isNull(param.limit) ){
        params.limit = param.limit;
      }
      // apply page if available
      if(!_.isUndefined(param.page) && !_.isNull(param.page) ){
        params.page = param.page;
      }
      // apply sort_order if available
      if(!_.isUndefined(param.sortOrder) && !_.isNull(param.sortOrder) ){
        params.sort_order = param.sortOrder;
      }
      // apply sort_by if available
      if(!_.isUndefined(param.sortBy) && !_.isNull(param.sortBy) ){
        params.sort_by = param.sortBy;
      }
      // apply status filter if available
      if(!_.isUndefined(param.status) && !_.isNull(param.status) ){
        params.status = param.status;
      }
      // apply type filter if available
      if(!_.isUndefined(param.type) && !_.isEmpty(param.type) && !_.isNull(param.type) ){
        params.type = param.type;
      }
      // apply from data filter if available
      if(!_.isUndefined(param.serachText) && !_.isNull(param.searchText) ){
        params.title = param.searchText;
      }

      var deferred = $q.defer();

      resource.query(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }


    //get by slug

    function getbyslug(slug) {
      // Query params
      var params = {
        'slug': slug
      };
      var deferred = $q.defer();

      resource.slug(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // Get Collection By ID

    function getPage(id){
      // Query params
      var params = {
        id: id
      };
      var deferred = $q.defer();

      resource.get(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // Delete by ID

    function removePage(id){
      // Query params
      var params = {
        id: id
      };
      var deferred = $q.defer();

      resource.delete(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // Edit

    function editPage(id,obj){
      // Query params
      var params = {id: id};

      var deferred = $q.defer();

      resource.edit(params,obj, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // Create new Page

    function createPage(params){
      // Query params
      params = params || {};

      var deferred = $q.defer();

      resource.save(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    return service;
  }

})(angular.module('adminPanel.api.pages',[]));
