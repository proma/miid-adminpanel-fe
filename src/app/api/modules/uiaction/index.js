(function(app){
  'use strict';

  app.factory('UIActionFactory', UIActionFactory);

  UIActionFactory.$inject = [ '$resource', '$q' , 'URL']

  function UIActionFactory($resource, $q, URL){

    /*
     * This factory is for UI action resource
     * - save ( create new UI actions)
     * - remove ( delete an existing UI action)
     * - edit ( update existing UI actions )
     * */

    // cached model data for UI resource
    var model = {};

    var service = {
      model: model,
      save: save,
      remove: remove,
      edit: edit
    };
    var uiActionURL = URL.UIACTION + '/ui_actions';
    var singleUIActionURL = URL.UIACTION + '/:type/:actionId';

     // save UI
    function save(productId,stepId,groupId,uiId,params){

      var uiResource = $resource(uiActionURL,
        { productId: productId,
          stepId: stepId,
          groupId: groupId,
          uiId: uiId});

      // API prepared payload
      params = params || {};

      var deferred = $q.defer();

      uiResource.save(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // remove UI
    function remove(productId,stepId,groupId,uiId,type,actionId){

      var uiResource = $resource(singleUIActionURL
        , { productId: productId,
            stepId: stepId,
            groupId: groupId,
            uiId: uiId,
            type: type,
            actionId: actionId });

      var deferred = $q.defer();

      uiResource.delete(function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // edit UI
    function edit(productId,stepId,groupId,uiId,params){

      var uiResource = $resource(uiActionURL
        , { productId: productId,
            stepId: stepId,
            groupId: groupId,
            uiId: uiId }
        , {'edit': { method:'PUT' }
      });

      var deferred = $q.defer();

      uiResource.edit(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // query all UIs
    function query(productId,stepId,groupId){

      var uiResource = $resource(uiActionURL, {productId: productId, stepId: stepId, groupId: groupId});

      var deferred = $q.defer();

      uiResource.query(function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }
    // return service object
    return service;
  }

})(angular.module('adminPanel.api.uiaction',[]));
