'use strict';

describe('Factories', function () {
  describe('UIActionFactory', function () {

    var data = {
      id: 'product1_id',
      name: 'product1',
      steps: [
        {
          name: 'step1',
          id: 'step1_id',
          groups:
            [
              {
                name: 'group1',
                id: 'group1_id',
                ui:[
                  {
                    id: 'ui1_id',
                    ui_action: [
                      {
                        id: 'ui_action_1'
                      },{
                        id: 'ui_action_2'
                      }
                    ]
                  }
                ]
              }
            ]
        }
      ]
    };

    var UIActionFactory,
      $httpBackend,
      $rootScope,
      $http,
      $q,
      URL,
      uiActionURL;

    // load the dependencies module
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.api.apiurl'));
    beforeEach(module('adminPanel.api.uiaction'));


    //Inject modules
    beforeEach(inject(function ($injector) {
      $httpBackend = $injector.get('$httpBackend');
      $rootScope = $injector.get('$rootScope');
      UIActionFactory = $injector.get('UIActionFactory');
      URL = $injector.get('URL');
      $http = $injector.get('$http');
      $q = $injector.get('$q');
      uiActionURL = URL.UIACTION.replace(':productId', data.id);
      uiActionURL = uiActionURL.replace(':stepId', data.steps[0].id);
      uiActionURL = uiActionURL.replace(':groupId', data.steps[0].groups[0].id);
      uiActionURL = uiActionURL.replace(':uiId', data.steps[0].groups[0].ui[0].id);
    }));


    describe('MA-77: Product UI Action', function() {

      it('should delete a ui action',function(){
        $httpBackend.expect('DELETE', uiActionURL + '/' + data.steps[0].groups[0].ui[0].ui_action[0].id ).respond(200, data.steps[0].groups[0].ui[0].ui_action[0]);
        var uiAction = {};
        UIActionFactory.remove(data.id, data.steps[0].id, data.steps[0].groups[0].id, data.steps[0].groups[0].ui[0].id, data.steps[0].groups[0].ui[0].ui_action[0].id ).then(function(data){
          uiAction = data; //deleted data
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.steps[0].groups[0].ui[0].ui_action[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(uiAction.id).toEqual(data.id);
        })
      });

      it('should create a ui action',function(){

        $httpBackend.expect('POST', uiActionURL).respond(200, data.steps[0].groups[0].ui[0].ui_action[0]);
        var uiAction = {};
        UIActionFactory.save(data.id, data.steps[0].id, data.steps[0].groups[0].id, data.steps[0].groups[0].ui[0].id, data.steps[0].groups[0].ui[0].ui_action[0]).then(function(data){
          uiAction = data; //created data
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.steps[0].groups[0].ui[0].ui_action[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(uiAction).toEqual(data);
        })
      });

      it('should edit ui action',function(){

        $httpBackend.expect('PUT', uiActionURL + '/' + data.steps[0].groups[0].ui[0].ui_action[0].id ).respond(200, data.steps[0].groups[0].ui[0].ui_action[0] );
        var uiAction = {};
        UIActionFactory.edit(data.id, data.steps[0].id, data.steps[0].groups[0].id, data.steps[0].groups[0].ui[0].id, data.steps[0].groups[0].ui[0].ui_action[0].id, data.steps[0].groups[0].ui[0].ui_action[0]).then(function(data){
          uiAction = data; //edited data
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.steps[0].groups[0].ui[0].ui_action[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(uiAction.id).toEqual(data.id);
        })
      });

    });

  });
});
