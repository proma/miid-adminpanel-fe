(function (app) {
  'use strict'

  app.factory('CustomizerFactory', CustomizerFactory);

  CustomizerFactory.$inject = ['$http', '$q', 'MIID_ASSETS_API', '$base64', 'Upload'];

  function CustomizerFactory($http, $q, MIID_ASSETS_API, $base64, Upload) {
    var service = {
      getDirectory: getDirectory,
      removeDirectory: removeDirectory,
      removeFile: removeFile,
      resize: resizeAssets,
      upload: uploadAssets
    };
    var url = MIID_ASSETS_API.base_url;

    function uploadAssets(file, cb, param, type) {
      var request;
      if (type == 'files') {
        request = getRequestObject('PUT', url + MIID_ASSETS_API.endpoints.uploadFiles);
        request.data = {directory: param};
      } else if (type == 'folders') {
        request = getRequestObject('PUT', url + MIID_ASSETS_API.endpoints.uploadSubFolder + '?parent_dir='+ param);
        request.data = {parent_dir: param};
      } else {
        request = getRequestObject('PUT', url + MIID_ASSETS_API.endpoints.uploadZip);
        request.data = {};
      }

      var deferred = $q.defer();

      request.data.file = file;
      Upload
        .upload(request)
        .then(function(resp) {
          deferred.resolve(resp.data);
        }, function(resp) {
          deferred.reject(resp.data);
        }, function(evt) {
          cb && cb(parseInt(100.0 * evt.loaded / evt.total));
        });

      return deferred.promise;
    }

    function getDirectory(miid) {

      var request = getRequestObject('GET', url + MIID_ASSETS_API.endpoints.getDirectory);
      var deferred = $q.defer();

      request.params = {
        directory: miid
      };

      $http(request)
        .success(function (result) {
          deferred.resolve(result);
        })
        .error(function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function resizeAssets(path) {
      var request = getRequestObject('POST', url + MIID_ASSETS_API.endpoints.resizeDirectory);
      var deferred = $q.defer();

      request.data = {
        directory: path
      };

      $http(request)
        .success(function (result) {
          deferred.resolve(result);
        })
        .error(function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function removeDirectory(path) {
      var request = getRequestObject('DELETE', url + MIID_ASSETS_API.endpoints.removeDirectory);
      var deferred = $q.defer();

      request.data = {
        directory: path
      };

      $http(request)
        .success(function (result) {
          deferred.resolve(result);
        })
        .error(function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function removeFile(path) {
      var request = getRequestObject('DELETE', url + MIID_ASSETS_API.endpoints.removeFile);
      var deferred = $q.defer();

      request.data = {
        filePath: path
      };

      $http(request)
        .success(function (result) {
          deferred.resolve(result);
        })
        .error(function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }



    function getRequestObject(method, url) {
      var request = {
        method: method,
        url: url,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Basic ' + $base64.encode(MIID_ASSETS_API.auth.username + ':' + MIID_ASSETS_API.auth.password)
        }
      };
      return request;
    }

    return service;
  }

})(angular.module('adminPanel.api.customizer', []));
