(function(app){
  'use strict'

  app.factory('LayersFactory', LayersFactory);

  LayersFactory.$inject = [ '$resource', '$q' , 'URL']

  function LayersFactory($resource, $q, URL){

    /*
     * This factory is for Layers resource
     * - get ( get existing Layer )
     * - query ( to get all existing Layers )
     * - remove ( delete an existing Layer )
     * - edit ( update existing Layer )
     * - editAll ( update all existing Layers )
     * */

    // cached model data for Layers resource
    var model = {};

    var service = {
      model: model,
      get: get,
      remove: remove,
      query: query,
      edit: edit,
      editAll: editAll
    };
    var layersURL = URL.LAYERS;
    var singleLayerURL = URL.LAYERS + '/:layerId';

    // get Layer
    function get(productId, layerId){

      var layersResource = $resource(singleLayerURL
        , {productId: productId, layerId: layerId});

      var deferred = $q.defer();

      layersResource.get( function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // remove Layer
    function remove(productId, layerId){

      var layersResource = $resource(singleLayerURL
        , {productId: productId, layerId: layerId});

      var deferred = $q.defer();

      layersResource.delete(function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // edit Layer
    function edit(productId, layerId, params){

      var layersResource = $resource(singleLayerURL
        , {productId: productId, layerId: layerId}
        , {'edit': { method:'PUT' }
      });

      var deferred = $q.defer();

      layersResource.edit(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // edit All Layers
    function editAll(productId, params){

      var layersResource = $resource(layersURL
        , {productId: productId}
        , {'edit': { method:'PUT', isArray:true }
        });

      var deferred = $q.defer();

      layersResource.edit(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // query all Layers
    function query(productId){

      var layersResource = $resource(layersURL, {productId: productId});

      var deferred = $q.defer();

      layersResource.query(function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }
    // return service object
    return service;
  }

})(angular.module('adminPanel.api.layers',[]));
