'use strict';

describe('Factories', function () {
  describe('LayerFactory', function () {

    var data = {
      id: 'product1_id',
      name: 'product1',
      layers:
      [
        {
          name: 'layer1',
          id: 'layer1_id'
        },
        {
          name: 'layer2',
          id: 'layer2_id'
        }
      ]
    };

    var LayerFactory,
      $httpBackend,
      $rootScope,
      $http,
      $q,
      URL,
      layersURL;

    // load the dependencies module
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.api.apiurl'));
    beforeEach(module('adminPanel.api.layers'));


    //Inject modules
    beforeEach(inject(function ($injector) {
      $httpBackend = $injector.get('$httpBackend');
      $rootScope = $injector.get('$rootScope');
      LayerFactory = $injector.get('LayersFactory');
      URL = $injector.get('URL');
      $http = $injector.get('$http');
      $q = $injector.get('$q');
      layersURL = URL.LAYERS.replace(':productId', data.id);
    }));


    describe('MA-96: Product layers', function() {

      it('should get all layers', function () {
        $httpBackend.expect('GET', layersURL).respond(200, data.layers);
        var layers = {};
        LayerFactory.query(data.id).then(function (data) {
          layers = data;
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.layers);
        deferred.promise.then(function (data) {
          $httpBackend.flush();
          expect(layers).toEqual(data);
        })
      });

      it('should get one layer',function(){
        $httpBackend.expect('GET', layersURL + '/' + data.layers[0].id).respond(200, data.layers[0]);
        var layer = {};
        LayerFactory.get(data.id, data.layers[0].id).then(function(data){
          layer = data;
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.layers[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(layer.id).toEqual(data.id);
        })
      });

      it('should delete a layer',function(){
        $httpBackend.expect('DELETE', layersURL + '/' + data.layers[0].id ).respond(200, data.layers[0]);
        var layer = {};
        LayerFactory.remove(data.id, data.layers[0].id).then(function(data){
          layer = data; //deleted data
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.layers[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(layer.id).toEqual(data.id);
        })
      });

      it('should edit layer',function(){

        $httpBackend.expect('PUT', layersURL + '/' + data.layers[0].id).respond(200, data.layers[0]);
        var layer = {};
        LayerFactory.edit(data.id, data.layers[0].id,data.layers[0]).then(function(data){
          layer = data; //edited data
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.layers[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(layer.id).toEqual(data.id);
        })
      });

      it('should edit all layers',function(){

        $httpBackend.expect('PUT', layersURL).respond(200, data.layers);
        var layer = {};
        LayerFactory.editAll(data.id, data.layers[0]).then(function(data){
          layer = data; //edited data
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.layers[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(layer.id).toEqual(data.id);
        })
      });

    });

  });
});
