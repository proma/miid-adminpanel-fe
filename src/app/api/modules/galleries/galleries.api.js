(function (app) {

  app.service('GalleriesFactory', GalleriesFactory);

  GalleriesFactory.$inject = ['$resource', '$q' , 'URL'];

  function GalleriesFactory ($resource, $q, URL) {
    var service = {
      query: getGalleries,
      getTags: getTags,
      get: getGallery,
      delete: removeGallery,
      edit: editGallery,
      create: createGallery
    };

    var url = URL.GALLERIES + '/:id';

    /*
     * Resource has the following methods
     * - query ( gets all - GET )
     * - getTags ( gets all Tags - GET )
     * - get ( get one {id} - GET )
     * - delete ( delete one {id} - DELETE )
     * - edit ( edit by id with data - PUT )
     * - create ( save new obj - POST )
     * */

    var resource = $resource(url , {} , {
      'query': {
        method: 'GET',
        isArray: false
      },
      edit: {
        method: 'PUT'
      },
      'getTags': {
        method: 'GET',
        isArray: true,
        url: URL.GALLERIES + '/tags'
      }
    });

    //Get ALL
    function getGalleries (params) {
      var page = params.page;
      params = _.pick(params, _.identity);
      params.page = page;

      var deferred = $q.defer();

      resource.query(params, function(result){
        deferred.resolve(result);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    function getTags () {
      var deferred = $q.defer();

      resource.getTags({} , function(result){
        deferred.resolve(result);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    //get one
    function getGallery (id) {
      var params = {'id': id};

      var deferred = $q.defer();

      resource.get(params, function(result){
        deferred.resolve(result);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // Delete one
    function removeGallery (id) {
      var params = {'id': id};

      var deferred = $q.defer();

      resource.delete(params, function(result){
        deferred.resolve(result);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    //edit one
    function editGallery (id, data) {
      var params = {'id': id};

      var deferred = $q.defer();

      resource.edit(params, data, function(result){
        deferred.resolve(result);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    //save one
    function createGallery (data) {
      var deferred = $q.defer();

      resource.save(data, function(result){
        deferred.resolve(result);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    return service;
  }

})(angular.module('adminPanel.api.galleries', []));
