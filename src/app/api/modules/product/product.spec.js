'use strict';

describe('Factories', function () {
  describe('ProductsFactory', function () {

    var data = [
      {
        name: 'product1',
        id: 'product1_id'
      },
      {
        name: 'product2',
        id: 'product2_id'
      }
    ];

    var ProductsFactory,
      $httpBackend,
      $rootScope,
      $http,
      $q,
      URL;

    // load the dependencies module
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.api.apiurl'));
    beforeEach(module('adminPanel.api.products'));


    //Inject modules
    beforeEach(inject(function ($injector) {
      $httpBackend = $injector.get('$httpBackend');
      $rootScope = $injector.get('$rootScope');
      ProductsFactory = $injector.get('ProductsFactory');
      URL = $injector.get('URL');
      $http = $injector.get('$http');
      $q = $injector.get('$q');
    }));


    describe('MA-92: Products list', function() {

      it('should get all products', function () {
        $httpBackend.expect('GET', URL.PRODUCTS).respond(200, data);
        var products = {};
        ProductsFactory.query().then(function (data) {
          products = data;
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data);
        deferred.promise.then(function (data) {
          $httpBackend.flush();
          expect(products).toEqual(data);
        })
      });

      it('should get all products for paginated query', function () {
        $httpBackend.expect('GET', URL.PRODUCTS+"?limit=5&page=1").respond(200, data);
        var products = {};
        ProductsFactory.paginatedQuery({limit:5,page:1}).then(function (data) {
          products = data;
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data);
        deferred.promise.then(function (data) {
          $httpBackend.flush();
          expect(products).toEqual(data);
        })
      });

      it('should get all products with sort order desc', function () {
        $httpBackend.expect('GET', URL.PRODUCTS+"?sortOrder=desc").respond(200, data);
        var products = {};
        ProductsFactory.paginatedQuery({sortOrder:"desc"}).then(function (data) {
          products = data;
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data);
        deferred.promise.then(function (data) {
          $httpBackend.flush();
          expect(products).toEqual(data);
        })
      });

      it('should get all products with sort by desc', function () {
        $httpBackend.expect('GET', URL.PRODUCTS+"?sortBy=status").respond(200, data);
        var products = {};
        ProductsFactory.paginatedQuery({sortby:"status"}).then(function (data) {
          products = data;
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data);
        deferred.promise.then(function (data) {
          $httpBackend.flush();
          expect(products).toEqual(data);
        })
      });

      it('should get all products filter by status', function () {
        $httpBackend.expect('GET', URL.PRODUCTS+"?status=ACTIVE").respond(200, data);
        var products = {};
        ProductsFactory.paginatedQuery({status:"ACTIVE"}).then(function (data) {
          products = data;
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data);
        deferred.promise.then(function (data) {
          $httpBackend.flush();
          expect(products).toEqual(data);
        })
      });

      it('should get all products filter by brand', function () {
        $httpBackend.expect('GET', URL.PRODUCTS+"?brand=NIKE").respond(200, data);
        var products = {};
        ProductsFactory.paginatedQuery({brand:"NIKE"}).then(function (data) {
          products = data;
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data);
        deferred.promise.then(function (data) {
          $httpBackend.flush();
          expect(products).toEqual(data);
        })
      });

      it('should get all products filter by date', function () {
        $httpBackend.expect('GET', URL.PRODUCTS+"?from=1/1/2014&to=1/1/2015").respond(200, data);
        var products = {};
        ProductsFactory.paginatedQuery({from:"1/1/2014", to:"1/1/2015"}).then(function (data) {
          products = data;
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data);
        deferred.promise.then(function (data) {
          $httpBackend.flush();
          expect(products).toEqual(data);
        })
      });

      it('should get all products including archived', function () {
        $httpBackend.expect('GET', URL.PRODUCTS+"?archived=true").respond(200, data);
        var products = {};
        ProductsFactory.paginatedQuery({archived:true}).then(function (data) {
          products = data;
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data);
        deferred.promise.then(function (data) {
          $httpBackend.flush();
          expect(products).toEqual(data);
        })
      });

      it('should get one product',function(){
        $httpBackend.expect('GET', URL.PRODUCTS+ '/product1_id').respond(200, data[0]);
        var product = {};
        ProductsFactory.get(data.id).then(function(data){
          product = data;
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(product.id).toEqual(data.id);
        })
      });

      it('should delete an product',function(){
        $httpBackend.expect('DELETE', URL.PRODUCTS+'/product1_id').respond(200, data[0]);
        var product = {};
        ProductsFactory.remove(data.id).then(function(data){
          product = data; //deleted data
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(product.id).toEqual(data.id);
        })
      });

      it('should create a product',function(){

        $httpBackend.expect('POST', URL.PRODUCTS).respond(200, data[0]);
        var product = {};
        ProductsFactory.save(data[0]).then(function(data){
          product = data; //created data
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(product).toEqual(data);
        })
      });

      it('should edit the status of product',function(){

        $httpBackend.expect('PUT', URL.PRODUCTS+'/product1_id').respond(200, data[0]);
        var product = {};
        ProductsFactory.edit(data.id,data[0]).then(function(data){
          product = data; //edited data
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(product.id).toEqual(data.id);
        })
      });

      it('should duplicate given product',function(){

        $httpBackend.expect('POST', URL.PRODUCTS+'/product1_id/duplicate').respond(200, data[0]);
        var product = {};
        ProductsFactory.duplicate(data.id,{miid_id:'testId'}).then(function(data){
          product = data; //edited data
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(product.id).toEqual(data.id);
        })
      });

    });

    describe('MA-273: Products list text search', function() {

      it('should get all products for text search', function () {
        $httpBackend.expect('GET', URL.PRODUCTS+"?text=product").respond(200, data);
        var products = {};
        ProductsFactory.paginatedQuery({text:'product'}).then(function (data) {
          products = data;
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data);
        deferred.promise.then(function (data) {
          $httpBackend.flush();
          expect(products).toEqual(data);
        })
      });
    });

    describe('MBA-151: Product List in drop down', function () {

      it('should call list_values EP and return product list for drop downs', function () {
        $httpBackend.expect('GET', URL.PRODUCTS+"/list_values").respond(200, data);
        var products = {};
        ProductsFactory.getList().then(function (data) {
          products = data;
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data);
        deferred.promise.then(function (data) {
          $httpBackend.flush();
          expect(products).toEqual(data);
        })
      });

    });

    describe('MBA-312: Delete cached images of a product', function () {

      it('should delete cached images of this product', function () {
          $httpBackend.expect('DELETE', URL.PRODUCTS+'/clear/product1_id').respond(200, data[0]);
          var product = {};
          ProductsFactory.clearCachedImages(data.id).then(function(data){
            product = data; //deleted data
          });
          var deferred = $q.defer();
          //creating fake promise to equal the both data.
          deferred.resolve(data[0]);
          deferred.promise.then(function(data){
            $httpBackend.flush();
            expect(product.id).toEqual(data.id);
          })
        });

    });

  });
});
