(function(app){
  'use strict';

  app.factory('ProductsFactory', ProductsFactory);

  ProductsFactory.$inject = [ '$resource', '$q' , 'URL']

  function ProductsFactory($resource, $q, URL){
    var cache = {}; //cache the list

    // Service Definition
    /*
     * This factory is for products resource
     * - save ( create new Product)
     * - get ( get existing Product )
     * - query ( to get all existing Products )
     * - remove ( delete an existing Product )
     * - edit ( update existing Product )
     * - paginatedQuery ( get paginated data for products )
     * - duplicate ( create a new product with duplicate data of selected product)
     * - editRelatedProducts ( PUT: add, remove related product ids from given product)
     * - getBySlug (GET: gets product by slug)
     * */

    var service = {
      model:{}, // Cached model data for product resource
      getProductId: function(){
        if(this.model.$$state && this.model.$$state.value && this.model.$$state.value.product_id){
          this.model.productId = this.model.$$state.value.product_id;
          return this.model.productId;
        } else if( this.model.productId){
          return this.model.productId;
        } else {
          return '';
        }
      },
      setProductId:function(id){
         this.model.productId = id;
      },
      setEdit: function(edit) {
        this.model.edit = edit;
      },
      getEdit: function(edit){
        return (this.model.edit && this.model.edit == true);
      },
      save: save,
      query: query,
      remove: remove,
      edit: edit,
      get: get,
      paginatedQuery: paginatedQuery,
      duplicate: duplicate,
      getList: getlistProducts,
      clearCachedImages: clearCachedImages,
      editRelatedProducts: editRelatedProd,
      getRelatedProducts: getRelatedProd,
      getBySlug: getBySlug,
      cache: {
        available: function() {
          return cache.productsList != null;
        },
        getProductsList: function() {
          return cache.productsList || [];
        }
      }
    };

    // Product resource URL
    var productURL = URL.PRODUCTS;
    var singleProductURL = URL.PRODUCTS + '/:productId';
    var duplicateProductURL = singleProductURL + '/duplicate';

    // Product Resource
    var productResource = $resource(productURL, {} , {
      'listsProducts': {
        url: URL.PRODUCTS + '/list_values',
        method: 'GET',
        isArray: true
      },
      'clearCachedImages': {
        url: URL.PRODUCTS + '/clear/:productId',
        method: 'DELETE'
      },
      'editRelatedProducts': {
        url: URL.PRODUCTS + '/:id/related',
        isArray: true,
        method: 'PUT'
      },
      'getRelatedProducts': {
        url: URL.PRODUCTS + '/:id/related',
        isArray: true,
        method: 'GET'
      },
      'getBySlug': {
        url: URL.PRODUCTS + '/slug/:slug',
        isArray: false,
        method: 'GET'
      }
    });

    function getBySlug(slug) {
      // API prepared payload

      var deferred = $q.defer();

      productResource.getBySlug({slug: slug}, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    function getlistProducts(params) {
      // API prepared payload
      params = params || {};

      var deferred = $q.defer();

      productResource.listsProducts(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    function getRelatedProd(productID, params) {
      params = params || {};

      var deferred = $q.defer();
      params.id = productID;
      productResource.getRelatedProducts(params, function (result) {
        URL.resolve(result, deferred);
      }, function (error) {
        deferred.reject(error);
      });

      return deferred.promise;
    }

		function editRelatedProd(productID, params) {
      params = params || {}; //this will be an array when provided

      var deferred = $q.defer();

      productResource.editRelatedProducts({id: productID}, params, function (result) {
        URL.resolve(result, deferred);
      }, function (error) {
        deferred.reject(error);
      });

      return deferred.promise;
    }


    // Create Product
    function save(params){
      // API prepared payload
      params = params || {};

      var deferred = $q.defer();

      productResource.save(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });
      this.model = deferred.promise;
      return deferred.promise;
    }

    // get Product
    function get(productId){
      var singleProductResource = $resource(singleProductURL, {productId: productId});
      var deferred = $q.defer();

      singleProductResource.get( function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // remove Product
    function remove(productId){
      var singleProductResource = $resource(singleProductURL, {productId: productId});
      var deferred = $q.defer();

      singleProductResource.delete(function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    //remove Product cached images
    function clearCachedImages(productId) {
      var deferred = $q.defer();

      productResource.clearCachedImages({
        productId: productId
      },function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // paginated data  Product
    function paginatedQuery(param){

      // Query params
      var params = {};

      // apply limit if available
      if(!_.isUndefined(param.limit) && !_.isNull(param.limit) ){
        params.limit = param.limit;
      }
      // apply page if available
      if(!_.isUndefined(param.page) && !_.isNull(param.page) ){
        params.page = param.page;
      }
      // apply sort_order if available
      if(!_.isUndefined(param.sortOrder) && !_.isNull(param.sortOrder) ){
        params.sort_order = param.sortOrder;
      }
      // apply sort_by if available
      if(!_.isUndefined(param.sortBy) && !_.isNull(param.sortBy) ){
        params.sort_by = param.sortBy;;
      }
      // apply status filter if available
      if(!_.isUndefined(param.status) && !_.isNull(param.status) ){
        params.status = param.status;
      }
      // apply brand filter if available
      if(!_.isUndefined(param.brand) && !_.isNull(param.brand) ){
        params.brand = param.brand;
      }
      // apply from data filter if available
      if(!_.isUndefined(param.from) && !_.isNull(param.from) ){
        params.from = param.from;
      }
      // apply to filter if available
      if(!_.isUndefined(param.to) && !_.isNull(param.to) ){
        params.to = param.to;
      }
      // apply archived filter if available
      if(!_.isUndefined(param.archived) && !_.isNull(param.archived) ){
        params.archived = param.archived;
      }
      // apply text filter if available
      if(!_.isUndefined(param.text) && !_.isNull(param.text) && !_.isEmpty(param.text)){
        params.text = param.text;
      }

      var productResourceForPagination = $resource(productURL, params);

      var deferred = $q.defer();

      productResourceForPagination.get(function(result){
        URL.resolve(result, deferred);
        if (!result.statusCode) {
          cache.productsList = result.data;
          cache.paging = result.paging.total;
        }
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // edit Product
    function edit(productId, params){

      // API prepared payload
      params = params || {};

      var singleProductResource = $resource(singleProductURL
        , {productId: productId}
        , {'edit': { method:'PUT' }
        });

      var deferred = $q.defer();

      singleProductResource.edit(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // query all Products
    function query(){

      var deferred = $q.defer();

      productResource.query(function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // duplicate Product
    function duplicate(productId, params, duplicateAsset){

      // API prepared payload
      params = params || {};

      var duplicateProductResource = $resource(duplicateProductURL, {productId: productId, copyAssets: duplicateAsset});

      var deferred = $q.defer();

      duplicateProductResource.save(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    return service;
  }

})(angular.module('adminPanel.api.products',[]));
