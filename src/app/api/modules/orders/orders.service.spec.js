'use strict';

describe('Factories', function () {

  describe('OrdersFactory', function() {

    var data = [
      {
        "_id":"569948fbc2e1970903ee0a38",
        "multi_order_id":"569948fbc2e1970903ee0a37",
        "brand":
        {
          "_id":"568a7dd6e6c1530a0dd82917"
          ,"brand_id":"BRAND1",
          "name":"My brand 1"
        },
        "customer":
        {
          "_id":"568e10222e6f69fd05b44249",
          "username":"mejunaid",
          "name":"Junaid"
        },
        "created_by":null,
        "updated_at":"2016-02-01T15:24:46.510Z",
        "created_at":"2016-01-15T19:31:07.041Z",
        "coupon":
        {
          "total_used_count":0,
          "total_discount":0,
          "code":"",
          "id":null
        },
        "designs":
          [
            {
              "design":"5694a34c411f85e915d90192",
              "unit_price":63,
              "quantity":2
            },
            {
              "design":"5694bbfdf90a474118529253",
              "unit_price":42,
              "quantity":3
            }
          ],
        "comment":"Ring the bell!",
        "paid_amount":0,
        "total_amount":null,
        "payment_method":null,
        "status":"PENDING"
      }
    ];

    var OrdersService,
      $httpBackend,
      $rootScope,
      $http,
      $q,
      URL;

    // load the dependencies module
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.api.apiurl'));
    beforeEach(module('adminPanel.api.orders'));

    beforeEach(inject(function($injector){
      $httpBackend = $injector.get('$httpBackend');
      $rootScope = $injector.get('$rootScope');
      OrdersService = $injector.get('OrdersFactory');
      URL = $injector.get('URL');
      $http = $injector.get('$http');
      $q = $injector.get('$q');
    }));

    it('should get all orders',function(){
      $httpBackend.expect('GET', URL.ORDERS).respond(200, data);
      var orders = {};
      OrdersService.getAll().then(function(data){
        orders = data;
      });
      var deferred = $q.defer();
      //creating fake promise to equal the both data.
      deferred.resolve(data);
      deferred.promise.then(function(data){
        $httpBackend.flush();
        expect(orders).toEqual(data);
      })
    });

    it('should get one order',function(){
      $httpBackend.expect('GET', URL.ORDERS+ '/569948fbc2e1970903ee0a38').respond(200, data[0]);
      var orders = {};
      OrdersService.getOne("569948fbc2e1970903ee0a38").then(function(data){
        orders = data;
      });
      var deferred = $q.defer();
      //creating fake promise to equal the both data.
      deferred.resolve(data);
      deferred.promise.then(function(data){
        $httpBackend.flush();
        expect(orders._id).toEqual(data._id);
      })
    });

    it('should delete an order',function(){
      $httpBackend.expect('DELETE', URL.ORDERS+'orders/569948fbc2e1970903ee0a38').respond(200, data[0]);
      var orders = {};
      OrdersService.delete("569948fbc2e1970903ee0a38").then(function(data){
        orders = data; //deleted data
      });
      var deferred = $q.defer();
      //creating fake promise to equal the both data.
      deferred.resolve(data);
      deferred.promise.then(function(data){
        $httpBackend.flush();
        expect(orders._id).toEqual(data._id);
      })
    });

    it('should create an order',function(){
      var data ={
        shipping_address: "address here",
        delivery: "delivery info here",
        payment_method: 'UNION_PAY',
        coupon: "_idsfsafds"
      };

      $httpBackend.expect('POST', URL.ORDERS).respond(200, data);
      var order = {};
      OrdersService.create(data).then(function(data){
        order = data; //created data
      });
      var deferred = $q.defer();
      //creating fake promise to equal the both data.
      deferred.resolve(data);
      deferred.promise.then(function(data){
        $httpBackend.flush();
        expect(order).toEqual(data);
      })
    });

    it('should edit the status of order',function(){
      var data ={
        status: "PAYMENT_ERROR"
      };
      $httpBackend.expect('PUT', URL.ORDERS+'orders/569948fbc2e1970903ee0a38').respond(200, data);
      var order = {};
      OrdersService.edit(data).then(function(data){
        order = data; //edited data
      });
      var deferred = $q.defer();
      //creating fake promise to equal the both data.
      deferred.resolve(data);
      deferred.promise.then(function(data){
        $httpBackend.flush();
        expect(order.status).toEqual(data.status);
      })
    });

    it('should get all orders with sorting and filter params',function(){
      $httpBackend.expect('GET', URL.ORDERS +"?limit=10&page=1&sort_by=date&sort_order=desc&brand=568a7dd6e6c1530a0dd82917&status=PENDING&from=1/1/2016&to=4/1/2016" ).respond(200, data);
      var orders = {};
      OrdersService.getAll(10,1,'desc','date','PENDING','568a7dd6e6c1530a0dd82917','1/1/2016','4/1/2016').then(function(data){
        orders = data;
      });
      var deferred = $q.defer();
      //creating fake promise to equal the both data.
      deferred.resolve(data);
      deferred.promise.then(function(data){
        $httpBackend.flush();
        expect(orders).toEqual(data);
      })
    });

    it('should edit note of specific order',function(){
      $httpBackend.expect('PUT', URL.ORDERS+'/569948fbc2e1970903ee0a38/note').respond(200, {success: true});
      var order = {};
      OrdersService.saveNote('569948fbc2e1970903ee0a38', 'test note').then(function(data){
        order = data; //edited data
      });
      $httpBackend.flush();
      expect(order.success).toBeTruthy();
    });

    it('should generate po files for specific order',function(){
      $httpBackend.expect('GET', URL.ORDERS+'/generate_po/569948fbc2e1970903ee0a38').respond(200, {success: true});
      var order = {};
      OrdersService.genPO('569948fbc2e1970903ee0a38').then(function(data){
        order = data; //edited data
      });
      $httpBackend.flush();
      expect(order.success).toBeTruthy();
    });

  });



});
