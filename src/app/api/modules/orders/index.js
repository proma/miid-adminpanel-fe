(function(app){
  'use strict';

  app.factory('OrdersFactory', OrdersFactory);

  OrdersFactory.$inject = [ '$resource', '$q' , 'URL']

  function OrdersFactory($resource, $q, URL){
    var cache = {}; //cache the list

    var service = {
      getAll: getOrders,
      getOne: getOrder,
      delete: remove,
      edit: edit,
      create: create,
      saveNote: saveNote,
      genPO: genPo,
      addTracking: addTracking,
      editTracking: editTracking,
      deleteTracking: deleteTracking,
      exportPO: exportPO,
      Templates: {
        create: postPOTemplate,
        delete: deletePOTemplate,
        query: getPOTemplate
      },
      cache: {
        available: function() {
          return cache.ordersList != null;
        },
        getOrdersList: function() {
          return cache.ordersList;
        },
        getPaging: function () {
          return cache.paging;
        },
        getPage: function () {
          return cache.page;
        }
      }
    };
    var url = URL.ORDERS + '/:id';

    /*
    * Resource has the following methods
    * - save ( create new - POST )
    * - delete ( delete - DELETE )
    * - query ( gets all - GET )
    * - get ( gets by id - GET )
    * - edit ( edit - PUT )
    * - saveNote ( saves note - POST )
    * - generate po ( GET )
    * - export po orders (GET)
    * - get PO Template (GET)
    * - save PO Template (POST)
    * - delete PO Template (DELETE)
    * */

    var resource = $resource(url , {} , {
      'query': {
        method: 'GET',
        isArray: false
      },
      edit: {
        method: 'PUT'
      },
      saveNote: {
        method: 'PUT',
        url: URL.ORDERS + '/:id/note'
      },
      genPO: {
        method: 'GET',
        url: URL.ORDERS + '/generate_po/:id'
      },
      addTracking: {
        method: 'POST',
        url: URL.ORDERS + '/:id/tracking_details'
      },
      editTracking: {
        method: 'PUT',
        url: URL.ORDERS + '/:id/tracking_details/:trackingId'
      },
      deleteTracking: {
        method: 'DELETE',
        url: URL.ORDERS + '/:id/tracking_details/:trackingId'
      },
      exportPO: {
        method: 'POST',
        url: URL.ORDERS + '/export_po',
        transformRequest: function (data, headersGetter) {
          var headers = headersGetter();
          console.log(headers);
          return JSON.stringify(data);
        }
      },
      postTemplate: {
        method: 'POST',
        url: URL.ORDERS + '/po/templates'
      },
      deleteTemplate: {
        method: 'DELETE',
        url: URL.ORDERS + '/po/templates/:templateID'
      },
      getTemplates: {
        method: 'GET',
        isArray: true,
        url: URL.ORDERS + '/po/templates'
      }
    });

    function saveNote(orderID, note) {
      var params = {
        id: orderID
      };
      var obj = {
        'note': note
      };

      var deferred = $q.defer();

      resource.saveNote(params, obj, function(result) {
        URL.resolve(result, deferred);
      }, function(err) {
        deferred.reject(err);
      });

      return deferred.promise;
    }

    function genPo(orderID) {
      var params = {
        id: orderID
      };

      var deferred = $q.defer();

      resource.genPO(params, function(result) {
        URL.resolve(result, deferred);
      }, function(err) {
        deferred.reject(err);
      });

      return deferred.promise;
    }

    // Get List of Orders

    function getOrders(limit, page, sortOrder, sortBy, status, brand, from, to, filterPaymentError, search){
      // Query params
      var params = {};

      // apply payment_error filter  if available
      if(!_.isUndefined(filterPaymentError) && !_.isNull(filterPaymentError) ){
        params.payment_error = filterPaymentError;
      }

      // apply limit  if available
      if(!_.isUndefined(limit) && !_.isNull(limit) ){
        params.limit = limit;
      }
      // apply page if available
      if(!_.isUndefined(page) && !_.isNull(page) ){
        params.page = page;
        cache.page = page;
      }
      // apply sort_order if available
      if(!_.isUndefined(sortOrder) && !_.isNull(sortOrder) ){
        params.sort_order = sortOrder;
      }
      // apply sort_by if available
      if(!_.isUndefined(sortBy) && !_.isNull(sortBy) ){
        params.sort_by = sortBy;
      }
      // apply status filter if available
      if(!_.isUndefined(status) && !_.isNull(status) ){
        params.status = status;
      }
      // apply brand filter if available
      if(!_.isUndefined(brand) && !_.isNull(brand) ){
        params.brand = brand;
      }
      // apply from data filter if available
      if(!_.isUndefined(from) && !_.isNull(from) ){
        params.from = from;
      }
      // apply to filter if available
      if(!_.isUndefined(to) && !_.isNull(to) ){
        params.to = to;
      }
      // apply search filter if available
      if(!_.isUndefined(search) && !_.isNull(search) && !_.isEmpty(search) ){
        params.search = search;
      }

      var deferred = $q.defer();

      resource.query(params, function(result){
        URL.resolve(result, deferred);
        if (!result.statusCode) {
          cache.ordersList = result.data;
          cache.paging = result.paging.total;
        }
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // Get Order By ID

    function getOrder(id){
      // Query params
      var params = {
        id: id
      };
      var deferred = $q.defer();

      resource.get(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // Delete by ID

    function remove(id){
      // Query params
      var params = {
        id: id
      };
      var deferred = $q.defer();

      resource.delete(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // Edit

    function edit(id,obj){
      // Query params
      var params = {id: id};

      var deferred = $q.defer();

      resource.edit(params,obj, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // Create new order

    function create(params){
      // Query params
      params = params || {};

      var deferred = $q.defer();

      resource.save(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // Add tracking
    function addTracking(id, obj){
      var deferred = $q.defer();
      resource.addTracking({id: id}, obj, function(result){
          URL.resolve(result, deferred);
      },function(error){
          deferred.reject(error);
      });
      return deferred.promise;
    }
    // Edit tracking
    function editTracking(id, trackingId, obj){
      var deferred = $q.defer();
      resource.editTracking({id: id, trackingId: trackingId}, obj, function(result){
          URL.resolve(result, deferred);
      },function(error){
          deferred.reject(error);
      });
      return deferred.promise;
    }
    // Delete tracking
    function deleteTracking(id, trackingId){
      var deferred = $q.defer();
      resource.deleteTracking({id: id, trackingId: trackingId}, function(result){
          URL.resolve(result, deferred);
      },function(error){
          deferred.reject(error);
      });
      return deferred.promise;
    }

    function postPOTemplate(data) {
      data = data || {};
      var deferred = $q.defer();
      resource.postTemplate(data, function(result){
          URL.resolve(result, deferred);
      },function(error){
          deferred.reject(error);
      });
      return deferred.promise;
    }
    function getPOTemplate() {
      var deferred = $q.defer();
      resource.getTemplates(function(result){
          URL.resolve(result, deferred);
      },function(error){
          deferred.reject(error);
      });
      return deferred.promise;
    }
    function deletePOTemplate(templateID) {
      var deferred = $q.defer();
      resource.deleteTemplate({templateID: templateID}, function(result){
          URL.resolve(result, deferred);
      },function(error){
          deferred.reject(error);
      });
      return deferred.promise;
    }
    function exportPO(data) {
      data = data || {};
      var deferred = $q.defer();
      resource.exportPO(data, function(result){
          URL.resolve(result, deferred);
      },function(error){
          deferred.reject(error);
      });
      return deferred.promise;
    }

    return service;
  }

})(angular.module('adminPanel.api.orders',[]));
