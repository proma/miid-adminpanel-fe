(function(app){
  'use strict'

  app.factory('StepsFactory', StepsFactory);

  StepsFactory.$inject = [ '$resource', '$q' , 'URL']

  function StepsFactory($resource, $q, URL){

    /*
     * This factory is for steps resource
     * - save ( create new Step)
     * - get ( get existing Step )
     * - query ( to get all existing Steps )
     * - remove ( delete an existing Step )
     * - edit ( update existing Step )
     * */

    // cached model data for steps resource
    var model = {};

    var service = {
      model: model,
      save: save,
      get: get,
      remove: remove,
      query: query,
      edit: edit
    };
    var stepsURL = URL.STEPS;
    var singleStepURL = URL.STEPS + '/:stepId';

     // save Step
    function save(productId,params){

      var stepsResource = $resource(stepsURL, {productId: productId});

      // API prepared payload
      params = params || {};

      var deferred = $q.defer();

      stepsResource.save(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // get Step
    function get(productId, stepId){

      var stepsResource = $resource(singleStepURL
        , {productId: productId, stepId: stepId});

      var deferred = $q.defer();

      stepsResource.get( function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // remove Step
    function remove(productId, stepId){

      var stepsResource = $resource(singleStepURL
        , {productId: productId, stepId: stepId});

      var deferred = $q.defer();

      stepsResource.delete(function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // edit Step
    function edit(productId, stepId, params){

      var stepsResource = $resource(singleStepURL
        , {productId: productId, stepId: stepId}
        , {'edit': { method:'PUT' }
      });

      var deferred = $q.defer();

      stepsResource.edit(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // query all Steps
    function query(productId, getTree){

      var queryParam = {
        productId: productId
      };

      if(getTree)
        queryParam.getTitleTree = 'yes';

      var stepsResource = $resource(stepsURL, queryParam);

      var deferred = $q.defer();

      stepsResource.query(function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }
    // return service object
    return service;
  }

})(angular.module('adminPanel.api.steps',[]));
