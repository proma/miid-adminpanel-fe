'use strict';

describe('Factories', function () {
  describe('StepsFactory', function () {

    var data = {
      id: 'product1_id',
      name: 'product1',
      steps:
      [
        {
          name: 'step1',
          id: 'step1_id'
        },
        {
          name: 'step2',
          id: 'step2_id'
        }
      ]
    };

    var StepsFactory,
      $httpBackend,
      $rootScope,
      $http,
      $q,
      URL,
      stepsURL;

    // load the dependencies module
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.api.apiurl'));
    beforeEach(module('adminPanel.api.steps'));


    //Inject modules
    beforeEach(inject(function ($injector) {
      $httpBackend = $injector.get('$httpBackend');
      $rootScope = $injector.get('$rootScope');
      StepsFactory = $injector.get('StepsFactory');
      URL = $injector.get('URL');
      $http = $injector.get('$http');
      $q = $injector.get('$q');
      stepsURL = URL.STEPS.replace(':productId', data.id);
    }));


    describe('MA-77: Product steps', function() {

      it('should get all steps', function () {
        $httpBackend.expect('GET', stepsURL).respond(200, data.steps);
        var steps = {};
        StepsFactory.query(data.id).then(function (data) {
          steps = data;
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.steps);
        deferred.promise.then(function (data) {
          $httpBackend.flush();
          expect(steps).toEqual(data);
        })
      });

      it('should get one step',function(){
        $httpBackend.expect('GET', stepsURL + '/' + data.steps[0].id).respond(200, data.steps[0]);
        var step = {};
        StepsFactory.get(data.id, data.steps[0].id).then(function(data){
          step = data;
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.steps[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(step.id).toEqual(data.id);
        })
      });

      it('should delete a step',function(){
        $httpBackend.expect('DELETE', stepsURL + '/' + data.steps[0].id ).respond(200, data.steps[0]);
        var step = {};
        StepsFactory.remove(data.id, data.steps[0].id).then(function(data){
          step = data; //deleted data
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.steps[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(step.id).toEqual(data.id);
        })
      });

      it('should create a step',function(){

        $httpBackend.expect('POST', stepsURL).respond(200, data.steps[0]);
        var step = {};
        StepsFactory.save(data.id, data.steps[0]).then(function(data){
          step = data; //created data
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.steps[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(step).toEqual(data);
        })
      });

      it('should edit step',function(){

        $httpBackend.expect('PUT', stepsURL + '/' + data.steps[0].id).respond(200, data.steps[0]);
        var step = {};
        StepsFactory.edit(data.id, data.steps[0].id,data.steps[0]).then(function(data){
          step = data; //edited data
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.steps[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(step.id).toEqual(data.id);
        })
      });

    });

  });
});
