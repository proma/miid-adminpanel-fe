'use strict';

describe('Factories', function () {
  describe('GroupsFactory', function () {

    var data = {
      id: 'product1_id',
      name: 'product1',
      components: [
        {
          id: 'comp1_id',
          title: 'comp1',
          have_color: true,
          path: 'product/comp',
          color_mix:
            [
              {
                id: 'mix1_id',
                label: 'mix1',
                value: '#333333',
                opacity: 1
              },
              {
                id: 'mix2_id',
                label: 'mix2',
                value: '#222222',
                opacity: 1
              }
            ]
        }
      ]
    };

    var ColorMixFactory,
      $httpBackend,
      $rootScope,
      $http,
      $q,
      URL,
      mixURL;

    // load the dependencies module
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.api.apiurl'));
    beforeEach(module('adminPanel.api.colormix'));


    //Inject modules
    beforeEach(inject(function ($injector) {
      $httpBackend = $injector.get('$httpBackend');
      $rootScope = $injector.get('$rootScope');
      ColorMixFactory = $injector.get('ColorMixFactory');
      URL = $injector.get('URL');
      $http = $injector.get('$http');
      $q = $injector.get('$q');
      mixURL = URL.COLORMIX.replace(':productId', data.id);
      mixURL = mixURL.replace(':componentId', data.components[0].id);
    }));


    describe('Color mixes', function() {

      it('should delete a color mix',function(){
        $httpBackend.expect('DELETE', mixURL + '/' + data.components[0].color_mix[0].id ).respond(200, { result: 'success' });
        var response = {};
        ColorMixFactory.deleteMix(data.id, data.components[0].id, data.components[0].color_mix[0].id).then(function(data){
          expect(data.result).toEqual('success');
        });
      });

      it('should update color mixes',function(){

        $httpBackend.expect('PUT', mixURL).respond(200, data.components[0]);
        ColorMixFactory.update(data.id, data.components[0].id, data.components[0].color_mix[0]).then(function(resp){
          expect(resp.id).toEqual(data.id);
        });
      });

    });

  });
});
