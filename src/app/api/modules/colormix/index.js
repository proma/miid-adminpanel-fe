(function(app){
  'use strict'

  app.factory('ColorMixFactory', ColorMixFactory);

  ColorMixFactory.$inject = [ '$resource', '$q' , 'URL']

  function ColorMixFactory($resource, $q, URL){

    // Service Definition
    /*
     * This factory is for color mix resource
     * - update ( update component color mixes )
     * - delete ( delete component color mix )
     * */

    var service = {
      model:{}, // Cached model data for component resource
      create: create,
      update: update,
      deleteMix: deleteMix
    };

    var colorMixUrl = URL.COLORMIX;
    var singleColorMixURL = URL.COLORMIX + '/:mixId';

    function create(productId, componentId, mixData){

      var mixResource = $resource(colorMixUrl, {
        productId: productId,
        componentId: componentId
      }, {
        'edit': { method: 'PUT' }
      });

      var deferred = $q.defer();

      mixResource.edit(mixData, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    function update(productId, componentId, mixId, mixData){

      var mixResource = $resource(singleColorMixURL, {
        productId: productId,
        componentId: componentId,
        mixId: mixId
      }, {
        'edit': { method: 'PUT' }
      });

      var deferred = $q.defer();

      mixResource.edit(mixData, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    function deleteMix(productId, componentId, mixId){

      var mixResource = $resource(singleColorMixURL, {
        productId: productId,
        componentId: componentId,
        mixId: mixId
      });

      var deferred = $q.defer();

      mixResource.delete(function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // return service object
    return service;
  }

})(angular.module('adminPanel.api.colormix',[]));
