(function(app){
  'use strict'

  app.factory('UserReviewsFactory', UserReviewsFactory);

  UserReviewsFactory.$inject = [ '$resource', '$q' , 'URL']

  function UserReviewsFactory($resource, $q, URL){
    var cache = {}; //cache the list

    var service = {
      getAll: getReviews,
      updateStatus: updateStatus
    };
    var url = URL.USER_REVIEWS;

    var resource = $resource(url , {} , {
      getReviews: {
        method: 'GET',
        isArray: false
      },
      updateStatus: {
        method: 'PUT',
        url: URL.USER_REVIEWS.replace('/product', '') + '/:id/:status'
      }
    });

    function updateStatus(id, status) {
      //url params
      var params = {
        id: id,
        status: status
      };

      var deferred = $q.defer();

      resource.updateStatus(params, null, function(data) {
        URL.resolve(data, deferred);
      }, function(err) {
        deferred.reject(err);
      });

      return deferred.promise;
    }

    // Get List of Reviews

    function getReviews(limit, page, sortOrder, sortBy, status, brand, from, to, text, productId){
      // Query params
      var params = {};

      // apply limit  if available
      if(!_.isUndefined(limit) && !_.isNull(limit) ){
        params.limit = limit;
      }
      // apply page if available
      if(!_.isUndefined(page) && !_.isNull(page) ){
        params.page = page;
        cache.page = page;
      }
      // apply sort_order if available
      if(!_.isUndefined(sortOrder) && !_.isNull(sortOrder) ){
        params.sort_order = sortOrder;
      }
      // apply sort_by if available
      if(!_.isUndefined(sortBy) && !_.isNull(sortBy) ){
        params.sort_by = sortBy;
      }
      // apply status filter if available
      if(!_.isUndefined(status) && !_.isNull(status) ){
        params.status = status;
      }
      // apply brand filter if available
      if(!_.isUndefined(brand) && !_.isNull(brand) ){
        params.brand = brand;
      }
      // apply from data filter if available
      if(!_.isUndefined(from) && !_.isNull(from) ){
        params.from = from;
      }
      // apply to filter if available
      if(!_.isUndefined(to) && !_.isNull(to) ){
        params.to = to;
      }
      // apply TEXT filter if available
      if(!_.isUndefined(text) && !_.isNull(text) ){
        params.text = text;
      }
      // product Id
      if(!_.isUndefined(productId) && !_.isNull(productId) ){
        params.productId = productId;
      }

      var deferred = $q.defer();

      resource.getReviews(params, function(result){
        URL.resolve(result, deferred);
        if (!result.statusCode) {
          cache.userReviewsList = result.data;
          cache.paging = result.paging.total;
        }

      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    return service;
  }

})(angular.module('adminPanel.api.userReviews',[]));
