'use strict';

describe('Factories', function () {
  describe('CouponsFactory', function () {

    var data = {
      total: 3,
      data: [
        {
          name: 'coupon 1',
          id:'coupon_1'
        },
        {
          name: 'coupon 2',
          id:'coupon_2'
        }
      ]
    };

    var CouponsFactory,
      $httpBackend,
      $rootScope,
      $http,
      $q,
      URL;

    // load the dependencies module
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.api.apiurl'));
    beforeEach(module('adminPanel.api.coupons'));


    //Inject modules
    beforeEach(inject(function ($injector) {
      $httpBackend = $injector.get('$httpBackend');
      $rootScope = $injector.get('$rootScope');
      CouponsFactory = $injector.get('CouponsFactory');
      URL = $injector.get('URL');
      $http = $injector.get('$http');
      $q = $injector.get('$q');
    }));


    describe('MBA-151: Create Coupons', function() {

      it('should create a coupon',function(){

        $httpBackend.expect('POST', URL.COUPONS).respond(200, data.data[0]);
        var coupon = {};
        CouponsFactory.save(data[0]).then(function(data){
          coupon = data; //created data
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.data[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(coupon).toEqual(data);
        })
      });

      it('should get coupon by code',function(){

        $httpBackend.expect('GET', URL.COUPONS + '/code/123456').respond(200, data.data[0]);
        var coupon = {};
        CouponsFactory.getByCode('123456').then(function(data){
          coupon = data; //created data
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.data[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(coupon).toEqual(data);
        })
      });

    });

    describe('MBA-317: Coupons list', function() {


      it('should get all coupons', function () {
        $httpBackend.expect('GET', URL.COUPONS + '?limit=10&page=0').respond(200, data);
        var coupons = {};
        CouponsFactory.query({page: 0, limit: 10}).then(function(data){
          coupons = data; //created data
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.data[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(coupons).toEqual(data);
        })
      });

      it('should edit a coupon', function () {
        $httpBackend.expect('PUT', URL.COUPONS + data.data[0].id).respond(200, data.data[0]);
        var coupon = {};
        CouponsFactory.edit(data.data[0].id, {test: 'test'}).then(function(data){
          coupon = data; //created data
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data.data[0]);
        deferred.promise.then(function(data){
          $httpBackend.flush();
          expect(coupon).toEqual(data);
        })
      });

      it('should delete a coupon', function () {
        $httpBackend.expect('DELETE', URL.COUPONS +'/'+ data.data[0].id).respond(200, {success:true});
        var coupon = {};
        CouponsFactory.delete(data.data[0].id).then(function(data){
          coupon = data; //created data
        });

        $httpBackend.flush();
        expect(coupon).toBeTruthy();
      });

    });

  });
});
