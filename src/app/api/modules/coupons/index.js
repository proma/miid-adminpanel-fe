(function (app) {
  'use strict';

  app.factory('CouponsFactory', CouponsFactory);

  CouponsFactory.$inject = ['$resource', '$q', 'URL'];

  function CouponsFactory($resource, $q, URL) {
    var service = {
      query: getCoupons,
      get: getCoupon,
      save: postCoupon,
      getByCode: getCouponByCode,
      edit: editCoupon,
      delete: deleteCoupon
    };
    var url = URL.COUPONS + '/:id';


    var couponsRes = $resource(url, {}, {
      'query': {
        method: 'GET',
        isArray: false
      },
      'getByCode': {
        method: 'GET',
        url: URL.COUPONS + '/code/:code'
      },
      'edit': {
        method: 'PUT'
      }
    });

    function getCoupons(params) {

      params = params || {};

      var deferred = $q.defer();

      couponsRes.query(params, function (result) {
        URL.resolve(result, deferred);
      }, function (error) {
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // Delete by ID
    function deleteCoupon(id){
      // Query params
      var params = {
        id: id
      };
      var deferred = $q.defer();

      couponsRes.delete(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    function editCoupon(id, obj) {
      var obj = obj || {};
      var param = {'id': id};

      var deferred = $q.defer();
      couponsRes.edit(param, obj ,function (result) {
        URL.resolve(result, deferred);
      }, function (error) {
        deferred.reject(error);
      });

      return deferred.promise;
    }

    function getCouponByCode(code) {
      var deferred = $q.defer();
      couponsRes.getByCode({ 'code': code} ,function (result) {
        URL.resolve(result, deferred);
      }, function (error) {
        deferred.reject(error);
      });

      return deferred.promise;
    }

    function getCoupon(id) {
      var deferred = $q.defer();
      couponsRes.get({ 'id': id} ,function (result) {
        URL.resolve(result, deferred);
      }, function (error) {
        deferred.reject(error);
      });

      return deferred.promise;
    }

    function postCoupon (postObj) {

      postObj = postObj || {};

      var deferred = $q.defer();

      couponsRes.save(postObj ,function (result) {
        URL.resolve(result, deferred);
      }, function (error) {
        deferred.reject(error);
      });

      return deferred.promise;

    }

    return service;
  }

})(angular.module('adminPanel.api.coupons', []));
