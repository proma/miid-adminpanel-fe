(function(app){
  'use strict'

  app.factory('ComponentsFactory', ComponentsFactory);

  ComponentsFactory.$inject = [ '$resource', '$q' , 'URL']

  function ComponentsFactory($resource, $q, URL){

    // Service Definition
    /*
     * This factory is for product components resource
     * - query ( get all product components)
     * - createSubComponents (used for creating sub components of a sub folder uploaded)
     * - create (Used for creating all components after uploading assets)
     * - deleteAll (Used to delete all components if directory removed)
     * - queryGroupParts (get group parts only from product components)
     * - edit ( update existing Product Component label )
     * */

    var service = {
      model:{}, // Cached model data for component resource
      query: query,
      create: createComponents,
      createSubComponents: subComponents,
      queryGroupParts: queryGroupParts,
      edit: edit,
      deleteAll: deleteAll
    };

    // Components resource URL
    var componentsURL = URL.COMPONENTS;
    var singleComponentURL = URL.COMPONENTS + '/:componentId';

    function subComponents(productId, data) {
      var componentResource = $resource(URL.SUB_COMPONENTS, {productId: productId}, {
        edit: {
          method: 'PUT'
        }
      });

      var deferred = $q.defer();

      componentResource.edit(data, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    function createComponents(productId, data) {
      var componentResource = $resource(componentsURL, {productId: productId});

      var deferred = $q.defer();

      componentResource.save(data, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    function deleteAll(productId) {
      var componentResource = $resource(componentsURL, {productId: productId});

      var deferred = $q.defer();

      componentResource.delete(function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // query all Components
    function query(productId){

      var componentResource = $resource(componentsURL, {productId: productId});

      var deferred = $q.defer();

      componentResource.query(function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // query all Group Parts from Components
    function queryGroupParts(productId){

      var componentResource = $resource(componentsURL, {productId: productId});

      var deferred = $q.defer();

      componentResource.query({"type": "GROUPS_PARTS"},function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // edit Product Component
    function edit(productId, componentId, params){

      // API prepared payload
      params = params || {};

      var singleComponentResource = $resource(singleComponentURL
        , {productId: productId, componentId: componentId}
        , {'edit': { method:'PUT' }
        });

      var deferred = $q.defer();

      singleComponentResource.edit(params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // return service object
    return service;
  }

})(angular.module('adminPanel.api.components',[]));
