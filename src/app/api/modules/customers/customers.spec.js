'use strict';

describe('Factories', function () {
  describe('CustomersFactory', function () {

    var data = [
      {
        "_id": "56b8918c936e8b0600231ed5",
        "username": "roman",
        "name": "Roman",
        "password": "$2a$10$.YlrIEkPngeJ11P3qzaAF.Qqn5U4zNj5vNIll1U8JeYz2nC3Y/XJC",
        "followings": [],
        "media": [],
        "__v": 0,
        "updated_at": "2016-02-08T13:01:00.516Z",
        "created_at": "2016-02-08T13:01:00.503Z",
        "status": "ACTIVE",
        "following": [],
        "followers": [],
        "addresses": [],
        "background_img": null,
        "thumb": null,
        "gender": "M",
        "designs_collection": [],
        "login_with": {
          "qq": {
            "provider_id": null,
            "refresh_token": null,
            "access_token": null
          },
          "weibo": {
            "provider_id": null,
            "refresh_token": null,
            "access_token": null
          },
          "wechat": {
            "provider_id": null,
            "refresh_token": null,
            "access_token": null
          }
        },
        "contact": {
          "address": null,
          "email": null,
          "weibo": null,
          "wechat": null,
          "qq": null,
          "phone": null
        },
        "phone": "+8612341234",
        "email": null
      },
      {
        "_id": "56c6acc364c4760600b4e9bc",
        "username": "sheldon2",
        "name": "sheldon22",
        "password": "$2a$10$8kKTxUgyFzkdMqiypSuFpu/yawekJgYDlcYgK149XPpICqOfPwsk.",
        "followings": [],
        "__v": 0,
        "refreshToken": "338965ed-f00a-43a3-bab3-42810785d472",
        "updated_at": "2016-02-19T05:48:51.697Z",
        "created_at": "2016-02-19T05:48:51.685Z",
        "status": "ACTIVE",
        "following": [],
        "followers": [],
        "addresses": [],
        "background_img": null,
        "thumb": null,
        "gender": "M",
        "designs_collection": [],
        "login_with": {
          "qq": {
            "provider_id": null,
            "refresh_token": null,
            "access_token": null
          },
          "weibo": {
            "provider_id": null,
            "refresh_token": null,
            "access_token": null
          },
          "wechat": {
            "provider_id": null,
            "refresh_token": null,
            "access_token": null
          }
        },
        "contact": {
          "address": null,
          "email": null,
          "weibo": null,
          "wechat": null,
          "qq": null,
          "phone": null
        },
        "phone": "+86135445784283",
        "email": null
      }
    ];

    var CustomersFactory,
      $httpBackend,
      $rootScope,
      $http,
      $q,
      URL;

    // load the dependencies module
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.api.apiurl'));
    beforeEach(module('adminPanel.api.customers'));


    //Inject modules
    beforeEach(inject(function ($injector) {
      $httpBackend = $injector.get('$httpBackend');
      $rootScope = $injector.get('$rootScope');
      CustomersFactory = $injector.get('CustomersFactory');
      URL = $injector.get('URL');
      $http = $injector.get('$http');
      $q = $injector.get('$q');
    }));


    describe('List customers', function () {

      it('should get all customers', function () {
        $httpBackend.expect('GET', URL.CUSTOMERS).respond(200, data);
        var customers = {};
        CustomersFactory.getAll(10, 0, {}).then(function (data) {
          customers = data;
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data);
        deferred.promise.then(function (data) {
          $httpBackend.flush();
          expect(customers).toEqual(data);
        })
      });

    });

  });
});
