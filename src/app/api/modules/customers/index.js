(function (app) {
  'use strict'

  app.factory('CustomersFactory', CustomersFactory);

  CustomersFactory.$inject = ['$resource', '$q', 'URL'];

  function CustomersFactory($resource, $q, URL) {
    var service = {
      getAll: getCustomers
    };
    var url = URL.CUSTOMERS;

    function getCustomers(limit, page, filters) {

      var customersRes = $resource(url);
      var deferred = $q.defer();

      var params = {};

      // apply limit  if available
      if(!_.isUndefined(limit) && !_.isNull(limit) ){
        params.limit = limit;
      }
      // apply page if available
      if(!_.isUndefined(page) && !_.isNull(page) ){
        params.page = page;
      }

      // apply from data filter if available
      if(!_.isUndefined(filters.from) && !_.isNull(filters.from) ){
        params.from = filters.from.valueOf();
      }
      // apply to filter if available
      if(!_.isUndefined(filters.to) && !_.isNull(filters.to) ){
        params.to = filters.to.valueOf();
      }
      // apply search filter if available
      if(!_.isUndefined(filters.text) && !_.isNull(filters.text) && filters.text.trim().length > 0 ){
        params.search = filters.text.trim();
      }


      customersRes.get(params, function (result) {
        URL.resolve(result, deferred);
      }, function (error) {
        deferred.reject(error);
      });

      return deferred.promise;
    }

    return service;
  }

})(angular.module('adminPanel.api.customers', []));
