(function (app) {
  'use strict'

  app.factory('CategoriesFactory', CategoriesFactory);

  CategoriesFactory.$inject = ['$resource', '$q', 'URL'];

  function CategoriesFactory($resource, $q, URL) {
    var service = {
      getAll: getCategories,
      getOne: getCategory
    };
    var url = URL.CATEGORIES;
    var singleUrl = URL.CATEGORIES + '/:slug';

    function getCategories() {

      var categoriesRes = $resource(url, {}, {
        'query': {
          method: 'GET',
          isArray: true
        }
      });

      var deferred = $q.defer();

      categoriesRes.query(function (result) {
        URL.resolve(result, deferred);
      }, function (error) {
        deferred.reject(error);
      });

      return deferred.promise;
    }

    function getCategory(slug) {

      var categoriesRes = $resource(singleUrl, {slug: slug});

      var deferred = $q.defer();

      categoriesRes.get(function (result) {
        URL.resolve(result, deferred);
      }, function (error) {
        deferred.reject(error);
      });

      return deferred.promise;
    }

    return service;
  }

})(angular.module('adminPanel.api.categories', []));
