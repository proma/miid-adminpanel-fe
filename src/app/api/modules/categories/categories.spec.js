'use strict';

describe('Factories', function () {
  describe('CategoriesFactory', function () {

    var data = [
      {
        title: "CAT 1",
        image: "some/image",
        slug: "SLUG1",
        sub_categories: [
          {
            title: "SUB 1.1",
            slug: "SLUG 1.1"
          },
          {
            title: "SUB 1.2",
            slug: "SLUG 1.2"
          }
        ]
      },
      {
        title: "CAT 2",
        image: "some/image2",
        slug: "SLUG2",
        sub_categories: [
          {
            title: "SUB 2.1",
            slug: "SLUG 2.1"
          },
          {
            title: "SUB 2.2",
            slug: "SLUG 2.2"
          }
        ]
      }
    ];

    var CategoriesFactory,
      $httpBackend,
      $rootScope,
      $http,
      $q,
      URL;

    // load the dependencies module
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.api.apiurl'));
    beforeEach(module('adminPanel.api.categories'));


    //Inject modules
    beforeEach(inject(function ($injector) {
      $httpBackend = $injector.get('$httpBackend');
      $rootScope = $injector.get('$rootScope');
      CategoriesFactory = $injector.get('CategoriesFactory');
      URL = $injector.get('URL');
      $http = $injector.get('$http');
      $q = $injector.get('$q');
    }));


    describe('List categories', function () {

      it('should get all categories', function () {
        $httpBackend.expect('GET', URL.CATEGORIES).respond(200, data);
        var categories = {};
        CategoriesFactory.getAll().then(function (data) {
          categories = data;
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data);
        deferred.promise.then(function (data) {
          $httpBackend.flush();
          expect(categories).toEqual(data);
        })
      });

    });

    describe('Get category by slug', function () {

      it('should return the category', function () {
        $httpBackend.expect('GET', URL.CATEGORIES + '/SLUG1').respond(200, data[0]);
        var cat = {};
        CategoriesFactory.getOne("SLUG1").then(function (data) {
          cat = data;
        });
        var deferred = $q.defer();
        //creating fake promise to equal the both data.
        deferred.resolve(data[0]);
        deferred.promise.then(function (data) {
          $httpBackend.flush();
          expect(cat).toEqual(data);
        })
      });

    });

  });
});
