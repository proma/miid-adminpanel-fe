(function(app){
  'use strict';

  app.factory('ConditionsFactory', ConditionsFactory);

  ConditionsFactory.$inject = [ '$resource', '$q' , 'URL'];

  function ConditionsFactory($resource, $q, URL){

    /*
     * This factory is for conditions resource
     * - save ( create new conditions)
     * - remove ( delete an existing condition )
     * - edit ( update existing condition )
     * - query ( gets all conditions )
     * - removeElement ( removes element from a specific condition )
     * */

    var service = {
      query: query,
      save: save,
      edit: edit,
      remove: remove,
      removeElement: removeElement
    };
    var conditionsURL = URL.CONDITIONS;

    var conditionsResource = $resource(conditionsURL, {}, {
      edit: {
        method: 'PUT'
      }
    });

    // save condition
    function save(productId, params){

      // API prepared payload
      params = params || {};

      var query = {
        'productID': productId
      };

      var deferred = $q.defer();

      conditionsResource.save(query, params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // remove condition
    function remove(productID, conditionID){

      var query = {
        'productID': productID,
        'conditionID': conditionID
      };

      var deferred = $q.defer();

      conditionsResource.delete(query, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // remove element from condition
    function removeElement(productID, conditionID, elementType, elementID){

      var query = {
        'productID': productID,
        'conditionID': conditionID,
        'elementID': elementID,
        'elementType': elementType
      };

      var deferred = $q.defer();

      conditionsResource.delete(query, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // query all conditions
    function query(productID){

      var deferred = $q.defer();

      var param = {
        'productID': productID
      };

      conditionsResource.query(param, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }


    // edit condition
    function edit(productID, conditionID, params){

      var query = {
        'productID': productID,
        'conditionID': conditionID
      };

      params = params || {};

      var deferred = $q.defer();

      conditionsResource.edit(query, params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // return service object
    return service;
  }

})(angular.module('adminPanel.api.conditions',[]));
