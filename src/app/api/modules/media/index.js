(function (app) {
  app.factory('MediaFactory', MediaFactory);
  MediaFactory.$inject = ['$q' ,'URL','$http'];
  function MediaFactory( $q, URL, $http) {
    var service = {
      removeImage: removeImage
    };

    // Generic Remove Image function

    function removeImage (id,type,path) {
      var deferred = $q.defer();
      $http({
        method: 'DELETE',
        url : URL.MEDIA_DELETE + type +'/' + id,
        headers:{
          'Content-Type': 'application/json'
        },
        data: {path: path}
      }).then(function (result) {
        URL.resolve(result,deferred);
      }, function (error) {
        deferred.reject(error);
      });
      return deferred.promise;
    }
    return service;
  }

})(angular.module('adminPanel.api.media',[]));
