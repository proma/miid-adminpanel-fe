(function(app){
  'use strict';

  app.factory('CustomImagesFactory', CustomImages);

  CustomImages.$inject = [ '$resource', '$q' , 'URL'];

  function CustomImages($resource, $q, URL){

    /*
     * This factory is for custom img resource
     * - save ( create new custom img)
     * - remove ( delete an existing custom img )
     * - edit ( update existing custom images )
     * */

    var service = {
      save: save,
      remove: remove,
      edit: edit
    };
    var customImgURL = URL.CUSTOMIMG;

    var customImgResource = $resource(customImgURL, {}, {
      edit: {
        method: 'PUT'
      }
    });

    // save custom img
    function save(productId,stepId, groupId,params){

      // API prepared payload
      params = params || {};
      var query = {
        'productId': productId,
        'stepId': stepId,
        'groupId': groupId
      };

      var deferred = $q.defer();

      customImgResource.save(query, params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // remove custom img
    function remove(productId,stepId,groupId, customImgID){

      var query = {
        'productId': productId,
        'stepId': stepId,
        'groupId': groupId,
        'customImgID': customImgID
      };

      var deferred = $q.defer();

      customImgResource.delete(query, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // edit custom img
    function edit(productId,stepId,groupId, customImgID, params){

      var query = {
        'productId': productId,
        'stepId': stepId,
        'groupId': groupId,
        'customImgID': customImgID
      };
      params = params || {};

      var deferred = $q.defer();

      customImgResource.edit(query, params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // return service object
    return service;
  }

})(angular.module('adminPanel.api.customImg',[]));
