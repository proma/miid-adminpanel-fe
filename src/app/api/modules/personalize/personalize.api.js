(function(app){
  'use strict';

  app.factory('PersonalizeFactory', PersonalizeFactory);

  PersonalizeFactory.$inject = [ '$resource', '$q' , 'URL'];

  function PersonalizeFactory($resource, $q, URL){

    /*
     * This factory is for Personalize resource
     * - get ( get personalize )
     * - save ( create new Personalize)
     * - remove ( delete an existing Personalize )
     * - updateComponent (PUT existing personalize and save component in it)
     * - edit ( update existing Personalize )
     * */

    var service = {
      get: getOne,
      save: save,
      remove: remove,
      updateComponent: updateComponent,
      edit: edit
    };
    var personalizeURL = URL.PERSONALIZE;

    var personalizeResource = $resource(personalizeURL, {}, {
      edit: {
        method: 'PUT'
      },
      updateComponent: {
        url: personalizeURL + '/component/:componentID',
        method: 'PUT'
      }
    });

    function getOne(productID, stepID, groupID, personalizeID) {
      var param =
      {
        'productId': productID,
        'stepId': stepID,
        'groupId': groupID,
        'personalizeID': personalizeID
      };

      var deferred = $q.defer();

      personalizeResource.get(param, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    //Update the component for Personalize
    function updateComponent(productId, stepId, groupId, personalizeId, componentId) {
      var deferred = $q.defer();
      console.log(componentId);
      var query = {
        'productId': productId,
        'stepId': stepId,
        'groupId': groupId,
        'personalizeID': personalizeId,
        'componentID': componentId
      };

      personalizeResource.updateComponent(query, {}, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // save personalize
    function save(productId,stepId, groupId,params){

      // API prepared payload
      params = params || {};
      var query = {
        'productId': productId,
        'stepId': stepId,
        'groupId': groupId
      };

      var deferred = $q.defer();

      personalizeResource.save(query, params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // remove personalize
    function remove(productId,stepId,groupId, personalizeID){

      var query = {
        'productId': productId,
        'stepId': stepId,
        'groupId': groupId,
        'personalizeID': personalizeID
      };

      var deferred = $q.defer();

      personalizeResource.delete(query, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // edit personalize
    function edit(productId,stepId,groupId, personalizeID,params){

      var query = {
        'productId': productId,
        'stepId': stepId,
        'groupId': groupId,
        'personalizeID': personalizeID
      };
      params = params || {};

      var deferred = $q.defer();

      personalizeResource.edit(query, params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // return service object
    return service;
  }

})(angular.module('adminPanel.api.personalize',[]));
