(function(app){
  'use strict';

  app.factory('SelectablesFactory', SelectablesFactory);

  SelectablesFactory.$inject = [ '$resource', '$q' , 'URL'];

  function SelectablesFactory($resource, $q, URL){

    /*
     * This factory is for selectables resource
     * - save ( create new selectable)
     * - remove ( delete an existing selectable )
     * - edit ( update existing selectable )
     * */

    var service = {
      save: save,
      remove: remove,
      edit: edit
    };
    var selectableURL = URL.SELECTABLE;

    var selectableResource = $resource(selectableURL, {}, {
      edit: {
        method: 'PUT'
      }
    });

    // save INPUT
    function save(productId,stepId, groupId,params){

      // API prepared payload
      params = params || {};
      var query = {
        'productId': productId,
        'stepId': stepId,
        'groupId': groupId
      };

      var deferred = $q.defer();

      selectableResource.save(query, params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // remove selectable
    function remove(productId,stepId,groupId, selectableID){

      var query = {
        'productId': productId,
        'stepId': stepId,
        'groupId': groupId,
        'selectableID': selectableID
      };

      var deferred = $q.defer();

      selectableResource.delete(query, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // edit selectable
    function edit(productId,stepId,groupId, selectableID, params){

      var query = {
        'productId': productId,
        'stepId': stepId,
        'groupId': groupId,
        'selectableID': selectableID
      };
      params = params || {};

      var deferred = $q.defer();

      selectableResource.edit(query, params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // return service object
    return service;
  }

})(angular.module('adminPanel.api.selectables',[]));
