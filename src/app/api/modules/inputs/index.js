(function(app){
  'use strict';

  app.factory('InputsFactory', InputsFactory);

  InputsFactory.$inject = [ '$resource', '$q' , 'URL'];

  function InputsFactory($resource, $q, URL){

    /*
     * This factory is for Inputs resource
     * - save ( create new Input)
     * - remove ( delete an existing Input )
     * - edit ( update existing Input )
     * */

    var service = {
      save: save,
      remove: remove,
      edit: edit
    };
    var inputsURL = URL.INPUTS;

    var inputResource = $resource(inputsURL, {}, {
      edit: {
        method: 'PUT'
      }
    });

    // save INPUT
    function save(productId,stepId, groupId,params){

      // API prepared payload
      params = params || {};
      var query = {
        'productId': productId,
        'stepId': stepId,
        'groupId': groupId
      };

      var deferred = $q.defer();

      inputResource.save(query, params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // remove INPUT
    function remove(productId,stepId,groupId, inputID){

      var query = {
        'productId': productId,
        'stepId': stepId,
        'groupId': groupId,
        'inputID': inputID
      };

      var deferred = $q.defer();

      inputResource.delete(query, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // edit INPUT
    function edit(productId,stepId,groupId, inputID,params){

      var query = {
        'productId': productId,
        'stepId': stepId,
        'groupId': groupId,
        'inputID': inputID
      };
      params = params || {};

      var deferred = $q.defer();

      inputResource.edit(query, params, function(result){
        URL.resolve(result, deferred);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    // return service object
    return service;
  }

})(angular.module('adminPanel.api.inputs',[]));
