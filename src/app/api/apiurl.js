(function(app){

  app.service('URL', APIURL);

  APIURL.$inject = ['MIID_API'];

  function APIURL(MIID_API){
    return {
      ORDERS: MIID_API.base_url + '/orders',
      BRANDS: MIID_API.base_url + '/brands',
      PRODUCTS: MIID_API.base_url + '/products',
      STEPS: MIID_API.base_url + '/products/:productId/steps',
      LAYERS: MIID_API.base_url + '/products/:productId/layers',
      GROUPS: MIID_API.base_url + '/products/:productId/steps/:stepId/groups',
      UI: MIID_API.base_url + '/products/:productId/steps/:stepId/groups/:groupId/ui',
      INPUTS: MIID_API.base_url + '/products/:productId/steps/:stepId/groups/:groupId/inputs/:inputID',
      PERSONALIZE: MIID_API.base_url + '/products/:productId/steps/:stepId/groups/:groupId/personalize/:personalizeID',
      SELECTABLE: MIID_API.base_url + '/products/:productId/steps/:stepId/groups/:groupId/selectable/:selectableID',
      CUSTOMIMG: MIID_API.base_url + '/products/:productId/steps/:stepId/groups/:groupId/custom_images/:customImgID',
      UIACTION: MIID_API.base_url + '/products/:productId/steps/:stepId/groups/:groupId/ui/:uiId',
      COMPONENTS: MIID_API.base_url + '/products/:productId/components',
      SUB_COMPONENTS: MIID_API.base_url + '/products/:productId/sub_components',
      COLORMIX: MIID_API.base_url + '/products/:productId/components/:componentId/colorMixes',
      CATEGORIES: MIID_API.base_url + '/categories',
      CUSTOMERS: MIID_API.base_url + '/customers',
      COLLECTIONS: MIID_API.base_url + '/collections',
      NAVIGATIONS: MIID_API.base_url + '/navigations',
      PAGES: MIID_API.base_url + '/pages',
      MEDIA_DELETE : MIID_API.base_url + '/media/delete/',
      COUPONS: MIID_API.base_url + '/coupons',
      SCENES: MIID_API.base_url + '/products/:productId/scene/ui_actions',
      CONDITIONS: MIID_API.base_url + '/products/:productID/conditions/:conditionID/:elementType/:elementID',
      USER_REVIEWS: MIID_API.base_url + '/reviews/product',
      ARTWORKS: MIID_API.base_url + '/artworks',
      GALLERIES: MIID_API.base_url + '/galleries',
      EXPORT: MIID_API.base_url + '/orders/export_po',
      RedirectURL: 'admin-panel.not-found',
      resolve: function (response, deferred) {
        if (!response.statusCode)
          deferred.resolve(response); //when request fails it has statusCode
        else
          deferred.reject(response);
      }
    }
  }

})(angular.module('adminPanel.api.apiurl',[]));
