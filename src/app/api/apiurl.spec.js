'use strict';

describe('Services', function () {
  describe('URL', function() {
    var URL,
      MIID_API;


    // load the dependencies module
    beforeEach(module('adminPanel'));
    beforeEach(module('adminPanel.api.apiurl'));

    beforeEach(inject(function($injector){
      URL = $injector.get('URL');
      MIID_API = $injector.get('MIID_API');
    }));

    it('should get url for orders',function(){
      expect(URL.ORDERS).toBe( MIID_API.base_url + "/orders");
    });

  });


});
