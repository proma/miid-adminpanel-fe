(function(app){

  app.factory('API', API);

  API.$inject = [
    'OrdersFactory',
    'BrandsFactory',
    'ProductsFactory',
    'StepsFactory',
    'GroupsFactory',
    'UIFactory',
    'UIActionFactory',
    'ComponentsFactory',
    'LayersFactory',
    'ColorMixFactory',
    'CategoriesFactory',
    'CustomersFactory',
    'CollectionsFactory',
    'NavigationFactory',
    'PagesFactory',
    'CouponsFactory',
    'ScenesFactory',
    'InputsFactory',
    'SelectablesFactory',
    'ConditionsFactory',
    'CustomImagesFactory',
    'UserReviewsFactory',
    'ArtworksFactory',
    'GalleriesFactory',
    'PersonalizeFactory',
    'CustomizerFactory',
    'MediaFactory'
  ];

  function API(
    OrdersFactory,
    BrandsFactory,
    ProductsFactory,
    StepsFactory,
    GroupsFactory,
    UIFactory,
    UIActionFactory,
    ComponentsFactory,
    LayersFactory,
    ColorMixFactory,
    CategoriesFactory,
    CustomersFactory,
    CollectionsFactory,
    NavigationFactory,
    PagesFactory,
    CouponsFactory,
    ScenesFactory,
    InputsFactory,
    SelectablesFactory,
    ConditionsFactory,
    CustomImagesFactory,
    UserReviewsFactory,
    ArtworksFactory,
    GalleriesFactory,
    PersonalizeFactory,
    CustomizerFactory,
    MediaFactory
  ){
    var service = {};

    service.Orders = OrdersFactory;
    service.Brands = BrandsFactory;
    service.Products = ProductsFactory;
    service.Steps = StepsFactory;
    service.Groups = GroupsFactory;
    service.UI = UIFactory;
    service.UIAction = UIActionFactory;
    service.Components = ComponentsFactory;
    service.Layers = LayersFactory;
    service.ColorMix = ColorMixFactory;
    service.Categories = CategoriesFactory;
    service.Customers = CustomersFactory;
    service.Collections = CollectionsFactory;
    service.Navigations = NavigationFactory;
    service.Pages = PagesFactory;
    service.Coupons = CouponsFactory;
    service.Scenes = ScenesFactory;
    service.Inputs = InputsFactory;
    service.Selectables = SelectablesFactory;
    service.Conditions = ConditionsFactory;
    service.CustomImages = CustomImagesFactory;
    service.UserReviews = UserReviewsFactory;
    service.Artworks = ArtworksFactory;
    service.Galleries = GalleriesFactory;
    service.Personalize = PersonalizeFactory;
    service.Customizer = CustomizerFactory;
    service.Media = MediaFactory;

    return service;
  }

})(angular.module('adminPanel.api',[
  'adminPanel.api.brands',
  'adminPanel.api.orders',
  'adminPanel.api.products',
  'adminPanel.api.steps',
  'adminPanel.api.groups',
  'adminPanel.api.ui',
  'adminPanel.api.uiaction',
  'adminPanel.api.components',
  'adminPanel.api.layers',
  'adminPanel.api.colormix',
  'adminPanel.api.categories',
  'adminPanel.api.customers',
  'adminPanel.api.collections',
  'adminPanel.api.pages',
  'adminPanel.api.coupons',
  'adminPanel.api.scenes',
  'adminPanel.api.inputs',
  'adminPanel.api.selectables',
  'adminPanel.api.conditions',
  'adminPanel.api.customImg',
  'adminPanel.api.apiurl',
  'adminPanel.api.userReviews',
  'adminPanel.api.artworks',
  'adminPanel.api.galleries',
  'adminPanel.api.personalize',
  'adminPanel.api.navigation',
  'adminPanel.api.customizer',
  'adminPanel.api.media'
]));
