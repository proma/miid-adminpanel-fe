(function (app) {

  app.service('RolesService', RolesService);

  RolesService.$inject = ['ROLES', 'PermissionStore'];

  function RolesService(ROLES, PermissionStore) {
    var service  = {}, ROLE = null;


    service.createPermissions = function (role) {

      /*
      * Permissions are organized as follows
      * - Left Menu
      *   - Home
      *   - Brands
      *   - Orders
      *   - Products
      *   - Users
      *   - CMS
      *     - Collections
      *     - User Reviews
      *   - Coupons
      *   - Settings
      *     - Brand
      * - Orders Section
      *   - Orders List
      *   - Order Details
      *   - Download PDF/EXCEL
      *   - Filtering Options
      *   - Change Status
      *   - Archive Orders
      *   - Cancel Orders
      *   - Orders Export Modal
      *     - Separate by brand
      *     - Separate by Manufacturer
      *
      * - Products Section
      *
      *   - Products List
      *   - Filtering Options
      *   - Change Status
      *   - Duplicate Products
      *   - Delete Products
      *   - Product Details
      *   - See Basic Information: Client
      *   - Edit Basic Info: Client
      *   - See Basic Info: Product
      *   - Edit Basic Info: Product
      *   - See Media Library
      *   - Edit Media Library
      *   - Customisation Description
      *   - Edit Customisation Description
      *   - Visual Assets
      *   - Edit Visual Assets
      *
      * - Brands Section
      *
      *   - Brands List
      *   - Add Brand
      *   - Delete Brand
      *   - Brand details view
      *   - Change Status
      *
      * - Users Section
      *
      *   - Users Listing
      *   - Filtering Options
      *
      * - CMS
      *
      *   - Collections Section
      *
      *     - Collections List
      *     - Add Collection
      *     - Delete Collection
      *     - Change status
      *     - Collection details view
      *     - Edit Collection details view
      *   - User Reviews
      *
      *     - Listing
      *     - Filtering Options
      *     - Change Status
      *
      * - Coupons Section
      *
      *   - Coupons Listing
      *   - Create Coupon
      *   - Delete Coupon
      *   - Change Status
      *
      * - Artworks ( Personalize Galleries/Artworks)
      *
      *   - Brands Listing
      *   - Simple Nav Link
      *
      * */

      //ROLES which don't have permissions are only listed. So we are explicitly defining
      // who don't have permissions

      var permission =
        {
          'canSee.leftMenu.Home' : {}, //all have permissions
          'canSee.leftMenu.Brands':
          {
            'MIID_PRODUCT_DPT': false,
            'MIID_SALES': false,
            'BRAND_ADMIN': false,
            'BRAND_SALES': false
          },
          'canSee.leftMenu.Orders':
          {
            'MIID_PRODUCT_DPT': false
          },
          'canSee.leftMenu.Products':
          {
            'MIID_SALES': false,
            'BRAND_SALES': false
          },
          'canSee.leftMenu.Users':
          {
            'MIID_PRODUCT_DPT': false,
            'MIID_SALES': false,
            'BRAND_ADMIN': false,
            'BRAND_SALES': false
          },
          'canSee.leftMenu.CMS':
          {
            'MIID_PRODUCT_DPT': false,
            'MIID_SALES': false,
            'BRAND_SALES': false
          },
          'canSee.leftMenu.Coupons':
          {
            'MIID_PRODUCT_DPT': false,
            'MIID_SALES': false,
            'BRAND_SALES': false
          },
          'canSee.leftMenu.Settings':
          {
            'MIID_PRODUCT_DPT': false,
            'MIID_SALES': false,
            'BRAND_SALES': false
          },
          'canSee.leftMenu.Settings.Brand':
          {
            'ADMIN': false,
            'MIID_MANAGER': false,
            'MIID_PRODUCT_DPT': false,
            'MIID_SALES': false,
            'BRAND_SALES': false
          },
          'canSee.Orders.Listing':
          {
            'MIID_PRODUCT_DPT': false
          },
          'canSee.Orders.Details':
          {
            'MIID_PRODUCT_DPT': false
          },
          'canSee.Orders.FilterByBrand':
          {
            'BRAND_ADMIN': false,
            'BRAND_SALES': false
          },
          'canSee.Orders.DownloadFilesButton':
          {
            'MIID_PRODUCT_DPT': false
          },
          'canSee.Orders.FilteringOptions':
          {
            'MIID_PRODUCT_DPT': false
          },
          'canSee.Orders.ChangeStatus':
          {
            'MIID_PRODUCT_DPT': false
          },
          'canSee.Orders.ArchiveOrders':
          {
            'MIID_PRODUCT_DPT': false,
            'MIID_SALES': false,
            'BRAND_SALES': false
          },
          'canSee.Orders.CancelOrders':
          {
            'MIID_PRODUCT_DPT': false,
            'MIID_SALES': false,
            'BRAND_SALES': false
          },
          'canSee.Orders.Export.Separate':
          {
            'MIID_PRODUCT_DPT': false,
            'BRAND_ADMIN': false,
            'MIID_SALES': false,
            'BRAND_SALES': false
          },
          'canSee.Products.Listing':
          {
            'MIID_SALES': false,
            'BRAND_SALES': false
          },
          'canSee.Products.FilterByBrand':
          {
            'BRAND_ADMIN': false,
            'BRAND_SALES': false
          },
          'canSee.Products.DeleteButton':
          {
            'MIID_MANAGER': false,
            'MIID_PRODUCT_DPT': false,
            'MIID_SALES': false,
            'BRAND_ADMIN': false,
            'BRAND_SALES': false
          },
          'canSee.Products.ProductDetails':
          {
            'MIID_SALES': false,
            'BRAND_SALES': false
          },
          'canSee.Products.VisualAssets':
          {
            'MIID_MANAGER': false,
            'MIID_SALES': false,
            'BRAND_ADMIN': false,
            'BRAND_SALES': false
          },
          'canEdit.Products.VisualAssets':
          {
            'MIID_MANAGER': false,
            'MIID_SALES': false,
            'BRAND_ADMIN': false,
            'BRAND_SALES': false
          },
          'canSee.Brands.Listing':
          {
            'MIID_PRODUCT_DPT': false,
            'MIID_SALES': false,
            'BRAND_ADMIN': false,
            'BRAND_SALES': false
          },
          'canSee.Brands.DeleteButton':
          {
            'MIID_MANAGER': false,
            'MIID_PRODUCT_DPT': false,
            'MIID_SALES': false,
            'BRAND_ADMIN': false,
            'BRAND_SALES': false
          },
          'canSee.Users.Listing':
          {
            'MIID_PRODUCT_DPT': false,
            'MIID_SALES': false
          },
          'canSee.CMS.Collections.Listing':
          {
            'MIID_PRODUCT_DPT': false,
            'MIID_SALES': false,
            'BRAND_SALES': false
          },
          'canSee.CMS.Navigations':
          {
            'MIID_MANAGER': true,
            'ADMIN': true,
            'BRAND_ADMIN': false,
            'BRAND_SALES': false,
            'MIID_SALES': false,
            'MIID_PRODUCT_DPT': false
          },
          'canSee.CMS.Collections.Listing.MIID':
          {
            'BRAND_ADMIN': false,
            'BRAND_SALES': false
          },
          'canSee.CMS.Collections.Listing.Brands':
          {
            'BRAND_ADMIN': false,
            'BRAND_SALES': false
          },
          'canEdit.CMS.Collections.Listing.CreateCollection':
          {
            'BRAND_ADMIN': false,
            'BRAND_SALES': false
          },
          'canSee.Coupons.Listing':
          {
            'MIID_PRODUCT_DPT': false,
            'BRAND_SALES': false
          },
          'canSee.Coupons.DeleteButton':
          {
            'MIID_PRODUCT_DPT': false,
            'MIID_SALES': false,
            'BRAND_SALES': false
          },
          'canSee.Coupons.Create.AllProductsOption':
          {
            'BRAND_ADMIN': false,
            'BRAND_SALES': false
          },

          'canSee.Coupons.Create.ProductsOverPriceOption':
          {
            'BRAND_ADMIN': false,
            'BRAND_SALES': false
          },
          'canSee.UserReviews.FilteringOptions':
          {
            'MIID_SALES': false,
            'BRAND_ADMIN': false
          },
          'canSee.UserReviews.ChangeStatus':
          {
            'MIID_SALES': false,
            'BRAND_ADMIN': false
          },
          'canSee.CMS.UserReviews.Listing':
          {
            'MIID_PRODUCT_DPT': false,
            'MIID_SALES': false,
            'BRAND_SALES': false
          },
          'canSee.Settings.Brand': {
            'ADMIN': false,
            'MIID_MANAGER': false,
            'MIID_PRODUCT_DPT': false,
            'MIID_SALES': false,
            'BRAND_SALES': false
          },
          'canSee.Personalize.BrandsListing':
          {
            'MIID_PRODUCT_DPT': false,
            'MIID_SALES': false,
            'BRAND_ADMIN': false,
            'BRAND_SALES': false
          },
          'canSee.Personalize.SimpleArtworksNav':
          {
            'MIID_MANAGER': false,
            'ADMIN': false
          }

        };

      ROLE = role;
      var permissions = _.map(_.filter(_.pairs(permission),_.last),_.first);

      PermissionStore.defineManyPermissions(permissions, function (currPermission) {
        //if we find that permission for given role then obviously the role doesn't have permission
        //if we don't find the permission it means it has the permission
        return permission[currPermission][role] !== false;
      });

    };

    //this method validates the permission in controllers, will return true if permission is granted else false.
    service.checkPermission = function (permission) {
      return PermissionStore.getStore()[permission].validationFunction(permission);
    };

    service.flush = function () {
      PermissionStore.clearStore();
    };

    service.isUser = function (role) {
      return ROLE === role;
    };

    return service;
  }

})(angular.module('adminPanel.services.roles',
  [
    'permission',
    'adminPanel.constants'
  ]));
