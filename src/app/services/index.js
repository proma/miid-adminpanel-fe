(function (app) {

})(angular.module('adminPanel.services', [
  'adminPanel.services.alert',
  'adminPanel.services.notifier',
  'adminPanel.services.roles',
  'adminPanel.services.modal',
  'adminPanel.services.progress_blanket'
]));
