(function (app) {

  app.factory('ProgressBlanket', ProgressBlanket);

  ProgressBlanket.$inject = ['$uibModal'];

  function ProgressBlanket($uibModal) {

    var factory = {};
    var modalInstance = {};

    factory.controller = function ($uibModalInstance, TotalOperations, Requests) {
      var modal = this;
      modal.completedOperations = 0;
      modal.operationsSuccessfullyPerformed = 0;
      modal.errors = 0;
      modal.totalOperations = TotalOperations;

      Requests.forEach(function (fn) {
        fn(function (error) {
          modal.completedOperations++;
          if (error)
            modal.errors++;
          else
            modal.operationsSuccessfullyPerformed++;
        });
      });

      modal.close = function () {
        $uibModalInstance.dismiss();
      }
    };

    factory.open = function (fnStart) {
      fnStart(factory.init);
    };

    factory.complete = function () {
      if (!modalInstance.dismiss)
        throw 'complete function called before instantiation of modal';
      modalInstance.dismiss && modalInstance.dismiss();
    };

    factory.init = function (requests) {
      modalInstance = $uibModal.open({
        size: 'md',
        controller: ['$uibModalInstance', 'TotalOperations', 'Requests', factory.controller],
        controllerAs: 'modal',
        templateUrl: 'app/services/progress-blanket/progressBlanket.template.v1.html',
        backdrop: 'static',
        resolve: {
          TotalOperations: function () {
            return requests.length;
          },
          Requests: function () {
            return requests;
          }
        }
      });
    };

    return factory;
  }

})(angular.module('adminPanel.services.progress_blanket', []));
