(function (app) {

  app.service('Notify', Notify);

  Notify.$inject = ['toastr', '$translate'];

  function Notify(toastr, $translate) {
    var service  = {};

    // Information toastrs
    service.info = function (msg) {
      toastr.info(msg);
    };
    service.infoSaved = function () {
      toastr.info(
        $translate.instant('notify.info.infoSaved.label')||'Your changes were saved successfully',
        $translate.instant('notify.info.infoSaved.title')||'Changes Saved!');
    };

    service.infoImageSaved = function () {
      toastr.info(
        $translate.instant('notify.info.infoImageSaved.label')||'The image you uploaded was saved successfully',
        $translate.instant('notify.info.infoImageSaved.title')||'Image Uploaded Successfully');
    };

    service.infoDeleted = function () {
      toastr.info(
        $translate.instant('notify.info.infoDeleted.label')||'The item you selected has been deleted successfully',
        $translate.instant('notify.info.infoDeleted.title')||'Item Deleted Successfully');
    };

    service.infoSaveChanges = function () {
      toastr.info(
        $translate.instant('notify.info.infoSaveChanges.label')||'Please save your changes first.',
        $translate.instant('notify.info.infoSaveChanges.title')||'Save Changes');
    };

    service.infoFilesUploaded = function () {
      toastr.info(
        $translate.instant('notify.info.infoFilesUploaded.label')||'Your files were uploaded successfully',
        $translate.instant('notify.info.infoFilesUploaded.title')||'Files Upload Successful');
    };

    service.infoPOFiles = function () {
      toastr.info(
        $translate.instant('notify.info.infoPOFiles.label')||'Order PDF and Excel files created successfully',
        $translate.instant('notify.info.infoPOFiles.title')||'Successfully recreated PO files.');
    };

    service.infoNoItems = function () {
      toastr.info(
        $translate.instant('notify.info.infoNoItems.label')||'There are no more items in this navigation side. Go back to List.',
        $translate.instant('notify.info.infoNoItems.title')||'No more Items');
    };

    service.infoSameItemStatus = function () {
      toastr.info(
        $translate.instant('notify.info.infoSameItemStatus.label')||'Selected item has already this status!',
        $translate.instant('notify.info.infoSameItemStatus.title')||'Item already has status');
    };

    service.infoChangeStatus = function (status) {
      toastr.info(
        $translate.instant('notify.info.infoChangeStatus.label', {status: status})||'Item status was changed to ' + status,
        $translate.instant('notify.info.infoChangeStatus.label')||'Status changed successfully');
    };

    service.infoChangeItemsStatus = function (noOfCalls,completedReq,status){
      toastr.info(
        $translate.instant('notify.info.infoChangeStatus.label', {status: status, noOfCalls: noOfCalls, completedReq: completedReq})||'Changed status of '+noOfCalls+'/'+completedReq+' items with Status: '+status,
        $translate.instant('notify.info.infoChangeStatus.title')||'Status Changed');
    };

    service.infoSavedItems = function (noOfCalls, completedReq){
      toastr.info('Saved ' + noOfCalls + '/' + completedReq + ' items successfully', 'Changes Saved!');
    };

    service.infoItemsDeleted = function (noOfCalls, completedReq) {
      toastr.info(
        $translate.instant('notify.info.infoItemsDeleted.label', {noOfCalls: noOfCalls,completedReq: completedReq})||'Deleted '+noOfCalls+'/'+completedReq + ' items' ,
        $translate.instant('notify.info.infoItemsDeleted.title')||'Items deleted');
    };

    service.infoFilesDeleted = function () {
      toastr.info(
        $translate.instant('notify.info.infoFilesDeleted.label')||'Your files were deleted successfully' ,
        $translate.instant('notify.info.infoFilesDeleted.title')||'Files deleted');
    };

    service.infoResized = function () {
      toastr.info(
        $translate.instant('notify.info.infoResized.label')||'Components resized successfully' ,
        $translate.instant('notify.info.infoResized.title')||'Assets Resized');
    };

    // warning toastrs
    service.warning = function (msg) {
      toastr.warning(msg);
    };
    service.warningInputMissing = function () {
      toastr.warning(
        $translate.instant('notify.warnings.warningInputMissing.label')||'Please complete your inputs.',
        $translate.instant('notify.warnings.warningInputMissing.title')||'Input missing');
    };

    //error toastrs
    service.error = function (msg) {
      toastr.error(msg);
    };

    service.onlyZipAllowed = function (msg) {
      toastr.error(
        $translate.instant('notify.errors.onlyZipAllowed.label')||'Only ZIP files are allowed',
        $translate.instant('notify.errors.onlyZipAllowed.title')||'Invalid File Type');
    };

    service.errorImageMissing = function () {
      toastr.error(
        $translate.instant('notify.errors.errorImageMissing.label')||'Please select an image.',
        $translate.instant('notify.errors.errorImageMissing.title')||'Image file missing');
    };

    service.errorSaving = function () {
      toastr.error(
        $translate.instant('notify.errors.errorSaving.label')||'You can try again after a while.',
        $translate.instant('notify.errors.errorSaving.title')||'Something went wrong');
    };

    service.errorOperation = function () {
      toastr.error(
        $translate.instant('notify.errors.errorOperation.label')||'The requested operation can\'t be completed',
        $translate.instant('notify.errors.errorOperation.title')||'Can\'t Complete Operation');
    };

    service.errorSlugSaving = function () {
      toastr.error(
        $translate.instant('notify.errors.errorSlugSaving.label')||'Object with this slug already exist',
        $translate.instant('notify.errors.errorSlugSaving.title')||'Object exist');
    };

    service.errorObjectExist = function () {
      toastr.error(
        $translate.instant('notify.errors.errorObjectExist.label')||'Object with this name already exist',
        $translate.instant('notify.errors.errorObjectExist.title')||'Object exist');
    };

    service.errorDirectorySelection = function () {
      toastr.error(
        $translate.instant('notify.errors.errorDirectorySelection.label')||'Please select a directory to proceed',
        $translate.instant('notify.errors.errorDirectorySelection.title')||'Directory not Selected');
    };

    service.errorItemStatus = function () {
      toastr.error(
        $translate.instant('notify.errors.errorItemStatus.label')||'There was an error changing status of Item',
        $translate.instant('notify.errors.errorItemStatus.title')||'Error while changing Status');
    };

    service.errorPOFiles = function () {
      toastr.error(
        $translate.instant('notify.errors.errorPOFiles.label')||'There was an error while creating PO files. Try again later.',
        $translate.instant('notify.errors.errorPOFiles.title')||'Error while recreating PO files');
    };

    service.errorPercentageValue = function (){
      toastr.error(
        $translate.instant('notify.errors.errorPercentageValue.label')||'Percentage value should be in between 0 and 100.',
        $translate.instant('notify.errors.errorPercentageValue.title')||'Error in Percentage value');
    };

    service.errorItemSelection = function (count) {
      toastr.error(count ?
        $translate.instant('notify.errors.errorItemSelection.label'):
        $translate.instant('notify.errors.errorItemSelection.label2'),
        $translate.instant('notify.errors.errorItemSelection.title')||'Item not selected');
    };

    service.errorCodeExist = function (){
      toastr.error(
        $translate.instant('notify.errors.errorCodeExist.label')||'The code you provided already exist. Try regenerating or provide another code',
        $translate.instant('notify.errors.errorCodeExist.title')||'Code exists');
    };

    service.errorResizing = function (){
      toastr.error(
        $translate.instant('notify.errors.errorResizing.label'),
        $translate.instant('notify.errors.errorResizing.title'));
    };

    service.errorSavingOrder = function () {
      toastr.error($translate.instant('notify.errors.errorSavingOrder.label')||'There was an error when trying to save the new items order, please try again.');
    };


    return service;
  }

})(angular.module('adminPanel.services.notifier', ['toastr']));
