(function (app) {

  app.service('ALERT', ALERT);

  ALERT.$inject = ['SweetAlert', '$translate'];

  function ALERT(SweetAlert, $translate) {
    var service  = {};

    service.confirm = function (fn, title, desc, type, btnText) {
      SweetAlert.swal({
          title: title? title : $translate.instant('alert.confirm.title')||"There was an Error.",
          text: desc ? desc : $translate.instant('alert.confirm.label')||"We couldn't get data from server. You can reload or continue as is.",
          type: type? type: "error",
          showCancelButton: true,
          cancelButtonColor: '#DD6B55',
          cancelButtonText: $translate.instant('buttons.cancel'),
          confirmButtonText: btnText? btnText :$translate.instant('alert.buttons.reload')||"Reload",
          closeOnConfirm: true
        },
        function (isConfirm) {
          if (isConfirm) {
            fn && fn(); //invoke function given if available.
          }
        });
    };

    service.confirmWarning = function (fn) {
      service.confirm(
        fn,
        $translate.instant('alert.label')||'Are you sure?',
        $translate.instant('alert.confirmWarning.label')||'This will delete the selected item Permanently',
        'warning',
        $translate.instant('alert.buttons.confirm')||'Confirm'
      );
    };

    service.confirmItemStatus = function (fn, status) {
      service.confirm(
        fn,
        $translate.instant('alert.label')||'Are you sure?',
        $translate.instant('alert.confirmItemStatus.label',{status: status})||'This will change the status of selected items to ' + status,
        'warning',
        $translate.instant('alert.buttons.confirm')||'Confirm'
      )
    };

    service.confirmGroupWarning = function (fn) {
      service.confirm(
        fn,
        $translate.instant('alert.label')||'Are you sure?',
        $translate.instant('alert.confirmGroupWarning.label')||'This will delete the selected items Permanently',
        'warning',
        $translate.instant('alert.buttons.confirm')||'Confirm'
      );
    };

    service.errorDataSaving = function (fn) {
      service.confirm(
        fn,
        $translate.instant('alert.errorDataSaving.title')||'We couldn\'t save your changes.',
        $translate.instant('alert.errorDataSaving.label')||'There was an error while trying to save your changes',
        'error',
        $translate.instant('alert.buttons.try')||'Try Again'
      )
    };

    service.errorDeleting = function (fn) {
      service.confirm(
        fn,
        $translate.instant('alert.errorDeleting.title')||"Error Deleting",
        $translate.instant('alert.errorDeleting.label')||"Error while deleting, Press 'Retry' to try again or Press 'Cancel' to continue",
        "error",
        $translate.instant('alert.buttons.retry')||"Retry"
      )
    };

    service.confirmDeleting = function (fn) {
      service.confirm(
        fn,
        $translate.instant('alert.confirmDeleting.title')||"Are you sure you want to delete this?",
        $translate.instant('alert.confirmDeleting.label')||"Press 'OK' to delete and 'Cancel' to continue.",
        "warning",
        $translate.instant('alert.buttons.ok')||"OK"
      )
    };

    service.warningRemove = function (fn,msg){
      service.confirm(
        fn,
        $translate.instant('alert.label')||"Are you sure?",
        msg,
        "warning",
        $translate.instant('alert.buttons.yes')||"Yes, remove it"
      )
    };

    service.successRemove = function (fn) {
      service.confirm(
        fn,
        $translate.instant('alert.successRemove.title')||'Item removed successfully',
        $translate.instant('alert.successRemove.label')||'The selected item was removed permanently from our Database'
      );
    };

    service.confirmUnsavedChanges = function (fn) {
      service.confirm(
        fn,
        $translate.instant('alert.confirmUnsavedChanges.title')||'You have Unsaved Changes',
        $translate.instant('alert.confirmUnsavedChanges.label')||'This will discard your unsaved changes. You might want to save your changes first.',
        'warning',
        $translate.instant('alert.buttons.proceed')||'Proceed'
      );
    };

    return service;
  }

})(angular.module('adminPanel.services.alert', ['oitozero.ngSweetAlert']));
