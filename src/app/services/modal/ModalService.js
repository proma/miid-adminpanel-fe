(function (app) {

  app.service('Modal', ModalService);

  ModalService.$inject = ['$uibModal'];

  /**
   * This Service forces everyone to have controllers in other files for
   * the maintainability of the Admin Panel controllers. ;)
   * @returns {{}}
   * @constructor
   */
  function ModalService($uibModal) {
   var service = {};


   service.open = function (ctrl, ctrlAs, template, resolve, size, backdrop, bdClass, windowClass) {
     var config = {};

     config.animation = false;
     config.windowTopClass = 'bounceIn animated';
     config.controller = ctrl;
     config.controllerAs = ctrlAs;
     config.templateUrl = template;
     config.size = size || 'sm';
     config.resolve = resolve || {};
     config.backdropClass = bdClass;
     config.windowClass = windowClass;

     if (_.isBoolean(backdrop))
       config.backdrop = backdrop;
     else
       config.backdrop = 'static';

     return $uibModal.open(config);
   };



   return service;
  }

})(angular.module('adminPanel.services.modal', ['ui.bootstrap']));
