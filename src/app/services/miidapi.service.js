(function() {

  'use strict';

  angular.module('adminPanel')
    .factory('APIService', ['$q', '$http', 'MIID_API', APIService]);

  function APIService($q, $http, MIID_API) {

    var sendRequest = function(method, path, data, params, skipAuth, deferred) {
      var url = MIID_API.base_url + path;
      var httpParams = {
        method: method,
        url: url,
        data: data,
        params: params,
        skipAuthorization: skipAuth,
        headers: {
          'Content-Type': 'application/json'
        }
      };

       makeHttpCall(httpParams, deferred);
    };

    var makeHttpCall = function (httpParams, deferred) {
      $http(httpParams)
        .success(function(data) {
          deferred.resolve(data);
        })
        .error(function(error) {
          deferred.reject(error);
        });
    };

    return {

      sendRequest: function(options) {
        var deferred = $q.defer();
        sendRequest(options.method, options.path, options.data, options.params, options.skipAuth, deferred);
        return deferred.promise;
      }

    }

  }

})();
