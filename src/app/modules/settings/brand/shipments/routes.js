(function(app) {

  app.config(['$stateProvider', 'URLProvider', function($stateProvider, URLProvider) {

    $stateProvider
      .state('admin-panel.settings.brand.shipments', {
        url: '/shipments',
        templateUrl: 'app/modules/settings/brand/shipments/shipments.html',
        controller: 'ShipmentsController',
        controllerAs: 'vm'
      });

  }]);

})(angular.module('adminPanel.settings.brand.shipments.routes',[]));
