(function (app) {

  app.controller('ShipmentsController', ShipmentsController);

  ShipmentsController.$inject = ['API', 'QINGSTOR', 'ALERT', 'toastr', '$uibModal', '$localStorage'];

  function ShipmentsController(API, QINGSTOR, ALERT, toastr, $uibModal, $localStorage) {

    var vm = this;
    vm.shipping_options = [];

    vm.brandId = $localStorage.userData.brand._id;
    vm.loading = false;

    vm.loadShippingOptions = function () {

      vm.loading = true;

      API
        .Brands
        .getShippingOptions(vm.brandId).then(function (res) {
          if (res) {
            vm.shipping_options = res;
          }
          vm.loading = false;

        }).catch(function (err) {
          vm.loading = false;
          ALERT.confirm(vm.loadShippingOptions);
        });
    };

    vm.showModal = function(){

      var modalInstance =
        $uibModal
          .open({
            animation: true,
            size: 'md',
            resolve: {
              BRAND_ID: function() {
                return vm.brandId;
              },
              SHIPPING_OPTIONS : function(){
                return vm.shipping_options;
              }
            },
            templateUrl: 'app/modules/settings/brand/shipments/shipping_options_modal/shipping_options_modal.html',
            controller: 'ShippingOptionsModalController',
            controllerAs: "modal",
            backdrop: 'static' //disables click outside close
          });

      modalInstance
        .result
        .then(function (data) {
        }, function (canceled) {})
        .finally(function () {
          vm.loadShippingOptions();
        });

    };

    vm.loadShippingOptions();
  }

})(angular.module('adminPanel.settings.brand.shipments.controllers', []));
