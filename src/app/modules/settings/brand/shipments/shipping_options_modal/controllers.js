(function (app) {

  app.controller('ShippingOptionsModalController', ShippingOptionsModalController);

  ShippingOptionsModalController.$inject = ['BRAND_ID', 'SHIPPING_OPTIONS', '$uibModalInstance', 'Notify', 'API'];

  function ShippingOptionsModalController(BRAND_ID, SHIPPING_OPTIONS, $uibModalInstance, Notify, API) {

    var modal = this;

    modal.isSaving = false;
    modal.originShippingOptions = SHIPPING_OPTIONS;
    modal.shippingOptions = angular.copy(SHIPPING_OPTIONS);

    modal.close = function(){
      $uibModalInstance.dismiss('cancel');
    };

    modal.submit = function() {

      if(modal.shippingOptions.length == 0) {

        Notify.warningInputMissing();

        return;
      }

      modal.isSaving = true;

      API
        .Brands
        .updateShippingOptions(BRAND_ID, modal.shippingOptions).then(function (res) {

          if(res.result == 'success') {
            modal.finishSaving(true);
          } else {
            modal.finishSaving(false);
          }

        }).catch(function (err) {
          modal.finishSaving(false);
        });

    };

    modal.add = function() {
      modal.shippingOptions.push({
        company: "",
        charges: "",
        is_active: true
      });
    };

    modal.remove = function(one) {

      modal.shippingOptions = _.without(modal.shippingOptions, one);

    };

    modal.hasChanges = function() {

      return !angular.equals(modal.originShippingOptions, modal.shippingOptions);
    };

    modal.finishSaving = function(success) {

      modal.isSaving = false;
      $uibModalInstance.dismiss('cancel');

      if(!success) {
        Notify.errorSaving();
      }

      //vm.loadShippingOptions();
    };

  }

})(angular.module('adminPanel.settings.brand.shipments.shipping_options_modal.controllers', []));
