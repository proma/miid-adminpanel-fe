(function(app) {

})(angular.module('adminPanel.settings.brand.shipments',[
  'adminPanel.settings.brand.shipments.routes',
  'adminPanel.settings.brand.shipments.controllers',
  'adminPanel.settings.brand.shipments.shipping_options_modal'
]));
