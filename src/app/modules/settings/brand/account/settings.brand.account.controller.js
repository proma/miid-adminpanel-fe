(function (app) {

  app.controller('BrandAccountSettingsController', BrandAccountSettingsController);

  BrandAccountSettingsController.$inject = ['$localStorage', 'Slug', 'QINGSTOR', 'API', 'MIID_API', 'Upload', 'Notify', 'ALERT', '$state'];

  function BrandAccountSettingsController($localStorage, Slug, QINGSTOR, API, MIID_API, Upload, Notify, ALERT, $state) {
    var vm = this;

    if ($localStorage.userData && $localStorage.userData.brand && $localStorage.userData.brand._id) {
      vm.brandID = $localStorage.userData.brand._id;
      vm.brandRegisteredEmail = $localStorage.userData.email;
    }
    else {
      $state.go('admin-panel.not-found');
      return;
    }

    vm.oldBrand = vm.newBrand = {};
    vm.logo = '';
    vm.loadingBrand = true;
    vm.howManyReq = 0;
    vm.uploader = {
      uploading: false,
      progress: 0
    };
    vm.QRCode = {
      logo: null,
      fileSrc: null,
      uploading: false,
      progress: 0
    };

    vm.contactExist = {
      found: false,
      id: null
    };
    vm.assetURL = QINGSTOR.base_url;
    vm.imageArray = [ [] ];

    vm.slugify = function () {
      vm.newBrand.basic.slug = Slug.slugify(vm.newBrand.basic.slug);
    };

    vm.save = function () {
      vm.loadingBrand = true;

      //it means changes have been made and we need to save this data
      if(!angular.equals(vm.oldBrand.basic, vm.newBrand.basic) ||
         !angular.equals(vm.oldBrand.notifications, vm.newBrand.notifications) ||
         !angular.equals(vm.oldBrand.officeContact, vm.newBrand.officeContact)) {

        var payload = angular.copy(vm.newBrand.basic);

        payload.notifications = _.pick(vm.newBrand.notifications, _.identity);
        payload.notifications.notify_on = _.chain(payload.notifications.notify_on).pick(_.identity).keys().value();
        payload.notifications.notify = vm.newBrand.notifications.notify;
        payload.company_contact = _.pick(vm.newBrand.officeContact, _.identity);

        if (payload.notifications.email_addresses.length < 1) delete payload.notifications.email_addresses;
        if (payload.notifications.notify_on.length < 1)       delete payload.notifications.notify_on;

        vm.howManyReq++;
        API
          .Brands
          .edit(vm.brandID, payload)
          .then(function (obj) {
            if(obj.result === 'success') {
              vm.oldBrand.basic = angular.copy(vm.newBrand.basic);
              vm.oldBrand.notifications = angular.copy(vm.newBrand.notifications);
              vm.oldBrand.officeContact = angular.copy(vm.newBrand.officeContact);
              vm.enableButton();
              vm.showSuccess();
            }
          })
          .catch(function (err) {
            vm.howManyReq--;
            vm.loadingBrand = false;
            Notify.errorSaving();
          });
      }

      //it means changes have been made and we need to save this data
      if(!angular.equals(vm.oldBrand.contact, vm.newBrand.contact)) {
        var payload = angular.copy(vm.newBrand.contact);
        payload = _.pick(payload, _.identity);
        payload.type = 'CUSTOMER_SERVICE';
        if(!payload.person)
          payload.person = 'default'; //so it's not required in GUI.
        vm.howManyReq++;

        if(vm.contactExist.found) {
          //it exist and we need to update it.
          API
            .Brands
            .editContact(vm.brandID, vm.contactExist.id, {contact: payload})
            .then(function (obj) {
              vm.oldBrand.contact = angular.copy(vm.newBrand.contact);
              vm.enableButton();
              vm.showSuccess();
            })
            .catch(function (err) {
              vm.howManyReq--;
              vm.loadingBrand = false;
              Notify.errorSaving();
            });
        }else {
          API
            .Brands
            .createContact(vm.brandID, {contact: payload})
            .then(function (obj) {
              vm.oldBrand.contact = angular.copy(vm.newBrand.contact);
              vm.contactExist.id = obj._id;
              //need to add obj.id to contact.id
              vm.enableButton();
              vm.showSuccess();
            })
            .catch(function () {
              vm.howManyReq--;
              vm.loadingBrand = false;
              Notify.errorSaving();
            });

        }

      }
    };

    vm.uploadImage = function (file, type, cb) {
      if(!file) return; //bug in uploader

      var uploader, url = MIID_API.base_url;
      if (type == 'qr') {
        uploader = vm.QRCode;
        url += '/media/upload/br_wc_qr/';
      } else {
        uploader = vm.uploader;
        url += '/media/upload/br_logo/';
      }

      uploader.uploading = true;
      uploader.progress = 0;
      url += vm.brandID;
      Upload.upload({
        'url': url,
        data: {'file': file}
      })
        .then(function (resp) {
          cb(resp.data);
          Notify.infoImageSaved();
        }, function () {
          Notify.errorSaving();
        }, function (evt) {
          uploader.progress = parseInt(100.0 * evt.loaded / evt.total);
        })
        .finally(function () {
          uploader.uploading = false;
        });
    };

    vm.uploadQRCode = function (qrCode) {
      Upload.base64DataUrl(qrCode)
        .then(function(src){
          vm.QRCode.fileSrc = src;
        });

      vm.uploadImage(qrCode, 'qr', function (obj) {
        vm.QRCode.logo = obj.path;
        console.log(obj);
      })
    };

    vm.uploadLogo = function (logo) {
      vm.uploadImage(logo, 'logo', function (obj) {
        vm.logo = obj.path;
        vm.imageArray[0][0] = vm.assetURL + vm.logo;
      });
    };

    vm.showSuccess = function () {
      vm.howManyReq--;
      if(vm.howManyReq === 0){
        Notify.infoSaved();
        vm.loadingBrand = false;
      }
    };

    vm.loadBrand = function () {
      vm.loadingBrand = true;

      API
        .Brands
        .get(vm.brandID)
        .then(function (obj) {
          vm.oldBrand = {
            basic: {
              brand_id: obj.brand_id,
              name: obj.name,
              slug: obj.slug
            },
            contact: {
              person: '',
              email: '',
              wechat: '',
              phone: '',
              qq: ''
            },
            notifications: {
              email_addresses: [],
              notify: false,
              notify_on: {
                NEW_PAID_ORDER: false,
                BRAND_STATUS_CHANGE: false,
                PRODUCT_STATUS_CHANGE: false
              }
            },
            officeContact: {
              name: '',
              person_name: '',
              phone: '',
              email: '',
              other: '',
              address: ''
            }
          };
          vm.logo = obj.logo;

          if(obj && obj.contacts.length > 0) { //safe check
            var contact = _.find(obj.contacts, function (contact) { return contact.type === 'CUSTOMER_SERVICE';});
            if(contact) {
              vm.contactExist.found = true; //enable's PUT instead of POST
              vm.contactExist.id = contact._id;

              vm.oldBrand.contact.email = contact.email;
              vm.oldBrand.contact.wechat = contact.wechat;
              vm.oldBrand.contact.phone = contact.phone;
              vm.oldBrand.contact.qq = contact.qq;
              vm.oldBrand.contact.person = contact.person;
              vm.QRCode.logo = contact.wechat_qr ? vm.assetURL + contact.wechat_qr: null;
            }
          }

          if(obj && obj.notifications) {
            vm.oldBrand.notifications.email_addresses = obj.notifications.email_addresses;
            vm.oldBrand.notifications.notify = obj.notifications.notify;
            obj.notifications.notify_on.forEach(function (type) {
              vm.oldBrand.notifications.notify_on[type] = true;
            });
          }

          if(obj && obj.company_contact) {
            angular.forEach(obj.company_contact, function(value, key) {
              vm.oldBrand.officeContact[key] = value;
            });
          }
          vm.newBrand = angular.copy(vm.oldBrand);
          vm.imageArray[0].push(vm.assetURL + vm.logo);
        })
        .catch(function (err) {
          ALERT.confirm(vm.loadBrand);
        })
        .finally(function () {
          vm.loadingBrand = false;
        });
    };

    vm.removeImage = function () {
      vm.logo = null;
    };

    vm.enableButton = function () {
      return angular.equals(vm.oldBrand, vm.newBrand);
    };

    //call loadBrand to populate

    vm.loadBrand();
  }

})(angular.module('adminPanel.settings.brand.account.controller', []));
