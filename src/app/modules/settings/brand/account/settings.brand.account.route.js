(function (app) {

  app.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('admin-panel.settings.brand.account', {
        url: '/account',
        templateUrl: 'app/modules/settings/brand/account/settings.brand.account.html',
        controller: 'BrandAccountSettingsController',
        controllerAs: 'vm'
      });
  }]);

})(angular.module('adminPanel.settings.brand.account.route', []));
