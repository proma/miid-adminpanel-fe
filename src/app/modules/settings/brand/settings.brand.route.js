(function (app) {
  app.config(['$stateProvider', 'URLProvider', function ($stateProvider, URLProvider) {
    $stateProvider
      .state('admin-panel.settings.brand', {
        url: '/brand',
        template: '<ui-view></ui-view>',
        abstract: true,
        data: {
          permissions: {
            only: 'canSee.Settings.Brand',
            redirectTo: URLProvider.$get().RedirectURL
          }
        }
      });
  }])
})(angular.module('adminPanel.settings.brand.route', []));
