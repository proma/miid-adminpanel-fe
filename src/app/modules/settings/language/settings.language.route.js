(function (app) {
  app.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('admin-panel.settings.language', {
        url: '/language',
        controller: 'SettingsLanguageController',
        controllerAs: 'vm',
        templateUrl: 'app/modules/settings/language/settings.language.html'
      });
  }])
})(angular.module('adminPanel.settings.language.route', []));
