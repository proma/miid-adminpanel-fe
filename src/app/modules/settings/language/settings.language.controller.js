(function (app) {

  app.controller('SettingsLanguageController', SettingsLanguageController);

  SettingsLanguageController.$inject = ['$rootScope', '$translate', '$localStorage'];

  function SettingsLanguageController($rootScope, $translate, $localStorage) {
    var vm = this;

    vm.lang = $translate.use();
    vm.prevLang = vm.lang;

    vm.changeLang = function (lang) {
      $translate.use(lang);
      $rootScope.$broadcast('$translationChangeSuccess'); //doesn't work if not broad casted
      $localStorage.lang = lang;
      vm.prevLang = vm.lang;
    }
  }

})(angular.module('adminPanel.settings.language.controller', []));
