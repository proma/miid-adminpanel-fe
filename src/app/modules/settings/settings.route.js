(function (app) {
  app.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('admin-panel.settings', {
        url: '/settings',
        abstract: true,
        template: '<ui-view></ui-view>'
      });
  }]);
})(angular.module('adminPanel.settings.route', []));
