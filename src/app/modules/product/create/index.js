(function(app){

})(angular.module('adminPanel.product.create',
  [
    'adminPanel.product.create.routes',
    'adminPanel.product.create.basic_info',
    'adminPanel.product.create.product_media',
    'adminPanel.product.create.customization_desc',
    'adminPanel.product.create.visual_assets'
  ]
));
