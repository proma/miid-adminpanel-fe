(function (app) {
  'use-strict';

  app.controller('ProductVisualAssetsLinkageController', ProductVisualAssetsLinkageController);

  ProductVisualAssetsLinkageController.$inject = ['$scope', 'ProgressBlanket', 'API', 'ALERT', '$stateParams', 'QINGSTOR', 'Notify']; //min-safe code

  /**
   *
   * @param $scope
   * @param APIService
   * @param API
   * @param ALERT
   * @param $stateParams
   * @param QINGSTOR
   * @param Notify
   */
  function ProductVisualAssetsLinkageController($scope, ProgressBlanket, API, ALERT, $stateParams, QINGSTOR, Notify) {
    var vm = this;
    vm.baseURL = QINGSTOR.base_url;
    var productId = $stateParams.productID;
    $scope.product_id = productId;
    vm.lang = 'ch';

    vm.steps_info = {
      steps: [],
      components: [],
      cache: {}
    };

    vm.componentsInfo = {
      personalizeComponents: [],
      components: [],
      componentsByObject: {}
    };

    vm.scenes = [];
    vm.oldScenes = [];
    vm.defaultVisibilityOptions = [
      {colorMix: false, visibility: true, label: 'Yes'},
      {colorMix: false, visibility: false, label: 'No'}
    ];

    vm.loading = true;

    // get all components for selected product
    vm.loadComponents = function () {
      API
        .Components
        .query(productId)
        .then(function (components) {
          var tempMaterials = [], tempShapes = [];
          vm.componentsInfo.components = components;
          // filter parts and add respective materials shapes and multi select to there parent folders
          vm.componentsInfo.components.forEach(function (component) {
            // check if data is valid
            if (component && component.path && component.title) {
              // check for materials shapes and multi select
              if (component.title.indexOf('$') == 0) {
                vm.componentsInfo.personalizeComponents.push(component);
                vm.componentsInfo.components = _.without(vm.componentsInfo.components, component);
              } else if (/^[!_#$].*$/.test(component.title)) {
                var pathWithOutCurrentTitle = component.path.slice(0, component.path.lastIndexOf('/'));
                // in case the materials are under shapes
                var shapeIndex = -1;
                if ((shapeIndex = pathWithOutCurrentTitle.lastIndexOf('/!')) > 0) {
                  pathWithOutCurrentTitle = component.path.slice(0, shapeIndex);
                }

                // parent via path form collection
                var parent = _.find(vm.componentsInfo.components, function (item) {
                  return item.path == pathWithOutCurrentTitle;
                });
                // check if parent exists
                if (parent) {
                  //material
                  if (component.title.indexOf('_') == 0) {
                    if (!parent.materials) {
                      parent.materials = [];
                    }
                    parent.materials.push(component);

                  } else if (component.title.indexOf('!') == 0) { // shape
                    if (!parent.shapes) {
                      parent.shapes = [];
                    }
                    parent.shapes.push(component);
                  } else { // multi select
                    if (!parent.multiselect) {

                      parent.multiselect = [];
                    }
                    parent.multiselect.push(component);
                  }
                }
                // remove shape material or multi select form main collection
                vm.componentsInfo.components = _.without(vm.componentsInfo.components, component);
              } else {
                // don't add component to its self
                if (component.path != component.title) {
                  var pathWithOutCurrentTitle = component.path.slice(0, component.path.lastIndexOf('/'));
                  // parent via path form collection
                  var parent = _.find(vm.componentsInfo.components, function (item) {
                    return item.path == pathWithOutCurrentTitle;
                  });
                  if (parent) {
                    if (!parent.children) {
                      parent.children = [];
                    }
                    // add each component to its parent in current path
                    parent.children.push(component);
                  }
                }
              }
            }
          });

          //fill all common shapes materials and multi select
          vm.componentsInfo.components.forEach(function (component) {

            if (component && component.children) {
              component.shapes = _.union(component.shapes, vm.findCommonInChild(component.children, 'shapes'));
              component.materials = _.union(component.materials, vm.findCommonInChild(component.children, 'materials'));
              component.multiselect = _.union(component.multiselect, vm.findCommonInChild(component.children, 'multiselect'));
            }

            //Merging UI with their Color Mixes.
            component.shapes = vm._mergeColorMixAndUI(component.shapes);
            component.materials = vm._mergeColorMixAndUI(component.materials);
            component.multiselect = vm._mergeColorMixAndUI(component.multiselect);

            /*
             * Creating an Object of components with Component._id as key. This will be easier
             * to locate the component instead of running like 100 loops everytime.
             * */
            vm.componentsInfo.componentsByObject[component._id] = component;
          });

          // after loading components load steps and scenes
          vm.loadScenes();
          vm.loadSteps()

        }).catch(function (err) {
        ALERT.confirm(vm.loadComponents);
      });
    };

    vm.loadScenes = function () {
      // get all scenes for selected product
      API
        .Scenes
        .query(productId)
        .then(function (scenes) {
          vm.scenes = scenes;
          if (vm.scenes && vm.scenes.length) {
            //Set Material object
            vm.scenes.forEach(function (action) {
              action.sceneOption = vm.getTo(action, action.type);
            });
            vm.oldScenes = angular.copy(vm.scenes);
          } else {
            vm.addSceneUIaction(vm.scenes);
          }
        }).catch(function (err) {
        ALERT.confirm(vm.loadScenes);
      });
    };

    vm.loadSteps = function () {
      // get all steps, groups and UIs existing for selected product
      var oldStep ={}, oldGroup = {}, oldMaterial = {};
      API
        .Steps
        .query(productId)
        .then(function (steps) {
          steps.forEach(function (step) {
            oldStep = {
              id: step.icon,
              title: step[vm.lang].title,
              step_id: step._id,
              groups: []
            };
            step.groups.forEach(function (group) {
              oldGroup = {
                type: group.type,
                title: group[vm.lang].title,
                group_id: group._id,
                materials: []
              };
              if (group.type == 'PERSONALIZE') {
                group.personalize.forEach(function (personalize) {
                  oldMaterial = {
                    type: personalize.element.type,
                    val: personalize.element.val,
                    name: personalize[vm.lang].name,
                    tech: personalize.tech,
                    component: personalize.component,
                    material_id: personalize._id
                  };
                  vm.steps_info.cache[oldMaterial.material_id] = angular.copy(oldMaterial);
                  oldGroup.materials.push(oldMaterial);
                });
              }
              else if (group.type == 'MATERIALS' || group.type == 'SHAPES') {
                group.ui.forEach(function (ui) {
                  oldMaterial = {
                    type: ui.element.type,
                    val: ui.element.val,
                    name: ui[vm.lang].name,
                    tech: ui.tech,
                    uiAction: ui.ui_action,
                    material_id: ui._id
                  };

                  if (oldMaterial.uiAction && oldMaterial.uiAction.length) {
                    //set ui_action _set value to component object
                    oldMaterial.uiAction.forEach(function (action) {
                      action.option = vm.getTo(action, group.type);
                    });
                  } else {
                    vm.addUIaction(oldMaterial);
                  }
                  vm.steps_info.cache[oldMaterial.material_id] = angular.copy(oldMaterial);
                  oldGroup.materials.push(oldMaterial);
                });
              }
              oldStep.groups.push(oldGroup);
            });
            vm.steps_info.steps.push(oldStep);
          });
          // mark loading as done
          vm.loading = false;
        }).catch(function (err) {
        ALERT.confirm(vm.loadSteps);
      });
    };

    // Data processing methods

    /*
     * @param array: Array; This can be Shapes, Materials and MultiSelects (not done yet)
     * @description; This function merges color mixes in Materials, Shapes and MultiSelects to
     * themselves. If array has size 4 with 2 color mixes end result size of array will be 6
     * */
    vm._mergeColorMixAndUI = function (array) {
      var tempArray = [];
      if (!array)
        return tempArray;

      array.forEach(function (option) {
        if (option.color_mix && option.color_mix.length) {
          option.color_mix.forEach(function (clrMix) {
            tempArray.push({colorMix: true, component_title: option.title, label: clrMix.label, id: clrMix._id});
          });
        } else if (option.label) {
          tempArray.push({colorMix: false, component_title: option.title, label: option.label});
        }
      });

      return tempArray;
    };

    vm.findCommonInChild = function (components, propertyName) {
      var common, componentCommon;
      if (components) {
        components.forEach(function (component) {
          componentCommon = component[propertyName];
          if (componentCommon) {
            if (component.children) {
              componentCommon = vm.intersectionObjects(componentCommon, vm.findCommonInChild(component.children, propertyName));
            }

            if (common) {
              common = vm.intersectionObjects(common, componentCommon);
            } else {
              common = componentCommon;
            }
          }
        });
      }
      return common;
    };

    vm.intersectionObjects = function (array1, array2) {
      var intesection = [];
      if (array1 && array1.length && array2 && array2.length) {
        array1.forEach(function (item) {
          var item1 = _.find(array2, function (item2) {
            return item2.title == item.title;
          });

          if (item1) {
            intesection.push(item1);
          }
        });
      }
      return intesection;
    };

    vm.getOptions = function (uiAction, type) {
      var typeLower = type.toLowerCase();
      if (typeLower == 'personalize')
        return vm.componentsInfo.personalizeComponents;
      else if (typeLower === 'visibility') { //TODO: need to handle multi select
        return vm.defaultVisibilityOptions;
      }
      else if (uiAction && uiAction._set)
        return vm.componentsInfo.componentsByObject[uiAction._set][typeLower];
      else
        return [];
    };

    vm.getTo = function (uiaction, type) {
      var options = vm.getOptions(uiaction, type);
      var selectedOption = null;
      if (uiaction.color_mix) {
        options.forEach(function (option) {
          if (option.colorMix == true && option.id == uiaction.color_mix) {
            selectedOption = option;
          }
        });
      } else if (uiaction.type == 'VISIBILITY') {
        if (uiaction.visibility == true) {
          selectedOption = vm.defaultVisibilityOptions[0];
        } else {
          selectedOption = vm.defaultVisibilityOptions[1];
        }
      }
      else if (uiaction._to) {
        options.forEach(function (option) {
          if (option.colorMix == false && option.component_title == uiaction._to) {
            selectedOption = option;
          }
        });
      }

      return selectedOption;
    };

    vm.setTo = function (uiaction, option) {
      if (option) {
        if (option.colorMix == true) {
          uiaction.color_mix = option.id;
        } else if (!_.isUndefined(option.visibility)) {
          uiaction.visibility = option.visibility;
          return;
        } else {
          uiaction.color_mix = null;
        }
        uiaction._to = option.component_title;
      }
    };

    //add ui action to UI
    vm.addUIaction = function (material) {
      if (material && material.uiAction) {
        material.uiAction.push({
          _to: null,
          _set: null,
          visibility: true
        });
      }
    };

    vm.checkForChanges = function (material, type) {
      if (type && type.toLowerCase() == 'personalize')
        return angular.equals(material.component, vm.steps_info.cache[material.material_id].component);
      else
        return angular.equals(material.uiAction,
         vm.steps_info.cache[material.material_id].uiAction);
    };

    vm.checkForSceneChanges = function () {
      return angular.equals(vm.scenes, vm.oldScenes);
    };

    vm.saveAllChanges = function () {
      ProgressBlanket.open(vm.getAllChanges);
    };

    vm.getAllChanges = function (fn) {
      var requests = [];
      var errorInUIAction = false;
      if(!vm.checkForSceneChanges()) {
        vm.scenes.forEach(function (action) {
          if ((!action._set || !action._to) && !action.sceneOption) {
            errorInUIAction = true;
          }
        });

        if (!errorInUIAction)
          requests.push(function(cb){ return vm.createUpdateSceneUIActions(cb);});
      }
      errorInUIAction = false;

      vm.steps_info.steps.forEach(function (step) {
        step.groups.forEach(function (group) {
          group.materials.forEach(function (material) {
            if (!vm.checkForChanges(material, group.type)) {
              if (group.type.toLowerCase() != 'personalize')
                material.uiAction.forEach(function (action) {
                  if (!action._set || !action._to) {
                    errorInUIAction = true;
                  }
                });

              if (!errorInUIAction)
                requests.push(function(cb){ return vm.createUpdateUIActions(step, group, material, cb);});
            }

            errorInUIAction = false;
          });
        });
      });
      fn && fn(requests);
      return requests;
    };

    //add ui action to Scene
    vm.addSceneUIaction = function () {
      if (!vm.scenes) {
        vm.scenes = [];
      }

      vm.scenes.push(
        {
          "_id": null,
          "color_mix": null,
          "_to": null,
          "_set": null,
          "visibility": true,
          "type": "MATERIALS"
        }
      );
    };

    // Create update UI actions
    vm.createUpdateUIActions = function (step, group, material, cb) {
      material.loading = true;
      if (material && material.uiAction) {
        var uiActions = [];
        material.uiAction.forEach(function (action) {
          var actionObj = {
            _to: action._to,
            _set: action._set,
            visibility: action.visibility
          };
          if (action.color_mix) {
            actionObj.color_mix = action.color_mix;
          }
          if (action._id) {
            actionObj.action_id = action._id;
          }
          uiActions.push(actionObj);
        });

        if (uiActions.length > 0) {
          API
            .UIAction
            .edit(productId, step.step_id, group.group_id, material.material_id, uiActions)
            .then(function (result) {
              //Save _ids of newly created UI Actions
              material.uiAction.forEach(function (value, key) {
                if (!value._id)
                  material.uiAction[key]._id = result.ids[key];
              });
              //Update the cache (for comparison to work)
              vm.steps_info.cache[material.material_id].uiAction = angular.copy(material.uiAction);
              if (cb)
                cb(null, true);
              else
                Notify.infoSaved();
            })
            .catch(function (err) {
              console.error('Error while creating/updating linkage at' + new Date(), err);

              if (cb)
                cb(err);
              else
                ALERT
                  .errorDataSaving(function () {
                    return vm.createUpdateUIActions(step, group, material);
                  });
            })
            .finally(function () {
              material.loading = false;
            });
        }
      } else if (group.type.toLowerCase() == 'personalize'){ //Its Personalize Group
        API
          .Personalize
          .updateComponent(productId, step.step_id, group.group_id, material.material_id, material.component)
          .then(function (result) {
            console.log(result);
            vm.steps_info.cache[material.material_id].component = material.component;

            if (cb)
              cb(null, true);
            else
              Notify.infoSaved();
          })
          .catch(function (err) {
            console.error('Error while creating/updating linkage of Personalize at' + new Date(), err);
            if (cb)
              cb(err);
            else
              ALERT
                .errorDataSaving(function () {
                  return vm.createUpdateUIActions(step, group, material);
                });
          })
          .finally(function () {
            material.loading = false;
          });
      }
    };

    // Create update UI actions for product scene
    vm.createUpdateSceneUIActions = function (cb) {
      vm.processingScene = true;
      if (vm.scenes && vm.scenes.length) {
        var uiActions = [];
        vm.scenes.forEach(function (action) {
          var newAction = {
            _to: action._to,
            _set: action._set,
            visibility: action.visibility,
            type: action.type
          };
          if (action.color_mix) {
            newAction.color_mix = action.color_mix;
          }

          if (action._id) {
            newAction.action_id = action._id;
          }

          uiActions.push(newAction);
        });

        if (uiActions.length > 0) {
          API
            .Scenes
            .save(productId, uiActions)
            .then(function (result) {
              if (result.hasOwnProperty('ids')) {
                var newActions = _.filter(vm.scenes, function (action) {
                  return !action.hasOwnProperty('_id');
                });
                newActions.forEach(function (action, index) {
                  action._id = result.ids[index];
                });
              }
              vm.oldScenes = angular.copy(vm.scenes);
              if (cb)
                cb(null, true);
              else
                Notify.infoSaved();
            })
            .catch(function (err) {
              if (cb)
                cb(err);
              else
                ALERT
                  .errorDataSaving(function () {
                    return vm.createUpdateSceneUIActions();
                  });
            })
            .finally(function () {
              vm.processingScene = false;
            });
        }
      }
    };

    // Delete UI Action
    vm.deleteUIAction = function (step, group, material, uiaction) {
      ALERT.confirmDeleting(function () {
        return vm.deleteUIActionOnConfirmation(step, group, material, uiaction);
      })
    };

    // Delete UI Action for scene
    vm.deleteSceneUIAction = function (uiaction) {
      ALERT.confirmDeleting(function () {
        return vm.deleteSceneUIActionOnConfirmation(uiaction);
      });
    };

    vm.deleteUIActionOnConfirmation = function (step, group, material, uiaction) {
      if (uiaction && uiaction._id) {
        material.loading = true;
        API
          .UIAction
          .remove(productId, step.step_id, group.group_id, material.material_id, 'ui_action', uiaction._id)
          .then(function (result) {
            material.uiAction = _.without(material.uiAction, uiaction);
            if (material.uiAction.length == 0) {
              vm.addUIaction(material);
            }
            //Update the cache
            vm.steps_info.cache[material.material_id].uiAction = angular.copy(material.uiAction);
          }).catch(function (err) {
            console.error("Error deleting linkage at " + new Date(), err);
            ALERT
              .errorDeleting(function () {
                return vm.deleteUIActionOnConfirmation(step, group, material, uiaction);
              });
          })
          .finally(function () {
            material.loading = false;
          });
      } else {
        material.uiAction = _.without(material.uiAction, uiaction);
        if (material.uiAction.length == 0) {
          vm.addUIaction(material);
        }
        //Updating the cache
        vm.steps_info.cache[material.material_id].uiAction = angular.copy(material.uiAction);
      }
    };

    vm.deleteSceneUIActionOnConfirmation = function (uiaction) {
      if (uiaction && uiaction._id) {
        vm.processingScene = true;
        API
          .Scenes
          .remove(productId, uiaction._id)
          .then(function (result) {
            vm.scenes = _.without(vm.scenes, uiaction);
            if (vm.scenes.length == 0) {
              vm.addSceneUIaction();
            }
            vm.oldScenes = angular.copy(vm.scenes);
          }).catch(function (err) {
            ALERT
              .errorDeleting(function () {
                return vm.deleteSceneUIActionOnConfirmation(uiaction);
              });
          })
          .finally(function () {
            vm.processingScene = false;
          });
      } else {
        vm.scenes = _.without(vm.scenes, uiaction);
        if (vm.scenes.length == 0) {
          vm.addSceneUIaction();
        }
      }
    };

    // load components
    vm.loadComponents();
  }

  app.filter('capitalize', function () {
    return function (input, all) {
      var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
      return (!!input) ? input.replace(reg, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      }) : '';
    }
  });

})(angular.module('adminPanel.product.create.visual_assets.va_linkage.controller', []));
