(function(app){

  app.config(function($stateProvider){

    $stateProvider
      .state('admin-panel.product.create.va.va_linkage',{
        url: '/linkage',
        templateUrl: 'app/modules/product/create/visual_assets/va_linkage/va_linkage.html',
        controller: 'ProductVisualAssetsLinkageController',
        controllerAs: 'vm'
      });

  });

})(angular.module('adminPanel.product.create.visual_assets.va_linkage.routes',[]));
