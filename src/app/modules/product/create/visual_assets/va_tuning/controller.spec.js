describe('Controllers', function () {
  describe('ProductVisualAssetsTuningController', function(){

    var data = {
      id: 'product1_id',
      miid_id: 'product_miid_id',
      name: 'product1',
      components: [
        {
          id: 'comp1_id',
          title: 'comp1',
          have_color: true,
          path: 'product/comp',
          color_mix:
            [
              {
                id: 'mix1_id',
                label: 'mix1',
                value: '#333333',
                opacity: 1
              },
              {
                id: 'mix2_id',
                label: 'mix2',
                value: '#222222',
                opacity: 1
              }
            ]
        }
      ]
    };

    var
      ProductVisualAssetsTuningController,
      $stateParams,
      URL,
      $rootScope,
      mixesURL,
      productURL,
      ALERT,
      componentsURL,
      $httpBackend;

    fabric = {
      Canvas: function(id) {
        return {
          setHeight: function(num) {},
          setWidth: function(num) {}
        }
      }
    };


    // load the dependencies module
    beforeEach(module('ui.router'));
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.services.alert'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.api'));
    beforeEach(module('adminPanel.product.create.visual_assets.va_tuning.controller'));

    beforeEach(inject(function($injector){
      $rootScope = $injector.get('$rootScope');
      $stateParams = $injector.get('$stateParams');
      $stateParams.productID = data.id;
      $httpBackend = $injector.get('$httpBackend');
      $rootScope.treeInstance = {
        jstree: function(a) {
          return {
            show_all: function() {}
          }
        }
      };
      URL = $injector.get('URL');
      ALERT = $injector.get('ALERT');
      var $controller = $injector.get('$controller');
      ProductVisualAssetsTuningController = $controller('ProductVisualAssetsTuningController',{
        '$scope': $rootScope,
        'API': $injector.get('API'),
        'ALERT': ALERT,
        '$stateParams' : $stateParams
      });
      mixesURL = URL.COLORMIX.replace(':productId', data.id);
      productURL = URL.PRODUCTS + '/' + data.id;
      componentsURL = URL.COMPONENTS.replace(':productId', data.id);
      spyOn(ProductVisualAssetsTuningController, 'loadComponents');
    }));

    describe('MA-97: Products assets tuning', function(){

      it('should initialize scope variables correctly',function(){
        expect(ProductVisualAssetsTuningController.loading).toBeTruthy();
        expect(ProductVisualAssetsTuningController.isPreview).toBeFalsy();
        expect(ProductVisualAssetsTuningController.currentView).toBe(0);
        expect(ProductVisualAssetsTuningController.showColorEnabled).toBeFalsy();
        expect(ProductVisualAssetsTuningController.selectedColor).toBe('#000000');
        expect(ProductVisualAssetsTuningController.mixType).toBe('Overlay');
        expect(ProductVisualAssetsTuningController.opacity).toBe(100);
        expect(ProductVisualAssetsTuningController.showStatusMsg).toBeFalsy();
        expect(ProductVisualAssetsTuningController.treeConfig.core.multiple).toBeFalsy();
      });


      it('should stop loading after load', function() {
        $httpBackend.expect('GET', productURL).respond(200, data);
        $httpBackend.expect('GET', componentsURL).respond(200, data.components);
        $httpBackend.flush();
        ProductVisualAssetsTuningController.loadComponents();
        expect(ProductVisualAssetsTuningController.loading).toBeFalsy();
      });

      it('should load the jstree tree model when loading components', function() {
        $httpBackend.expect('GET', productURL).respond(200, data);
        $httpBackend.expect('GET', componentsURL).respond(200, data.components);

        ProductVisualAssetsTuningController.loadComponents();
        $httpBackend.flush();

        expect(ProductVisualAssetsTuningController.treeModel).toBeDefined();
      });

      it('should call loadComponents() first', function() {
        $httpBackend.expect('GET', productURL).respond(200, data);
        $httpBackend.expect('GET', componentsURL).respond(200, data.components);
        ProductVisualAssetsTuningController.loadComponents();
        $httpBackend.flush();
        expect(ProductVisualAssetsTuningController.loadComponents).toHaveBeenCalled();
      });


      it('should call alert when error loading product',function(){
        spyOn(ALERT, 'confirm');
        $httpBackend.expect('GET', productURL).respond(500, data);
        $httpBackend.flush();
        ProductVisualAssetsTuningController.loadComponents();
        expect(ALERT.confirm).toHaveBeenCalled();
      });

      it('should call alert when error loading components',function(){
        spyOn(ALERT, 'confirm');
        $httpBackend.expect('GET', productURL).respond(200, data);
        $httpBackend.expect('GET', componentsURL).respond(500, data.components);
        $httpBackend.flush();
        ProductVisualAssetsTuningController.loadComponents();
        expect(ALERT.confirm).toHaveBeenCalled();
      });

    });

  });

});
