(function (app) {
  'use-strict';

  app.controller('ProductVisualAssetsTuningController', ProductVisualAssetsTuningController);

  ProductVisualAssetsTuningController.$inject = ['$scope', 'API', 'ALERT', '$stateParams', 'MIID_ASSETS_API'];

  function ProductVisualAssetsTuningController($scope, API, ALERT, $stateParams, MIID_ASSETS_API) {
    var vm = this;

    var productId = $stateParams.productID;
    $scope.product_id = productId;

    vm.loading = true;
    vm.isPreview = false;
    vm.enableCreate = false;
    vm.currentView = 0;

    vm.showColorEnabled = false;
    $scope.$watch('vm.showColorEnabled', function () {
      vm.showOnlyColorEnabled();
    });

    vm.selectedColor = '#000000';
    vm.mixType = 'Overlay';
    $scope.$watch('vm.mixType', function () {
      vm.updateColorMixNodeBlend();
    });
    vm.opacity = 100;
    $scope.$watch('vm.opacity', function () {
      vm.updateColorMixNodeOp();
    });
    vm.copiedMix = null;
    vm.statusMsg = '';
    vm.showStatusMsg = false;
    vm.selectedMaterial = "";
    vm.controlsEnabled = false;

    vm.defaultMixLabel = 'Default';
    vm.defaultMixData = {
      label: vm.defaultMixLabel,
      value: '#000000',
      opacity: 1,
      blend: 'overlay'
    };

    // fabricjs canvas
    vm.canvas = new fabric.Canvas('previewCanvas');
    vm.canvas.setHeight(250);
    vm.canvas.setWidth(225);

    // Blend type has been selected in the dropdown
    vm.selectMixType = function (type) {
      vm.mixType = type;
    };

    // JSTree configuration object
    vm.treeConfig = {
      core: {
        multiple: false,
        check_callback: true,
        themes: {
          dots: false,
          icons: false
        }
      },
      plugins: [
        'sort'
      ],
      version: 1 // version is for recreating the tree when needed
    };

    vm.getJsTree = function () {
      return $scope.treeInstance.jstree(true);
    };

    vm.treeModel = [];
    vm.nonColorEnabledNodes = [];
    vm.nodeId = 0;
    /**
     * Loads the jstree tree model by using the loaded components from the database
     * @param miid_id
     */
    vm.loadComponentTreeModel = function (miid_id) {
      var directoryIds = {};
      vm.treeModel.push({
        id: 'root',
        parent: '#',
        text: miid_id,
        icon: ''
      });
      for (var i = 0; i < vm.components.length; i++) {
        var c = vm.components[i];
        c.root_path = miid_id + '/' + c.path;

        var treeComp = vm.generateParentNode(c, directoryIds, vm.nodeId++);
        vm.treeModel.push(treeComp);

        if (c.color_mix && c.color_mix.length > 0) {
          for (var j = 0; j < c.color_mix.length; j++) {
            var mix = c.color_mix[j];
            var mixComp = vm.generateColorMixNode(c, mix, vm.nodeId++, treeComp.id);
            vm.treeModel.push(mixComp);
          }
        } else if (/^[_].*$/.test(c.title) && c.label) { // add default with component label
          var data = {
            label: c.label,
            value: "#000000",
            opacity: 1,
            blend: "overlay",
            enable: false
          };
          var mixComp = vm.generateColorMixNode(c, data, vm.nodeId++, treeComp.id);
          vm.treeModel.push(mixComp);
        }
      }

      // Setup click handlers for dynamically added content
      angular.element('.jstree').on('click', 'input.color-mix-name-input', function (e) {
        // prevent click event to avoid user experience problems
        e.preventDefault();
        e.stopPropagation();
      });

      angular.element('.jstree').on('click', 'a.apply-changes', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var parentId = $(this).data('parent-node-id');
        var thisNodeId = $(this).data('node-id');
        var componentNode = vm.getJsTree().get_node(parentId);

        var node = vm.getJsTree().get_node(thisNodeId);
        var mixNode = vm.getJsTree().get_node(thisNodeId, true);
        var label = mixNode.find('.color-mix-name-input').val().trim();
        var color = mixNode.find('.color-mix-hex').text();
        var opacity = parseInt(mixNode.find('.color-mix-op').text()) / 100;
        var blend = mixNode.find('.color-mix-blend').val().toLowerCase();
        var enabled = mixNode.find('.color-mix-enabled').is(':checked');

        //check if label exists
        if (!label.length || (node.original.label != label && checkIfLabelExits(componentNode, label))) {
          vm.setToLabelError(thisNodeId);
          return;
        } else {
          vm.removeLabelError(thisNodeId);
        }

        var data = {
          label: label,
          value: color,
          opacity: opacity,
          blend: blend,
          enable: enabled
        };

        // in case the node not save on serve then for sure this one is default
        // node added when there is no color mix for material
        // for this if only label is updated the only update material label
        // and if color of opacity is not default then create new color mix
        if (node && node.original && !node.original.dbId) {
          if (color != '#000000' || opacity != 1) {
            vm.loading = true;
            vm.createComponentColorMix(null, parentId, data, function (mix) {
              node.original.dbId = mix._id;
              // update jstree node data in tree model
              data.text = vm.generateColorMixNodeHtml(data, thisNodeId, parentId)
              vm.updateNodeInTreeModel(thisNodeId, data);
              mixNode.find('.color-mix-name').html(mix.label);
              vm.setToReadOnlyMode(thisNodeId, true);
              vm.loading = false;
            });
          } else {
            vm.loading = true;
            API
              .Components
              .edit($scope.product_id, componentNode.original.dbId, {label: data.label})
              .then(function (resp) {
                mixNode.find('.color-mix-name').html(resp.label);
                vm.setToReadOnlyMode(thisNodeId, true);
                vm.loading = false;
              })
              .catch(function () {
                ALERT.errorDataSaving();
                vm.loading = false;
              });
          }
        } else {
          // do normal color mix update
          // update jstree node data in tree model
          vm.updateNodeInTreeModel(thisNodeId, {
            label: label,
            value: color,
            opacity: opacity,
            blend: blend,
            enabled: enabled,
            text: vm.generateColorMixNodeHtml(data, thisNodeId, parentId)
          });

          vm.loading = true;
          vm.updateComponentColorMix(thisNodeId, parentId, data, function (mix) {
            mixNode.find('.color-mix-name').html(mix.label);
            vm.setToReadOnlyMode(thisNodeId, true);
            vm.loading = false;
          });
        }
      });

      angular.element('.jstree').on('click', 'a.cancel-changes', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var nodeId = $(this).data('node-id');
        vm.removeLabelError(nodeId);
        vm.setToReadOnlyMode(nodeId, true);
        $scope.$apply(function () {
          vm.currentlyEditing = null;
          vm.currentlyEditingDOM = null;
        });
      });

      angular.element('.jstree').on('click', 'input.color-mix-enabled', function (e) {
        e.stopPropagation();
        var nodeId = $(this).data('node-id');
        var parentId = vm.getJsTree().get_parent(nodeId);
        var componentNode = vm.getJsTree().get_node(parentId);

        var node = vm.getJsTree().get_node(nodeId);
        var mixNode = vm.getJsTree().get_node(nodeId, true);
        var label = mixNode.find('.color-mix-name-input').val().trim();
        var color = mixNode.find('.color-mix-hex').text();
        var opacity = parseInt(mixNode.find('.color-mix-op').text()) / 100;
        var blend = mixNode.find('.color-mix-blend').val().toLowerCase();
        var enabled = mixNode.find('.color-mix-enabled').is(':checked');
        node.original.enabled = enabled;

        //check if label exists
        if (!label.length || (node.original.label != label && checkIfLabelExits(componentNode, label))) {
          vm.setToLabelError(nodeId);
          return;
        } else {
          vm.removeLabelError(nodeId);
        }

        var data = {
          label: label,
          value: color,
          opacity: opacity,
          blend: blend,
          enable: enabled
        };

        // in case the node not save on serve then for sure this one is default
        // node added when there is no color mix for material
        // for this if only label is updated the only update material label
        // and if color of opacity is not default then create new color mix
        if (node && node.original && !node.original.dbId) {
          vm.loading = true;
          vm.createComponentColorMix(null, parentId, data, function (mix) {
            node.original.dbId = mix._id;
            // update js tree node data in tree model
            data.text = vm.generateColorMixNodeHtml(data, nodeId, parentId)
            vm.updateNodeInTreeModel(nodeId, data);
            mixNode.find('.color-mix-name').html(mix.label);
            vm.setToReadOnlyMode(nodeId, true);
            vm.loading = false;
          });
        } else {
          vm.loading = true;
          vm.updateComponentColorMix(nodeId, node.parent, {enable: enabled}, function () {
            vm.enableControls(enabled);
            vm.updateCanvasImage(enabled);
            vm.loading = false;
          });
        }
      });
    };

    angular.element('.dropdown-btn').click(function (e) {
      if ($(this).hasClass('disabled')) e.preventDefault();
    });

    angular.element('.sp-container.sp-input-disabled').click(function (e) {
      e.preventDefault();
      e.stopPropagation();
    });

    vm.enableControls = function (enable) {
      if (enable) {
        vm.controlsEnabled = true;
        angular.element('.sp-container').removeClass('sp-input-disabled');
      } else {
        vm.controlsEnabled = false;
        angular.element('.sp-container').addClass('sp-input-disabled');
      }
    };

    /**
     * Given a database component it will generate the corresponding json data
     * for the jstree configuration
     * @param c
     * @param directoryIds
     * @param nodeId
     * @returns the JSON representation of this node for the jstree configuration
     */
    vm.generateParentNode = function (c, directoryIds, nodeId) {
      var treeComp = {};
      treeComp.id = 'ID' + nodeId;
      treeComp.dbId = c._id;
      directoryIds[c.path] = treeComp.id;

      if (c.path.indexOf('/') < 0) {
        treeComp.parent = 'root';
      } else {
        var parent = c.path.substr(0, c.path.lastIndexOf("/"));
        treeComp.parent = directoryIds[parent];
      }

      treeComp.text = c.title;
      if (c.title.indexOf('_') == 0) {
        treeComp.isMaterial = true;
        treeComp.imageSrc = c.root_path;
      }

      if (treeComp.isMaterial && !c.have_color) {
        vm.nonColorEnabledNodes.push(treeComp.id);
      }
      return treeComp;
    };

    /**
     * Given a database component and a color mix, it will generate the corresponding json data
     * for the jstree configuration
     * @param c
     * @param directoryIds
     * @param nodeId
     * @returns the JSON representation of this color mix node for the jstree configuration
     */
    vm.generateColorMixNode = function (c, mix, nodeId, parent) {
      var treeComp = {};
      treeComp.id = 'ID' + nodeId;
      treeComp.compDbId = c._id;
      treeComp.dbId = mix._id;

      treeComp.parent = parent;
      treeComp.li_attr = {
        style: 'margin: 10px 0px'
      };
      treeComp.a_attr = {
        class: 'color-mix-cont'
      };

      treeComp.text = vm.generateColorMixNodeHtml(mix, treeComp.id, parent);
      treeComp.imageSrc = c.root_path;
      treeComp.isColorMix = true;
      treeComp.label = mix.label;
      treeComp.enabled = mix.enable;
      treeComp.blend = mix.blend;

      return treeComp;
    };

    /**
     * Since color mix nodes are complex, we need to generate HTML for them to be used as
     * the text property of the jstree JSON configuration. This method creates the html
     * structure for a color mix node in the jstree.
     * @param mix
     * @param nodeId
     * @param parentId
     * @returns Color mix node HTML string
     */
    vm.generateColorMixNodeHtml = function (mix, nodeId, parentId) {
      var html =
        '<div class="color-mix">' +
        '<span class="color-mix-name">' + mix.label + '</span>' +
        '<input type="text" class="color-mix-name-input" value="' + mix.label + '"/>' +
        '<span class="color-mix-label">This material name already exists</span>' +
        '<span class="color-mix-prev" style="background-color: ' + mix.value + '"></span>' +
        '<span class="color-mix-hex">' + mix.value + '</span>' +
        '<span class="color-mix-op">' + mix.opacity * 100 + '</span><span class="perc">%</span>' +
        '<input type="checkbox" class="color-mix-enabled" data-node-id="' + nodeId + '" ' + (mix.enable ? 'checked' : '') + '/>' +
        '<span class="color-mix-apply"><a href="#" class="apply-changes label label-primary" data-parent-node-id="' + parentId + '" data-node-id="' + nodeId + '">APPLY</a></span>' +
        '<span class="color-mix-cancel"><a href="#" class="cancel-changes label" data-node-id="' + nodeId + '">CANCEL</a></span>' +
        '<input type="hidden" class="color-mix-blend" value="' + mix.blend + '" />' +
        '</div>';

      return html;
    };

    /**
     * Shows/hides color enabled components in the jstree
     */
    vm.showOnlyColorEnabled = function () {
      if (!vm.showColorEnabled) {
        vm.getJsTree().show_all();
      } else {
        for (var i = 0; i < vm.nonColorEnabledNodes.length; i++) {
          vm.getJsTree().hide_node(vm.nonColorEnabledNodes[i]);
        }
      }
    };

    /**
     * Updates the component color mixes by calling the corresponding API endpoint
     * @param nodeId
     * @param parentId
     */
    vm.updateComponentColorMix = function (nodeId, parentId, data, callback) {
      var componentNode = vm.getJsTree().get_node(parentId);
      var mixNode = vm.getJsTree().get_node(nodeId);
      API
        .ColorMix
        .update($scope.product_id, componentNode.original.dbId, mixNode.original.dbId, data)
        .then(function (resp) {
          callback && callback(resp);
        })
        .catch(function () {
          ALERT.errorDataSaving();
          callback && callback();
        });
    };

    /**
     * Updates the component color mixes by calling the corresponding API endpoint
     * to create a new color mix with default data
     * @param nodeId
     * @param parentId
     */
    vm.createComponentColorMix = function (nodeId, parentId, data, callback) {
      var componentNode = vm.getJsTree().get_node(parentId);
      API
        .ColorMix
        .create($scope.product_id, componentNode.original.dbId, data)
        .then(function (resp) {
          if (nodeId) {
            vm.setToReadOnlyMode(nodeId, true);
          }
          callback && callback(resp);
        })
        .catch(function () {
          ALERT.errorDataSaving();
          callback && callback();
        });
    };

    /**
     * Removes a color mix from a components mixes list by calling the API endpoint
     */
    vm.removeColorMix = function () {
      if (!vm.currentlyEditing) return;
      var currentNode = vm.getJsTree().get_node(vm.currentlyEditing);
      if (!currentNode.original.dbId) return;
      var componentNode = vm.getJsTree().get_node(currentNode.parent);
      vm.loading = true;
      API
        .ColorMix
        .deleteMix($scope.product_id, componentNode.original.dbId, currentNode.original.dbId)
        .then(function (resp) {
          vm.getJsTree().delete_node(currentNode.id);
          vm.statusIcon = "fa fa-check text-primary";
          vm.statusMsg = "Color mix was deleted succesfully";
          vm.showStatusMsg = true;
          vm.loading = false;
        })
        .catch(function () {
          ALERT.errorDataSaving();
          vm.loading = false;
        });
    };

    /**
     * Makes a copy of the current selection and persists it by calling the API
     * The new copy will be added to the same component where the original mix was
     */
    vm.duplicateColorMix = function () {
      if (!vm.currentlyEditing) return;
      vm.copyColorMix();
      vm.pasteColorMix();
    };

    /**
     * Copies on the client side the current selected color mix to be duplicated
     * when the paste button is clicked
     */
    vm.copyColorMix = function () {
      var jstree = vm.getJsTree();
      var current = jstree.get_node(jstree.get_selected()[0]);
      if (!current.original.isColorMix) return;
      var currentDOM = jstree.get_node(current, true);
      var label = currentDOM.find('.color-mix-name').text();
      vm.copiedMix = {};
      vm.copiedMix.opacity = vm.opacity;
      vm.copiedMix.label = label;
      vm.copiedMix.selectedColor = vm.selectedColor;
      vm.copiedMix.mixType = vm.mixType;
      vm.statusMsg = '"' + label + '" copied successfully';
      vm.statusIcon = "fa fa-check text-primary";
      vm.showStatusMsg = true;
    };

    /**
     * Pastes the current copied color mix into the selected component. If the current
     * selection is not a material it does nothing and shows error message.
     */
    vm.pasteColorMix = function () {
      var jstree = vm.getJsTree();
      var selected = jstree.get_node(jstree.get_selected()[0]);
      if (!selected) {
        vm.statusIcon = "fa fa-times text-error";
        vm.statusMsg = "Please select the component where you want to paste this color mix";
        vm.showStatusMsg = true;
        return;
      }
      if (selected && selected.original.isColorMix) {
        selected = jstree.get_node(selected.parent);
      }
      if (selected && !selected.original.isMaterial) {
        vm.statusIcon = "fa fa-times text-error";
        vm.statusMsg = "Please paste styles only into material components (components starting with '_')";
        vm.showStatusMsg = true;
        return;
      }

      var newNode = {id: ++vm.nodeId};
      newNode.li_attr = {
        style: 'margin: 10px 0px'
      };
      newNode.a_attr = {
        class: 'color-mix-cont'
      };
      var nodeData = {
        label: getColorMixLabel(selected, vm.copiedMix.label),
        value: vm.copiedMix.selectedColor,
        opacity: vm.copiedMix.opacity / 100,
        blend: vm.copiedMix.mixType.toLowerCase()
      };
      newNode.text = vm.generateColorMixNodeHtml(nodeData, newNode.id, selected.id);
      newNode.imageSrc = selected.original.imageSrc;
      newNode.isColorMix = true;
      newNode.label = nodeData.label;

      vm.loading = true;
      vm.createComponentColorMix(vm.currentlyEditing.id, selected.id, nodeData, function (mix) {
        newNode.dbId = mix._id;
        vm.copiedMix = null;
        vm.getJsTree().create_node(selected, newNode, 0, function () {
          vm.getJsTree().open_node(selected);
          vm.loading = false;
        });
      });
    };

    vm.createDefaultColorMix = function () {
      var jstree = vm.getJsTree();
      var selected = jstree.get_node(jstree.get_selected()[0]);
      if (!selected) {
        vm.statusIcon = "fa fa-times text-error";
        vm.statusMsg = "Please select the component where you want to create this color mix";
        vm.showStatusMsg = true;
        return;
      }
      if (selected && selected.original.isColorMix) {
        selected = jstree.get_node(selected.parent);
      }
      if (selected && !selected.original.isMaterial) {
        vm.statusIcon = "fa fa-times text-error";
        vm.statusMsg = "You can create styles only on material components (components starting with '_')";
        vm.showStatusMsg = true;
        return;
      }

      // check if there is default node added then save it as colo mix
      var childNodes = vm.getJsTree().get_children_dom(selected);
      var addDefault = true;
      if (childNodes && childNodes.length == 1) {
        var node_id = $(childNodes[0]).attr('id');
        var parent_id = $(childNodes[0]).find('.apply-changes').data('parent-node-id');
        if (node_id) {
          var node = vm.getJsTree().get_node(node_id);
          if (node && node.original && !node.original.dbId) {
            var mixNode = vm.getJsTree().get_node(node_id, true);
            var label = mixNode.find('.color-mix-name-input').val().trim();
            var color = mixNode.find('.color-mix-hex').text();
            var opacity = parseInt(mixNode.find('.color-mix-op').text()) / 100;
            var blend = mixNode.find('.color-mix-blend').val().toLowerCase();
            var enabled = mixNode.find('.color-mix-enabled').is(':checked');
            var data = {
              label: (selected.text == label) ? label : getColorMixLabel(selected, label),
              value: color,
              opacity: opacity,
              blend: blend,
              enable: enabled
            };
            addDefault = false;
            vm.loading = true;
            vm.createComponentColorMix(null, selected.id, data, function (mix) {
              node.original.dbId = mix._id;
              // update jstree node data in tree model
              data.text = vm.generateColorMixNodeHtml(data, node_id, parent_id)
              vm.updateNodeInTreeModel(node_id, data);
              vm.getJsTree().open_node(selected);
              // set label as material name by default
              addDefaultColorMix(selected);
            });
          }
        }
      }

      if (addDefault) {
        // set label as material name by default
        vm.loading = true;
        addDefaultColorMix(selected);
      }
    };

    /**
     * Add default color mix to selected material
     * @param selected
     */
    function addDefaultColorMix(selected) {
      var data = {
        label: getColorMixLabel(selected, selected.text.substr(1)),
        value: '#000000',
        opacity: 1,
        blend: 'overlay'
      };
      var newNode = getNewNode(selected, data);

      vm.loading = true;
      vm.createComponentColorMix(null, selected.id, data, function (mix) {
        vm.loading = false;
        newNode.dbId = mix._id;
        vm.getJsTree().create_node(selected, newNode, 0, function () {
          vm.getJsTree().open_node(selected);
          vm.loading = false;
        });
      });
    };

    /***
     * create new color mix node
     * @param selected
     * @param data
     * @returns {Color mix node object}
     */
    function getNewNode(selected, data) {
      var newNode = {id: ++vm.nodeId};
      newNode.li_attr = {
        style: 'margin: 10px 0px'
      };
      newNode.a_attr = {
        class: 'color-mix-cont'
      };

      newNode.text = vm.generateColorMixNodeHtml(data, newNode.id, selected.id);
      newNode.imageSrc = selected.original.imageSrc;
      newNode.isColorMix = true;
      newNode.label = data.label;
      return newNode;
    };

    /**
     * Checks if the give label already exists in color mix
     * @param parent
     * @param label
     * @returns {boolean}
     */
    function checkIfLabelExits(parent, currentLabel) {
      var childDom = vm.getJsTree().get_children_dom(parent);
      var labels = $(childDom).find(".color-mix-name");
      var isExits = false;
      _.forEach(labels, function (label) {
        if ($(label).text() == currentLabel) {
          isExits = true;
          return isExits;
        }
      });
      return isExits;
    };

    /**
     * Return available default label
     * @param parent
     * @param label
     * @returns {string}
     */
    function getColorMixLabel(parent, label) {
      var availableLabel = label;
      var sameLabelsCount = 1, isExists = false;

      while (checkIfLabelExits(parent, availableLabel)) {
        sameLabelsCount++;
        availableLabel = label + " (" + sameLabelsCount + ")";
      }

      return availableLabel;
    }

    /**
     * Given a jstree node id it will iterate over the DOM children and generate
     * an array of color mix objects ready to be sent to the backend through
     * the API PUT endpoint
     * @param nodeId
     * @returns {Array}
     */
    function generateMixesArray(nodeId) {
      var mixesDOM = vm.getJsTree().get_children_dom(nodeId);
      var mixArray = [];
      for (var i = 0; i < mixesDOM.length; i++) {
        var mix = vm.getJsTree().get_node(mixesDOM[i], true);
        if (mix.hasClass('default-mix')) continue;

        var label = mix.find('.color-mix-name-input').val();
        var color = mix.find('.color-mix-hex').text();
        var opacity = parseInt(mix.find('.color-mix-op').text()) / 100;
        var blend = mix.find('.color-mix-blend').val().toLowerCase();
        mix.find('.color-mix-name').html(label);

        mixArray.push({
          label: label,
          value: color,
          opacity: opacity,
          blend: blend
        });
      }
      return mixArray;
    }

    /**
     * Loads from the database the components for the product that is being edited
     */
    vm.loadComponents = function () {
      vm.loading = true;
      API.Products.get(productId)
        .then(function (product) {
          API
            .Components
            .query(productId)
            .then(function (comps) {
              vm.components = comps;

              if (comps.length > 0) {
                vm.selectedComponent = comps[0];
              }

              vm.loadComponentTreeModel(product.miid_id);
              vm.loading = false;
            }).catch(function (err) {
            ALERT.confirm(vm.loadComponents);
          });
        })
        .catch(function (err) {
          ALERT.confirm(vm.loadComponents);
        });

    };
    vm.loadComponents();

    /**
     * Handler for jstree node selected event
     * @param e
     * @param data
     */
    vm.nodeSelected = function (e, data) {
      // Display preview only if node is material or color mix
      $scope.$apply(function () {
        if (data.node.original.isMaterial || data.node.original.isColorMix) {
          var src = data.node.original.imageSrc;
          vm.enableCreate = true;
          vm.currentImgBaseSrc = src;
          vm.currentView = 0;
          vm.isPreview = true;
          if (data.node.original.isMaterial) {
            vm.selectedMaterial = data.node.original.text;
          } else {
            vm.selectedMaterial = data.node.original.label;
            vm.enableControls(data.node.original.enabled);
          }
          if (data.node.original.isColorMix) {
            vm.setToEditMode(data.node.id);
          }
          vm.updateCurrentImage(function () {
            if (data.node.original.isColorMix)
              vm.updateCanvasImage(data.node.original.enabled);
          });
        } else {
          vm.isPreview = false;
          vm.enableCreate = false;
        }
      });
    };

    /**
     * Sets a jstree node to edit mode. This means that the selected node properties can now
     * be edited (label, color, opacity, mix type)
     * @param nodeId
     */
    vm.setToEditMode = function (nodeId) {
      if (vm.currentlyEditing) {
        vm.setToReadOnlyMode(vm.currentlyEditing, false);
      }
      var domNode = vm.getJsTree().get_node(nodeId, true);
      vm.currentlyEditing = nodeId;
      vm.currentlyEditingDOM = domNode;
      vm.selectedColor = domNode.find('.color-mix-hex').html();
      vm.opacity = parseInt(domNode.find('.color-mix-op').html());
      var blend = domNode.find('.color-mix-blend').val();
      vm.mixType = blend.charAt(0).toUpperCase() + blend.slice(1);
      domNode.addClass('edit-mode');
      domNode.find('input.color-mix-name-input').focus();
    };

    /**
     * Sets a node back to read only mode. This method DOES NOT persist the changes in the
     * node being set to read only mode.
     * @param nodeId
     */
    vm.setToReadOnlyMode = function (nodeId, updateControls) {
      var domNode = vm.getJsTree().get_node(nodeId, true);
      if (domNode) {
        domNode.removeClass('edit-mode');
        vm.currentlyEditingDOM = null;
        vm.currentlyEditing = null;
        if (updateControls) {
          vm.enableControls(false);
        }
      }
    };

    /**
     * Sets a node as error in label
     * @param nodeId
     */
    vm.setToLabelError = function (nodeId) {
      var domNode = vm.getJsTree().get_node(nodeId, true);
      if (domNode) {
        $(domNode).find('.color-mix-label').addClass('error');
      }
    };

    /**
     * Remove error  in label
     * @param nodeId
     */
    vm.removeLabelError = function (nodeId) {
      var domNode = vm.getJsTree().get_node(nodeId, true);
      if (domNode) {
        $(domNode).find('.color-mix-label').removeClass('error');
      }
    };

    /**
     * Colorpicker color changed event handler
     * @param color
     */
    vm.colorChanged = function (color) {
      vm.updateColorMixNode(color);
      var colorEnabled = (vm.currentlyEditingDOM) ? vm.currentlyEditingDOM.find('.color-mix-enabled').is(':checked') : false;
      vm.updateCanvasImage(colorEnabled);
    };

    /**
     * Updates the canvas image by applying the current filtering options
     */
    vm.updateCanvasImage = function (apply) {
      var img = vm.canvas.item(0);
      var color = vm.selectedColor;
      var type = vm.mixType.toLowerCase();

      if (img) {
        if (apply) {
          var filter;

          if (type === "overlay" || type === "multiply" || type === "multiplyx2") {
            filter = new fabric.Image.filters.Blend({
              color: color,
              mode: type
            });
          } else if (type === "fill") {
            filter = new fabric.Image.filters.Tint({
              color: color
            });
          } else if (type === "none") {
            filter = "none";
          }

          img.filters[0] = filter;
          //set empty
          if (filter === "none") {
            img.filters = [];
          }
        } else {
          img.filters = [];
        }

        img.applyFilters(vm.canvas.renderAll.bind(vm.canvas));
      }
    };

    /**
     * Updates the currently being edited jstree node with the values selected in the
     * color picker
     * @param color
     */
    vm.updateColorMixNode = function (color) {
      if (vm.currentlyEditingDOM) {
        vm.currentlyEditingDOM.find('.color-mix-hex').html(color);
        vm.currentlyEditingDOM.find('.color-mix-prev').css({'background-color': color});
      }
    };

    /**
     * Updates the currently being edited jstree node with the opacity value selected in the
     * opacity input
     * @param color
     */
    vm.updateColorMixNodeOp = function () {
      if (vm.currentlyEditingDOM) {
        vm.currentlyEditingDOM.find('.color-mix-op').html(vm.opacity);
        vm.canvas.item(0).opacity = vm.opacity / 100;
        vm.canvas.renderAll();
      }
    };

    /**
     * Updates the currently being edited jstree node with the mix type value selected in the
     * mix type combo box
     * @param color
     */
    vm.updateColorMixNodeBlend = function () {
      if (vm.currentlyEditingDOM) {
        vm.currentlyEditingDOM.find('.color-mix-blend').val(vm.mixType.toLowerCase());
        var colorEnabled = vm.currentlyEditingDOM.find('.color-mix-enabled').is(':checked');
        vm.updateCanvasImage(colorEnabled);
      }
    };

    /**
     * Updates the current image being shown in the preview canvas. It will generate the image SRC automatically
     * by using the current view (angle) of the selected component
     * @param color
     */
    vm.updateCurrentImage = function (callback) {
      var url = MIID_ASSETS_API.base_url + MIID_ASSETS_API.endpoints.getImage;
      url += '?filePath=' + encodeURIComponent(vm.currentImgBaseSrc + '/' + vm.currentView + '.png') + '&rnd=' + Math.random();
      fabric.Image.fromURL(url, function (oImg) {
        oImg.set({
          top: 0,
          left: 0,
          scaleY: vm.canvas.height / oImg.height,
          scaleX: vm.canvas.width / oImg.width,
          lockMovementX: true,
          lockMovementY: true,
          lockScalingX: true,
          lockScalingY: true,
          lockUniScaling: true,
          lockRotation: true,
          selectable: false,
          hasRotatingPoint: false
        });
        vm.canvas.clear().renderAll();
        oImg.selectable = false;
        vm.canvas.add(oImg);
        callback && callback();
      }, {crossOrigin: 'Anonymous'});
    };

    /**
     * Move to the next angle in the current component
     */
    vm.next = function () {
      if (vm.currentView == 7) {
        vm.currentView = 0;
      } else {
        vm.currentView++;
      }
      vm.updateCurrentImage();
    };

    /**
     * Move to the previous angle in the current component
     */
    vm.previous = function () {
      if (vm.currentView == 0) {
        vm.currentView = 7;
      } else {
        vm.currentView--;
      }
      vm.updateCurrentImage();

    };

    /**
     * Show an alert box with a message using Sweet Alert lib
     * @param title
     * @param text
     * @param type
     * @param confirmBtnText
     * @param showCancel
     * @param callBack
     */


    vm.updateNodeInTreeModel = function (nodeId, newData) {
      var idx = _.findIndex(vm.treeModel, function (nodeModel) {
        return nodeModel.id == nodeId;
      });
      _.extend(vm.treeModel[idx], newData);

      var jstreeNode = vm.getJsTree().get_node(nodeId);
      jstreeNode.original.enabled = newData.enabled;
      jstreeNode.original.label = newData.label;
      jstreeNode.original.blend = newData.blend;
      jstreeNode.text = newData.text;
    }

  }

})(angular.module('adminPanel.product.create.visual_assets.va_tuning.controller', []));
