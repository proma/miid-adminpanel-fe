(function(app){

  app.config(function($stateProvider){

    $stateProvider
      .state('admin-panel.product.create.va.va_tuning',{
        url: '/tuning',
        templateUrl: 'app/modules/product/create/visual_assets/va_tuning/va_tuning.html',
        controller: 'ProductVisualAssetsTuningController',
        controllerAs: 'vm'
      });

  });

})(angular.module('adminPanel.product.create.visual_assets.va_tuning.routes',[]));
