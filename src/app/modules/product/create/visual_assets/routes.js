(function(app){
  app.config(['$stateProvider', 'URLProvider', function($stateProvider, URLProvider){

    $stateProvider
      .state('admin-panel.product.create.va',{
        url: '/visual-assets',
        abstract: true,
        templateUrl: 'app/modules/product/create/visual_assets/visual_assets.html',
        controller: ['$stateParams','API', function($stateParams, API){
          var va = this;
          va.productID = $stateParams.productID;
          va.productName = '';

          API
            .Products
            .get(va.productID)
            .then(function (product) {
              va.productName = product.en.name;
            });

        }],
        controllerAs: 'va',
        data: {
          permissions: {
            only: 'canSee.Products.VisualAssets',
            redirectTo: URLProvider.$get().RedirectURL
          }
        }
      });

  }]);

})(angular.module('adminPanel.product.create.visual_assets.routes',[]));
