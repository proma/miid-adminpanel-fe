(function(app){

  app.config(function($stateProvider){

    $stateProvider
      .state('admin-panel.product.create.va.va_upload',{
        url: '/upload',
        templateUrl: 'app/modules/product/create/visual_assets/va_upload/va_upload.html',
        controller: 'ProductVisualAssetsUploadController',
        controllerAs: 'vm'
      });

  });

})(angular.module('adminPanel.product.create.visual_assets.va_upload.routes',[]));
