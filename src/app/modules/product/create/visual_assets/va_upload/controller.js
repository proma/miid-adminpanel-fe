(function (app) {

  app.controller('ProductVisualAssetsUploadController', ['$scope', 'MIID_ASSETS_API', 'API', '$stateParams', 'Notify', '$timeout', ProductVisualAssetsController]);

  function ProductVisualAssetsController($scope, MIID_ASSETS_API, API, $stateParams, Notify, $timeout) {
    var vm = this;
    var idCounter = 0;
    vm.fileSize = 0; //file size for triggering the auto resize of components
    vm.treeStructure = []; //for caching the tree structure
    vm.product = {};
    vm.searchString = '';
    vm.uploader = {
      progress: 0,
      processing: false,
      showUpload: true,
      state: {
        step1: false,
        step2: false,
        step3: false,
        removing: false
      }
    };
    vm.loading = true;
    // JSTree configuration object
    $scope.treeConfig = {
      core: {
        multiple: false,
        check_callback: true,
        themes: {
          dots: false
        }
      },
      plugins: [
        'sort',
        'search'
      ],
      version : 1 // version is for recreating the tree when needed
    };
    vm.previewButtons = {
      disable: {
        prev: false,
        next: false
      },
      downloadLink: '',
      downloadLinkDisabled: true,
      previewLink: '/img/icons/img-placeholder.png'
    };
    $scope.treeModel = [];

    // If there's a product ID then load the product components in the tree
    var productId = $stateParams.productID;

    API.Products.get(productId)
      .then(function(product) {
        vm.product = product;
        vm.createDirectoryTree(vm.product.miid_id);
      })
      .catch(function() {
        vm.loading = false;
        $scope.treeModel = [];
      });

    vm.createDirectoryTree = function (miid) {
      vm.loading = true;
      API
        .Customizer
        .getDirectory(miid)
        .then(function(response) {
          if (response.status == 'error')
            return;
          vm.uploader.showUpload = false;
          vm.treeStructure = response; //cache the response
          if (response.length > 0) {
            vm.createTree(response);
          }
        })
        .catch(function() {
          $scope.treeModel = [];
        })
        .finally(function () {
          vm.loading = false;
        });
    };

    vm.createTree = function (tree) {
      $scope.treeModel = [];
      var directoryIds = {}, productComponents = [];

      // push the root with miid_id
      $scope.treeModel.push({
        id: 'root',
        parent: '#',
        text: vm.product.miid_id,
        icon: 'fa fa-folder',
        isDirectory: true
      });
      tree.forEach(function(file) {
        if (file && file.path) {
          if (file.type == "directory") {
            file.isDirectory = true;
            file.icon = 'fa fa-folder';
            productComponents.push({
              title: file.name,
              have_color: file.name.indexOf('#') == 0,
              path: file.path.substr(file.path.indexOf('/') + 1)
            });
          } else {
            file.isDirectory = false;
            file.icon = 'fa fa-file';
          }

          file.id = 'ID' + idCounter++;
          if (file.isDirectory) {
            directoryIds[file.path] = file.id;
          }

          if (file.parent == '#') {
            file.parent = 'root';
          } else {
            var parent = file.path.substr(0, file.path.lastIndexOf("/"));
            file.parent = directoryIds[parent];
          }

          file.text = file.path.substr(file.path.lastIndexOf("/") + 1, file.path.length);
          $scope.treeModel.push(file);
        }

      });
      vm.recreateTree();
      return productComponents;
    };

    vm.getComponents = function (structure) {
      var components = [];
      structure.forEach(function (file) {
        if (file && file.path) {
          if (file.type == "directory") {
            components.push({
              title: file.name,
              have_color: file.name.indexOf('#') == 0,
              path: file.path.substr(file.path.indexOf('/') + 1)
            });
          }
        }
      });
      return components;
    };

    vm.getJsTree = function() {
      return vm.treeInstance.jstree(true);
    };

    vm.recreateTree = function() {
      $scope.treeConfig.version++;
    };

    vm.imageLoader = function () {
      vm.loadingImage = false;
    };

    vm.showProgress = function (key) {
      angular.forEach(vm.uploader.state, function (val, keyT) {
        vm.uploader.state[keyT] = false;
      });
      vm.uploader.state[key] = true;
      $timeout(function () {
        vm.uploader.processing = true;
      });
    };

    var visualizeAsset = function (path) {
      vm.loadingImage = true;
      var url = MIID_ASSETS_API.base_url + MIID_ASSETS_API.endpoints.getImage;
      url += '?filePath=' + encodeURIComponent(path) + '&rnd=' + Math.random();
      var downloadURL = MIID_ASSETS_API.base_url + MIID_ASSETS_API.endpoints.downloadImage;
      downloadURL += '?filePath=' + encodeURIComponent(path) + '&rnd=' + Math.random();
      vm.previewButtons.downloadLink = downloadURL;
      $timeout(function () {
        vm.previewButtons.previewLink = url;
      }, 100);
    };

    var enableDisableNavigateBtns = function() {
      vm.previewButtons.disable.next = false;
      vm.previewButtons.disable.prev = false;

      var current = vm.getJsTree().get_selected()[0];
      var next = vm.getJsTree().get_next_dom(current);
      var prev = vm.getJsTree().get_prev_dom(current);

      if (!next || next.length == 0) {
        vm.previewButtons.disable.next = true;
      }
      if (!prev || prev.length == 0) {
        vm.previewButtons.disable.prev = true;
      }
    };

    $scope.nodeSelected = function(e, data) {
      enableDisableNavigateBtns();
      if (data.node.original.isDirectory) {
        vm.previewButtons.downloadLinkDisabled = true;
        return;
      }
      var path = vm.getJsTree().get_path(data.node, '/');
      vm.previewButtons.downloadLinkDisabled = false;
      visualizeAsset(path);
    };

    $scope.nodeOpened = function() {
      enableDisableNavigateBtns();
    };

    $scope.nodeClosed = function() {
      enableDisableNavigateBtns();
    };

    vm.visualizeNext = function() {
      if (vm.hasNoNext) return;
      var current = vm.getJsTree().get_selected()[0];
      var next = vm.getJsTree().get_next_dom(current);
      if (next.length > 0) {
        vm.getJsTree().deselect_all();
        vm.getJsTree().select_node(next);
      }
    };

    vm.visualizePrevious = function() {
      if (vm.hasNoPrevious) return;
      var current = vm.getJsTree().get_selected()[0];
      var prev = vm.getJsTree().get_prev_dom(current);
      if (prev.length > 0) {
        vm.getJsTree().deselect_all();
        vm.getJsTree().select_node(prev);
      }
    };

    /**
     * Remove node from UI and send request to remove it on the server.
     * It will check if it's a file or directory and call the corresponding endpoint.
     */
    vm.removeNode = function () {
      var selected_nodes = vm.getJsTree().get_selected();
      var nodeData = vm.getJsTree().get_node(selected_nodes[0]);
      var path = vm.getJsTree().get_path(selected_nodes[0], '/');
      var factory;

      var removeAll = false;
      if (nodeData.original.isDirectory && nodeData.parent !== '#') {
        return;
      }
      else if (nodeData.original.isDirectory && nodeData.parent === '#') {
        factory = API.Customizer.removeDirectory;
        removeAll = true;
      } else {
        factory = API.Customizer.removeFile;
      }

      vm.showProgress('removing');

      factory(path)
        .then(function (data) {
          if (data.status == 'error') {
            Notify.errorSaving();
            return;
          }
          if (removeAll) {
            // Remove components from DB via MIID API
            vm.treeStructure = [];
            API
              .Components
              .deleteAll(productId)
              .then(function() {
                vm.uploader.showUpload = true;
                Notify.infoFilesDeleted();
                $scope.treeModel = [];
                vm.getJsTree().delete_node(selected_nodes);
              }).catch(function() {
                Notify.errorSaving();
              });
          } else {
            Notify.infoFilesDeleted();
            vm.getJsTree().delete_node(selected_nodes);
          }
        })
        .catch(function () {
          Notify.errorSaving();
        })
        .finally(function () {
          vm.uploader.processing = false;
        });
    };

    vm.uploadZipSubFolders = function (file) {
      $scope.uploadZip(file, vm.getSelectedDirectory());
    };

    $scope.uploadZip = function (file, obj) {
      if (!file) return; //Bug in uploader, idk
      if (!obj) obj = {};
      if(file && file.name) {
        var name = file.name;
        var extension = name.slice(name.lastIndexOf('.'), name.length);
        // check if for extension that must be .zip
        if (extension != '.zip') {
          Notify.onlyZipAllowed();
          return;
        }
      } else { // in case file name is not exists
        Notify.onlyZipAllowed();
        return;
      }
      if (file.size) {
        vm.fileSize = file.size/ (1024*1024); //file size is in bytes we want in MBs
      }
      vm.showProgress('step1');

      API
        .Customizer
        .upload(file, function (progress) {
          vm.uploader.progress = progress;
        }, obj.directory || null, obj.directory? 'folders': null)
        .then(function (files) {
          if (!obj.directory) {
            vm.treeStructure = files;
            vm.createComponents(vm.createTree(files));
          } else {
            vm.createComponents(vm.getComponents(files), obj.directory);
          }
          Notify.infoFilesUploaded();
        })
        .catch(function () {})
        .finally(function () {
          vm.uploader.processing = false;
          vm.uploader.progress = 0;
        });
    };


    vm.createComponents = function (productComponents, parent) {
      // create product component entries in the DB via API
      vm.showProgress('step2');
      vm.loading = true;
      var factory;
      if (!productComponents) {
        productComponents = [];
        vm.treeStructure.forEach(function(file) {
          if (file && file.path) { //just to IN CASE!
            if (file.type == "directory") {
              productComponents.push({
                title: file.name,
                have_color: file.name.indexOf('#') == 0,
                path: file.path.substr(file.path.indexOf('/') + 1)
              });
            }
          }
        });
      }

      if (!productComponents.length) { //If no components, then move to resize
        vm.resizeComponents(parent);
        return;
      }
      if (parent)
        factory = API.Components.createSubComponents;
      else
        factory = API.Components.create;


        factory(productId, productComponents)
        .then(function(data) {
          if (data.statusCode) {
            Notify.error(data.message);
          } else {
            Notify.infoSaved();
            vm.resizeComponents(parent);
          }
        }).catch(function() {
          vm.uploader.showUpload = true;
          $scope.treeModel = [];
          vm.recreateTree();
          vm.createDirectoryTree(vm.product.miid_id);
          Notify.errorSaving();
        })
        .finally(function () {
          vm.loading = false;
          vm.uploader.processing = false;
        });
    };

    vm.resizeComponents = function (parent) {
      vm.loading = true;
      vm.showProgress('step3');

      var directory = '';
      if (parent) {
        directory = parent;
      } else {
        directory = vm.getSelectedDirectory().directory;
      }

      if (!directory) {
        Notify.errorDirectorySelection();
        vm.loading = false;
        vm.uploader.processing = false;
        return;
      }

      API
        .Customizer
        .resize(directory)
        .then(function(data) {
          if (data.status == 'error') {
            Notify.error(data.msg);
          } else {
            Notify.infoResized();
            vm.treeStructure = data;
            vm.createTree(data); //data is new so recreating
          }
        })
        .catch(function(err) {
          vm.recreateTree();
          vm.createDirectoryTree(vm.product.miid_id);
          Notify.errorResizing();
        })
        .finally(function () {
          vm.uploader.processing = false;
          vm.loading = false;
        });

    };

    $scope.uploadFiles = function (file) {
      if (!file) return;
      var obj = vm.getSelectedDirectory();

      vm.uploader.processing = true;

      API
        .Customizer
        .upload(file, function (progress) {
          vm.uploader.progress = progress;
        }, obj.directory,'files')
        .then(function () {
          var dirChildren = vm.getJsTree().get_children_dom(obj.directoryID);
          // get nodes data from DOM children
          var childrenNodes = [];
          for (var i = 0; i < dirChildren.length; i++) {
            childrenNodes.push(vm.getJsTree().get_node(dirChildren[i]));
          }
            // check if file was replaced or if it's a new one
          var exists = _.find(childrenNodes, function(node) {
            return node.text == file.name;
          });
          // if it's a new one then add it to the jstree.
          if (!exists) {
            vm.getJsTree().create_node(obj.directoryID, {
              id: 'ID' + idCounter++,
              parent: obj.directoryID,
              text: file.name,
              icon: 'fa fa-file'
            });
          }
        })
        .catch(function () {

        })
        .finally(function () {
          vm.uploader.processing = false;
          vm.uploader.progress = 0;
        });
    };

    vm.getSelectedDirectory = function () {
      var selected_nodes = vm.getJsTree().get_selected();
      var directory;
      var directoryId;
      if (selected_nodes.length > 0) {
        var nodeData = vm.getJsTree().get_node(selected_nodes[0]);
        if (nodeData.original.isDirectory) {
          directory = vm.getJsTree().get_path(selected_nodes[0], '/');
          directoryId = selected_nodes[0];
        } else {
          var parentDir = vm.getJsTree().get_parent(selected_nodes[0]);
          directoryId = parentDir;
          directory = vm.getJsTree().get_path(parentDir, '/');
        }
      }
      return {directory: directory, directoryID: directoryId}
    };

    vm.searchTree = function () {
      vm.getJsTree().search(vm.searchString);
    }

  }
})(angular.module('adminPanel.product.create.visual_assets.va_upload.controller', []));
