(function(app){

  app.config(function($stateProvider){

    $stateProvider
      .state('admin-panel.product.create.va.va_layering',{
        url: '/layering',
        templateUrl: 'app/modules/product/create/visual_assets/va_layering/va_layering.html',
        controller: 'ProductVisualAssetsLayeringController',
        controllerAs: 'vm'
      });

  });

})(angular.module('adminPanel.product.create.visual_assets.va_layering.routes',[]));
