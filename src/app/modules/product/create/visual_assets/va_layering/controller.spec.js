'use strict';

describe('Controllers', function () {
  describe('ProductVisualAssetsLayeringController', function(){

    var data = {
      data: [
        {
          view: '0',
            id: 'LAYER1'
        },
        {
          view: '1',
            id: 'LAYER2'
        }
      ],
      productId: 'PRODUCT1',
      components: [
        {
          _id : "id",
          title: "title"
        }
      ]
    };

    var
      ProductVisualAssetsLayeringController,
      $stateParams,
      URL,
      ALERT,
      $rootScope,
      layersURL,
      $httpBackend;



    // load the dependencies module
    beforeEach(module('ui.router'));
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.services.alert'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.api'));
    beforeEach(module('adminPanel.product.create.visual_assets.va_layering.controller'));

    beforeEach(inject(function($injector){
      $rootScope = $injector.get('$rootScope');
      $stateParams = $injector.get('$stateParams');
      $stateParams.productID = data.productId;
      $httpBackend = $injector.get('$httpBackend');
      URL = $injector.get('URL');
      ALERT = $injector.get('ALERT');
      var $controller = $injector.get('$controller');
      ProductVisualAssetsLayeringController = $controller('ProductVisualAssetsLayeringController',{
        '$scope': $rootScope,
        'API': $injector.get('API'),
        'ALERT': ALERT,
        '$stateParams' : $stateParams
      });
      layersURL = URL.LAYERS.replace(':productId', data.productId);
      $httpBackend.expect('GET',URL.COMPONENTS.replace(':productId', data.productId)).respond(200, data.components);
    }));

    describe('MA-96: Products assets layering', function(){

      it('should make products layers empty first',function(){
        expect(ProductVisualAssetsLayeringController.layers.length).toBe(0);
      });

      it('should make current view 0',function(){
        expect(ProductVisualAssetsLayeringController.currentView).toBe(0);
      });

      it('should be loading first', function() {
        expect(ProductVisualAssetsLayeringController.loading).toBeTruthy();
      });

      it('should stop loading after load', function() {
        $httpBackend.when('GET',layersURL ).respond(200, data.data);

        ProductVisualAssetsLayeringController.loadLayers();
        $httpBackend.flush();

        expect(ProductVisualAssetsLayeringController.loading).toBeFalsy();
      });

      it('should call loadLayers() first', function() {
        spyOn(ProductVisualAssetsLayeringController,'loadLayers');
        $httpBackend.when('GET',layersURL).respond(200, data.data);
        ProductVisualAssetsLayeringController.loadLayers();
        $httpBackend.flush();
        expect(ProductVisualAssetsLayeringController.loadLayers).toHaveBeenCalled();
      });

      it('should set selected layer as first of response', function() {
        $httpBackend.expect('GET',layersURL).respond(200, data.data);
        $httpBackend.flush();
        expect(ProductVisualAssetsLayeringController.selectedLayer.view).toEqual(data.data[0].view);
        expect(ProductVisualAssetsLayeringController.selectedLayer.id).toEqual(data.data[0].id);
      });

      it('should call alert when error',function(){
        spyOn(ALERT,'confirm');
        $httpBackend.expect('GET',layersURL).respond(500, data.data);
        $httpBackend.flush();
        ProductVisualAssetsLayeringController.loadLayers();
        expect(ALERT.confirm).toHaveBeenCalled();
      });

      it('should populate layers', function() {
        $httpBackend.expect('GET',layersURL).respond(200, data.data);
        $httpBackend.flush();
        expect(ProductVisualAssetsLayeringController.layers.length).toBeGreaterThan(0);
      });

    });

  });

});
