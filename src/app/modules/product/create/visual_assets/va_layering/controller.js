(function(app){
  'use-strict';

  app.controller('ProductVisualAssetsLayeringController', ProductVisualAssetsLayeringController);

  ProductVisualAssetsLayeringController.$inject = [ '$scope', 'API', 'ALERT', '$stateParams']; //min-safe code

  function ProductVisualAssetsLayeringController($scope, API, ALERT, $stateParams) {
    var vm = this;

    var productId = $stateParams.productID;
    $scope.product_id = productId;

    vm.loading = true;
    vm.isPreview = false; // preview functionality is not available yet
    vm.currentView = 0;
    vm.layers =[];
    vm.defaultLayers = [];
    vm.selectedLayer = {};

    // load components for default layering order
    vm.loadComponents = function(){
      API
        .Components
        .query(productId)
        .then(function(components) {
          vm.defaultLayers = [];
          _.each(components, function(component){
            //check if component is not any property
            if(!/^[!_#].*$/.test(component.title)) {
              vm.defaultLayers.push({
                comp_id : component._id,
                title: component.title,
                visible: true //by default visibility true
              });
            }
          });
        }).catch(function(err) {
        ALERT.confirm(vm.loadComponents);
      });
    };

    // get all layers for selected product
    vm.loadLayers = function(){
      vm.loading = true;
      API
        .Layers
        .query(productId)
        .then(function(layers) {
          vm.layers = layers;

          if(layers.length > 0) {
            vm.selectedLayer = layers[0];
          }

          vm.loading = false;
        }).catch(function(err) {
        ALERT.confirm(vm.loadLayers);
      });
    };

    vm.next = function() {
      if(vm.hasNext()) {
        vm.currentView++;
        vm.selectedLayer = vm.layers[vm.currentView];
      }
    };

    vm.previous = function() {
      if(vm.hasPrevious()) {
        vm.currentView--;
        vm.selectedLayer = vm.layers[vm.currentView];
      }
    };

    vm.hasNext = function(){
      return vm.currentView < vm.layers.length-1;
    };

    vm.hasPrevious = function(){
      return vm.currentView > 0;
    };

    vm.apply = function(setDefault) {
      setDefault = (_.isUndefined(setDefault) || _.isNull(setDefault)) ? false : setDefault;
      var layer = {};
      layer.view = vm.selectedLayer.view;
      layer.components = [];

      if (setDefault === true) { // update with default order
        vm.selectedLayer.components = [];
        _.each(vm.defaultLayers, function (comp) {
          layer.components.push({
            comp_id: comp.comp_id,
            visible: comp.visible
          });
          // set default orders of layers in selected view
          vm.selectedLayer.components.push({
            comp_id: comp.comp_id,
            visible: comp.visible,
            title: comp.title
          });
        });
      } else { // update with user selected order
        _.each(vm.selectedLayer.components, function (comp) {
          layer.components.push({
            comp_id: comp.comp_id,
            visible: comp.visible
          });
        });
      }

      vm.loading = true;
      API
        .Layers
        .edit(productId, vm.selectedLayer._id, layer)
        .then(function(layer) {
          vm.loading = false;
        }).catch(function(err) {
        vm.loading = false;
        ALERT.errorDataSaving(function(){
          return vm.apply(setDefault);
        });
      });
    };

    vm.applyAll = function(){
      var layers = [];
      var components = [] ;
      _.each(vm.selectedLayer.components,function(comp){
        components.push({
          comp_id: comp.comp_id,
          visible: comp.visible
        });
      });

      vm.layers = _.each(vm.layers, function(layer){
        layers.push({
          _id: layer._id,
          view: layer.view,
          components: components
        });
        layer.components = _.map(vm.selectedLayer.components, _.clone);
        return layer;
      });

      vm.loading = true;
      API
        .Layers
        .editAll(productId, layers)
        .then(function(layer) {
          vm.loading = false;
        }).catch(function(err) {
        vm.loading = false;
        ALERT.errorDataSaving(vm.applyAll);
      });
    };

    vm.setDefaultOrder = function() {
      vm.apply(true);
    }


    // load components
    vm.loadComponents();
    // load layers
    vm.loadLayers();

  }

  app.filter('capitalize', function() {
    return function(input, all) {
      var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
      return (!!input) ? input.replace(reg, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    }
  });

})(angular.module('adminPanel.product.create.visual_assets.va_layering.controller',[]));
