(function(app){

  app.config(function($stateProvider){

    $stateProvider
      .state('admin-panel.product.create.product_media',{
        url: '/product-media',
        abstract: true,
        templateUrl: 'app/modules/product/create/product_media/wizard_product_media.html',
        controller: 'ProductMediaController',
        controllerAs: 'vm'
      })
      .state('admin-panel.product.create.product_media.thumbnail',{
        url: '/product-media/thumbnail',
        templateUrl: 'app/modules/product/create/product_media/thumbnail/wizard_product_media_thumbnail.html'
      })
      .state('admin-panel.product.create.product_media.desc_lg',{
        url: '/product-media/desc-lg',
        templateUrl: 'app/modules/product/create/product_media/description-lg/wizard_product_media_desc_lg.html'
      })
      .state('admin-panel.product.create.product_media.desc_mob',{
        url: '/product-media/desc-mob',
        templateUrl: 'app/modules/product/create/product_media/description-mob/wizard_product_media_desc_mob.html'
      })
      .state('admin-panel.product.create.product_media.banner_lg',{
        url: '/product-media/banner-lg',
        templateUrl: 'app/modules/product/create/product_media/banner-lg/wizard_product_media_banner_lg.html'
      })
      .state('admin-panel.product.create.product_media.banner_mob',{
        url: '/product-media/banner-mob',
        templateUrl: 'app/modules/product/create/product_media/banner-mob/wizard_product_media_banner_mob.html'
      })
      .state('admin-panel.product.create.product_media.size_ref',{
        url: '/product-media/size-ref',
        templateUrl: 'app/modules/product/create/product_media/size-ref/wizard_product_media_size_ref.html'
      })
    ;

  });


})(angular.module('adminPanel.product.create.product_media.routes', []));
