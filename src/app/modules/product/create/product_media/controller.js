(function (app) {

  app.controller('ProductMediaController', ['$scope', 'MIID_API', 'QINGSTOR', '$state', 'ALERT', 'Notify', 'Upload', 'API',
    'APIService', '$stateParams', ProductMediaController]);


  function ProductMediaController($scope, MIID_API, QINGSTOR, $state, ALERT, Notify, Upload, API, APIService, $stateParams) {
    var vm = this;
    vm.loadingProduct = true;
    vm.loadingReorder = false;
    vm.loadingDelete = false;
    vm.loadingImageSrc = '';
    vm.currentType = 'pr_thumb';

    $scope.file = null;

    vm.updateType = function (type) {
      vm.currentType = type;
    };

    var productId = $stateParams.productID;
    vm.productID = $stateParams.productID;

    if ($stateParams.productID) {
      API.Products.setProductId(productId); //set product id
    }
    vm.product = null;
    var qingStorBase = QINGSTOR.base_url;
    if (productId) {
      API.Products.get(productId).then(function (p) {
        vm.uploading = false;
        vm.product = p;
        vm.description_large = [];
        vm.description_mobile = [];

        // get product thumbnail
        if (vm.product['images']['thumb']) {
          vm.thumbnailSrc = qingStorBase + vm.product.images.thumb;
        }

        // get product banner desktop
        if (vm.product['images']['web_banner']) {
          vm.bannerLargeSrc = qingStorBase + vm.product.images.web_banner;
        }

        // get product banner mobile
        if (vm.product['images']['mob_banner']) {
          vm.bannerMobileSrc = qingStorBase + vm.product.images.mob_banner;
        }

        // get product size reference
        if (vm.product['size']['img']) {
          vm.sizeRefSrc = qingStorBase + vm.product.size.img;
        }

        // get product promotion banner web
        if (vm.product['images']['promo_web_banner']) {
          vm.bannerPromotionLargeSrc = qingStorBase + vm.product['images']['promo_web_banner'];
        }

        // get product promotion banner mobile
        if (vm.product['images']['promo_mob_banner']) {
          vm.bannerPromotionMobileSrc = qingStorBase + vm.product['images']['promo_mob_banner'];
        }

        // get product descriptions for mobile and desktop
        if (vm.product.media.desktop.length > 0) {
          for (var i = 0; i < vm.product.media.desktop.length; i++) {
            vm.description_large.push(qingStorBase + vm.product.media.desktop[i]);
          }
        }
        if (vm.product.media.mobile.length > 0) {
          for (var i = 0; i < vm.product.media.mobile.length; i++) {
            vm.description_mobile.push(qingStorBase + vm.product.media.mobile[i]);
          }
        }

        vm.loadingProduct = false;
      });
    }

    /**
     * Move up an image from the product description
     * Will swap values in the array and persist the new order in the database
     * @param type
     * @param index
     */
    vm.moveUp = function (type, index) {
      if (index == 0) return;
      vm.loadingReorder = true;
      var newOrder = type == 'mobile' ? vm.description_mobile.slice(0) : vm.description_large.slice(0);

      // reorder in copied array
      var tmp = newOrder[index];
      newOrder[index] = newOrder[index - 1];
      newOrder[index - 1] = tmp;
      for (var i = 0; i < newOrder.length; i++) {
        newOrder[i] = newOrder[i].replace(qingStorBase, '');
      }

      APIService.sendRequest({
        method: 'PUT',
        path: '/products/' + productId + '/media/' + type + '/reorder',
        data: {
          media: newOrder
        }
      }).then(function () {
        if (type == 'mobile') {
          for (var i = 0; i < newOrder.length; i++) {
            vm.description_mobile[i] = qingStorBase + newOrder[i];
          }
        } else {
          for (var i = 0; i < newOrder.length; i++) {
            vm.description_large[i] = qingStorBase + newOrder[i];
          }
        }
        vm.loadingReorder = false;
      }).catch(function () {
        vm.loadingReorder = false;
        Notify.errorSavingOrder();
      });
    };

    /**
     * Move down an image from the product description
     * Will swap values in the array and persist the new order in the database
     * @param type
     * @param index
     */
    vm.moveDown = function (type, index) {
      var length = type == 'mobile' ? vm.description_mobile.length : vm.description_large.length;
      if (index == length - 1) return;
      vm.loadingReorder = true;
      var newOrder = type == 'mobile' ? vm.description_mobile.slice(0) : vm.description_large.slice(0);

      // reorder in copied array
      var tmp = newOrder[index];
      newOrder[index] = newOrder[index + 1];
      newOrder[index + 1] = tmp;
      for (var i = 0; i < newOrder.length; i++) {
        newOrder[i] = newOrder[i].replace(qingStorBase, '');
      }

      APIService.sendRequest({
        method: 'PUT',
        path: '/products/' + productId + '/media/' + type + '/reorder',
        data: {
          media: newOrder
        }
      }).then(function () {
        if (type == 'mobile') {
          for (var i = 0; i < newOrder.length; i++) {
            vm.description_mobile[i] = qingStorBase + newOrder[i];
          }
        } else {
          for (var i = 0; i < newOrder.length; i++) {
            vm.description_large[i] = qingStorBase + newOrder[i];
          }
        }
        vm.loadingReorder = false;
      }).catch(function () {
        vm.loadingReorder = false;
        Notify.errorSavingOrder();
      });
    };

    /**
     * Remove an image from the product media. Asks for confirmation first.
     * @param type
     * @param path
     */
    vm.removeImage = function (type, path) {
      var msg = getRemoveMessage(type);
      ALERT.warningRemove(function () {
        vm.loadingDelete = true;
        vm.loadingImageSrc = path;
        var url = '/media/delete/' + type + '/' + productId;
        path = path.replace(qingStorBase, '');
        APIService.sendRequest({
          method: 'DELETE',
          path: url,
          data: {
            path: path
          },
          skipAuth: false
        }).then(function () {
          clearImageFromVM(type, path);
          ALERT.successRemove();
          vm.loadingDelete = false;
          vm.loadingImageSrc = '';
        })
      }, msg)
    };

    vm.uploadImage = function (file) {
      if (!file) return;
      vm.uploading = true;
      vm.uploadProgress = 0;
      var uploadUrl = MIID_API.base_url + '/media/upload/' + vm.currentType + '/' + productId;
      Upload.upload({
        url: uploadUrl,
        data: {file: file, 'username': $scope.username}
      }).then(function (resp) {
        vm.uploading = false;
        if (vm.currentType == 'pr_thumb') {
          vm.thumbnailSrc = qingStorBase + resp.data.path;
        } else if (vm.currentType == 'pr_web') {
          vm.description_large.push(qingStorBase + resp.data.path);
        } else if (vm.currentType == 'pr_mob') {
          vm.description_mobile.push(qingStorBase + resp.data.path);
        } else if (vm.currentType == 'pr_bnr_web') {
          vm.bannerLargeSrc = qingStorBase + resp.data.path;
        } else if (vm.currentType == 'pr_bnr_mob') {
          vm.bannerMobileSrc = qingStorBase + resp.data.path;
        }else if (vm.currentType == 'pr_promo_mob') {
          vm.bannerPromotionMobileSrc = qingStorBase + resp.data.path;
        }else if (vm.currentType == 'pr_promo_web') {
          vm.bannerPromotionLargeSrc = qingStorBase + resp.data.path;
        } else if (vm.currentType == 'pr_size_ref') {
          vm.sizeRefSrc = qingStorBase + resp.data.path;
        }
      }, function () {
        vm.uploading = false;
      }, function (evt) {
        vm.uploadProgress = parseInt(100.0 * evt.loaded / evt.total);
      });
    };

    function getRemoveMessage(type) {
      var msg = 'Are you sure you want to remove ';
      if (type == 'pr_thumb') {
        msg += 'the thumbnail image?';
      } else if (type == 'pr_web') {
        msg += 'this desktop product description image?';
      } else if (type == 'pr_mob') {
        msg += 'this mobile product description image?';
      } else if (type == 'pr_bnr_web') {
        msg += 'the desktop banner image?';
      } else if (type == 'pr_bnr_mob') {
        msg += 'the mobile banner image?';
      } else if (type == 'pr_size_ref') {
        msg += 'the product size reference image?';
      }
      return msg;
    }

    function clearImageFromVM(type, path) {
      path = qingStorBase + path;
      if (type == 'pr_thumb') {
        vm.thumbnailSrc = null;
      } else if (type == 'pr_web') {
        vm.description_large = _.without(vm.description_large, path);
      } else if (type == 'pr_mob') {
        vm.description_mobile = _.without(vm.description_mobile, path);
      } else if (type == 'pr_bnr_web') {
        vm.bannerLargeSrc = null;
      } else if (type == 'pr_bnr_mob') {
        vm.bannerMobileSrc = null;
      } else if (type == 'pr_size_ref') {
        vm.sizeRefSrc = null;
      }
    }
  }

})(angular.module('adminPanel.product.create.product_media.controller', []));
