(function(app){

  app.config(function($stateProvider){

    $stateProvider
      .state('admin-panel.product.create.basic_info',{
        url: '/basic-info',
        templateUrl: 'app/modules/product/create/basic_info/wizard_basic_info.html',
        controller: 'ProductController',
        controllerAs: 'vm'
      });

  });


})(angular.module('adminPanel.product.create.basic_info.routes', []));
