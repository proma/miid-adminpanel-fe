var products = null;
(function (app) {

  app.controller('RelatedProductsModalController', RelatedProductsModalController);

  RelatedProductsModalController.$inject = ['API', '$uibModalInstance', 'PRODUCT_ID', 'Notify', 'QINGSTOR', 'ALERT', '$translate'];

  /**
   * @param API
   * @param $uibModalInstance
   * @param PRODUCT_ID
   * @param Notify
   * @param QINGSTOR
   * @param ALERT
   * @param $translate
   */
  function RelatedProductsModalController(API, $uibModalInstance, PRODUCT_ID, Notify, QINGSTOR, ALERT, $translate) {
    var prod = this;

    prod.baseURL = QINGSTOR.base_url;
    prod.products = [];
    prod.selectorForProducts = {};
    prod.selectorForRelatedProducts = {};
    prod.relatedProducts = [];
    prod.cachedRelatedProducts = {deleted: [], current: []}; //we have to keep track of these as admin can apply filters and we can loose previous products
    prod.loadingProducts = true;
    prod.brands = [{name: 'Brands', _id: -1}];
    prod.status = {
      'STATUS': $translate.instant('filters.status.label')||'Status',
      'ACTIVE': 'Active',
      'IN_ACTIVE': 'Inactive'
    };
    prod.selectedBrandForProducts = prod.brands[0];
    prod.selectedBrandForRelatedProducts = prod.brands[0];
    prod.selectedStatusForProducts = 'STATUS';
    prod.selectedStatusForRelatedProducts = 'STATUS';
    prod.searchedTextForProducts = '';
    prod.searchedTextForRelatedProducts = '';

    prod.leftListBulk = [
      {
        title: $translate.instant('modals.product.add')||'Add to Listing',
        action: function () {
          prod.addToList();
        }
      }
    ];

    prod.rightListBulk = [
      {
        title: $translate.instant('modals.product.remove')||'Remove from Listing',
        action: function () {
          prod.removeFromList();
        }
      }
    ];


    /**
     * This function acts as generic function to add or remove items from both listings
     * @param {Array} source
     * @param {Array} dest
     * @param {Function} cb has argument 'selected' which are selected items
     * @returns {*[]}
     */
    prod.addRemoveFromList = function (source, dest, cb) {
      var selected = [];
      selected = _.filter(source, function (item) { //find selected items
        var isChecked;
        if (item.checked) {
          item.changed = true;
        }
        isChecked = item.checked || false; //two in one processing ;)
        item.checked = false;
        return isChecked;
      });
      source = _.difference(source, selected); //remove those items from source
      dest = _.union(dest, selected); //add those to destination
      if (cb) cb(selected);

      prod.selectorForProducts.reset();
      prod.selectorForRelatedProducts.reset();
      return [source, dest];
    };

    /**
     * Adds the selected items from the list and removes it from the other list. The added
     * item will have little border at left to show what's been changed.
     */
    prod.addToList = function () {
      var result;
      result = prod.addRemoveFromList(prod.products, prod.relatedProducts, null); //source is products, dest is relatedProduts
      prod.products = result[0];
      prod.relatedProducts = result[1];
    };


    /**
     * Removes the selected items from the list and adds it to the other list. The removed
     * item will have little border at left to show what's been changed.
     */
    prod.removeFromList = function () {
      var IDs = [], result;

      result = prod.addRemoveFromList(prod.relatedProducts, prod.products, function (selected) {
        selected = _.filter(prod.cachedRelatedProducts.current, function (item) { return _.find(selected, {_id: item._id}) });
        // selected = selected.map(function (item) { item.brandID = item.brand._id; return item });
        prod.cachedRelatedProducts.current = _.difference(prod.cachedRelatedProducts.current, selected);
        prod.cachedRelatedProducts.deleted = _.union(prod.cachedRelatedProducts.deleted, selected);
      });
      prod.relatedProducts = result[0];
      prod.products = result[1];
    };

    /**
     * Submits the form and post the data to the API we only sends IDs
     * If user has applied some filtering then we have lost our all relatedProducts so we
     * use cachedRelatedProducts with union so that we don't miss any related product.
     * We close the modal after saving
     */
    prod.submit = function () {
      prod.loadingProducts = true;
      var productIDs;

      productIDs = _.union(_.pluck(prod.relatedProducts, '_id'), _.pluck(prod.cachedRelatedProducts.current, '_id'));

      API
        .Products
        .editRelatedProducts(PRODUCT_ID, productIDs)
        .then(function (data) {
          prod.loadingProducts = false;
          Notify.infoSaved();
          prod.close();
        })
        .catch(function (err) {
          prod.loadingProducts = false;
          Notify.errorSaving();
        });
    };


    /**
     * Loads all brands in the Brands drop down for filtering
     */
    prod.loadBrands = function () {
      API
        .Brands
        .getAll(null, null, 'LIST_VALUES')
        .then(function (data) {
          prod.brands = data.data;
          prod.brands.unshift({name: $translate.instant('filters.brand.label')||'Brands', _id: -1});
          prod.selectedBrandForProducts = prod.brands[0];
        })
        .catch(function () {
          prod.brands[0] = {name: $translate.instant('errors.errorLoadingBrand')||'Error Loading...', _id: -1};
          ALERT.confirm(prod.loadBrands);
        });
    };


    /**
     * Loads all the products on the left table in UI. We filter this list with related products
     * so that we don't get what's in the related products ;)
     */
    prod.loadProductsData = function () {
      prod.loadingProducts = true;
      var params;
      params = {basic_info: true};

      if (prod.selectedBrandForProducts._id != -1)
        params.brand = prod.selectedBrandForProducts._id;
      if (prod.selectedStatusForProducts.toLowerCase() != 'status')
        params.status = prod.selectedStatusForProducts;
      if (prod.searchedTextForProducts !== '')
        params.text = prod.searchedTextForProducts;

      API
        .Products
        .getList(params) //TODO: find a way to add pagination to that table
        .then(function (data) {
          prod.loadingProducts = false;
          prod.products = data;
          if (prod.relatedProducts.length == 0)//load once from server
            prod.loadRelatedProducts();
          else
            prod.removeDuplicates();
        })
        .catch(function () {
          prod.loadingProducts = false;
          ALERT.confirm(prod.loadProductsData);
        });
    };


    /**
     * Checks the status of the submit button to mark it disabled or not. This will get updated
     * after every digest cycle of angular
     */
    prod.checkStatus = function () {
      //look for 'changed' property on the relatedProducts
      var changed = true;
      prod.relatedProducts.forEach(function (item) {
        if (item.changed) {
          return changed = false;
        }
      });
      if (changed)
        prod.products.forEach(function (item) {
          if (item.changed) {
            return changed = false;
          }
        });
      return changed;
    };


    /**
     * This function is used to load the functions i-e loadRelatedProducts and loadProductsData
     * based on type showing the ALERT to confirm if there are unsaved changes
     * @param {string} type 'relatedProducts' | 'products'
     */
    prod.triggerLoad = function (type) {
      var fn;
      fn = type == 'products' ? prod.loadProductsData : prod.filterRelatedProducts;

      if (!prod.checkStatus())
        ALERT.confirmUnsavedChanges(function () {
          //resets the cache
          prod.cachedRelatedProducts.current = _.union(prod.cachedRelatedProducts.current, prod.cachedRelatedProducts.deleted);
          prod.products = _.union(prod.products, prod.cachedRelatedProducts.deleted);
          prod.cachedRelatedProducts.deleted = [];
          prod.relatedProducts = prod.cachedRelatedProducts.current;
          fn();
        });
      else
        fn();
    };


    /**
     * Loads the data of related Products from the server.
     */
    prod.loadRelatedProducts = function () {
      prod.loadingProducts = true;

      API
        .Products
        .getRelatedProducts(PRODUCT_ID, {}) //TODO: find a way to add pagination to that table
        .then(function (data) {
          prod.cachedRelatedProducts.current = data;
          prod.relatedProducts = data;
          prod.loadingProducts = false;
          prod.removeDuplicates();
          prod.relatedProducts = _.map(data, function (item){ item.brandID = item.brand._id; return item});//workaround for filtetring by brand
          prod.cachedRelatedProducts.current = angular.copy(prod.relatedProducts);
        })
        .catch(function () {
          prod.loadingProducts = false;
          ALERT.confirm(prod.loadRelatedProducts);
        });
    };


    /**
     * Remove the duplicates i-e related products which should not be in the left hand side listing
     */
    prod.removeDuplicates = function () {
      var related;
      related = _.union(_.pluck(prod.cachedRelatedProducts.current, '_id'), _.pluck(prod.relatedProducts, '_id'));
      prod.products = _.reject(prod.products, function(item) {
        return _.find(related, function (prod){return prod == item._id}) || item._id == PRODUCT_ID;
      });
    };

    /**
     * Filters the related products by search text, brand and status. Because this list is not too long
     * so it make sense to do this on client side.
     */
    prod.filterRelatedProducts = function () {
      var findObj = {};

      if (prod.selectedBrandForRelatedProducts._id != -1)
        findObj.brandID = prod.selectedBrandForRelatedProducts._id;
      if (prod.selectedStatusForRelatedProducts.toLowerCase() != 'status')
        findObj.status = prod.selectedStatusForRelatedProducts;

      prod.relatedProducts = _.unique(_.where(prod.cachedRelatedProducts.current, findObj), function(prod){return prod._id});
    };

    prod.close = function () {
      $uibModalInstance.close();
    };

    prod.loadBrands();
    prod.triggerLoad('products');
  }

})(angular.module('adminPanel.product.create.basic_info.add_products.controller', []));
