describe('Controllers', function () {
  describe('RelatedProductsModalController', function () {


    var
      Controller,
      $stateParams,
      URL,
      $rootScope,
      modalInstance,
      Notify,
      ALERT,
      $httpBackend,
      data;

    data = {
      products: [
        {
          _id: '123456',
          name: 'some name 1',
          miid_id: 'miid',
          brand: {_id:124}
        },
        {
          _id: '123457',
          name: 'some name 2',
          miid_id: 'miid',
          brand: {_id:124}
        },
        {
          _id: '123458',
          name: 'some name 3',
          miid_id: 'miid',
          brand: {_id:124}
        }
      ],
      relatedProducts: [
        {
          _id: '123456',//common in both
          name: 'some name 1',
          miid_id: 'miid',
          brand: {_id:124}
        },
        {
          _id: '123447',
          name: 'some name 2',
          miid_id: 'miid',
          brand: {_id:124}
        }
      ]
    };

    // load the dependencies module
    beforeEach(module('ngResource'));
    beforeEach(module('oitozero.ngSweetAlert'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('ui.bootstrap'));
    beforeEach(module('adminPanel.services.notifier'));
    beforeEach(module('adminPanel.api'));
    beforeEach(module('adminPanel.services.alert'));
    beforeEach(module('adminPanel.product.create.basic_info.add_products.controller'));

    beforeEach(inject(function ($injector) {
      modalInstance = {close: function () {}};
      spyOn(modalInstance, 'close');

      $rootScope = $injector.get('$rootScope');
      $httpBackend = $injector.get('$httpBackend');
      ALERT = $injector.get('ALERT');
      Notify = $injector.get('Notify');
      URL = $injector.get('URL');
      var $controller = $injector.get('$controller');

      Controller = $controller('RelatedProductsModalController', {
        '$scope': $rootScope,
        'QINGSTOR': $injector.get('QINGSTOR'),
        '$uibModalInstance': modalInstance,
        'API': $injector.get('API'),
        'PRODUCT_ID': '123456',
        'ALERT': ALERT,
        'Notify': Notify
      });

      Controller.selectorForProducts.reset = function () {};
      Controller.selectorForRelatedProducts.reset = function () {};

      $httpBackend.when('GET', URL.BRANDS + '?list_type=LIST_VALUES').respond({data:[]}, 200);
      $httpBackend.when('GET', URL.PRODUCTS + '/list_values?basic_info=true').respond(data.products, 200);
      $httpBackend.when('GET', URL.PRODUCTS + '/123456/related').respond(data.relatedProducts, 200);
      $httpBackend.flush();
    }));

    describe('MBA-454: Add Related Products', function () {


      it('should have filtered the common products', function () {
        expect(Controller.products.length).toEqual(2);
        expect(Controller.relatedProducts.length).toEqual(2);
      });

      it('should check status for the button', function () {
        Controller.products[0].checked = true;
        expect(Controller.checkStatus()).toBeTruthy();
      });

      it('should have initialized the arrays', function () {
        expect(Controller.products.length).not.toEqual(0);
        expect(Controller.relatedProducts).not.toEqual(0);
      });

      it('it should add checked products from products array to related products array', function () {
        Controller.products[0].checked = true;
        Controller.addToList();
        expect(Controller.relatedProducts.length).toEqual(3);
      });

      it('it should remove checked products from products array', function () {
        Controller.products[0].checked = true;
        Controller.addToList();
        expect(Controller.products.length).toEqual(1);
      });

      it('it should add checked products from related products array to products array', function () {
        Controller.relatedProducts[1].checked = true;
        Controller.removeFromList();
        expect(Controller.products.length).toEqual(3);
      });

      it('it should remove checked products from related products array', function () {
        Controller.relatedProducts[1].checked = true;
        Controller.removeFromList();
        expect(Controller.relatedProducts.length).toEqual(1);
      });

      it('should submit data to server and save', function () {
        $httpBackend.expect('PUT', URL.PRODUCTS + '/123456/related').respond([], 200);
        spyOn(Notify, 'infoSaved');

        Controller.relatedProducts[0].checked = true;
        Controller.submit();

        $httpBackend.flush();
        expect(Notify.infoSaved).toHaveBeenCalled();
      });

      it('should close the modal', function () {
        Controller.close();
        expect(modalInstance.close).toHaveBeenCalled();
      });

      it('should trigger the load of products if no change', function () {
        spyOn(Controller, 'loadProductsData');
        Controller.triggerLoad('products');
        expect(Controller.loadProductsData).toHaveBeenCalled();
      });

      it('should trigger alert if unsaved changes', function () {
        Controller.products[0].changed = true;
        spyOn(ALERT, 'confirmUnsavedChanges');
        Controller.triggerLoad('products');
        expect(ALERT.confirmUnsavedChanges).toHaveBeenCalled();
      });

    });

  });

});
