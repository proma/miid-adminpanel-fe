(function (app) {

  app.controller('ProductController', ['$scope', '$state', 'API', '$stateParams', 'Modal', 'ALERT', 'Notify', '$translate', 'QINGSTOR', 'Slug', ProductController]);

  function ProductController($scope, $state, API, $stateParams, Modal, ALERT, Notify, $translate, QINGSTOR, Slug) {
    var vm = this;

    vm.lang = 'en';
    vm.productID = $stateParams.productID;
    vm.firstLoad = !!vm.productID;
    vm.isEdit = function () {
      return !!$stateParams.productID;
    };

    //Editor options
    vm.editorOptions = {
      toolbar: [
        // [groupName, [list of button]]
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']]
      ],
      placeholder: 'Write description here..',
      disableDragAndDrop: true,
      shortcuts: false,
      disableResizeEditor: true
    };

    // Set today date in date pickers
    var d, year, month, day;
    d = new Date();
    year = d.getFullYear();
    month = d.getMonth() + 1;
    if (month < 10) {
      month = '0' + month;
    }
    day = d.getDate();

    vm.relatedProducts = [];
    vm.oldRelatedProducts = [];
    vm.loadingProducts = false;
    vm.loading = true;
    vm.slugExist = false;
    vm.baseURL = QINGSTOR.base_url;

    vm.product = {
      id: null,
      miidInfo: {
        miid: '',
        merchantType: 'BRAND',
        merchantValue: '',
        created: new Date()
      },
      basicInfo: {
        name: '',
        description: '',
        sku: '',
        price: 1,
        cost: 1,
        time: 1,
        weight: 0,
        manufacturer: '',
        notes: ''
      },
      visibility: {
        status: 'IN_ACTIVE',
        channels: {
          WEB_MOBILE: false,
          WEB_DESKTOP: false,
          IOS: false,
          ANDROID: false,
          VR: false
        }
      },
      seo: {
        title: '',
        description: '',
        slug: '',
        tag: '',
        tags: []
      },
      organisation: {
        categories: {
          category: '',
          sub_categories: []
        },
        tag: '',
        tags: []
      },
      media: {},
      promotions: {
        enable: false,
        activatePromotionBanner: false,
        conditions: {
          discountType: 'AMOUNT',
          discountValue: 10
        },
        expiry: {
          expires: false,
          start: moment(),
          end: moment().add('week', 1)
        },
        quantity: {
          type: 'NOT_LIMITED',
          value: 100
        }
      },
      reviews: {
        total: 0,
        avg: 0
      },
      relatedProducts: [],
      brands: [{name: $translate.instant('info.loadingBrands') || 'Loading brands...', _id: "-1"}],
      categories: [{title: $translate.instant('info.category') || 'Select category'}]
    };
    vm.oldProduct = angular.copy(vm.product);

    vm.checkForChanges = function () {
      var temp1, temp2;
      temp1 = angular.copy(vm.product);
      temp2 = angular.copy(vm.oldProduct);
      delete temp1.relatedProducts;
      delete temp2.relatedProducts;
      delete temp1.categories;
      delete temp2.categories;
      delete temp1.brands;
      delete temp2.brands;
      return vm.product.organisation.categories.sub_categories.length < 1 ||
             vm.product.organisation.categories.sub_categories.length > 4 ||
             (angular.equals(temp1, temp2) &&
             angular.equals(vm.relatedProducts, vm.oldRelatedProducts));
    };

    vm.slugify = function (miid) {
      if (miid) {
        vm.product.seo.slug = Slug.slugify(miid);
        console.log('slugify', vm.product.seo.slug);
      }
      else
        vm.product.seo.slug = Slug.slugify(vm.product.seo.slug);
    };

    vm.addTag = function (tag, type) {
      var tags = [], value = {};
      if (type == 'seo')
        tags = vm.product.seo.tags;
      else
        tags = vm.product.organisation.tags;

      if (tag && !_.isEmpty(tag) && tags.indexOf(tag) < 0) {
        tags.push(tag);
        value = (type == 'seo' ? vm.product.seo : vm.product.organisation);
        value.tag = '';
      }
    };

    vm.removeTag = function (tag, type) {
      if (type == 'seo')
        vm.product.seo.tags = _.without(vm.product.seo.tags, tag);
      else
        vm.product.organisation.tags = _.without(vm.product.organisation.tags, tag);
    };

    API
      .Brands
      .getAll(null, null, 'LIST_VALUES')
      .then(function (brands) {
        if (brands.data && brands.data.length) {
          if (!vm.isEdit())
            vm.product.miidInfo.merchantValue = brands.data[0]._id;
          // vm.product.basicInfo.manufacturer = brands.data[0]._id; TODO: when api allows manufacturer
          vm.product.brands = brands.data;
        }
      }).catch(function () {
      vm.product.brands = [{name: $translate.instant('errors.errorLoadingBrand') || 'Error loading brands', _id: '-1'}];
    });

    vm.validateSubcategoriesCount = function () {
      var arr = vm.product.organisation.categories.sub_categories;
      if (arr && arr.length > 4) {
        vm.product.organisation.categories.sub_categories = arr.slice(0, 4);
      }
    };

    // product info it product is going to be edit
    if (vm.isEdit()) {
      vm.loading = true;
      API
        .Products
        .get(vm.productID)
        .then(function (product) {
          if (vm.firstLoad) vm.firstLoad = false;
          if (product && product._id) {
            vm.product.id = product._id;
            //MIID info
            vm.product.miidInfo.miid = product['miid_id'];
            vm.product.miidInfo.created = new Date(product['created_at']);
            vm.product.miidInfo.merchantType = 'BRAND'; //TODO: change dynamically
            vm.product.miidInfo.merchantValue = product['brand'];
            //MIID basic info
            vm.product.basicInfo.name = product[vm.lang]['name'];
            vm.product.basicInfo.description = product[vm.lang]['desc'];
            vm.product.basicInfo.price = product['price'];
            vm.product.basicInfo.sku = product['sku'];
            vm.product.basicInfo.weight = product['weight'];
            vm.product.basicInfo.cost = product['production_cost'] || 1;
            vm.product.basicInfo.manufacturer = product['brand'];
            vm.product.basicInfo.notes = product['production_notes'];
            vm.product.basicInfo.time = (product['production'] && product['production']['min_time']) || 0;
            //SEO
            vm.product.seo.title = (product['seo'] && product['seo']['page_title']) || '';
            vm.product.seo.description = (product['seo'] && product['seo']['meta_desc']) || '';
            vm.product.seo.slug = product['slug'];
            vm.product.seo.tags = (product['seo'] && product['seo']['meta_kws'] && product['seo']['meta_kws'].split(',')) || [];
            //Status and visibility
            vm.product.visibility.status = product['status'];
            vm.product.visibility.channels = product['visibility'];
            //Organisation
            vm.product.organisation.tags = product['tags'];

            //Reviews
            vm.product.reviews = product['productReviews'];
            //Related products
            vm.product.relatedProducts = product['related_products'] || [];
            //Promotion
            if (product['promotion'] && product['promotion']['_is']) {
              vm.product.promotions.enable = product['promotion']['_is'];
              vm.product.promotions.conditions.discountType = product['promotion']['type'];
              vm.product.promotions.conditions.discountValue = product['promotion']['price'];
              vm.product.promotions.activatePromotionBanner = product['promotion_settings']['banner'];
              vm.product.promotions.quantity.type = product['promotion_settings']['usage_limit']['type'];
              vm.product.promotions.quantity.value = product['promotion_settings']['usage_limit']['limit'];
              vm.product.promotions.expiry.expires = !product['promotion_settings']['expires']['never'];
              //expiry
              if (!product['promotion_settings']['expires']['never']) {
                vm.product.promotions.expiry.start = moment(product['promotion_settings']['expires']['starts']);
                vm.product.promotions.expiry.end = moment(product['promotion_settings']['expires']['ends']);
              }
            }

            if (product.categories) {
              API.Categories.getAll().then(function (allCat) {
                vm.product.categories = allCat;
                var category = _.find(allCat, function (c) {
                  return c._id == product.categories[0].cat_id;
                });
                vm.product.organisation.categories.category = category;
                _.each(product.categories[0].sub_categories, function (subCat) {
                  var sub_category = _.find(category.sub_categories, function (sc) {
                    return sc._id == subCat;
                  });
                  vm.product.organisation.categories.sub_categories.push(sub_category._id);
                });
                vm.oldProduct = angular.copy(vm.product);
              });
            }
          }
        })
        .catch(function (err) {
          console.log(err);
        })
        .finally(function () {
          vm.loading = false;
        });
    } else {
      vm.loading = true;
      API
        .Categories
        .getAll()
        .then(function (cat) {
          vm.product.categories = cat;
          vm.product.organisation.categories.category = cat[0];
        })
        .finally(function () {
          vm.loading = false;
        });
    }

    vm.processBasicInfo = function () {
      vm.slugExist = false;
      vm.updateRelatedProducts();
      if (angular.equals(vm.product, vm.oldProduct))
        return;

      vm.loading = true;
      var product_categories = [angular.copy(vm.product.organisation.categories)];
      product_categories[0].cat_id = product_categories[0].category._id;
      delete product_categories[0].category;

      var postData = {
        miid_id: vm.product.miidInfo.miid,
        brand: vm.product.miidInfo.merchantValue,
        price: vm.product.basicInfo.price,
        sku: vm.product.basicInfo.sku,
        slug: vm.product.seo.slug,
        status: vm.product.visibility.status,
        weight: vm.product.basicInfo.weight,
        production_cost: vm.product.basicInfo.cost,
        production: {
          min_time: vm.product.basicInfo.time,
          max_time: vm.product.basicInfo.time // no need for max time so need to remove this later on
        },
        visibility: vm.product.visibility.channels,
        tags: vm.product.organisation.tags,
        categories: product_categories
      };

      if (vm.product.basicInfo.notes)
        postData['production_notes'] = vm.product.basicInfo.notes;

      // add promotion data if selected
      if (vm.product.promotions.enable) {
        postData['promotion'] = {
          _is: vm.product.promotions.enable,
          type: vm.product.promotions.conditions.discountType,
          price: vm.product.promotions.conditions.discountValue
        };
        postData['promotion_settings'] =
        {
          banner: vm.product.promotions.activatePromotionBanner,
          usage_limit: {
            type: vm.product.promotions.quantity.type,
            limit: vm.product.promotions.quantity.value
          },
          expires: {
            never: !vm.product.promotions.expiry.expires
          }
        };
        if (vm.product.promotions.expiry.expires) {
          postData['promotion_settings']['expires'].starts = vm.product.promotions.expiry.start;
          postData['promotion_settings']['expires'].ends = vm.product.promotions.expiry.end;
        }
      } else {
        postData['promotion'] = {
          _is: vm.product.promotions.enable
        };
      }
      //This allows to save language dependent fields dynamically
      postData[vm.lang] = {name: vm.product.basicInfo.name, desc: vm.product.basicInfo.description || '-'};
      postData['ch'] = {name: vm.product.basicInfo.name, desc: vm.product.basicInfo.description || '-'};

      // add seo info
      if (vm.product.seo.title.length ||
          vm.product.seo.description.length ||
          vm.product.seo.tags.join(', ').length) {

        postData['seo'] = {};
        if (vm.product.seo.title.length) {
          postData['seo']['page_title'] = vm.product.seo.title;
        }
        if (vm.product.seo.description.length) {
          postData['seo']['meta_desc'] = vm.product.seo.description;
        }
        if (vm.product.seo.tags.join(', ').length) {
          postData['seo']['meta_kws'] = vm.product.seo.tags.join(', ');
        }
      }

      //Check if slug has been changed, if error we proceed with saving else we show error
      if (vm.product.seo.slug != vm.oldProduct.seo.slug) {
        API
          .Products
          .getBySlug(vm.product.seo.slug)
          .then(function () {
            vm.slugExist = true;
            Notify.errorObjectExist();
            vm.loading = false;
          })
          .catch(function (err) {
            if (err.statusCode == 404 || (err.data && err.data.statusCode == 404)) {
              vm.updateProduct(postData);
            }
          })
      } else {
        vm.updateProduct(postData);
      }

    };

    vm.updateProduct = function (postData) {
      if (vm.product.id) {
        if (vm.oldProduct.seo.slug == vm.product.seo.slug)
          delete postData['slug'];
        if (vm.oldProduct.miidInfo.miid == vm.product.miidInfo.miid)
          delete postData['miid_id'];

        API
          .Products
          .edit(vm.product.id, postData)
          .then(function (result) {
            if (result && result.result === 'success') {
              vm.oldProduct = angular.copy(vm.product);
              Notify.infoSaved();
            }
          })
          .catch(function (err) {
            Notify.errorSaving();
            console.log(err);
          })
          .finally(function () {
            vm.loading = false;
          });
      } else {
        API
          .Products
          .save(postData)
          .then(function (newProduct) {
            if (newProduct && newProduct.product_id) {
              Notify.infoSaved();
              $state.go('admin-panel.product.create.basic_info', {productID: newProduct.product_id});
              vm.oldProduct = angular.copy(vm.product);
            }
          })
          .catch(function (err) {
            Notify.errorSaving();
            console.log(err);
          })
          .finally(function () {
            vm.loading = false;
          });
      }
    };

    vm.addRelatedProducts = function () {
      Modal
        .open(
          'RelatedProductsModalController',
          'prod',
          "app/modules/product/create/basic_info/add_products/product.create.basic_info.add_products.html",
          {
            PRODUCTS: function () {
              return vm.relatedProducts;
            },
            PRODUCT_ID: function () {
              return vm.productID || null;
            }
          },
          'prod-listing-custom'
        )
        .result
        .then(function (data) {
          console.log(data);
        })
        .finally(function () {
          vm.loadRelatedProducts();
        });
    };

    vm.loadRelatedProducts = function () {
      vm.loadingProducts = true;

      API
        .Products
        .getRelatedProducts(vm.productID) //TODO: find a way to add pagination to this list
        .then(function (data) {
          vm.relatedProducts = data;
          vm.oldRelatedProducts = angular.copy(vm.relatedProducts);
        })
        .catch(function () {
          ALERT.confirm(vm.loadRelatedProducts);
        })
        .finally(function () {
          vm.loadingProducts = false;
        });
    };

    vm.confirmDelete = function (productID, product) {
      ALERT.confirmWarning(function () {
        return vm.updateRelatedProducts(productID, product);
      });
    };

    /**
     * Updates the related products, this function is so generic that it can be used for both
     * deletion and update of the related products ;p
     * If product param is provided, it filters the list and post, otherwise post it directly
     * @param {string} productID
     * @param product
     */
    vm.updateRelatedProducts = function (productID, product) {
      if (!product && angular.equals(vm.relatedProducts, vm.oldRelatedProducts))
        return;
      if (product)
        product.loading = true;
      else
        vm.loadingProducts = true;

      var productIDs;

      productIDs = _.pluck(vm.relatedProducts, '_id');
      productIDs = productID ? _.without(productIDs, productID) : productIDs;
      API
        .Products
        .editRelatedProducts(vm.productID, productIDs)
        .then(function () {
          Notify.infoSaved();
          if (product)
            vm.relatedProducts = _.without(vm.relatedProducts, product);

          vm.oldRelatedProducts = angular.copy(vm.relatedProducts);
        })
        .catch(function () {
          Notify.errorSaving();
        })
        .finally(function () {
          if (product)
            product.loading = false;
          else
            vm.loadingProducts = false;
        });
    };

    $scope.$watch("vm.product.promotions.expiry.start", function () {
      if (vm.product.promotions.expiry.start.valueOf() > vm.product.promotions.expiry.end.valueOf()) {
        vm.product.promotions.expiry.start = moment(vm.product.promotions.expiry.end);
      }
    });

    $scope.$watch("vm.product.promotions.expiry.end", function () {
      if (vm.product.promotions.expiry.start.valueOf() > vm.product.promotions.expiry.end.valueOf()) {
        vm.product.promotions.expiry.end = moment(vm.product.promotions.expiry.start);
      }
    });

    //Listen to state change to know if we have unsaved changes or not.
    var unSub = $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
      if (!(angular.equals(vm.product, vm.oldProduct) && angular.equals(vm.relatedProducts, vm.oldRelatedProducts)) && toState['name'].indexOf('basic_info') == -1) {
        event.preventDefault();
        ALERT.confirmUnsavedChanges(function () {
          unSub(); //un subscribe before running into infinite loop
          $state.transitionTo(toState.name, toParams, {reload: true});
        })
      }
    });

    if (vm.productID)
      vm.loadRelatedProducts();
  }

})(angular.module('adminPanel.product.create.basic_info.controller', []));
