(function(app){

  app.controller('ProductCustomizationController', ['$scope','$uibModal', 'API', '$stateParams', 'QINGSTOR', 'ALERT', 'Upload', 'MIID_API', '$timeout', '$window', 'Notify', 'Modal', '$anchorScroll', '$location',  ProductCustomizationController]);


  function ProductCustomizationController($scope, $uibModal, API, $stateParams, QINGSTOR, ALERT, Upload, MIID_API, $timeout, $window, Notify, Modal, $anchorScroll, $location) {
    var vm = this;

    //var productId = API.Products.getProductId();
    //get from state params
    var productId = $stateParams.productID;
    vm.productID = productId;
    vm.lang = 'ch'; //default chinese
    vm.baseURL = QINGSTOR.base_url;
    vm.colorPicker = {options: {flat: true, showButtons: false}};
    vm.fonts = [
      {
        name: 'Montserrat Bold'
      },
      {
        name: 'Montserrat Light'
      },
      {
        name: 'Centhury Gothic Italic'
      },
      {
        name: 'Centhury Gothic Bold'
      }
    ];
    vm.langs = [
      'EN',
      'CH'
    ];
    vm.conditionRef =
    {
      'SELECTED': "Selected",
      'ENABLE': "Enabled",
      'VISIBLE': "Visible",
      'UN_SELECTED': "Not selected",
      'DISABLE': "Not enabled",
      'IN_VISIBLE': "Not visible"
    };

    if($stateParams.productID){
      API.Products.setProductId(productId); //set product id
    }
    // Data members for data bindings
    vm.steps_info = {
      angles: [0,1,2,3,4,5,6,7],
      steps: []
    };
    vm.cachedData = [];

    // Data processing methods
    // step
    vm.createStep = function(){
      vm.steps_info.steps.push({
        step_id: null,
        id: '',
        title: '',
        tech: '',
        angle: 0,
        enable: true,
        visible: true,
        stepFinalized: false,
        groups : []
      });
      $timeout(function () {
        $location.hash('step' + (vm.steps_info.steps.length-1));
        $anchorScroll();
      }, 200); //wait for digest cycle to compelte then scroll to that step!
    };

    vm.addOrUpdateStep = function(step){
      step.loading = true;
      var postData = {
        icon: step.id,
        tech: step.tech,
        en: {
          title: step.title
        },
        ch: {
          title: step.title
        },
        angle: step.angle,
        enable: step.enable,
        visible: step.visible
      };

      if(step.step_id) {
        API
          .Steps
          .edit(productId, step.step_id, postData)
          .then(function (updateStep) {
            vm.cachedData[_.findIndex(vm.steps_info.steps, step)] = angular.copy(step);

            step.stepFinalized = true;
            step.loading = false;

            Notify.infoSaved();
          }).catch(function (err) {
          step.loading = false;
          Notify.errorSaving();
        });
      } else {
        API
          .Steps
          .save(productId, postData)
          .then(function (newStep) {
            if (newStep && newStep.step_id) {
              step.step_id = newStep.step_id;
              vm.cachedData.push(angular.copy(step));
            }
            step.stepFinalized = true;
            step.loading = false;

            Notify.infoSaved();
          }).catch(function (err) {
          Notify.errorSaving();
          step.loading = false;
        });
      }
    };

    vm.deleteStepConfirm = function (step, index) {
      ALERT
        .confirmWarning(function () {
          return vm.deleteStep(step, index);
        });
    };

    vm.deleteStep = function(step, stepIndex){
      step.loading = true;
      if(step.step_id){
        API
          .Steps
          .remove(productId, step.step_id)
          .then(function (deletedStep) {
            vm.steps_info.steps = _.without(vm.steps_info.steps,step);
            vm.cachedData = _.without(vm.cachedData, vm.cachedData[stepIndex]);
            step.loading = false;

            Notify.infoDeleted();
          }).catch(function (err) {
          step.loading = false;
          Notify.errorSaving();
        });
      } else {
        vm.steps_info.steps = _.without(vm.steps_info.steps,step);
        step.loading = false;
      }
    };

    vm.getCleanStep = function (step) {
      delete step.groups;
      delete step.stepFinalized;
      delete step.loading;
      return step;
    };

    vm.discardStepChanges = function (step, index) {
      var oldStp = angular.copy(vm.cachedData[index]);
      var newStp = angular.copy(step);
      oldStp = vm.getCleanStep(oldStp);
      newStp = vm.getCleanStep(newStp);

      step.stepFinalized = true;
      if(!angular.equals(oldStp, newStp)) {
        step = _.extend(step, oldStp);
      }
    };

    vm.getCleanGroup = function (group) {
      delete group.materials;
      delete group.groupFinalized;
      delete group.loading;
      return group;
    };

    vm.discardGroupChanges = function (step, group, stepIndex, groupIndex) {
      var oldGrp = angular.copy(vm.cachedData[stepIndex].groups[groupIndex]);
      var newGrp = angular.copy(group);
      oldGrp = vm.getCleanGroup(oldGrp);
      newGrp = vm.getCleanGroup(newGrp);

      group.groupFinalized = true;
      if(!angular.equals(oldGrp, newGrp)) {
        group = _.extend(group, oldGrp);
      }
    };

    vm.getCleanMaterial = function (material) {
      delete material.default;
      delete material.removeImage;
      delete material.materialFinalized;
      delete material.uploading;
      delete material.file;
      delete material.loading;
      delete material.uploadProgress;
      return material;
    };

    vm.discardTextMaterialChanges = function (type, step, group, material, stepIndex, groupIndex, materialIndex) {
      var oldMat = angular.copy(vm.cachedData[stepIndex].groups[groupIndex].materials[materialIndex]);
      var newMat = angular.copy(material);
      oldMat = vm.getCleanMaterial(oldMat);
      newMat = vm.getCleanMaterial(newMat);

      material.materialFinalized = true;
      if(!angular.equals(oldMat, newMat)) {
        material = _.extend(material, oldMat);
        if(type === 'INPUT')
          material.fonts = _.filter(vm.fonts, function (val) {
            return _.find(material.fonts, {name: val.name});
          });
      }
    };

    vm.updateVisibilityControlsStepsGroups = function (type, elementType, step, group, stepIndex, groupIndex) {
      if (!(type == 'STEP' && step.step_id || type == 'GROUP' && group.group_id))
        return;

      var factory, ref;
      ref = type == 'GROUP' ? group: step;
      ref.loading = true;
      var postData = {};

      if (type == 'GROUP')
        factory = API.Groups;
      else
        factory = API.Steps;

      if(elementType == 'enable')
        postData.enable = type == 'GROUP' ? group.enable : step.enable;
      else
        postData.visible = type == 'GROUP' ? group.visible : step.visible;

      factory
        .edit(
          vm.productID,
          step.step_id,
          type == 'GROUP' ? group.group_id : postData,
          postData
        )
        .then(function () {
          ref.loading = false;
          //update cache;
          if (type == 'GROUP')
            vm.cachedData[stepIndex].groups[groupIndex] = angular.copy(group);
          else
            vm.cachedData[stepIndex] = angular.copy(step);
          Notify.infoSaved();
        })
        .catch(function () {
          ref.loading = false;
          Notify.errorSaving();
        })

    };

    // groups processing

    vm.createGroup = function(type, step){
      step.groups.push({
        enable: true,
        multipleSelection: false,
        visible: true,
        type: type,
        tech: '',
        title: '',
        id: '',
        group_id: null,
        groupFinalized : false,
        materials: []
      });
    };

    vm.addOrUpdateGroup = function(step, group, stepIndex, groupIndex){
      group.loading = true;
      var postData = {
        type: group.type,
        tech: group.tech,
        en: {
          title: group.title
        },
        ch: {
          title: group.title
        }
      };
      postData.multiple_selection = group.multipleSelection;
      postData.enable = group.enable;
      postData.visible = group.visible;

      if(group.group_id) {
        API
          .Groups
          .edit(productId, step.step_id, group.group_id, postData)
          .then(function (oldGroup) {
            group.groupFinalized = true;
            group.loading = false;

            if (vm.cachedData[stepIndex].groups[groupIndex].multipleSelection != group.multipleSelection){
              vm.loadSteps();
              return;
            }

            vm.cachedData[stepIndex].groups[groupIndex] = angular.copy(group);

            Notify.infoSaved();
          }).catch(function (err) {
          group.loading = false;
          Notify.errorSaving();
        });
      } else {
        API
          .Groups
          .save(productId, step.step_id, postData)
          .then(function (newGroup) {
            if (newGroup && newGroup.group_id) {
              group.group_id = newGroup.group_id;
              vm.cachedData[stepIndex].groups.push(angular.copy(group));
            }
            group.groupFinalized = true;
            group.loading = false;

            Notify.infoSaved();
          }).catch(function (err) {
          group.loading = false;
          Notify.errorSaving();
        });
      }
    };

    vm.deleteGroup = function(step,group, stepIndex, groupIndex){
      group.loading = true;
      if(group.groupFinalized) {

        API
          .Groups
          .remove(productId, step.step_id, group.group_id)
          .then(function () {
            step.groups = _.without(step.groups, group);
            vm.cachedData[stepIndex].groups = _.without(vm.cachedData[stepIndex].groups, vm.cachedData[stepIndex].groups[groupIndex]);

            group.loading = false;

            Notify.infoDeleted();
          }).catch(function (err) {
          group.loading = false;
          Notify.errorSaving();
        });
      } else {
        step.groups = _.without(step.groups, group);
        group.loading = false;
        return;
      }
    };


    vm.deleteGroupConfirm = function (step, group, sIndex, gIndex) {
      ALERT
        .confirmWarning(function () {
          return vm.deleteGroup(step, group, sIndex, gIndex);
        });
    };

    vm.setDefaultMaterial = function(type, step, group, material) {
      var postDataNewDefault = {};

      if (type === 'enable')
        postDataNewDefault.enable = material.enable;
      else if (type === 'visible')
        postDataNewDefault.visible = material.visible;
      else {
        // make if false to get old default
        if(!material.default) {
          Notify.errorOperation();
          $timeout(function () {
            material.default = true; //set true again
          }, 500); //reverse value after 5ms
          return;
        }
        material.default = false;

        var oldDefault = _.find(group.materials, function(material){
          return material.default;
        });

        if(oldDefault && oldDefault.material_id == material.material_id){
          // nothing to do as the current material is already default
          return;
        }

        // set defaults
        if (oldDefault) {
          oldDefault.default = false;
        }
        material.default = true;

        postDataNewDefault.default = true;
      }

      vm.saveMaterial(step, group, material, postDataNewDefault);
    };

    vm.saveMaterial = function(step, group, material, postData) {
      material.loading = true;
      var stepNo = _.findIndex(vm.steps_info.steps, step);
      var groupNo = _.findIndex(vm.steps_info.steps[stepNo].groups, group);
      var materialNo = _.findIndex(vm.steps_info.steps[stepNo].groups[groupNo].materials, group);
      var doUpload = !postData;
      if (!postData) {
        postData = {
          default: material.default,
          tech: material.tech,
          tech2: material.tech2,
          tech3: material.tech3,
          element: material.element,
          en: {
            name: material.name,
            desc: material.description
          },
          price:  material.price,
          ip_cost: material.ipCost,
          prod_cost: material.prodCost,
          visible: material.visible,
          enable: material.enable
        };
        postData.ch = postData.en;
        if (material.tech2)
          postData.tech2 = material.tech2;
        if (material.tech3)
          postData.tech3 = material.tech3;

        if (material.element.type == 'TXT'){
          postData.element.val = material.name;// TODO:  need to update this and select the language from locale to get name of that language
        }
      }

      if(!material.material_id) {

        //check if this material is first one
        material.default = postData.default = (vm.steps_info.steps[stepNo].groups[groupNo].materials.length < 1);

        API
          .UI
          .save(productId, step.step_id, group.group_id, postData)
          .then(function (newMaterial) {
            if(newMaterial && newMaterial['group_ui_id']){
              material.material_id = newMaterial['group_ui_id'];
            }
            if (material.element.type == 'CLR')
              material.val = material.element.val;
            if (material.element.type == 'IMG' && doUpload)
              vm.uploadImage(step, group, material);
            else {
              material.loading = false;
              Notify.infoSaved();
              material.materialFinalized = true;
            }

            vm.cachedData[stepNo].groups[groupNo].materials.push(angular.copy(material));
          }).catch(function (err) {
          Notify.errorSaving();
        });
      } else {
        API
          .UI
          .edit(productId, step.step_id, group.group_id, material.material_id, postData)
          .then(function (oldMaterial) {
            // replace with updated material
            var index = _.indexOf(vm.steps_info.steps[stepNo].groups[groupNo].materials,
              _.find(vm.steps_info.steps[stepNo].groups[groupNo].materials, {material_id: material.material_id}));
            vm.steps_info.steps[stepNo].groups[groupNo].materials.splice(index, 1, material);
            vm.cachedData[stepNo].groups[groupNo].materials[materialNo] = angular.copy(material);
            if (material.element.type == 'CLR')
              material.val = material.element.val;
            if (material.element.type == 'IMG' && doUpload && !material.imgSrc)
              vm.uploadImage(step, group, material);
            else {
              material.loading = false;
              Notify.infoSaved();
              material.materialFinalized = true;
            }
          }).catch(function (err) {
          Notify.errorSaving();
        });
      }
    };

    vm.updateMaterialAvailability = function(step, group, material) {

      material.loading = true;

      var postData = {
        status:(material.status == 'ACTIVE')?'IN_ACTIVE':'ACTIVE'
      };

      API
          .UI
          .edit(productId, step.step_id, group.group_id, material.material_id, postData)
          .then(function () {

            material.status = postData.status;

            Notify.infoSaved();
          }).catch(function (err) {
            Notify.errorSaving();
          }).finally(function() {
            material.loading = false;
          });
    };

    vm.addMaterial = function(group) {
      var material = {
        element: {type: 'CLR', val: '#ffffff'},
        name: '-',
        description: '-',
        tech: '-',
        tech2: '-',
        tech3: '-',
        price: 0,
        enable: true,
        visible: true,
        prodCost: 0,
        ipCost: 0
      };
      group.materials.push(material);
      vm.currMaterial = material;
    };

    vm.deleteMaterialConfirm = function (step, group, material, sIndex, gIndex, mIndex, type) {
      var fn;
      if(group.type === 'INPUT' || group.type === 'PERSONALIZE' || group.type === 'SELECTABLE' || group.type === 'CUSTOM_IMG')
        fn = vm.deleteCustomMaterial;
      else
        fn = vm.deleteMaterial;

      ALERT
        .confirmWarning(function () {
          return fn(step, group, material, sIndex, gIndex, mIndex, type);
        });
    };

    vm.deleteCustomMaterial = function (step, group, material, stepIndex, groupIndex, materialIndex, type) {
      if(!material.material_id) {
        group.materials = _.without(group.materials, material);
        return;
      }

      material.loading = true;

      var factory;
      if (type === 'INPUT')
        factory = API.Inputs;
      else if(type === 'SELECTABLE')
        factory = API.Selectables;
      else if(type === 'CUSTOM_IMG')
        factory = API.CustomImages;
      else if (type == 'PERSONALIZE')
        factory = API.Personalize;

      factory
        .remove(
          vm.productID,
          step.step_id,
          group.group_id,
          material.material_id
        )
        .then(function () {
          group.materials = _.without(group.materials, material);
          vm.cachedData[stepIndex].groups[groupIndex].materials =
            _.without(vm.cachedData[stepIndex].groups[groupIndex].materials,
              vm.cachedData[stepIndex].groups[groupIndex].materials[materialIndex]);
          Notify.infoDeleted();
        })
        .catch(function () {
          Notify.errorSaving();
        })
        .finally(function () {
          material.loading = false;
        });
    };


    vm.addPersonalize = function (group) {
      var personalize =
      {
        materialFinalized: false,
        enable: true,
        visible: true,
        element: {
          type: 'TXT',
          val: ''
        },
        en: {
          name: '',
          desc: ''
        },
        tech: '',
        price: 0,
        prod_cost: 0,
        ip_cost: 0
      };

      group.materials.push(personalize);
      vm.currMaterial = personalize;
    };

    vm.addCustomImg = function (group) {
      var material = {
        materialFinalized: false,
        enable: true,
        visible: true,
        tech: '-',
        tech2: '-',
        tech3: '-',
        prod_cost: 0,
        ip_cost: 0,
        price: '',
        element: {
          type: 'TXT'
        },
        en : {
          name: '-',
          desc: '-'
        }

      };
      if(group.materials.length == 0)
        material.default = true; //first selected
      group.materials.push(material);
      vm.currMaterial = material;
    };

    vm.changeCustomImgControls = function (type, step, group, material, stepIndex, groupIndex, materialIndex) {
      material.loading = true;
      var postData = {};
      if(type === 'visible')
        postData.visible = material.visible;
      else if (type === 'enable')
        postData.enable = material.enable;
      else
        postData.default = material.default;

      if(type === 'default') {
        var prevKey;
        if (group.multipleSelection === false) {
          var mChange = material.default;
          material.default = false;// to find previous one if exist.

          group.materials.forEach(function (value, key) {
            value.default = false;
            vm.cachedData[stepIndex].groups[groupIndex].materials[key].default = false;
          });

          vm.cachedData[stepIndex].groups[groupIndex].materials[materialIndex].default = mChange;
          material.default = mChange;
        }
      }

      API
        .CustomImages
        .edit(vm.productID, step.step_id, group.group_id, material.material_id, postData)
        .then(function () {
          material.loading = false;
          Notify.infoSaved();
          vm.cachedData[stepIndex].groups[groupIndex].materials[materialIndex] = angular.copy(material);
        })
        .catch(function () {
          Notify.errorSaving();
          material.loading = false;
        });

    };

    vm.addSelectableOption = function (material) {
      material.options.push({
        is_default: false,
        val: '',
        price: '',
        prod_cost: 0,
        ip_cost: 0,
        tech: ''
      });
    };

    vm.changeSelectableOptionDefault = function (material, optionIndex) {
      //make all false
      material.options.forEach(function (option) {
        option.is_default = false
      });
      //make current one default
      material.options[optionIndex].is_default = true;
    };

    vm.deleteSelectableOption = function (material, option) {
      //check if its the only option -> can't delete
      if(material.options.length < 2) {
        Notify.errorOperation(); //can't delete
        return;
      }
      //check if the option was selected.
      var isDefault = option.is_default;
      //delete the option
      material.options = _.without(material.options, option);
      if(isDefault === true) { //if true then need to set first one default
        material.options[0].is_default = true;
      }

    };

    vm.addSelectable = function (group) {
      var material = {
        materialFinalized: false,
        enable: true,
        visible: true,
        tech: '',
        element: {
          type: 'TXT',
          val: ''
        },
        en : {
          name: '',
          desc: ''
        },
        options: [{
          is_default: true,
          val: '',
          price: '',
          prod_cost: 0,
          ip_cost: 0,
          tech: ''
        }]
      };
      group.materials.push(material);
      vm.currMaterial = material;
    };

    vm.addTextMaterial = function (group) {
      var material = {
        enable: true,
        visible: true,
        element: {
          type: 'TXT'
        },
        min_char: 1,
        max_char: 10,
        prod_cost: 0,
        ip_cost: 0,
        fonts: [vm.fonts[0]],
        noFonts: false,
        lang: [vm.langs[0]],
        price: 0,
        en: {
          name: '',
          placeholder: 'Text',
          desc: ''
        }
      };
      group.materials.push(material);
      vm.currMaterial = material;
    };

    vm.saveImage = function (file) {
      vm.currMaterial.file = file;
    };

    vm.uploadImage = function (step, group, material) {

      material.uploading = true;
      material.uploadProgress = 0;
      var uploadUrl = MIID_API.base_url + '/products/media/'+ vm.productID +'/steps/' + step.step_id+ '/groups/'+ group.group_id +'/'+ group.type +'/' + material.material_id;
      Upload.upload({
        url: uploadUrl,
        data: {file: material.file}
      }).then(function (resp) {
        material.uploading = false;
        material.loading = false;
        if(resp.data.result == 'success') {
          Notify.infoImageSaved();
          material.materialFinalized = true;
          var d = new Date();
          material.imgSrc = resp.data.link + '?' + d.getTime();
          material.imgArr = [
            vm.baseURL + resp.data.link + '?' + d.getTime()
          ];

          material.removeImage = function () {
            material.imgSrc = null;
          };

          return;
        }
        Notify.errorSaving();

      }, function (err) {
        material.uploading = false;
        material.loading = false;
        Notify.errorSaving();

      }, function (evt) {
        material.uploadProgress = parseInt(100.0 * evt.loaded / evt.total);
      });
    };

    vm.getCleanCustomMaterial = function (material, type) {
      delete material.uploading;
      delete material.file;
      delete material.uploadProgress;
      delete material.materialFinalized;
      delete material.imgArr;
      delete material.imgSrc;
      delete material.loading;
      delete material.material_id;
      delete material.noFonts;
      delete material['def_scene'];
      delete material.galleries;
      delete material.status;
      if (material.element.type == 'TXT')
        delete material.element.val; //we don't need this
      if (!material.element.val)
        delete material.element.val;
      if (type != 'SELECTABLE')
        delete material.options;
      if(type !== 'CUSTOM_IMG')
        delete material.default;
      //For Custom images
      if (!material.tech2)
        delete material.tech2;
      if (!material.tech3)
        delete material.tech3;

      return material;
    };

    vm.addUpdateCustomMaterial = function (type, step, group, material, stepIndex, groupIndex, materialIndex, postData) {
      material.loading = true;

      var factory;
      if(type == 'TEXT')
        factory = API.Inputs;
      else if (type === 'SELECTABLE')
        factory = API.Selectables;
      else if (type === 'CUSTOM_IMG')
        factory = API.CustomImages;
      else if (type == 'PERSONALIZE')
        factory = API.Personalize;

      material.ch = material.en;

      var param = angular.copy(material);
      param = vm.getCleanCustomMaterial(param, type);



      if(!material.material_id) {
        if(material.element.type == 'IMG' && !material.file) {
          Notify.errorImageMissing();
          material.loading = false;
          return;
        }

        factory
          .save(
            vm.productID,
            step.step_id,
            group.group_id,
            param//postData
          )
          .then(function (obj) {
            material.material_id = obj._id; //newly created input id
            if(material.element.type == 'IMG') {
              vm.uploadImage(step, group, material);
            }else {
              material.loading = false;
              Notify.infoSaved();
              material.materialFinalized = true;
            }

            if (type == 'PERSONALIZE') {
              material.loading = true;
              API
                .Personalize
                .get(vm.productID, step.step_id, group.group_id, obj._id)
                .then(function (obj) {
                  delete obj['_id'];
                  _.extend(material, obj);
                  vm.cachedData[stepIndex].groups[groupIndex].materials.push(angular.copy(material)); //we want whole object to be cached
                })
                .finally(function () {
                  material.loading = false
                });
            } else {
              vm.cachedData[stepIndex].groups[groupIndex].materials.push(angular.copy(material));
            }
          })
          .catch(function () {
            material.loading = false;
            Notify.errorSaving();
          });

      }else {

        if(material.element.type == 'IMG' && !material.imgSrc && !material.file) {
          Notify.errorImageMissing();
          material.loading = false;
          return;
        }

        factory
          .edit(
            vm.productID,
            step.step_id,
            group.group_id,
            material.material_id,
            postData? postData: param
          )
          .then(function (obj) {
            if(material.element.type == 'IMG' && !material.imgSrc) {
              vm.uploadImage(step, group, material);
            }else {
              material.loading = false;
              Notify.infoSaved();
              material.materialFinalized = true;
            }

            vm.cachedData[stepIndex].groups[groupIndex].materials[materialIndex] = angular.copy(material);
          })
          .catch(function () {
            material.loading = false;
            Notify.errorSaving();
          });
      }

    };


    vm.updateVisibilityControlsMaterials = function (type, elementType, step, group, material, stepIndex, groupIndex, materialIndex) {
      if (!material.material_id)
        return;
      var factory;
      material.loading = true;
      var postData = {};

      if (type == 'SELECTABLE') {
        factory = API.Selectables;
        postData.options = material.options;
      }
      else if (type == 'INPUT')
        factory = API.Inputs;

      if(elementType == 'enable')
        postData.enable = material.enable;
      else
        postData.visible = material.visible;

      factory
        .edit(
          vm.productID,
          step.step_id,
          group.group_id,
          material.material_id,
          postData
        )
        .then(function () {
          material.loading = false;
          //update cache;
          vm.cachedData[stepIndex].groups[groupIndex].materials[materialIndex] = angular.copy(material);
          Notify.infoSaved();
        })
        .catch(function () {
          material.loading = false;
          Notify.errorSaving();
        })

    };

    // materials processing

    // vm.addMaterial = function(group, step, material, isEdit){
    //   var modalInstance =
    //     $uibModal
    //       .open({
    //         animation: true,
    //         size: 'lg',
    //         resolve: {
    //           STEP: function(){
    //             return step;
    //           },
    //           GROUP: function(){
    //             return group;
    //           },
    //           MATERIAL: function(){
    //             return material;
    //           },
    //           PRODUCT: function () {
    //             return vm.productID;
    //           }
    //         },
    //         templateUrl: 'app/modules/product/create/customization_description/modal.html',
    //         controller: ['$scope', 'GROUP', 'STEP', 'MATERIAL', 'PRODUCT', '$uibModalInstance', 'MIID_API', 'Upload', 'QINGSTOR', '$window', '$timeout',
    //           function($scope, GROUP, STEP, MATERIAL, PRODUCT, $uibModalInstance, MIID_API, Upload, QINGSTOR, $window, $timeout){
    //             var modal = this;
    //
    //             modal.uploading = false;
    //             modal.imgArr = [];
    //             modal.baseURL = QINGSTOR.base_url;
    //             modal.uploadProgress = 0;
    //             modal.activeTab = (isEdit) ? 'custom' : 'library';
    //             modal.colorPicker = {
    //               options: {flat: true, showButtons: false}
    //             };
    //             if(typeof (MATERIAL) != 'undefined') { //enables editing
    //               modal.material = {
    //                 default: MATERIAL.default,
    //                 type: MATERIAL.type,
    //                 nameEN: MATERIAL.nameEN,
    //                 nameCH: MATERIAL.nameCH,
    //                 descCH: MATERIAL.descCH,
    //                 descEN: MATERIAL.descEN,
    //                 tech: MATERIAL.tech,
    //                 enable: MATERIAL.enable ? MATERIAL.enable : false,
    //                 visible: MATERIAL.visible ? MATERIAL.visible: false,
    //                 price: MATERIAL.price,
    //                 //stock: MATERIAL.stock,
    //                 //status: MATERIAL.status,
    //                 //hasOverlay: MATERIAL.hasOverlay,
    //                 //color: MATERIAL.color,
    //                 //opacity: MATERIAL.opacity,
    //                 material_id: MATERIAL.material_id
    //               };
    //               // set value to colorHex in case of CLR material
    //               if(MATERIAL.type == 'CLR'){
    //                 modal.material.colorHex = MATERIAL.val;
    //                 modal.colorPicker.options.color = MATERIAL.val.replace('#','');
    //               }else {
    //                 modal.material.colorHex = '#ffffff'; //white as default
    //                 modal.colorPicker.options.color = modal.material.colorHex.replace('#','');
    //               }
    //
    //               if(MATERIAL.type == 'IMG'){
    //                 modal.material.val = MATERIAL.val;
    //                 modal.material.imgSrc = MATERIAL.val;
    //                 modal.imgArr.push(modal.baseURL + MATERIAL.val);
    //               }
    //             }
    //             else {
    //               modal.material = {
    //                 default: false,
    //                 type: "CLR",
    //                 nameEN: "",
    //                 nameCH: "",
    //                 descCH: "",
    //                 descEN: "",
    //                 tech: "",
    //                 enable: true,
    //                 visible: true,
    //                 price: 0,
    //                 //stock: MATERIAL.stock,
    //                 //status: "",
    //                 //hasOverlay: "",
    //                 //color: "",
    //                 //opacity: "",
    //                 material_id: null,
    //                 colorHex: '#ffffff'
    //               };
    //               modal.colorPicker.options.color = 'ffffff';
    //             }
    //
    //             modal.removeImage = function () {
    //               modal.material.imgSrc = null; //faking image remove, this shows the uploader to user again.
    //             };
    //
    //             modal.uploadImage = function (file) {
    //               if(typeof (MATERIAL) == 'undefined'){
    //                 Notify.infoSaveChanges();
    //                 return;
    //               }
    //
    //               modal.uploading = true;
    //               modal.uploadProgress = 0;
    //               var uploadUrl = MIID_API.base_url + '/products/media/'+ PRODUCT +'/steps/' + STEP.step_id+ '/groups/'+ GROUP.group_id +'/'+ GROUP.type +'/' + MATERIAL.material_id;
    //               Upload.upload({
    //                 url: uploadUrl,
    //                 data: {file: file}
    //               }).then(function (resp) {
    //                 modal.uploading = false;
    //                 if(resp.data.result == 'success') {
    //                   Notify.infoImageSaved();
    //                   modalInstance.close({
    //                     path: resp.data.link
    //                   });
    //                   return;
    //                 }
    //                 Notify.errorSaving();
    //
    //               }, function (err) {
    //                 modal.uploading = false;
    //                 Notify.errorSaving();
    //
    //               }, function (evt) {
    //                 modal.uploadProgress = parseInt(100.0 * evt.loaded / evt.total);
    //               });
    //             };
    //
    //             modal.setTabActive = function(tab){
    //               modal.activeTab = tab;
    //             };
    //
    //             modal.close = function(){
    //               $uibModalInstance.dismiss('cancel');
    //             };
    //
    //             modal.submit = function(){
    //
    //               $uibModalInstance.close({
    //                 material: modal.material,
    //                 group: GROUP,
    //                 step: STEP
    //               });
    //
    //             };
    //
    //           }],
    //         controllerAs: 'modal',
    //         backdrop: 'static' //disables click outside close
    //       });
    //
    //   modalInstance.result
    //     .then(function(data){
    //       if (data.path) {
    //         material.type = 'IMG';
    //         material.val = data.path;
    //         return;
    //       }
    //       var stepNo = _.findIndex(vm.steps_info.steps, data.step);
    //       if(stepNo != -1){
    //         var groupNo = _.findIndex(vm.steps_info.steps[stepNo].groups, data.group);
    //         if( groupNo != -1) {
    //           var postData = {
    //             default: data.material.default,
    //             tech: data.material.tech,
    //             element: {
    //               type: data.material.type,
    //               val: data.material.imgSrc
    //             },
    //             en: {
    //               name: data.material.nameEN,
    //               desc: data.material.descEN
    //             },
    //             ch: {
    //               name: data.material.nameCH,
    //               desc: data.material.descCH
    //             },
    //             price:  data.material.price,
    //             visible: data.material.visible,
    //             enable: data.material.enable
    //           };
    //
    //           if(data.material.type == 'CLR'){
    //             postData.element.val = data.material.colorHex;
    //             data.material.val = data.material.colorHex;
    //           } else if (data.material.type == 'TXT'){
    //             postData.element.val = data.material.nameEN;// TODO:  need to update this and select the language from locale to get name of that language
    //             data.material.val = data.material.nameEN;
    //           }
    //
    //           vm.saveMaterial(data.step, data.group, data.material, postData);
    //         }
    //       }
    //     },function(canceled){
    //     });
    // };

    // vm.editMaterial = function(group,step, material){
    //   vm.addMaterial(group,step, material, true);
    // };

    vm.deleteMaterial = function(step,group,material){
      if(!material.material_id) {
        group.materials = _.without(group.materials, material);
        return;
      }
      material.loading = true;
      API
        .UI
        .remove(productId, step.step_id, group.group_id,  material.material_id)
        .then(function (deletedMaterial) {
          var index = _.findIndex(vm.steps_info.steps,step);
          if(index != -1) {
            var groupIdx = _.findIndex(vm.steps_info.steps[index].groups, group);
            if(groupIdx != -1) {
              vm.steps_info.steps[index].groups[groupIdx].materials = _.without(vm.steps_info.steps[index].groups[groupIdx].materials, material);
            }
          }

          Notify.infoDeleted();
          // check if need to update default material
          if(material.default == true && group.materials.length > 0){
            var newDefault = group.materials[0];
            newDefault.default = true;
            vm.setDefaultMaterial(step,group,newDefault);
          }
        }).catch(function (err) {
        material.loading = false;
        Notify.errorSaving();
      });
    };

    vm.setUpPersonalize = function (step, group, personalize) {
      var modalInstance = Modal.open(
        'ProductCreateSetUpPersonalizeModal',
        'modal',
        'app/modules/product/create/customization_description/set_up_personalize/product.create.customization_description.setUpPersonalizeModal.html',
        {
          PRODUCT: function () { return vm.productID},
          STEP: function () { return step},
          GROUP: function () { return group},
          PERSONALIZE: function () { return personalize}
        },
        'personalize-lg',
        null,
        null,
        'personalize-modal'
      );

      modalInstance.result.then(function (updatedButtonOptions) {
        _.extend(personalize, updatedButtonOptions);
      });

      modalInstance.opened.then(function () {
        $timeout(function () {
          $scope.$broadcast('rzSliderForceRender');
        });
      });
    };

    // get all steps, groups and UIs existing for selected product
    vm.loadSteps = function () {
      vm.cachedData = [];
      vm.steps_info.steps = [];
      vm.loading = true;
      API
        .Steps
        .query(productId)
        .then(function(steps) {
          steps.forEach(function(step) {
            var oldStep = {
              id: step.icon,
              title: step[vm.lang]['title'],
              tech: step.tech || '',
              angle: step.angle,
              step_id: step._id,
              stepFinalized: true,
              groups : [],
              enable: step.enable,
              visible: step.visible
            };
            step.groups.forEach(function(group) {
              var oldGroup = {
                type: group.type,
                title: group[vm.lang]['title'],
                tech: group.tech || '',
                id: "",
                group_id: group._id,
                multipleSelection: group.multiple_selection || false,
                enable: group.enable,
                visible: group.visible,
                groupFinalized : true,
                materials: []
              };

              if(group.type == 'INPUT') {
                group.inputs.forEach(function (input) {
                  input.material_id = input._id;
                  input.materialFinalized = true; //hides input fields
                  input.default = input.enable || false;
                  delete input.punctuation;
                  delete input.ch.tag;
                  delete input.ch.placeholder;
                  delete input.en.tags;
                  delete input._id;
                  if (input.element.type === 'TXT')
                    delete input.element.val;

                  if(input.element.type == 'IMG'){
                    input.imgArr = [
                      vm.baseURL + input.element.val
                    ];
                    input.imgSrc = input.element.val;
                    input.removeImage = function () {
                      input.imgSrc = null;
                    };
                  }
                  if (input.fonts.length > 0)
                    input.fonts = _.filter(vm.fonts, function (val) {
                      return _.find(input.fonts, {name: val.name});
                    });

                  input.noFonts = input.fonts.length < 1;

                  oldGroup.materials.push(input);
                });
              }else if(group.type === 'SELECTABLE'){

                group.selectable.forEach(function (select) {
                  select.material_id = select._id;
                  select.materialFinalized = true; //hides input fields
                  delete select.ch.tag;
                  delete select.en.tags;
                  delete select._id;
                  if (select.element.type === 'TXT')
                    delete select.element.val;
                  if(select.element.type == 'IMG'){
                    select.imgArr = [
                      vm.baseURL + select.element.val
                    ];
                    select.imgSrc = select.element.val;
                    select.removeImage = function () {
                      select.imgSrc = null;
                    };
                  }
                  select.options.forEach(function (sel) {
                    delete sel._id;
                  });

                  oldGroup.materials.push(select);
                });

              } else if (group.type === 'PERSONALIZE'){

                group.personalize.forEach(function (personalize) {
                  personalize.material_id = personalize._id;
                  personalize.materialFinalized = true; //hides input fields
                  delete personalize.ch.tag;
                  delete personalize.en.tags;
                  delete personalize._id;
                  if (personalize.element.type === 'TXT')
                    delete personalize.element.val;
                  if(personalize.element.type == 'IMG'){
                    personalize.imgArr = [
                      vm.baseURL + personalize.element.val
                    ];
                    personalize.imgSrc = personalize.element.val;
                    personalize.removeImage = function () {
                      personalize.imgSrc = null;
                    };
                  }
                  oldGroup.materials.push(personalize);
                });

              } else if(group.type === 'CUSTOM_IMG'){

                group['custom_images'].forEach(function (cIMG) {
                  cIMG.material_id = cIMG._id;
                  cIMG.materialFinalized = true; //hides input fields
                  delete cIMG.ch.tag;
                  delete cIMG.en.tags;
                  delete cIMG._id;
                  if (cIMG.element.type === 'TXT')
                    delete cIMG.element.val;
                  if(cIMG.element.type == 'IMG'){
                    cIMG.imgArr = [
                      vm.baseURL + cIMG.element.val
                    ];
                    cIMG.imgSrc = cIMG.element.val;
                    cIMG.removeImage = function () {
                      cIMG.imgSrc = null;
                    };
                  }

                  oldGroup.materials.push(cIMG);
                });

              } else {
                group.ui.forEach(function(ui) {
                  var oldMaterial = {
                    default: (ui.default)? ui.default : false,
                    materialFinalized: true,
                    name: ui[vm.lang].name,
                    description: ui[vm.lang].desc,
                    tech: ui.tech,
                    tech2: ui.tech2 || '',
                    tech3: ui.tech3 || '',
                    element: ui.element,
                    val: ui.element && ui.element.val,
                    prodCost: ui['prod_cost'],
                    ipCost: ui['ip_cost'],
                    enable: ui.enable,
                    visible: ui.visible,
                    price: ui.price,
                    status: ui.status,
                    material_id: ui._id
                  };
                  if(oldMaterial.element && oldMaterial.element.type == 'IMG'){
                    oldMaterial.imgArr = [
                      vm.baseURL + oldMaterial.element.val
                    ];
                    oldMaterial.imgSrc = oldMaterial.element.val;
                    oldMaterial.removeImage = function () {
                      oldMaterial.imgSrc = null;
                    };
                  }
                  oldGroup.materials.push(oldMaterial);
                });
              }
              oldStep.groups.push(oldGroup);
            });
            vm.steps_info.steps.push(oldStep);
          });
          vm.cachedData = angular.copy(vm.steps_info.steps);
          vm.loading = false;
        }).catch(function(err) {
        Notify.errorSaving();
        vm.loading = false;
      });

    };

    vm.loadSteps();

    //condition start

    //upload steps titles
    vm.steps_titles = [];
    //upload conditions
    vm.conditions = [];

    vm.loadConditions = function () {
      vm.loadingConditions = true;

      API
        .Conditions
        .query(vm.productID)
        .then(function (data) {
          vm.conditions = data;
        })
        .catch(function () {
          ALERT
            .confirm(vm.loadConditions);
        })
        .finally(function () {
          vm.loadingConditions = false;
        });

    };

    vm.addOrEditCondition = function (condition) {
      var modalInstance =
        $uibModal
          .open({
            animation: true,
            size: 'lg',
            resolve: {
              CONDITION: function () {
                if (condition) {
                  return angular.copy(condition);
                } else {
                  return false;
                }
              },
              //update case if condition in argument
              PRODUCT_ID: function () {
                return productId;
              }
            },
            templateUrl: 'app/modules/product/create/customization_description/conditions_modal/conditions.modal.html',
            controller: 'ProductCreateConditionsModal',
            controllerAs: "modal",
            backdrop: 'static' //disables click outside close
          });

      modalInstance
        .result
        .then(function (data) {
        }, function (canceled) {})
        .finally(function () {
          vm.loadConditions();
        });
    };

    vm.deleteConditionConfirm = function (condition) {
      ALERT
        .confirmWarning(function () {
          return vm.deleteCondition(condition);
        });
    };

    vm.deleteCondition = function (condition) {
      vm.loadingConditions = true;

      API
        .Conditions
        .remove(vm.productID, condition._id)
        .then(function () {
          Notify.infoDeleted();
          vm.conditions = _.without(vm.conditions, condition);
        })
        .catch(function () {
          Notify.errorSaving();
        })
        .finally(function () {
          vm.loadingConditions = false;
        });
    };

    vm.duplicateCondition = function (condition) {
      //remove id
      vm.loadingConditions = true;
      var duplicate = angular.copy(condition);
      delete duplicate._id;

      duplicate._if.elements = duplicate._if.elements.map(removeIfThenIds);
      duplicate._then.elements = duplicate._then.elements.map(removeIfThenIds);

      API
        .Conditions
        .save(vm.productID, duplicate)
        .then(function (data) {
          Notify.infoSaved();
          vm.loadConditions();
        }).catch(function (err) {
        Notify.errorSaving();
      })
        .finally(function () {
          vm.loadingConditions = true;
        });
    };

    vm.loadProductsData = function () {
      API
        .Products
        .get(vm.productID)
        .then(function (data) {
          vm.productName = data.en.name;
        });
    };

    function removeIfThenIds(el) {
      el = _.pick(el, _.identity);
      delete el._id;
      delete el.title;
      return el;
    }

    //load conditions data
    vm.loadConditions();
    vm.loadProductsData();
    //conditions finish
  }

})(angular.module('adminPanel.product.create.customization_desc.controller',[]));
