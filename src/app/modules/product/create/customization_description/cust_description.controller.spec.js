describe('Controllers', function () {
  describe('ProductCustomizationController', function () {

    var data =
        [{
          "angle": 0,
          "_id": "573b1389fc0f5c0500d461ea",
          "groups": [{
            "type": "INPUT",
            "_id": "573b14acfc0f5c0500d461eb",
            "inputs": [{
              "tech": "test changed",
              "_id": "573e0001bea659d4300c71d6",
              "price": 20,
              "punctuation": null,
              "lang": ["EN"],
              "fonts": [{"name": "Montserrat Bold"}, {"name": "Centhury Gothic Bold"}],
              "max_char": 10,
              "min_char": 1,
              "ch": {"name": "test hk"},
              "en": {"name": "test hk", "desc": "test", "placeholder": "Write something"},
              "element": {"type": "TXT", "val": "pr/5739f69dfc0f5c0500d4618b/ui_menu//573e0001bea659d4300c71d6.jpg"},
              "enable": true,
              "visible": false
            }],
            "ch": {"title": "TEST INPUT"},
            "en": {"title": "TEST INPUTsad"},
            "enable": false,
            "visible": true
          }, {
            "type": "SELECTABLE",
            "_id": "573b14acfc0f5c0500d461eb",
            "ch": {"title": "test hk"},
            "en": {"title": "test hk"},
            "enable": true,
            "visible": false,
            selectable: [
              {
                ch: {name: 'asdf'},
                enable: true,
                visible: false,
                en: {name: 'asdfsadf'},
                "element": {"type": "TXT", "val": "txt"},
                options: [
                  {
                    is_default: true,
                    val: 'saf',
                    price: 23,
                    tech: '323'
                  }
                ]
              }
            ]
          }],
          ch: {title: "TEST"},
          en: {title: "TEST"},
          enable: true
        }];

    var
      Controller,
      $stateParams,
      URL,
      $rootScope,
      toastr,
      Upload,
      Notify,
      ALERT,
      $httpBackend,
      stepURL,
      inputURL,
      uploadURL;


    // load the dependencies module
    beforeEach(module('ui.router'));
    beforeEach(module('ngResource'));
    beforeEach(module('oitozero.ngSweetAlert'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('toastr'));
    beforeEach(module('ui.bootstrap'));
    beforeEach(module('adminPanel.services.notifier'));
    beforeEach(module('ngFileUpload'));
    beforeEach(module('adminPanel.api'));
    beforeEach(module('adminPanel.services.alert'));
    beforeEach(module('adminPanel.product.create.customization_desc.controller'));

    beforeEach(inject(function ($injector) {
      $rootScope = $injector.get('$rootScope');
      $stateParams = $injector.get('$stateParams');
      $stateParams.productID = '12345';
      $httpBackend = $injector.get('$httpBackend');
      toastr = $injector.get('toastr');
      ALERT = $injector.get('ALERT');
      Upload = $injector.get('Upload');
      Notify = $injector.get('Notify');
      URL = $injector.get('URL');
      var $controller = $injector.get('$controller');
      Controller = $controller('ProductCustomizationController', {
        '$scope': $rootScope,
        '$uibModal': $injector.get('$uibModal'),
        '$window': $injector.get('$window'),
        'QINGSOTR': $injector.get('QINGSTOR'),
        '$timeout': $injector.get('$timeout'),
        'toastr': toastr,
        'MIID_API': $injector.get('MIID_API'),
        'Upload': Upload,
        'API': $injector.get('API'),
        '$stateParams': $stateParams,
        'ALERT': ALERT,
        'Notify': Notify
      });
      stepURL = URL.STEPS.replace(':productId', '12345');
      inputURL = URL.GROUPS.replace(':productId','12345');
      inputURL = inputURL.replace(':stepId',data[0]._id);
      inputURL += '/' + data[0].groups[0]._id + '/inputs';
      uploadURL = URL.GROUPS.substring(0, URL.GROUPS.indexOf(':productId')) + 'media/' + URL.GROUPS.substring(URL.GROUPS.indexOf(':productId'));
      uploadURL = uploadURL.replace(':productId','12345');
      uploadURL = uploadURL.replace(':stepId',data[0]._id);
      uploadURL = uploadURL.replace('groupId', data[0].groups[0]._id);
      uploadURL += '/' + data[0].groups[0]._id + '/' + data[0].groups[0].type + '/' + data[0].groups[0].inputs[0]._id;

      $httpBackend.when('GET', stepURL).respond(200, data);
      $httpBackend.when('GET', URL.PRODUCTS + '/12345/conditions').respond(200, data);
      $httpBackend.flush();
    }));

    describe('MBA-341: Input Groups', function () {

      it('fill set steps', function () {
        expect(Controller.steps_info.steps.length).toEqual(1);
      });

      it('it should add INPUT GROUP', function () {
        Controller.createGroup('INPUT', Controller.steps_info.steps[0]);
        expect(Controller.steps_info.steps[0].groups.length).toEqual(3);
        expect(Controller.steps_info.steps[0].groups[0].type).toEqual('INPUT');
      });

      it('should open dialog for confirmation when click on delete material', function () {
        spyOn(ALERT, 'confirm');
        Controller.deleteMaterialConfirm(Controller.steps_info.steps[0], Controller.steps_info.steps[0].groups[0], Controller.steps_info.steps[0].groups[0].materials[0],0,0,0, 'INPUT');

        expect(ALERT.confirm).toHaveBeenCalled();
      });

      it('should delete text material and call toastr on success', function () {
        spyOn(toastr, 'info');
        $httpBackend.expect('DELETE', inputURL + '/' + data[0].groups[0].inputs[0]._id).respond(200, {success:true});
        Controller.deleteCustomMaterial(Controller.steps_info.steps[0], Controller.steps_info.steps[0].groups[0], Controller.steps_info.steps[0].groups[0].materials[0],0,0,0, 'INPUT');
        $httpBackend.flush();
        expect(toastr.info).toHaveBeenCalled();
        expect(Controller.steps_info.steps[0].groups[0].materials.length).toEqual(0);
      });

      it('should not delete text material and call toastr error on server error', function () {
        spyOn(toastr, 'error');
        $httpBackend.expect('DELETE', inputURL + '/' + data[0].groups[0].inputs[0]._id).respond(500, {success:true});
        Controller.deleteCustomMaterial(Controller.steps_info.steps[0], Controller.steps_info.steps[0].groups[0], Controller.steps_info.steps[0].groups[0].materials[0],0,0,0, 'INPUT');
        $httpBackend.flush();
        expect(toastr.error).toHaveBeenCalled();
        expect(Controller.steps_info.steps[0].groups[0].materials.length).not.toEqual(0);
      });

      it('should add text material and set currMaterial', function () {
        Controller.addTextMaterial(Controller.steps_info.steps[0].groups[0]);
        expect(Controller.currMaterial).toBeDefined();
        expect(Controller.steps_info.steps[0].groups[0].materials.length).toEqual(2);
      });

      it('should set file to the currMaterial', function () {
        Controller.currMaterial = {}; //faking
        Controller.saveImage('bla bla');
        expect(Controller.currMaterial.file).toEqual('bla bla');
      });

      it('should upload image to server and call toastr', function () {
        Controller.steps_info.steps[0].groups[0].materials[0].file = {};//faking
        spyOn(toastr, 'info');
        $httpBackend.expect('POST', uploadURL).respond(200, {result: 'success'});
        Controller.uploadImage(Controller.steps_info.steps[0], Controller.steps_info.steps[0].groups[0], Controller.steps_info.steps[0].groups[0].materials[0]);
        $httpBackend.flush();
        expect(toastr.info).toHaveBeenCalled();
      });

      it('should not upload image to server and call error toastr', function () {
        Controller.steps_info.steps[0].groups[0].materials[0].file = {};//faking
        spyOn(toastr, 'error');
        $httpBackend.expect('POST', uploadURL).respond(500, {result: 'false'});
        Controller.uploadImage(Controller.steps_info.steps[0], Controller.steps_info.steps[0].groups[0], Controller.steps_info.steps[0].groups[0].materials[0]);
        $httpBackend.flush();
        expect(toastr.error).toHaveBeenCalled();
      });

      it('should create material when type is text', function () {
        Controller.addTextMaterial(Controller.steps_info.steps[0].groups[0]); // adding a fake material
        spyOn(toastr,'info');
        $httpBackend.expect('POST', inputURL).respond(200, {_id: '123456'});
        Controller.addUpdateCustomMaterial('TEXT', Controller.steps_info.steps[0], Controller.steps_info.steps[0].groups[0], Controller.steps_info.steps[0].groups[0].materials[1], 0, 0, 1);
        $httpBackend.flush();
        expect(toastr.info).toHaveBeenCalled();
        expect( Controller.steps_info.steps[0].groups[0].materials[1].material_id).toEqual('123456');
      });

      it('should create material and upload img type is IMG', function () {
        Controller.addTextMaterial(Controller.steps_info.steps[0].groups[0]); // adding a fake material

        Controller.steps_info.steps[0].groups[0].materials[1].element.type = 'IMG'; //faking
        Controller.steps_info.steps[0].groups[0].materials[1].file = 'fake'; //faking

        spyOn(Controller, 'uploadImage');
        $httpBackend.expect('POST', inputURL).respond(200, {_id: '123456'});
        Controller.addUpdateCustomMaterial('TEXT', Controller.steps_info.steps[0], Controller.steps_info.steps[0].groups[0], Controller.steps_info.steps[0].groups[0].materials[1],0,0,1);
        $httpBackend.flush();
        expect(Controller.uploadImage).toHaveBeenCalled();
      });

      it('should edit material when type is text', function () {
        spyOn(toastr, 'info');
        $httpBackend.expect('PUT', inputURL + '/' + data[0].groups[0].inputs[0]._id).respond(200, {_id: '123456'});
        Controller.addUpdateCustomMaterial('TEXT', Controller.steps_info.steps[0], Controller.steps_info.steps[0].groups[0], Controller.steps_info.steps[0].groups[0].materials[0],0,0,0);
        $httpBackend.flush();
        expect(toastr.info).toHaveBeenCalled();
      });

      it('should edit material and update img when type is IMG', function () {
        Controller.steps_info.steps[0].groups[0].materials[0].element.type = 'IMG'; //faking
        Controller.steps_info.steps[0].groups[0].materials[0].file = 'fake'; //faking

        spyOn(Controller, 'uploadImage');
        $httpBackend.expect('PUT', inputURL + '/' + data[0].groups[0].inputs[0]._id).respond(200, {_id: '123456'});
        Controller.addUpdateCustomMaterial('TEXT', Controller.steps_info.steps[0], Controller.steps_info.steps[0].groups[0], Controller.steps_info.steps[0].groups[0].materials[0],0,0,0);
        $httpBackend.flush();
        expect(Controller.uploadImage).toHaveBeenCalled();
      });

      it('when editing material should call toastr if img not selected', function () {
        Controller.steps_info.steps[0].groups[0].materials[0].element.type = 'IMG'; //faking
        Controller.steps_info.steps[0].groups[0].materials[0].file = null; //faking

        spyOn(toastr, 'error');
        Controller.addUpdateCustomMaterial('TEXT', Controller.steps_info.steps[0], Controller.steps_info.steps[0].groups[0], Controller.steps_info.steps[0].groups[0].materials[0]);
        expect(toastr.error).toHaveBeenCalled();
      });

    });

    describe('MBA-380: Selectable', function () {

      it('should add option to material', function () {
        Controller.addSelectableOption(Controller.steps_info.steps[0].groups[1].materials[0])
        expect(Controller.steps_info.steps[0].groups[1].type).toEqual('SELECTABLE');
        expect(Controller.steps_info.steps[0].groups[1].materials[0].options.length).toEqual(2);
      });

      it('should add selectable material', function () {
        Controller.addSelectable(Controller.steps_info.steps[0].groups[1]);
        expect(Controller.steps_info.steps[0].groups[1].materials.length).toEqual(2);
      });

      it('should change selectable option default', function () {
        Controller.addSelectableOption(Controller.steps_info.steps[0].groups[1].materials[0]);
        Controller.changeSelectableOptionDefault(Controller.steps_info.steps[0].groups[1].materials[0], 1);
        expect(Controller.steps_info.steps[0].groups[1].materials[0].options[1].is_default).toBeTruthy();
      });

      it('should delete selectable option ', function () {
        Controller.addSelectableOption(Controller.steps_info.steps[0].groups[1].materials[0]);
        Controller.deleteSelectableOption(Controller.steps_info.steps[0].groups[1].materials[0], Controller.steps_info.steps[0].groups[1].materials[0].options[1]);
        expect(Controller.steps_info.steps[0].groups[1].materials[0].options.length).toEqual(1);
      });

    });

  });

});
