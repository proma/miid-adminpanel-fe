(function(app){

})(angular.module('adminPanel.product.create.customization_desc',
  [
    'adminPanel.product.create.customization_desc.routes',
    'adminPanel.product.create.customization_desc.controller',
    'adminPanel.product.create.customization_desc.conditions_modal',
    'adminPanel.product.create.customization_desc.setUpPersonalizeModal'
  ]
));
