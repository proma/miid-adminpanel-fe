(function (app) {

  app.controller('ProductCreateSetUpPersonalizeSelectColorModal', ProductCreateSetUpPersonalizeSelectColorModal);

  ProductCreateSetUpPersonalizeSelectColorModal.$inject = ['COLORS' , 'KEY', '$uibModalInstance'];

  function ProductCreateSetUpPersonalizeSelectColorModal(COLORS, KEY, $uibModalInstance) {
    var colorModal = this;

    colorModal.chosenColor = '#FFFFFF';
    colorModal.colors = COLORS.fixed_colors;
    colorModal.type = COLORS.def_value.mode;
    colorModal.defaultColor = COLORS.def_value.value || '#FFFFFF';
    colorModal.removeColor = function (color) {
      colorModal.colors = _.without(colorModal.colors, color);
    };

    colorModal.colorPicker = {
      options: {flat: true, showButtons: false}
    };

    colorModal.addColor = function (color) {
      if (colorModal.colors.indexOf(color) == -1 && (color.indexOf('#') != -1) && (color.length == 7 || color.length == 4)) {
        colorModal.colors.push(color);
      }
    };

    colorModal.submit = function () {
      $uibModalInstance.close({
        key: KEY,
        fixedColors: colorModal.colors,
        mode: colorModal.type,
        defaultValue: colorModal.defaultColor
      })
    };

    colorModal.close = function () {
      $uibModalInstance.dismiss();
    };

  }

})(angular.module('adminPanel.product.create.customization_desc.setUpPersonalizeModal.selectColorModal.controller', []));

