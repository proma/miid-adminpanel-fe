(function (app) {

  app.controller('ProductCreateSetUpPersonalizeAddGalleriesModal', ProductCreateSetUpPersonalizeAddGalleriesModal);

  ProductCreateSetUpPersonalizeAddGalleriesModal.$inject = ['GALLERIES', 'API', '$uibModalInstance', 'Notify'];

  function ProductCreateSetUpPersonalizeAddGalleriesModal(GALLERIES, API, $uibModalInstance, Notify) {
    var galleriesModal = this;
    GALLERIES = GALLERIES || [];

    galleriesModal.loadGalleries = function () {
      galleriesModal.loading = true;

      API
        .Galleries
        .query({list_type: 'LIST_VALUES'})
        .then(function (response) {
          galleriesModal.galleries = response.data.map(function (gallery) {
            gallery.checked = (_.where(GALLERIES, {_id: gallery._id}).length > 0);
            return gallery;
          });
        })
        .finally(function () {
          galleriesModal.loading = false;
        })
    };

    galleriesModal.submit = function () {
      $uibModalInstance.close(
        _.chain(galleriesModal.galleries)
          .where({checked: true})
          .map(function (v) { return {_id: v._id} })
          .value()
      );
    };

    galleriesModal.close = function () {
      $uibModalInstance.dismiss();
    };

    galleriesModal.loadGalleries();
  }

})(angular.module('adminPanel.product.create.customization_desc.setUpPersonalizeModal.addGalleriesModal.controller', []));

