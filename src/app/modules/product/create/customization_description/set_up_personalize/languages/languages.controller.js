(function (app) {

  app.controller('ProductCreateSetUpPersonalizeAddLanguageModal', ProductCreateSetUpPersonalizeAddLanguageModal);

  ProductCreateSetUpPersonalizeAddLanguageModal.$inject = ['LANGUAGES', '$uibModalInstance'];

  function ProductCreateSetUpPersonalizeAddLanguageModal(LANGUAGES, $uibModalInstance) {
    var language = this;

    language.list = LANGUAGES;

    language.submit = function () {
      $uibModalInstance.close(language.list);
    };

    language.close = function () {
      $uibModalInstance.dismiss();
    };

  }

})(angular.module('adminPanel.product.create.customization_desc.setUpPersonalizeModal.languages.controller', []));

