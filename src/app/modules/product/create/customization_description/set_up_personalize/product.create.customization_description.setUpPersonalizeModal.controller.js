(function (app) {

  app.controller('ProductCreateSetUpPersonalizeModal', ProductCreateSetUpPersonalizeModal);

  ProductCreateSetUpPersonalizeModal.$inject = ['$scope', '$timeout', 'PRODUCT', 'STEP', 'GROUP', 'PERSONALIZE', 'API', '$uibModalInstance', 'Notify', 'Modal', 'Upload', 'MIID_API', 'QINGSTOR', '$http'];

  function ProductCreateSetUpPersonalizeModal($scope, $timeout, PRODUCT, STEP, GROUP, PERSONALIZE, API, $uibModalInstance, Notify, Modal, Upload, MIID_API, QINGSTOR, $http) {
    var modal = this;

    modal.assetURL = QINGSTOR.base_url;
    modal.background = {
      uploading: false,
      progress: 0
    };
    modal.mask = {
      uploading: false,
      progress: 0
    };


    modal.personalize = angular.copy(PERSONALIZE);
    modal.oldPersonalize = angular.copy(PERSONALIZE);
    console.log(modal.personalize);

    //Wait to load the template before initializing Canvas
    $timeout(function () {
      modal.svgPreviewCanvas = new fabric.Canvas('svgPreviewCanvas');

      if (modal.personalize['mask_image']) {
        modal.changeSvgMask(modal.assetURL + modal.personalize['mask_image']);
      }
    }, 2000);

    modal.changeSvgMask = function (svgPath) {
      $http.get(svgPath).success(function (data) {
        modal.svgPreviewCanvas.clear();
        fabric.loadSVGFromString(data, function(objects, options) {
          var loadedObjects = fabric.util.groupSVGElements(objects, options);
          loadedObjects.set({
            lockMovementX : true,
            lockMovementY : true,
            lockScalingX : true,
            lockScalingY : true,
            lockUniScaling : true,
            lockRotation : true,
            selectable: false,
            hasRotatingPoint:false,
            top: 0,
            left: 0,
            scaleY: modal.svgPreviewCanvas.height / loadedObjects.height,
            scaleX: modal.svgPreviewCanvas.width / loadedObjects.width
          });
          $scope.$apply(function () {
            modal.svgPreviewCanvas.add(loadedObjects).renderAll();
          });
        });
      });
    };

    modal.getPercentageValue = function (value) {
      return value + '%';
    };

    modal.getPixelValue = function (value) {
      return value + 'px';
    };

    modal.reRenderSlider = function () {
      $timeout(function () {
        $scope.$broadcast('rzSliderForceRender');
      });
    };

    modal.uploadAssets = function (file, uploader, type) {
      if(!file) return; //bug in uploader

      var url = MIID_API.base_url + '/products/media/'+ PRODUCT +'/steps/'+ STEP.step_id +'/groups/'+ GROUP.group_id +'/PERSONALIZE/'+ PERSONALIZE.material_id +'/';
      if (type == 'background') {
        url += 'bg_image';
      } else {
        url += 'mask_image';
      }

      uploader.uploading = true;
      uploader.progress = 0;

      Upload.upload({
        'url': url,
        data: {'file': file}
      })
        .then(function (resp) {
          modal.personalize[type == 'background'? 'bg_image': 'mask_image'] = resp.data.link + '?' + Date.now();
          if (type != 'background') {
            modal.changeSvgMask(modal.assetURL + modal.personalize['mask_image']);
          }
          Notify.infoImageSaved();
        }, function () {
          Notify.errorSaving();
        }, function (evt) {
          uploader.progress = parseInt(100.0 * evt.loaded / evt.total);
        })
        .finally(function () {
          uploader.uploading = false;
        });
    };

    modal.enableButton = function () {
      return angular.equals(modal.personalize, modal.oldPersonalize);
    };

    modal.setFonts = function () {
      Modal.open(
        'ProductCreateSetUpPersonalizeAddFontsModal',
        'fonts',
        'app/modules/product/create/customization_description/set_up_personalize/add_fonts/addFontsModal.html',
        {
          FONTS: function () { return modal.personalize.options.texts.sub.text_font.available_fonts; }
        },
        null,
        null,
        null,
        'sub-modal'
      )
        .result
        .then(function (fonts) {
          modal.personalize.options.texts.sub.text_font.available_fonts = fonts;
        });
    };

    modal.setColor = function (key) {
      Modal.open(
        'ProductCreateSetUpPersonalizeSelectColorModal',
        'colorModal',
        'app/modules/product/create/customization_description/set_up_personalize/select_color/selectColorModal.html',
        {
          COLORS: function () { return fetchAsStringFromObject(modal.personalize, key); },
          KEY: function () { return key; }
        },
        'md',
        null,
        null,
        'sub-modal'
      )
        .result
        .then(function (obj) {
          var colorObj = fetchAsStringFromObject(modal.personalize, obj.key);
          colorObj.fixed_colors = obj.fixedColors;
          colorObj.def_value.mode = obj.mode;
          colorObj.def_value.value = obj.defaultValue;
        });

      function fetchAsStringFromObject(obj, prop) {
        if(typeof obj === 'undefined') {
          return false;
        }
        var _index = prop.indexOf('.');
        if(_index > -1) {
          return fetchAsStringFromObject(obj[prop.substring(0, _index)], prop.substr(_index + 1));
        }
        return obj[prop];
      }
    };

    modal.setGalleries = function () {
      Modal.open(
          'ProductCreateSetUpPersonalizeAddGalleriesModal',
          'galleriesModal',
          'app/modules/product/create/customization_description/set_up_personalize/add_galleries/addGalleriesModal.html',
          {
            GALLERIES: function () { return modal.personalize.galleries; }
          },
          null,
          null,
          null,
          'sub-modal'
        )
        .result
        .then(function (pickedGalleries) {
          modal.personalize.galleries = pickedGalleries;
        });
    };

    modal.setLanguages = function () {
      Modal.open(
        'ProductCreateSetUpPersonalizeAddLanguageModal',
        'language',
        'app/modules/product/create/customization_description/set_up_personalize/languages/languages.html',
        {
          LANGUAGES: function () { return modal.personalize['available_languages'] || {ch: true, en: true}; }
        },
        null,
        null,
        null,
        'sub-modal'
      )
        .result
        .then(function (languages) {
          modal.personalize['available_languages'] = languages;
        });
    };

    modal.submit = function () {
      modal.processing = true;
      var postData = angular.copy(modal.personalize);

      delete postData.materialFinalized;
      delete postData.material_id;
      delete postData.imgSrc;
      delete postData.imgArr;
      delete postData.loading;
      delete postData.file;
      delete postData.uploadProgress;
      delete postData.uploading;

      if (!modal.personalize['options']['texts']['sub']['text_font']['def_value'])
        delete postData['options']['texts']['sub']['text_font']['def_value'];
      if (!modal.personalize['options']['texts']['sub']['text_line']['def_value'])
        delete postData['options']['texts']['sub']['text_line']['def_value'];

      API
        .Personalize
        .edit(PRODUCT, STEP.step_id, GROUP.group_id, PERSONALIZE.material_id, postData)
        .then(function () {
          modal.oldPersonalize = angular.copy(modal.personalize);
          $timeout(function () {
            $uibModalInstance.close(modal.personalize);
          }, 500);
          Notify.infoSaved();
        })
        .catch(function () {
          Notify.errorSaving();
        })
        .finally(function () {
           modal.processing = false;
        });
    };

    modal.close = function () {
      $uibModalInstance.dismiss();
    };


    $timeout(modal.reRenderSlider, 500);
  }

})(angular.module('adminPanel.product.create.customization_desc.setUpPersonalizeModal.controller', []));

