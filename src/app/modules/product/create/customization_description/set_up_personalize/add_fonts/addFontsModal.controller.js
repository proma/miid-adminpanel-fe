(function (app) {

  app.controller('ProductCreateSetUpPersonalizeAddFontsModal', ProductCreateSetUpPersonalizeAddFontsModal);

  ProductCreateSetUpPersonalizeAddFontsModal.$inject = ['FONTS', '$uibModalInstance'];

  function ProductCreateSetUpPersonalizeAddFontsModal(FONTS, $uibModalInstance) {
    var fonts = this;

    fonts.defaultFonts =
      [
        {
          name: 'Impact'
        },
        {
          name: 'Palatino Linotype'
        },
        {
          name: 'Tahoma'
        },
        {
          name: 'Century Gothic'
        },
        {
          name: 'Lucida Sans Unicode'
        },
        {
          name: 'Arial Black'
        },
        {
          name: 'Arial'
        },
        {
          name: 'Times New Roman'
        },
        {
          name: 'Arial Narrow'
        },
        {
          name: 'Verdana'
        },
        {
          name: 'Lucida Console'
        },
        {
          name: 'Courier New'
        },
        {
          name: 'Georgia'
        }
      ];

    fonts.submit = function () {
      $uibModalInstance.close(
        _.chain(fonts.defaultFonts)
          .where({checked: true})
          .pluck('name')
          .value()
      );
    };

    fonts.close = function () {
      $uibModalInstance.dismiss();
    };


    fonts.defaultFonts = fonts.defaultFonts.map(function (font) {
      font.checked = (_.filter(FONTS, function(f) { return f == font.name}).length > 0);
      return font;
    });

  }

})(angular.module('adminPanel.product.create.customization_desc.setUpPersonalizeModal.addFontsModal.controller', []));

