(function(app){

})(angular.module('adminPanel.product.create.customization_desc.setUpPersonalizeModal',
  [
    'adminPanel.product.create.customization_desc.setUpPersonalizeModal.controller',
    'adminPanel.product.create.customization_desc.setUpPersonalizeModal.addGalleriesModal',
    'adminPanel.product.create.customization_desc.setUpPersonalizeModal.selectColorModal',
    'adminPanel.product.create.customization_desc.setUpPersonalizeModal.addFontsModal',
    'adminPanel.product.create.customization_desc.setUpPersonalizeModal.languages'
  ]
));
