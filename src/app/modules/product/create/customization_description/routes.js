(function(app){

  app.config(function($stateProvider){

    $stateProvider
      .state('admin-panel.product.create.customization_desc',{
        url: '/customization_description',
        templateUrl: 'app/modules/product/create/customization_description/customization_description.html',
        controller: 'ProductCustomizationController',
        controllerAs: 'vm'
      });

  });

})(angular.module('adminPanel.product.create.customization_desc.routes', []));
