(function (app) {

  app.controller('ProductCreateConditionsModal', ProductCreateConditionsModal);

  ProductCreateConditionsModal.$inject = ['CONDITION', 'PRODUCT_ID', 'API', '$uibModalInstance', 'Notify'];

  function ProductCreateConditionsModal(CONDITION, PRODUCT_ID, API, $uibModalInstance, Notify) {
      var modal = this;

      modal.productID = PRODUCT_ID;

      modal.property =
      {
        'SELECTED': "Selected",
        'ENABLE': "Enabled",
        'VISIBLE': "Visible",
        'UN_SELECTED': "Not selected",
        'DISABLE': "Not enabled",
        'IN_VISIBLE': "Not visible"
      };

      modal.logicCondition = {
        'AND': "And",
        'OR': 'Or'
      };
      modal.stepsTitles = [{name: 'Loading data..'}];
      modal.loading = true;

      //default
      modal.condition = {
        _if: {
          elements_condition: 'AND',
          elements: [],
          property: 'VISIBLE'
        },
        _then: {
          elements: [],
          property: 'VISIBLE'
        }
      };

      if (CONDITION) {
        modal.condition = CONDITION;
      }


      modal.compare = function(val1, val2) {
        var tmp1 = angular.copy(val1);
        var tmp2 = angular.copy(val2);
        delete tmp1.$$hashKey;
        delete tmp1.title;
        delete tmp1._id;
        delete tmp1.groupType;
        delete tmp2.groupType;
        delete tmp2.title;
        delete tmp2._id;
        delete tmp2.$$hashKey;
        return angular.equals(tmp1, tmp2);
      };

      modal.removeDuplicate = function (type) {
        var prev;
        var collection;
        if(type == 'if')
          collection = modal.condition._if.elements;
        else
          collection = modal.condition._then.elements;

        collection.forEach(function (element, key) {
          if( key != 0) {

            element.data = prev.data;
            element.data = _.reject(element.data, function (el) {
              return modal.compare(prev.newCondition, el);
            });

            if(!element.newCondition)
              element.newCondition = element.data[0];

            var index = -1;
            element.data.forEach(function (el, key) {
              if (modal.compare(element.newCondition, el)) {
                index = key;
              }
            });

            element.newCondition = element.data[index == -1 ? 0 : index];

            prev = element;
          }else {
            prev = element;
          }
        });
      };

      modal.addIfStep = function () {
        var elem = {
          newCondition: {},
          data: angular.copy(modal.stepsTitles)
        };

        modal.condition._if.elements.forEach(function (element) {
          elem.data = _.reject(elem.data, function (el) {
            var tmp1 = angular.copy(element.newCondition);
            var tmp2 = angular.copy(el);
            delete tmp1.$$hashKey;
            delete tmp2.$$hashKey;
            return angular.equals(tmp1, tmp2);
          });
        });

        elem.newCondition = elem.data[0];

        modal.condition._if.elements.push(elem);
      };

      modal.deleteIfStep = function (el) {
        modal.condition._if.elements = _.without(modal.condition._if.elements, el);
        modal.removeDuplicate('if');
      };

      modal.addThenStep = function () {
        var elem = {
          newCondition: {},
          data: angular.copy(modal.stepsTitles)
        };

        modal.condition._then.elements.forEach(function (element) {
          elem.data = _.without(elem.data, _.findWhere(elem.data, {step: element.newCondition.step}));
        });

        elem.newCondition = elem.data[0];

        modal.condition._then.elements.push(elem);
        modal.removeDuplicate('then');
      };

      modal.deleteThenStep = function (el) {
        modal.condition._then.elements = _.without(modal.condition._then.elements, el);
        modal.removeDuplicate('then');
      };

      modal.close = function () {
        $uibModalInstance.dismiss('cancel');
      };

      //save
      modal.submit = function () {
        modal.loading = true;
        var condition = angular.copy(modal.condition);

        var newIfCond = condition._if.elements.map(function (elem) {
          var id = elem._id;
          elem = elem.newCondition;
          delete elem.groupType;
          delete elem.title;
          delete elem._id;
          return elem;
        });
        var newThenCond = condition._then.elements.map(function (elem) {
          var id = elem._id;
          elem = elem.newCondition;

          delete elem.groupType;
          delete elem.title;
          delete elem._id;
          return elem;
        });


        var newCond = {
          _if: {
            elements_condition: condition._if.elements_condition,
            elements: newIfCond,
            property: condition._if.property
          },
          _then: {
            elements: newThenCond,
            property: condition._then.property
          }
        };

        //check the existing id, if yes - update, if no - new
        if (condition._id) {

          API
            .Conditions
            .edit(modal.productID, condition._id, newCond)
            .then(function (data) {
              Notify.infoSaved();
              $uibModalInstance.close({});
            })
            .catch(function () {
              Notify.errorSaving();
            })
            .finally(function () {
              modal.loading = false;
            });
          return;
        }

        API
          .Conditions
          .save(modal.productID, newCond)
          .then(function (data) {
            Notify.infoSaved();
            $uibModalInstance.close({});
          })
          .catch(function (err) {
            Notify.errorSaving();
          })
          .finally(function () {
            modal.loading = false;
          });
      };


      modal.loadConditionData = function (data) {
        var prev;
        data = data.map(function (element, key) {
          var index = -1;

          var condition = {
            newCondition: _.pick(element, _.identity), //removes null values
            data: angular.copy(modal.stepsTitles)
          };

          condition._id = element._id;

          element = condition;

          if ( key != 0) { //for first it's different
            element.data = prev.data;
            element.data = _.reject(element.data, function (el) {
              return modal.compare(prev.newCondition, el);
            });

            if(!element.newCondition)
              element.newCondition = element.data[0];

            prev = element;
          } else {
            prev = element;
          }

          index = _.findIndex(element.data, function (el) {
            return modal.compare(element.newCondition, el);
          });

          element.newCondition = element.data[index == -1 ? 0 : index];
          return element;
        });

        return data;
      };

      modal.loadStepTree = function () {
        API
          .Steps
          .query(modal.productID, true)
          .then(function (data) {
            modal.stepsTitles = data;

            if(CONDITION) {
              modal.condition._if.elements = modal.loadConditionData(modal.condition._if.elements);
              modal.condition._then.elements = modal.loadConditionData(modal.condition._then.elements);
              return;
            }

            //else load first values;
            modal.condition._if.elements.push({
              data: angular.copy(modal.stepsTitles)
            });
            modal.condition._if.elements[0].newCondition = modal.condition._if.elements[0].data[0];
            modal.condition._then.elements.push({
              data: angular.copy(modal.stepsTitles)
            });
            modal.condition._then.elements[0].newCondition = modal.condition._then.elements[0].data[0]; //first option will be selected.

          })
          .catch(function () {
            Notify.errorSaving();
          })
          .finally(function () {
            modal.loading = false;
          });
      };

      modal.loadStepTree();
    }

})(angular.module('adminPanel.product.create.customization_desc.conditions_modal.controller', []));

