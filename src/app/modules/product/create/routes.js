(function(app){

  app.config(['$stateProvider', 'URLProvider', function($stateProvider, URLProvider){

    $stateProvider
      .state('admin-panel.product.create',{
        abstract: true,
        url: '/create/:productID?',
        template: '<div ui-view></div>',
        controller: ['$stateParams', '$state', 'ALERT', 'API', 'Notify', function($stateParams, $state, ALERT, API, Notify){
          var parent = this;
          parent.productID = $stateParams.productID;
          parent.next = ($stateParams.productID && $stateParams.productID != 'new')  ? false : true;

          parent.navigate = function(direction) {
            //check if cache available if not ask user to go back to User list
            if(!API.Products.cache.available()) {
              Notify.infoNoItems();
              return;
            }
            var products = API.Products.cache.getProductsList();
            var index = _.findIndex(products, { '_id': parent.productID });
            if(direction === 'left') {
              index--;
            }else {
              index++;
            }
            if(typeof(products[index]) != 'undefined') {
              $state.go($state.current, {'productID': products[index]._id});
            }else{
              //if there are no orders in the list for navigation. ask user to go back to change pagination.
              Notify.infoNoItems();
            }
          };

          parent.deleteCacheImages = function() {
            ALERT.confirmDeleting(function(){
                API
                  .Products
                  .clearCachedImages($stateParams.productID)
                  .then(function () {
                    Notify.infoSaved();
                  })
                  .catch(function () {
                    Notify.errorSaving();
                  });
            });
          };
        }],
        controllerAs: 'parent',
        data: {
          permissions: {
            only: 'canSee.Products.ProductDetails',
            redirectTo: URLProvider.$get().RedirectURL
          }
        }
      });


  }]);

})(angular.module('adminPanel.product.create.routes',[]));
