(function(app){
  app.config(function($stateProvider){

    $stateProvider
      .state('admin-panel.product',{
        url: '/product',
        abstract: true,
        template: '<ui-view/>'
      });

  });

})(angular.module('adminPanel.product.routes',[]));
