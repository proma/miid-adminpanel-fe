'use strict';

describe('Controllers', function () {
  describe('ProductListController', function(){

    var data = {
      data: [
        {
          name: 'SHIRT001',
            id: 'SHIRT001_id'
        },
        {
          name: 'SHIRT002',
            id: 'SHIRT002_id'
        }
      ],
      url: '',
      paging: {
        total: 5
      },
      brands: {
        data: [
          {
            name: 'SOFLO',
            id: 'BRAND1'
          },
          {
            name: 'Brand2',
            id: 'testbrand'
          }
        ],
        paging: {
          total: 5
        }
      }
    };

    var
      ProductListController,
      $state,
      URL,
      Notify,
      ALERT,
      $httpBackend,
      QINGSTOR,
      Modal,
      $rootScope,
      $timeout,
      $scope;



    // load the dependencies module
    beforeEach(module('ui.router'));
    beforeEach(module('adminPanel.services.modal'));
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.services.notifier'));
    beforeEach(module('adminPanel.services.alert'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.api'));
    beforeEach(module('adminPanel.product.list.controller'));

    beforeEach(inject(function($injector){
      $state = $injector.get('$state');
      $httpBackend = $injector.get('$httpBackend');
      Notify = $injector.get('Notify');
      URL = $injector.get('URL');
      $rootScope = $injector.get('$rootScope');
      $timeout = $injector.get('$timeout');
      $scope = $rootScope.$new();
      Modal = $injector.get('Modal');
      ALERT = $injector.get('ALERT');
      var $controller = $injector.get('$controller');
      QINGSTOR =  $injector.get('QINGSTOR');
      ProductListController = $controller('ProductListController',{
        '$scope':$scope,
        '$state': $state,
        'API': $injector.get('API'),
        'ALERT': ALERT,
        'QINGSTOR' : QINGSTOR,
        'Notify': Notify,
        'Modal': Modal,
        '$timeout': $timeout
      });
      data.url = '?archived=false&from=' + ProductListController.filters.from.valueOf() + '&limit=10&page=0';
      data.url += '&to=' +  ProductListController.filters.to.valueOf();
      $httpBackend.expect('GET',URL.BRANDS + '?list_type=LIST_VALUES').respond(200, data.brands);
    }));

    describe('MA-92: List Products & Pagination', function(){

      it('should make products empty first',function(){
        expect(ProductListController.products.length).toBe(0);
      });

      it('should be loading first', function() {
        expect(ProductListController.loading).toBeTruthy();
      });

      it('should stop loading after load', function() {
        $httpBackend.expect('GET',URL.PRODUCTS + data.url ).respond(200, data);
        $httpBackend.flush();
        expect(ProductListController.loading).toBeFalsy();
      });

      it('should have set the asset url', function() {
        expect(ProductListController.assetUrl).toBe(QINGSTOR.base_url);
      });

      it('should set items limit to 10', function() {
        expect(ProductListController.itemsLimit).toBe(10);
      });

      it('should call loadProducts() first', function() {
        spyOn(ProductListController,'loadProducts');
        $httpBackend.expect('GET',URL.PRODUCTS + data.url ).respond(200, data);
        ProductListController.loadProducts();
        $httpBackend.flush();
        expect(ProductListController.loadProducts).toHaveBeenCalled();
      });

      it('should call alert when error',function(){
        spyOn(ALERT,'confirm');
        $httpBackend.expect('GET',URL.PRODUCTS + data.url ).respond(500, data);
        $httpBackend.flush();
        expect(ALERT.confirm).toHaveBeenCalled();
      });

      it('should populate products', function() {
        $httpBackend.expect('GET',URL.PRODUCTS + data.url).respond(200, data);
        $httpBackend.flush();
        expect(ProductListController.products.length).toBeGreaterThan(0);
      });

      it('should set totalItems to 5', function() {
        $httpBackend.expect('GET',URL.PRODUCTS + data.url ).respond(200, data);
        $httpBackend.flush();
        expect(ProductListController.totalItems).toBe(5);
      });

      it('should set itemsGot to 2', function() {
        $httpBackend.expect('GET',URL.PRODUCTS + data.url).respond(200, data);
        $httpBackend.flush();
        expect(ProductListController.itemsGot).toBe(2);
      });

      it('should query page 2 when called changePage()',function(){
        spyOn(ProductListController,'loadProducts');
        ProductListController.currentPage = 2; // page is index based so in query page 1 will be called
        $httpBackend.when('GET',URL.PRODUCTS + data.url ).respond(200, data);
        $httpBackend.when('GET',URL.PRODUCTS + data.url ).respond(200, data);

        ProductListController.changePage();

        $httpBackend.flush();

        expect(ProductListController.loadProducts).toHaveBeenCalled();
      });

      it('should call alert on deleteProduct',function(){
        spyOn(ALERT,'confirm');
        $httpBackend.when('DELETE', URL.PRODUCTS + '/sdf').respond(500, {});
        $httpBackend.when('GET', URL.PRODUCTS + data.url).respond(200, data);

        ProductListController.deleteProduct({_id: 'sdf'});
        $httpBackend.flush();

        expect(ALERT.confirm).toHaveBeenCalled();
      });

    });

  });

});
