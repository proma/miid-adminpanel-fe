(function(app){

  app.config(['$stateProvider', 'URLProvider', function($stateProvider, URLProvider){

    $stateProvider
      .state('admin-panel.product.list',{
        url: '/list',
        templateUrl: 'app/modules/product/list/products_list.html',
        controller: 'ProductListController',
        controllerAs: 'vm',
        data: {
          permissions: {
            only: 'canSee.Products.Listing',
            redirectTo: URLProvider.$get().RedirectURL
          }
        }
      });


  }]);

})(angular.module('adminPanel.product.list.routes',[]));
