(function(app){

  app.controller('ProductListController', ['$scope', '$state', 'API', 'ALERT', 'QINGSTOR', 'Notify', 'Modal', '$translate', ProductListController]);


  function ProductListController($scope, $state, API, ALERT, QINGSTOR, Notify, Modal, $translate) {
    var vm = this;

    vm.products = [];
    vm.loading = true;
    vm.selectedProductsCount = 0;
    vm.listBox = false;
    vm.changingStatus = false;
    vm.itemsLimit = 10;
    vm.assetUrl = QINGSTOR.base_url;
    vm.totalItems = 0;
    vm.itemsGot = 0;
    vm.previousPage = 0;
    vm.currentPage = 1;
    vm.statuses = [
      {
        name: $translate.instant('filters.status.label') || 'Status',
        disabled: true,
        value: 'status'
      },
      {
        name: 'Active',
        value: 'ACTIVE'
      },
      {
        name: 'Archived',
        value: 'ARCHIVED'
      },
      {
        name: 'Inactive',
        value: 'IN_ACTIVE'
      }];

    vm.statusLabelClasses = {
      IN_ACTIVE: 'pending',
      ACTIVE: 'paid',
      ARCHIVED: 'finalized'
    };

    vm.brands =[{
      name: $translate.instant('filters.brand.label') || 'Brand',
      id: -1
    }];

    vm.filters = {
      status: vm.statuses[0].value,
      brand: vm.brands[0],
      from: moment((new Date("1/1/" + moment().year())).valueOf()),
      to: moment().endOf('day'),
      archived: false,
      text:""
    };

    vm.selectedStatus = 'status';

    // Product actions processing

    //toggle all checkboxes
    vm.toggleCheckbox = function(checkAll){
      angular.forEach(vm.products,function(data){
        data.checked = checkAll;
      });
      if(checkAll)
        vm.selectedProductsCount = vm.products ? vm.products.length : 0 ;
      else
        vm.selectedProductsCount = 0;
      return false;
    };

    vm.markCheckBox = function(status){
      vm.selectedProductsCount = 0;
      var alreadyChecked = false;
      angular.forEach(vm.products, function (data) {
        if (data.checked) {
          alreadyChecked = true;//just need one checked box
          ++vm.selectedProductsCount; //inc counter
        }
      });

      if (status) {
        //mark checkbox as true
        vm.listBox = true;
      } else {
        //check if checkbox is already true
        //check if none boxes are checked
        if (!alreadyChecked) {
          vm.listBox = false;
        }
      }

    };

    //change single status

    vm.changeSingleStatus = function(product, status){
      product.changingStatus = true;

      if(product.status === status){
        Notify.infoSameItemStatus();
        product.changingStatus = false;
        return;
      }

      API.
        Products
        .edit(product._id, {'status': status})
        .then(function(result){
          if(result.result === 'success'){
            product.status = status;
            Notify.infoChangeStatus(status.replace('_','').toLowerCase());
          }
          product.changingStatus = false;
        })
        .catch(function(err){
          Notify.errorItemStatus();
          product.changingStatus = false;
        });
    };

    //change product status
    vm.changeStatus = function(){
      Modal.open(
        'ProductListChangeStatusModalController',
        'modal',
        'app/modules/product/list/change_status/product.list.changeStatusModal.html',
        {
          ProductsList : function(){
            return vm.products;
          },
          Status: function() {
            return vm.statuses;
          },
          Classes: function(){
            return vm.statusLabelClasses;
          }
        }
      );
    }; // end change status function

    //duplicate product modal
    vm.duplicate = function(product){
      Modal.open(
        'ProductListDuplicateModalController',
        'modal',
        'app/modules/product/list/duplicate_product/product.list.duplicateProductModal.html',
        {
          ProductId : function(){
            return product._id;
          },
          ProductListController: function(){
            return vm;
          }
        },
        'md'
      );
    }; // end duplicate product

    vm.edit = function(product){
      console.log(product);
      if(product && product.id){
        API.Products.setProductId(product.id);
        $state.go('admin-panel.product.create.basic_info',{ productID: product.id});
      }
    };

    vm.delete = function(product){
      ALERT
        .confirmDeleting(function(){
          return vm.deleteProduct(product)
        });
    };

    vm.deleteProduct = function(product){
      if(product && product._id){
        API
          .Products
          .remove(product._id)
          .then(function (result) {
            //vm.products = _.without(vm.products,product);
            console.log(result);
            vm.loadProducts();
          }).catch(function (err) {
          ALERT
            .errorDeleting(function(){
              vm.delete(product)
            });
        });
      }
    };

    vm.copy = function(product){
      //TODO: handle the copy operation
    };

    vm.effective = function(product){
      //TODO: handle the effective operation
    };

    // get all brands
    vm.listBrands = function() {
      API
        .Brands
        .getAll(null,null,'LIST_VALUES')
        .then(function (brands) {
          if (brands.data && brands.data.length) {

            brands.data.forEach(function (brand) {
              vm.brands.push({id: brand._id, name: brand.name});
            })
          }
        }).catch(function (err) {
        vm.brands = [{name: $translate.instant('errors.errorLoadingBrand') || 'Error loading brands', id: -1}]
      });
    };

    // load products
    vm.loadProducts = function(isfilter) {
      isfilter = (_.isNull(isfilter) || _.isUndefined(isfilter)) ? false : isfilter;

      // need to reset pagination when any filter is applied because fitered object may have only one page so when user
      // is on any other page user can't see any product
      if(isfilter === true){
        vm.currentPage = 1;
        vm.previousPage = 0;
      }

      vm.loading = true;
      //reset checkbox
      vm.listBox = false;
      vm.selectedProductsCount = 0;

      API
        .Products
        .paginatedQuery({limit:vm.itemsLimit
          , page:vm.currentPage-1
          , sortOrder:null // need to add sort order
          , sortBy:null //need to add sort category
          , status:(vm.filters.status != 'status')? vm.filters.status : null
          , brand:(vm.filters.brand.id != -1)? vm.filters.brand.id : null
          , from:vm.filters.from.valueOf()
          , to:vm.filters.to.valueOf()
          , archived:vm.filters.archived
          , text:vm.filters.text}
        ).then(function (products) {
          if (products){
            if(!_.isUndefined(products.data)) {
              vm.products = products.data;
              vm.itemsGot = vm.products.length;
            }

            if(!_.isUndefined(products.paging)){
              vm.totalItems =  products.paging.total;
            }
          }
        })
        .catch(function (err) {
          ALERT
            .confirm(vm.loadProducts);
        })
        .finally(function () {
          vm.loading = false;
        });
    };


    // change page
    vm.changePage = function(){
      vm.previousPage = (vm.currentPage-1);
      vm.loadProducts();
    };

    // to populate Brands drop down
    vm.listBrands();
    //Load products list
    vm.loadProducts();

    // work around for date picker bug, that is not except variables in vm but except scope variable and functions
    $scope.dateChanged = function(){
      vm.loadProducts(true);
    };

    $scope.$watch("vm.filters.from",function(){
      if(vm.filters.from.valueOf() > vm.filters.to.valueOf()){
        vm.filters.from = moment(vm.filters.to);
      }
    });

    $scope.$watch("vm.filters.to",function(){
      if (vm.filters.to.valueOf() < vm.filters.from.valueOf()) {
        vm.filters.to = moment(vm.filters.from);
      }
    });
  }

  app.filter('capitalize', function() {
    return function(input, all) {
      var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
      return (!!input) ? input.replace(reg, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}).replace(/[-_]/,'') : '';
    }
  });

})(angular.module('adminPanel.product.list.controller',[]));
