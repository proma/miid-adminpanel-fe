(function(app){

})(angular.module('adminPanel.product.list',
  [
    'adminPanel.product.list.routes',
    'adminPanel.product.list.controller',
    'adminPanel.product.list.changeStatusModal',
    'adminPanel.product.list.duplicateProductModal'
  ]
));
