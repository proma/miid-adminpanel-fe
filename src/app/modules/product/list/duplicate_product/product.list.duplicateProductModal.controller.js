(function (app) {

  app.controller('ProductListDuplicateModalController', ProductListDuplicateModalController);

  ProductListDuplicateModalController.$inject = ['ProductId', 'ProductListController', 'API', '$uibModalInstance', '$timeout', '$translate'];

  function ProductListDuplicateModalController (ProductId, ProductListController, API, $uibModalInstance, $timeout, $translate) {
    var modal = this;
    modal.miidId = '';
    modal.success = false;
    modal.error = false;
    modal.message = '';
    modal.successCount = 0;
    modal.duplicatingProduct = false;
    modal.sentMiidId = '';
    modal.timeOut = null;
    modal.duplicateAsset = false;
    modal.close = function(){
      $uibModalInstance.dismiss('cancel');
      // reload products if successfully added
      if(modal.successCount > 0){
        ProductListController.loadProducts();
      }
      // clear timeout
      if(modal.timeOut != null){
        $timeout.cancel(modal.timeOut);
      }
    };

    modal.submit = function(valid){

      function callBack(message, isError){
        if(isError === true){
          modal.error = true;
          modal.success = false;
        } else {
          modal.success = true;
          modal.error = false;
          modal.successCount++;
          modal.timeOut = $timeout(modal.close,2000);
        }

        modal.message = message;
        modal.duplicatingProduct = false;
      }

      if(valid) {
        modal.duplicatingProduct = true;
        modal.sentMiidId =  modal.miidId;
        API.
        Products
          .duplicate(ProductId, {miid_id: modal.miidId}, modal.duplicateAsset)
          .then(function (response) {
            callBack($translate.instant('modals.duplicate.msg4', {miid: modal.miidId}), false);
          })
          .catch(function (response) {
            if (response.status === 400) {
              callBack(response.data.message, true);
            }
          });
      }
    };
  }

})(angular.module('adminPanel.product.list.duplicateProductModal.controller', []));
