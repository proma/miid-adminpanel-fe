(function (app) {

  app.controller('ProductListChangeStatusModalController', ProductListChangeStatusModalController);

  ProductListChangeStatusModalController.$inject = ['ProductsList', 'Status', 'Classes', 'API', 'Notify', '$uibModalInstance'];

  function ProductListChangeStatusModalController (ProductsList, Status, Classes, API, Notify, $uibModalInstance) {
    var modal = this;

    modal.statuses = Status;
    modal.selectedStatus = 'ACTIVE';
    modal.changingStatus = false;
    modal.statusClasses = Classes;

    modal.close = function(){
      $uibModalInstance.dismiss('cancel');
    };

    modal.submit = function(){

      modal.changingStatus = true; //disables button
      var noOfCalls = 0; //no of http requests to call

      //checking if user has selected orders?

      angular.forEach(ProductsList,function(data){
        if (data.checked){
          if (modal.selectedStatus != data.status)
            ++noOfCalls;
        }
      });

      //if noOfCalls 0 then return

      if(noOfCalls === 0){
        modal.changingStatus = false;
        Notify.infoSameItemStatus();

        return;
      }

      // change status of all the objects.
      var completedReq = 0;
      var noOfErrors = 0;

      function callBack(err){
        if(!err){
          ++completedReq;
        }
        if(err){
          ++noOfErrors;
        }
        if(noOfCalls == (completedReq+noOfErrors)){
          modal.changingStatus = false;

          //all calls have finished, let's call Notify

          Notify.infoChangeItemsStatus(noOfCalls,completedReq , modal.selectedStatus.replace('_','').toLowerCase());

          // hide the modal

          $uibModalInstance.close({
            success: true
          });
        }
      }

      angular.forEach(ProductsList, function(data){
        //if status of selected product == selected status, we save http request.
        if(data.checked)
          if(modal.selectedStatus != data.status){
            //we send request to change status.
            var obj = {
              status: modal.selectedStatus
            };
            API.
            Products
              .edit(data._id, obj)
              .then(function(result){
                if(result.result === 'success'){
                  data.status = modal.selectedStatus;
                }
                callBack();
              })
              .catch(function(err){
                Notify.errorItemStatus();
                callBack(err);
              });
          }
      });

    };
  }

})(angular.module('adminPanel.product.list.changeStatusModal.controller', []));
