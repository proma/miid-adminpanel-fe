(function (app) {

  app.controller('NavigationListController', NavigationListController);

  NavigationListController.$inject = ['API', 'ALERT', 'Notify', '$translate'];

  function NavigationListController(API, ALERT, Notify, $translate) {

    var vm = this;

    vm.loading = true;
    vm.listSelectorOptions = {}; //for list selector that directive will bind some methods to this object.
    vm.navigations = [];
    vm.pages = [];
    vm.currentTab = 'NAV';
    vm.totalItems = 0; //TODO: need to make directive for these
    vm.itemsGot = 0;
    vm.previousPage = 0;
    vm.currentPage = 1;
    vm.itemsLimit = 10;
    vm.listSelectorOptionsHomePage = {};
    vm.currentPageHome = 1;
    vm.itemsLimitHomePage = 10;
    vm.previousPageHome = 0;
    vm.itemsGotHomePage = 0;
    vm.totalItemsHomePage = 0;

    vm.bulkActions = [
      {
        title:   $translate.instant('navigationCreatePage.deleteNavigation') || 'Delete Navigations',
        action: function () {
          vm.deleteNavigations();
        }
      }
    ];

    vm.listNavigations = function () {

      vm.loading = true;

      var dataPromise =
        API
          .Navigations
          .getAll({
            page: (vm.currentPage-1),
            limit: vm.itemsLimit
          });

      dataPromise
        .then(function (data) {
          vm.listSelectorOptions.reset();
          vm.totalItems = data.paging.total;
          vm.navigations = data.data;
          vm.itemsGot = vm.navigations.length;
        }).
      catch(function (err) {
        ALERT.confirm(function () {
          return vm.listNavigations();
        });
      }).
      finally(function () {
        vm.loading = false;
      });

      return dataPromise;
    };

    vm.changeTab = function (type) {
      vm.currentTab = type;
      if(type === 'HomePage') {
        vm.listPages();
        vm.changePageHome();
      }
    };

    vm.bulkActionHomePage = [
      {
        title: $translate.instant('homePageCreatePage.deletePage') || 'Delete Pages',
        action: function () {
          vm.deletePages();
        }
      }
    ];

    vm.changePageHome = function () {
      vm.previousPageHome = (vm.currentPageHome - 1);
      vm.loading = true;
      vm.listSelectorOptionsHomePage.reset();
      vm.listPages();
    };

    vm.deletePages = function () {
      vm.loading = true;
      ALERT.confirm(function () {
        var completedReq = 0;
        var noOfErrors = 0;
        var noOfCalls = 0; //no of http requests to call
        //checking if user has selected navigations?
        angular.forEach(vm.pages,function(data){
          if (data.checked){
            ++noOfCalls;
          }
        });
        angular.forEach(vm.pages, function (data) {
          if(data.checked) {
            data.performingOperation = true;
            API
              .Pages
              .delete(data._id)
              .then(function() {
                data.performingOperation = false;
                callBack();
              })
              .catch(function (err) {
                data.performingOperation = false;
                callBack(err);
              })
              .finally(function () {
                vm.loading = false;
              });
          }
        });
        function callBack(err){
          if(!err){
            ++completedReq;
          }
          if(err){
            ++noOfErrors;
          }
          if(noOfCalls == (completedReq+noOfErrors)){
            vm.listPages(); //reload collections
            Notify.infoItemsDeleted(noOfCalls,completedReq);
          }
        }
      }, 'Are you sure?', 'This will delete the selected pages permanently.', 'warning','Confirm');
    };

    vm.listPages = function () {
      vm.loading = true;
      var dataPromise =
        API.
        Pages.
        getAll({
          page: (vm.currentPageHome-1) ,
          limit: vm.itemsLimitHomePage
        });
      dataPromise.
      then(function (data) {
        vm.listSelectorOptionsHomePage.reset();
        vm.totalItemsHomePage = data.paging.total;
        vm.pages = data.data;
        vm.itemsGotHomePage = vm.pages.length;
      })
        .catch(function (err) {
          ALERT.confirm(function () {
            return vm.listPages();
          });
        })
        .finally(function () {
          vm.loading = false;
        });
      return dataPromise;
    };

    vm.changePage = function () {
      vm.previousPage = (vm.currentPage - 1);
      vm.loading = true;
      vm.listSelectorOptions.reset();
      vm.listNavigations();
    };


    vm.deleteNavigations = function() {

      ALERT.confirmWarning(function () {

        var completedReq = 0;
        var noOfErrors = 0;
        var noOfCalls = 0; //no of http requests to call
        //checking if user has selected navigations?
        angular.forEach(vm.navigations,function(data){
          if (data.checked){
            ++noOfCalls;
          }
        });
        angular.forEach(vm.navigations, function (data) {
          if(data.checked) {
            data.performingOperation = true;
            API.Navigations.delete(data._id)
              .then(function() {
                callBack();
              })
              .catch(function (err) {
                callBack(err);
              })
              .finally(function () {
                data.performingOperation = false;
              });
          }
        });

        function callBack(err){
          if(!err){
            ++completedReq;
          }
          if(err){
            ++noOfErrors;
          }
          if(noOfCalls == (completedReq+noOfErrors)){
            vm.listNavigations(); //reload collections
            Notify.infoItemsDeleted(noOfCalls,completedReq);
          }
        }
      });
    };
  }
})(angular.module('adminPanel.cms.navigation.list.controller',[]));
