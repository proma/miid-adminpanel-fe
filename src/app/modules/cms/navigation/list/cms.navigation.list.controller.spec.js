'use strict';

describe('Controllers', function () {

  describe('NavigationListController', function () {

    var data = {
      "paging": {
        "total": 1
      },
      "data": [
        {
          "_id": "580e24402514196a426debd6",
          "slug": "ss",
          "status": "ACTIVE",
          "title": {
            "en": "N4",
            "ch": "N4"
          }
        }
      ]
    };

    var
      NavigationListController,
      $rootScope,
      URL,
      $httpBackend,
      $timeout,
      QINGSTOR,
      ALERT,
      $scope,
      $uibModal,
      Notify;

    // load the dependencies module

    beforeEach(module('adminPanel.services.notifier'));
    beforeEach(module('adminPanel.services.modal'));
    beforeEach(module('ui.bootstrap'));
    beforeEach(module('ngResource'));
    beforeEach(module('oitozero.ngSweetAlert'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.services.alert'));
    beforeEach(module('adminPanel.api'));
    beforeEach(module('adminPanel.cms.navigation.list.controller'));


    beforeEach(inject(function ($injector) {
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      $httpBackend = $injector.get('$httpBackend');
      URL = $injector.get('URL');
      var $controller = $injector.get('$controller');
      QINGSTOR = $injector.get('QINGSTOR');
      Notify = $injector.get('Notify');
      $uibModal = $injector.get('Modal');
      ALERT = $injector.get('ALERT');
      $timeout = $injector.get('$timeout');

      NavigationListController = $controller('NavigationListController', {
        'Notify': Notify,
        '$scope': $scope,
        'Modal': $uibModal,
        'API': $injector.get('API'),
        'QINGSTOR': QINGSTOR,
        'ALERT': $injector.get('ALERT')
      });

      NavigationListController.listSelectorOptions.reset = function() {}; //fake coz of directive
      data.url = URL.NAVIGATIONS + '?limit=10&page=0';
      data.url2 = URL.NAVIGATIONS + '?limit=10&page=1';
      data.delURL = URL.NAVIGATIONS + '/571f50d9d5569128031874b2';
      NavigationListController.listNavigations();

    }));

    it('should be empty first', function () {

      expect(NavigationListController.navigations.length).toBe(0);

    });

    it('should be loading first', function () {

      expect(NavigationListController.loading).toBeTruthy();

    });

    it('should stop loading after load', function () {

      $httpBackend.expect('GET', data.url).respond(200, data);
      $httpBackend.flush();
      expect(NavigationListController.loading).toBeFalsy();

    });

    it('should set items limit to 10', function () {
      expect(NavigationListController.itemsLimit).toBe(10);
    });

    it('should call alert when error', function () {

      spyOn(ALERT, 'confirm');
      $httpBackend.expect('GET', data.url).respond(500, data);
      $httpBackend.flush();
      NavigationListController.listNavigations();
      expect(ALERT.confirm).toHaveBeenCalled();

    });

    it('should populate navigations', function () {

      $httpBackend.expect('GET', data.url).respond(200, data);
      $httpBackend.flush();
      expect(NavigationListController.navigations.length).toEqual(1);

    });

    it('should set totalItems to 2', function () {

      $httpBackend.expect('GET', data.url).respond(200, data);
      $httpBackend.flush();
      expect(NavigationListController.totalItems).toBe(1);

    });

    it('should set itemsGot to 2', function () {

      $httpBackend.expect('GET', data.url).respond(200, data);
      $httpBackend.flush();
      expect(NavigationListController.itemsGot).toBe(1);

    });

    it('should call listSelectorOptions.reset() when called changePage()', function () {

      spyOn(NavigationListController.listSelectorOptions, 'reset');
      NavigationListController.currentPage = 2; // page is index based so in query page 1 will be called
      $httpBackend.when('GET', data.url).respond(200, data);
      $httpBackend.when('GET', data.url2).respond(200, data);

      NavigationListController.changePage();

      $httpBackend.flush();

      expect(NavigationListController.listSelectorOptions.reset).toHaveBeenCalled();
    });

    it('should call delete api and alert when called deleteNavigations', function () {

      $httpBackend.when('GET', data.url).respond(200, data);
      $httpBackend.flush();
      NavigationListController.navigations[0].checked = true; //manually checking
      spyOn(ALERT, 'confirm');
      NavigationListController.deleteNavigations();
      expect(ALERT.confirm).toHaveBeenCalled();

    });

  });

});
