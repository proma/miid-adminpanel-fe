(function (app) {

  app.config(['$stateProvider', function ($stateProvider) {

    $stateProvider
      .state('admin-panel.cms.navigation.list', {
        url: '/list',
        templateUrl: 'app/modules/cms/navigation/list/cms.navigation.list.html',
        controller: 'NavigationListController',
        controllerAs: 'vm'
      });

  }]);

})(angular.module('adminPanel.cms.navigation.list.routes',[]));
