(function (app) {

  app.config(['$stateProvider','URLProvider', function ($stateProvider, URLProvider) {

    $stateProvider
      .state('admin-panel.cms.navigation', {
        url: '/navigation',
        abstract: true,
        template: '<ui-view></ui-view>',
        data: {
          permissions: {
            only: 'canSee.CMS.Navigations',
            redirectTo: URLProvider.$get().RedirectURL
          }
        }
      });
  }]);

})(angular.module('adminPanel.cms.navigation.routes',[]));
