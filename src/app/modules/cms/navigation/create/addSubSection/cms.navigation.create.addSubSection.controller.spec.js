'use strict';

describe('Controllers', function () {

  describe('NavigationAddSubSectionController', function () {

    var data = {
      "currentSection":  {
        "_id": "58127a0fb05f200001f63d91",
        "sub_sections": [
          {
            "_id": "5816005ad5fdf300013f3075",
            "collections": [
              {
                "_id": "5807828a7e05e400019d4584"
              },
              {
                "_id": "57593e28ed82800600000011"
              }
            ],
            "display_options": {
              "image_title": false,
              "section_title": true
            },
            "title": {
              "en": "ss1",
              "ch": "ss1"
            }
          }
        ],
        "title": {
          "en": "test",
          "ch": "test"
        }
      },
      "origNavigation": {
        "_id": "580e24402514196a426debd6",
        "sections": [
          {
            "_id": "58127a0fb05f200001f63d91",
            "sub_sections": [
              {
                "_id": "5816005ad5fdf300013f3075",
                "collections": [
                  {
                    "_id": "5807828a7e05e400019d4584"
                  },
                  {
                    "_id": "57593e28ed82800600000011"
                  }
                ],
                "display_options": {
                  "image_title": false,
                  "section_title": true
                },
                "title": {
                  "en": "ss1",
                  "ch": "ss1"
                }
              }
            ],
            "title": {
              "en": "test",
              "ch": "test"
            }
          }
        ],
        "title": {
          "en": "N4",
          "ch": "N4"
        }
      }
    };

    var
      Controller,
      $rootScope,
      URL,
      $httpBackend,
      $scope,
      $uibModalInstance;

    // load the dependencies module

    beforeEach(module('toastr'));
    beforeEach(module('ui.bootstrap'));
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.api'));
    beforeEach(module('adminPanel.cms.navigation.create.add_sub_section.controller'));

    beforeEach(inject(function ($injector) {

      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      $httpBackend = $injector.get('$httpBackend');
      URL = $injector.get('URL');
      var $controller = $injector.get('$controller');
      $uibModalInstance =  {
        // create a mock object using spies
        close: jasmine.createSpy('$uibModalInstance.close'),
      };

      Controller = $controller('NavigationAddSubSectionController', {
        '$scope': $scope,
        '$uibModalInstance': $uibModalInstance,
        'API': $injector.get('API'),
        'NAVIGATION': data
      });

    }));

    it('should close the modal', function () {

      Controller.close();
      expect($uibModalInstance.close).toHaveBeenCalled();

    });

    it('should initialize some variables', function () {

      expect(Controller.subSectionCreated).toBeFalsy();
      expect(Controller.subSectionExist).toBeFalsy();
      expect(Controller.loading).toBeFalsy();

    });

    it('should start loading after clicking submit', function () {

      Controller.submit();
      expect(Controller.loading).toBeTruthy();

    });

    it('should call api and save the sub section', function () {

      $httpBackend.expect('PUT', URL.NAVIGATIONS + '/580e24402514196a426debd6').respond(200, {success: true});
      Controller.submit();
      $httpBackend.flush();
      expect(Controller.loading).toBeFalsy();
      expect(Controller.subSectionCreated).toBeTruthy();
      expect(Controller.subSectionExist).toBeFalsy();

    });

    it('should make sectionExist true if error', function () {

      $httpBackend.expect('PUT', URL.NAVIGATIONS + '/580e24402514196a426debd6').respond(500, {success: true});
      Controller.submit();
      $httpBackend.flush();
      expect(Controller.loading).toBeFalsy();
      expect(Controller.subSectionCreated).toBeFalsy();
      expect(Controller.subSectionExist).toBeTruthy();

    });

  });

});
