(function (app) {

  app.controller('NavigationAddSubSectionController', NavigationAddSubSectionController);

  NavigationAddSubSectionController.$inject = ['$uibModalInstance', 'NAVIGATION', 'API'];

  function NavigationAddSubSectionController($uibModalInstance, NAVIGATION, API) {

    var vm = this;

    //   Variables

    vm.loading = false;
    vm.subSectionExist = false;
    vm.subSectionCreated = false;


    // Functions

    vm.close = close;
    vm.submit = submit;


    // Functions Implementations

    function close() {
      $uibModalInstance.close();
    }

    function submit() {

      vm.loading = true;

      var origNavigation = NAVIGATION.origNavigation;
      var currentSection = NAVIGATION.currentSection;

      var params = {
        sections: []
      };

      origNavigation.sections.forEach(function (data) {

        if(data._id == currentSection._id) {

          if(data.sub_sections.length > 0) {

            var sec = {
              title: {
                en: data.title.en,
                ch: data.title.ch
              },
              _id: data._id,
              sub_sections: []
            };

            data.sub_sections.forEach(function (subSection) {

              var subsection = {
                _id: subSection._id,
                title: { en: subSection.title.en, ch: subSection.title.en } ,
                display_options: {
                  section_title: subSection.display_options.section_title,
                  image_title: subSection.display_options.image_title
                },
                collections: []
              };

              if(subSection.collections.length <=0) {
                delete subsection.collections;
              } else {
                subsection.collections = _.pluck(subSection.collections, '_id');
              }
              sec.sub_sections.push(subsection);
            });

            sec.sub_sections.push({
              title: {
                en: vm.subSectionName,
                ch: vm.subSectionName },
              display_options: {
                section_title: true,
                image_title: false
                }
            });
            params.sections.push(sec);
          }

        } else {

          var sec = {
            title:
            { en: data.title.en,
              ch: data.title.ch
            },
            _id: data._id,
            sub_sections: []
          };

          data.sub_sections.forEach(function (subSection) {

            var subsection =
            {
              _id: subSection._id,
              title: { en: subSection.title.en, ch: subSection.title.en } ,
              display_options: { section_title: subSection.display_options.section_title,
                image_title: subSection.display_options.image_title
              },
              collections: []
            };

            if(subSection.collections.length <=0) {
              delete subsection.collections;
            } else {
              subsection.collections = _.pluck(subSection.collections, '_id');
            }
            sec.sub_sections.push(subsection);
          });
          params.sections.push(sec);
        }

      });

      API.
      Navigations.
      edit(origNavigation._id, params).
      then(function (result) {
        vm.subSectionCreated = true;
        vm.subSectionExist = false;
      }).
      catch(function (err) {
        vm.subSectionExist = true;
        vm.subSectionCreated = false;
      }).finally(function () {
        vm.loading = false;
      });
    }
  }

})(angular.module('adminPanel.cms.navigation.create.addSubSection.controller',[]));
