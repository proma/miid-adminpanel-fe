(function (app) {

  app.config(['$stateProvider','URLProvider', function ($stateProvider) {

    $stateProvider
      .state('admin-panel.cms.navigation.create', {
        url: '/create/:navigationID',
        templateUrl: 'app/modules/cms/navigation/create/cms.navigation.create.html',
        controller: 'CreateNavigationController',
        controllerAs: 'vm'
      });

  }]);

})(angular.module('adminPanel.cms.navigation.create.routes',[]));

