(function (app) {

  app.controller('CreateNavigationController', CreateNavigationController);

  CreateNavigationController.$inject = ['Modal','$stateParams', '$state', 'Slug', 'API', 'ALERT', 'QINGSTOR', 'Upload', 'MIID_API', 'Notify'];

  function CreateNavigationController(Modal, $stateParams, $state, Slug, API, ALERT, QINGSTOR, Upload, MIID_API, Notify) {

    var vm = this;


    // Variables

    vm.baseUrl = QINGSTOR.base_url;
    vm.loading = false;
    vm.slugExist = false;
    vm.origNavigation = {};
    vm.currentSection = {};
    vm.oldCurrentSection = {};
    vm.navigation = {
      title: {
        en: '',
        ch: ''
      },
      slug: '',
      status: 'INACTIVE', //default is inactive
      seo: {
        title: '',
        description: '',
        currentKeyword: '',
        keywords: []
      },
      sections: []
    };

    // Functions

    vm.addCollections = addCollections;
    vm.enableButton = enableButton;
    vm.changeSection = changeSection;
    vm.addSection = addSection;
    vm.loadNavigationData = loadNavigationData;
    vm.isEdit = isEdit;
    vm.setSlug = setSlug;
    vm.slugify = slugify;
    vm.fillSlugTitle = fillSlugTitle;
    vm.addTag = addTag;
    vm.removeTag = removeTag;
    vm.confirmDeleteSection = confirmDeleteSection;
    vm.deleteSection = deleteSection;
    vm.getSections = getSections;
    vm.saveChangings = saveChangings;
    vm.update = update;
    vm.addSubsection = addSubsection;
    vm.confirmDeleteCollection = confirmDeleteCollection;
    vm.deleteCollection = deleteCollection;


    // Functions Implementation


    function isEdit() {
      vm.navigationID = $stateParams.navigationID && $stateParams.navigationID != '' && $stateParams.navigationID != 'new' ? $stateParams.navigationID : null;
      return vm.navigationID != null;
    }

    function addSection() {

      Modal.open(
        'NavigationAddSectionController',
        'sec',
        'app/modules/cms/navigation/create/addSection/cms.navigation.create.addSection.addSectionModal.html',
        {
          NAVIGATION: function () {
            return vm.origNavigation;
          }
        },
        'sub-collection'
      ).result.finally(function () {
        vm.loadNavigationData();
      });

    }

    function confirmDeleteCollection(collectionID, subSectionID, sub, currentSection) {
      ALERT.confirmWarning(function () {
        return vm.deleteCollection(collectionID, subSectionID, sub, currentSection);
      });
    }

    function deleteCollection(collectionID, subSectionID, sub, currentSection) {

      vm.loading = true;

      var collectionIDs = _.pluck(sub.collections, '_id');

      collectionIDs = _.without(collectionIDs, collectionID);

      var parameters = {};

      parameters.sections = vm.origNavigation.sections;

      parameters.sections.forEach(function (data) {

        data.sub_sections.forEach(function (subSection) {

          if (subSection._id == subSectionID) {
            if (collectionIDs.length > 0)
              subSection.collections = collectionIDs;
            else
              delete subSection.collections;
            delete subSection._id;
          } else {

            if(subSection.collections.length <= 0) {
              delete subSection.collections;
            } else {
              subSection.collections = _.pluck(subSection.collections, '_id');
            }
          }

        });

      });

      API
        .Navigations
        .edit(vm.origNavigation._id, parameters)
        .then(function (data) {
          Notify.infoDeleted();
        })
        .catch(function (err) {
          Notify.errorSaving();
        }).finally(function () {
        vm.loading = false;
        setTimeout(function () {
          loadNavigationDataParam(currentSection);
        }, 2000);
      });

    }

    function addSubsection() {

      Modal.open(
        'NavigationAddSubSectionController',
        'vm',
        'app/modules/cms/navigation/create/addSubSection/cms.navigation.create.addSubSection.subSectionModal.html',
        {
          NAVIGATION: function () {
            var currentNavigation = { origNavigation: vm.origNavigation, currentSection: vm.currentSection };
            return currentNavigation;
          }
        },
        'sub-collection'
      ).result.finally(function () {
        loadNavigationDataParam(vm.currentSection);
      });

    }

    function saveChangings() {

      vm.loading = true;

      if (vm.isEdit()) { // means we are in editing mode.

        var params = {
          title: {
            en: '',
            ch: ''
          },
          seo: {}
        };

        params.title.en = vm.navigation.title.en;
        params.title.ch = vm.navigation.title.en;

        if (!_.isEmpty(vm.navigation.seo.title))
          params.seo.page_title = vm.navigation.seo.title;
        if (!_.isEmpty(vm.navigation.seo.description))
          params.seo.meta_desc = vm.navigation.seo.description;
        if (vm.navigation.seo.keywords.length > 0)
          params.seo.meta_kws = vm.navigation.seo.keywords.join(',');
        if (!_.isEmpty(vm.navigation.slug) && vm.origNavigation.slug != vm.navigation.slug)
          params.slug = vm.navigation.slug;
        if (vm.origNavigation.status != vm.navigation.status)
          params.status = vm.navigation.status;
        if (_.isEmpty(params.seo))
          delete params.seo;
        if (!angular.equals(vm.currentSection, vm.oldCurrentSection)) {

          var sections = angular.copy(vm.navigation.sections);

          sections.forEach(function (data) {

            data.sub_sections.forEach(function (subSection) {

              if(subSection.collections.length <=0) {
                delete subSection.collections;
              } else {
                subSection.collections = _.pluck(subSection.collections, '_id');
              }
            });

          });

          params.sections = sections;

        }

        if(vm.origNavigation.slug != vm.navigation.slug) {
          API
            .Navigations
            .getBySlug(vm.navigation.slug)
            .then(function (obj) {
              if(obj) {
                Notify.errorSlugSaving();
                vm.slugExist = true;
              }
            })
            .catch(function (err) {
              vm.slugExist = false;
              if(err.status == 404 || (err.data && err.data.statusCode == 404)) {
                vm.update(params);
                return;
              }
              Notify.errorSaving();
            }).
          finally(function () {
            vm.loading = false;
          });
          return;
        }

        vm.update(params);

        return;
      }

      // means we are in creating mode.
      var keywords = vm.navigation.seo.keywords.length > 0 && vm.navigation.seo.keywords.join(',') || '';
      var params = {
        title: {
          en: vm.navigation.title.en,
          ch: vm.navigation.title.en
        },
        slug: vm.navigation.slug,
        status: 'ACTIVE',
        seo: {},
        sections:
          [
            {
              title:
              {
                en: 'Default',
                ch: 'Default'
              },
              sub_sections:
                [
                  {
                    title:
                    {
                      en: 'Default',
                      ch: 'Default'
                    },
                    display_options:
                    {
                      section_title: true,
                      image_title: false
                    }
                  }
                ]
            } // default sub section.
          ]
      };
      if (vm.navigation.seo.title != '')
        params.seo.page_title = vm.navigation.seo.title;
      if (vm.navigation.seo.description != '' && vm.navigation.seo.description != undefined)
        params.seo.meta_desc = vm.navigation.seo.description;
      if (keywords != '')
        params.seo.meta_kws = keywords;


      API
        .Navigations
        .getBySlug(vm.navigation.slug)
        .then(function (obj) {
          if(obj) {
            Notify.errorSlugSaving();
            vm.slugExist = true;
          }
        })
        .catch(function (err) {
          vm.slugExist = false;
          if(err.status == 404 || (err.data && err.data.statusCode == 404)) {
            API
              .Navigations
              .create(params)
              .then(function (result) {
                $state.go('admin-panel.cms.navigation.create', {navigationID: result._id});
                vm.navigation.sections = result.sections;
                vm.navigation.seo = result.seo;
                vm.currentSection = vm.navigation.sections[0];
                Notify.infoSaved();
              })
              .catch(function (err) {
                Notify.errorSaving();
              })
              .finally(function () {
                vm.loading = false;
              });
            return;
          }
          Notify.errorSaving();
        }).finally(function () {
        vm.loading = false;
      });
    }

    function update(params) {

      API
        .Navigations
        .edit(vm.origNavigation._id, params)
        .then(function () {
          Notify.infoSaved();
          vm.oldCurrentSection = angular.copy(vm.currentSection);
          loadNavigationDataParam(vm.currentSection);
        })
        .catch(function () {
          Notify.errorSaving();
        })
        .finally(function () {
          vm.loading = false;
        });

    }

    function fillSlugTitle() {

      if (vm.isEdit()) return;
      vm.navigation.seo.title = vm.navigation.title.en;
      vm.navigation.slug = Slug.slugify(vm.navigation.title.en);

    }

    function setSlug() {

      vm.navigation.slug = Slug.slugify(vm.navigation.seo.title);

    }

    function confirmDeleteSection(section) {

      ALERT.confirmWarning(function () {
        return vm.deleteSection(section);
      });

    }

    function deleteSection(section) {

      vm.loading = true;

      var sections = vm.getSections();

      if (sections.length < 2) {
        Notify.errorOperation(); // can't delete if there is only 1 section.
        vm.loading = false;
        return;
      }
      sections = _.reject(sections, function (sec) { return sec.title.en == section.title.en });
      sections.forEach(function (data) {
        delete data._id;
      });

      var params = {
        'sections': sections
      };

      API
        .Navigations
        .edit(vm.origNavigation._id, params)
        .then(function (data) {
          Notify.infoDeleted();
          vm.loadNavigationData();

        })
        .catch(function (err) {
          Notify.errorSaving();
        }).
      finally(function () {
        vm.loading = false;
      });
    }

    function removeTag(tag) {
      vm.navigation.seo.keywords = _.without(vm.navigation.seo.keywords, tag);
    }

    function changeSection(section) {

      if (!angular.equals(vm.oldCurrentSection, vm.currentSection)) {
        Notify.infoSaveChanges();
        return;
      }
      vm.currentSection = section;
      vm.oldCurrentSection = angular.copy(vm.currentSection);

    }

    function slugify(input) {

      return Slug.slugify(input);

    }

    function addTag() {

      if (vm.navigation.seo.currentKeyword &&
        vm.navigation.seo.keywords.indexOf(vm.navigation.seo.currentKeyword) < 0) // avoid duplicate in repeat directive
        vm.navigation.seo.keywords.push(vm.navigation.seo.currentKeyword);
      vm.navigation.seo.currentKeyword = "";

    }

    function enableButton() {
      return vm.isEdit() ?
        (vm.navigation.title.en == '' || vm.navigation.slug == '' ? true :
        (vm.navigation.title.en == vm.origNavigation.title.en &&
        vm.navigation.seo.title == vm.origNavigation.seo.page_title &&
        vm.navigation.seo.description === vm.origNavigation.seo.meta_desc &&
        vm.navigation.slug == vm.origNavigation.slug &&
        vm.navigation.status == vm.origNavigation.status &&
        vm.navigation.seo.keywords.join(',') == (vm.origNavigation.seo.meta_kws === undefined ? '' : vm.origNavigation.seo.meta_kws)  ) &&
        angular.equals(vm.oldCurrentSection, vm.currentSection))
        : vm.navigation.title.en == '' || vm.navigation.slug == '';
    }

    function addCollections(subSection) {
      Modal.open(
        'NavigationAddCollectionModalController',
        'vm',
        'app/modules/cms/navigation/create/addCollections/cms.navigation.create.addCollections.addCollectionModal.html',
        {
          NAVIGATION: function () {
            var navigation = { origNavigation: vm.origNavigation, currentSection: vm.currentSection, subSection: subSection }
            return navigation;
          }
        },
        'lg'
      )
        .result.finally(function () {
        loadNavigationDataParam(vm.currentSection);
      });
    }

    function loadNavigationData() {

      vm.loading = true;

      API
        .Navigations
        .get(vm.navigationID)
        .then(function (data) {
          vm.navigation.title.en = data.title.en;
          vm.navigation.status = data.status;
          vm.navigation.slug = data.slug;
          vm.navigation.sections = data.sections;
          vm.navigation.seo.title = data.seo && data.seo.page_title;
          vm.navigation.seo.description = data.seo && data.seo.meta_desc;
          if (data.seo && data.seo.meta_kws && data.seo.meta_kws != '')
            vm.navigation.seo.keywords = data.seo.meta_kws.split(',');

          vm.currentSection = data.sections[0];
          vm.oldCurrentSection = angular.copy(vm.currentSection);
          vm.origNavigation = data;
        })
        .catch(function () {
          ALERT.confirm(vm.loadNavigationData);
        }).finally(function () {
        vm.loading = false;
      });

    }

    function loadNavigationDataParam(currentSection) {

      vm.loading = true;

      API
        .Navigations
        .get(vm.navigationID)
        .then(function (data) {
          vm.navigation.title.en = data.title.en;
          vm.navigation.status = data.status;
          vm.navigation.slug = data.slug;
          vm.navigation.sections = data.sections;
          vm.navigation.seo.title = data.seo && data.seo.page_title;
          vm.navigation.seo.description = data.seo && data.seo.meta_desc;
          if (data.seo && data.seo.meta_kws && data.seo.meta_kws != '')
            vm.navigation.seo.keywords = data.seo.meta_kws.split(',');

          data.sections.forEach(function (section) {
            if(section._id === currentSection._id) {
              vm.currentSection = section;
            }

          });

          vm.oldCurrentSection = angular.copy(vm.currentSection);
          vm.origNavigation = data;
        })
        .catch(function () {
          ALERT.confirm(loadNavigationDataParam);
        }).finally(function () {
        vm.loading = false;
      });

    }

    function getSections() {

      var sects = angular.copy(vm.origNavigation.sections);
      sects.forEach(function (data) {

        data.sub_sections.forEach(function (subSection) {
          if(subSection.collections && subSection.collections.length <= 0)
            delete subSection.collections;
          else
            subSection.collections = _.pluck(subSection.collections, '_id');
        });

      });

      return sects;

    }

    if(vm.isEdit()){
      vm.loadNavigationData();
    }

  }

})(angular.module('adminPanel.cms.navigation.create.controller',[]));
