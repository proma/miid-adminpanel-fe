(function (app) {

  app.controller('NavigationAddSectionController', NavigationAddSectionController);

  NavigationAddSectionController.$inject = ['$uibModalInstance','NAVIGATION','API'];

  function NavigationAddSectionController($uibModalInstance, NAVIGATION, API) {

    var sec = this;

    // Variables

    sec.loading = false;
    sec.sectionCreated = false;
    sec.sectionExist = false;

    // Functions

    sec.close = close;
    sec.submit = submit;


    // Functions Implementation

    function close() {

      $uibModalInstance.close();

    }

    function submit() {

      sec.loading = true;

      var params = {
        title: {
          en: NAVIGATION.title.en,
          ch: NAVIGATION.title.ch
        },
        slug: NAVIGATION.slug,
        sections: [],
        status: NAVIGATION.status
      };

      var section = {};
      var subsection = {};

      NAVIGATION.sections.forEach( function (data) {

        section = { _id: data._id , sub_sections: [] , title: { en: data.title.en, ch: data.title.en}};

        if(data.sub_sections.length > 0){

          data.sub_sections.forEach(function (subSection) {

            subsection =
            {
              _id: subSection._id,
              title: { en: subSection.title.en, ch: subSection.title.en } ,
              display_options: { section_title: subSection.display_options.section_title, image_title: subSection.display_options.image_title  },
              collections: []
            };

            if(subSection.collections && subSection.collections.length <= 0){
              delete subsection.collections;
            } else {
              subsection.collections = _.pluck(subSection.collections, '_id');
            }
            section.sub_sections.push(subsection);
          });
        }
        params.sections.push(section);
      });

      params.sections.push(
        {
          title:
          {
            en: sec.name,
            ch: sec.name
          },
          sub_sections:
            [{
              title:
              {
                en: 'Default',
                ch: 'Default'
              },
              display_options:
              {
                section_title: true,
                image_title: false
              }
            }]
        });

      API
        .Navigations
        .edit(NAVIGATION._id, params)
        .then(function (data) {
          sec.sectionCreated = true;
          sec.sectionExist = false;
        })
        .catch(function (err) {
          sec.sectionExist = true;
          sec.sectionCreated = false;
        }).
      finally(function () {
          sec.loading = false;
      });
    }
  }

})(angular.module('adminPanel.cms.navigation.create.addSection.controller',[]));
