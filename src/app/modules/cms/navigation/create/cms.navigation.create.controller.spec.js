'use strict';

describe('Controllers', function () {

  describe('CreateNavigationController', function () {

    var data = {
      "_id": "580e24402514196a426debd6",
      "sections": [
        {
          "_id": "58127a0fb05f200001f63d91",
          "sub_sections": [
            {
              "_id": "5816005ad5fdf300013f3075",
              "collections":
                [
                  {
                    "_id": "5807828a7e05e400019d4584"
                  },
                  {
                    "_id": "57593e28ed82800600000011"
                  }
                ],
              "display_options": {
                "image_title": false,
                "section_title": true
              },
              "title": {
                "en": "ss1",
                "ch": "ss1"
              }
            },
            {
              "_id": "5812785ab05f200001f63d8a",
              "collections": [],
              "display_options": {
                "image_title": false,
                "section_title": true
              },
              "title": {
                "en": "ss3",
                "ch": "ss3"
              }
            }
          ],
          "title": {
            "en": "s1",
            "ch": "s1"
          }
        }
      ],
      "title": {
        "en": "N4",
        "ch": "N4"
      }
    };

    var
      Controller,
      $rootScope,
      URL,
      $httpBackend,
      ALERT,
      $stateParams,
      API,
      $state,
      $scope,
      Notify,
      Modal,
      QINGSTOR;

    // load the dependencies module

    beforeEach(module('ui.bootstrap'));
    beforeEach(module('ngResource'));
    beforeEach(module('oitozero.ngSweetAlert'));
    beforeEach(module('slugifier'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('ngFileUpload'));
    beforeEach(module('adminPanel.services.notifier'));
    beforeEach(module('adminPanel.services.modal'));
    beforeEach(module('ui.router'));
    beforeEach(module('adminPanel.services.alert'));
    beforeEach(module('adminPanel.api'));
    beforeEach(module('adminPanel.cms.navigation.create.controller'));

    beforeEach(inject(function ($injector) {

      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      $httpBackend = $injector.get('$httpBackend');
      URL = $injector.get('URL');
      var $controller = $injector.get('$controller');
      $stateParams = {};
      $state = $injector.get('$state');
      API = $injector.get('API');
      Notify = $injector.get('Notify');
      Modal = $injector.get('Modal');
      ALERT = $injector.get('ALERT');
      QINGSTOR = $injector.get('QINGSTOR');
      Controller = $controller('CreateNavigationController', {
        '$scope': $scope,
        'API': API,
        'Modal': Modal,
        '$stateParams': $stateParams,
        '$state': $state,
        'Slug': $injector.get('Slug'),
        'ALERT': ALERT,
        'Notify': Notify,
        'MIID_API': $injector.get('MIID_API')
      });

      Controller.origNavigation = data;
      $httpBackend.when('GET', URL.NAVIGATIONS ).respond(200, data);

    }));


    it('should be in edit mode when there is navigationID', function () {

      $stateParams.navigationID = '12345';
      expect(Controller.isEdit()).toBeTruthy();

    });

    it('should be in create mode when there is no navigationID', function () {

      expect(Controller.isEdit()).toBeFalsy();

    });

    it('should have default status of INACTIVE', function () {

      expect(Controller.navigation.status).toEqual('INACTIVE');

    });

    it('should set slug form seo title', function () {

      Controller.navigation.seo.title = 'this is test';
      Controller.setSlug();
      expect(Controller.navigation.slug).toEqual('this-is-test');

    });

    it('should return slugified string', function () {

      expect(Controller.slugify('this is test')).toEqual('this-is-test');

    });

    it('should set seo title and slug from collection title when not in edit mode', function () {

      Controller.navigation.title.en = 'this is test';
      Controller.fillSlugTitle();
      expect(Controller.navigation.seo.title).toEqual('this is test');
      expect(Controller.navigation.slug).toEqual('this-is-test');

    });

    it('shouldnt set seo title and slug from navigation title while in edit mode', function () {

      Controller.navigation.title.en = 'this is test';
      $stateParams.navigationID = 'bla bla';
      Controller.fillSlugTitle();
      expect(Controller.navigation.seo.title).not.toEqual('this is test');
      expect(Controller.navigation.slug).not.toEqual('this-is-test');

    });

    it('should add keyword in seo', function () {

      Controller.navigation.seo.currentKeyword = 'this is test';
      Controller.addTag();
      expect(Controller.navigation.seo.keywords.length).toEqual(1);
      expect(Controller.navigation.seo.currentKeyword).toEqual('');
      expect(Controller.navigation.seo.keywords[0]).toEqual('this is test');

    });

    it('should remove keyword in seo', function () {

      Controller.navigation.seo.currentKeyword = 'this is test';
      Controller.addTag();
      expect(Controller.navigation.seo.keywords.length).toEqual(1);
      Controller.removeTag('this is test');
      expect(Controller.navigation.seo.keywords.length).toEqual(0);

    });

    it('should hit PUT while in edit mode', function () {
      spyOn(Controller,'update');

      Controller.origNavigation.slug = Controller.navigation.slug;
      $stateParams.navigationID = "asdfs";
      Controller.saveChangings();

      expect(Controller.update).toHaveBeenCalled();
    });

    it('should hit POST while not in edit mode', function () {
      spyOn($state, 'go');
      $httpBackend.expect('GET', URL.NAVIGATIONS +'/slug' ).respond(404, {statusCode: 404});
      $httpBackend.expect('POST', URL.NAVIGATIONS).respond(200, {sections: [{test: 's'}]});
      Controller.saveChangings();

      $httpBackend.flush();

      expect($state.go).toHaveBeenCalled();
    });

    it('should call loading false before the request starts', function () {

      expect(Controller.loading).toBeFalsy();

    });

    it('should call the enableButton function to be true when not in edit mode', function () {

      expect(Controller.isEdit()).toBeFalsy();
      expect(Controller.enableButton()).toBeTruthy();

    });

    it('should set the value of the baseUrl', function () {

      expect(Controller.baseUrl).toBe(QINGSTOR.base_url);

    });

  });

});
