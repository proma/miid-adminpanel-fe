(function (app) {

})(angular.module('adminPanel.cms.navigation.create',
  ['adminPanel.cms.navigation.create.addCollections',
    'adminPanel.cms.navigation.create.controller',
    'adminPanel.cms.navigation.create.routes',
    'adminPanel.cms.navigation.create.addSection',
    'adminPanel.cms.navigation.create.addSubSection']));
