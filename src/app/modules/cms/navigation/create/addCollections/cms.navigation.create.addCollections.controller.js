(function (app) {

  app.controller('NavigationAddCollectionModalController', NavigationAddCollectionModalController);

  NavigationAddCollectionModalController.$inject = ['$uibModalInstance', 'NAVIGATION','API', 'QINGSTOR', '$translate'];

  function NavigationAddCollectionModalController($uibModalInstance, NAVIGATION, API, QINGSTOR, $translate) {

    var vm = this;

    // Variables

    vm.baseUrl = QINGSTOR.base_url;
    vm.loading = false;
    vm.loadCollectionData = loadCollectionData;
    vm.selectorForCollections = {};
    var origNavigation = NAVIGATION.origNavigation;
    var currentSubSection = NAVIGATION.subSection;
    var origCollections = [];
    vm.collections = [];
    vm.leftListBulk = [
      {
        title: $translate.instant('modals.product.add') || 'Add to Listing',
        action: function () {
          vm.submit();
        }
      }
    ];

    // Functions

    vm.close = close;
    vm.submit = submit;


    // Functions Implementation

    function close() {

      $uibModalInstance.close();

    }


    function submit() {

      vm.loading = true;

      var params = {
        title: {
          en: origNavigation.title.en,
          ch: origNavigation.title.ch
        },
        slug: origNavigation.slug,
        sections: [],
        status: origNavigation.status
      };

      var section = {};
      var subsection = {};
      origNavigation.sections.forEach(function (data) {

        section = {
          _id: data._id ,
          sub_sections: [] ,
          title: {
            en: data.title.en,
            ch: data.title.en
          }
        };

        if(data.sub_sections.length > 0) {

          data.sub_sections.forEach(function (subSection) {

            subsection =
            {
              _id: subSection._id,
              title: { en: subSection.title.en, ch: subSection.title.en } ,
              display_options: { section_title: subSection.display_options.section_title, image_title: subSection.display_options.image_title  },
              collections: []
            };

            if(currentSubSection._id == subSection._id) {

              vm.collections.forEach(function (data) {

                if(data.checked) {
                  subsection.collections.push(data.coll._id);
                }
              });

            } else {
              if(subSection.collections.length <=0) {
                delete subsection.collections;
              } else {
                subsection.collections = _.pluck(subSection.collections, '_id');
              }
            }
            section.sub_sections.push(subsection);
          });
        }
        params.sections.push(section);
      });

      API
        .Navigations
        .edit(origNavigation._id, params)
        .then(function (data) {
          vm.close();
        })
        .catch(function (err) {

        })
        .finally(function () {
          vm.loading = false;
      });
    }

    function loadCollectionData() {

      vm.loading = true;
      var params = {};
      API.
      Collections.
      getAll(params).
      then(function (data) {

        origCollections = data.data;
        var subSectionCollectionIDs;
        if(currentSubSection.collections.length <=0 ) {

          data.data.forEach(function (collection) {
            vm.collections.push({ checked: false, coll: collection });
          });

        } else {

          subSectionCollectionIDs = _.intersection(_.pluck(origCollections, '_id'), _.pluck(currentSubSection.collections, '_id'));

          data.data.forEach(function (collection) {
            var filter = true;
            subSectionCollectionIDs.forEach(function (id) {
              if(id == collection._id){
                filter = false;
                vm.collections.push({ checked: true, coll: collection });
              }
            });
            if(filter) {
              vm.collections.push({ checked: false, coll: collection  });
            }

          });
        }
      }).catch(function (err) {

      }).finally(function () {
        vm.loading = false;
      });
    }
    vm.loadCollectionData();
  }


})(angular.module('adminPanel.cms.navigation.create.addCollections.controller',[]));

