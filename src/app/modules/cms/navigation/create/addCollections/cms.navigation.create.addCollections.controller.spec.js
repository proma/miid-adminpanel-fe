'use strict';

fdescribe('Controllers', function () {

  describe('NavigationAddCollectionModalController', function () {

    var data = [{
      "checked": false,
      "coll": {
        "_id": "5812d5d8b05f200001f63da7",
        "title": "NewMerchant",
        "slug": "newmerchant",
        "status": "ACTIVE",
        "sections": [],
        "images": {
          "d_top_banner": "",
          "m_top_banner": "",
          "logo": ""
        },
        "type": "BRANDS"
      }

    }];

    var navigation = {
      "subSection":
      {
        "_id": "5816005ad5fdf300013f3075",
        "collections": [
          {
            "_id": "5807828a7e05e400019d4584"
          },
          {
            "_id": "57593e28ed82800600000011"
          }
        ],
        "display_options": {
          "image_title": false,
          "section_title": true
        },
        "title": {
          "en": "ss1",
          "ch": "ss1"
        }
      },
      "origNavigation": {
        "_id": "580e24402514196a426debd6",
        "sections": [
          {
            "_id": "58127a0fb05f200001f63d91",
            "sub_sections": [
              {
                "_id": "5816005ad5fdf300013f3075",
                "collections": [
                  {
                    "_id": "5807828a7e05e400019d4584"
                  }
                ],
                "display_options": {
                  "image_title": false,
                  "section_title": true
                }
              }
            ]
          }
        ],
        "title": {
          "en": "N4",
          "ch": "N4"
        }
      }
    };

    var
      Controller,
      $rootScope,
      URL,
      $httpBackend,
      $timeout,
      ALERT,
      $scope,
      QINGSTOR,
      $uibModalInstance;

    // load the dependencies module

    beforeEach(module('ui.bootstrap'));
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.services.notifier'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.api'));
    beforeEach(module('adminPanel.services.alert'));
    beforeEach(module('adminPanel.cms.navigation.create.addCollections.controller'));

    beforeEach(inject(function ($injector) {
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      $httpBackend = $injector.get('$httpBackend');
      URL = $injector.get('URL');
      var $controller = $injector.get('$controller');
      ALERT = $injector.get('ALERT');
      QINGSTOR = $injector.get('QINGSTOR');
      $uibModalInstance =  {
        // create a mock object using spies
        close: jasmine.createSpy('$uibModalInstance.close')
      };

      Controller = $controller('NavigationAddCollectionModalController', {
        '$scope': $scope,
        '$uibModalInstance': $uibModalInstance,
        'API': $injector.get('API'),
        'NAVIGATION': navigation,
        'ALERT': ALERT,
        'QINGSTOR': QINGSTOR
      });

      Controller.selectorForCollections.reset = function() {}; //fake coz of directive
      Controller.collections = data;
      Controller.origNavigation = navigation.origNavigation;
      Controller.currentSubSection = navigation.subSection;
      Controller.loadCollectionData();

    }));

    it('collections array should be initialized', function () {
      expect(Controller.collections.length).toEqual(1);
    });

    it(' should call loading false before the request starts', function () {
      expect(Controller.loading).toBeTruthy();
    });

    it('should close the modal', function () {
      Controller.close();
      expect($uibModalInstance.close).toHaveBeenCalled();
    });

    it('should set the baseUrl', function () {
      expect(Controller.baseUrl).toBe(QINGSTOR.base_url);
    });

    it('should set the left list bulk title to Add to Listing', function () {
      Controller.leftListBulk.title = 'Add to Listing';
      expect(Controller.leftListBulk.title).toBe('Add to Listing');
    });
  });
});
