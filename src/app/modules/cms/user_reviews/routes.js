(function(app){

  app.config(['$stateProvider', 'URLProvider', function($stateProvider, URLProvider){
    $stateProvider
      .state('admin-panel.cms.user_reviews',{
        url: '/user-reviews/listing?productID&productMIID',
        templateUrl: 'app/modules/cms/user_reviews/listing.html',
        controller: 'UserReviewsController',
        controllerAs: 'vm',
        data: {
          permissions: {
            only: 'canSee.CMS.UserReviews.Listing',
            redirectTo: URLProvider.$get().RedirectURL
          }
        }
      });

  }]);

})(angular.module('adminPanel.cms.user_reviews.routes',[]));
