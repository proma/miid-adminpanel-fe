(function (app) {

  app.controller('CMSUserReviewsChangeStatusModalController', CMSUserReviewsChangeStatusModalController);

  CMSUserReviewsChangeStatusModalController.$inject = ['API', '$uibModalInstance', 'Notify', 'Status', 'Classes', 'ReviewsList'];

  function CMSUserReviewsChangeStatusModalController(API, $uibModalInstance, Notify, Status, Classes, ReviewsList) {
    var modal = this;


    modal.title = 'Change Status';
    modal.statuses = Status;
    modal.selectedStatus = 'APPROVED';
    modal.changingStatus = false;
    modal.statusClass = Classes;

    modal.close = function () {
      $uibModalInstance.dismiss('cancel');
    };

    modal.submit = function () {
      change();
    };

    function change() {

      modal.changingStatus = true; //disables button

      var noOfCalls = 0; //no of http requests to call

      angular.forEach(ReviewsList, function (data) {
        if (data.checked) {
          if (modal.selectedStatus != data.status)
            ++noOfCalls;
        }
      });

      //if noOfCalls 0 then return

      if (noOfCalls === 0) {
        modal.changingStatus = false;
        Notify.infoSaved();
        return;
      }

      // change status of all the objects.
      var completedReq = 0;
      var noOfErrors = 0;

      function callBack(err) {
        if (!err) {
          ++completedReq;
        }
        if (err) {
          ++noOfErrors;
        }
        if (noOfCalls == (completedReq + noOfErrors)) {
          modal.changingStatus = false;

          Notify.infoSaved();

          $uibModalInstance.close({
            success: true
          });
        }
      }

      angular.forEach(ReviewsList, function (data) {
        if (data.checked)
          if (modal.selectedStatus != data.status) {
            //we send request to change status.
            API.UserReviews
              .updateStatus(data._id, modal.selectedStatus)
              .then(function (result) {
                data.status = modal.selectedStatus;
                callBack();
              })
              .catch(function (err) {
                Notify.errorSaving();
                callBack(err);
              });
          }
      });


    }
  }

})(angular.module('adminPanel.cms.user_reviews.changeStatusModal.controller', []));
