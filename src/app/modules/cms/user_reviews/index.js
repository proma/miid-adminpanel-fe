(function (app) {

})(angular.module('adminPanel.cms.user_reviews', [
  'adminPanel.cms.user_reviews.routes',
  'adminPanel.cms.user_reviews.controller',
  'adminPanel.cms.user_reviews.changeStatusModal'
]));
