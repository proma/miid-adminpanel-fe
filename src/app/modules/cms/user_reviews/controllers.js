(function (app) {
  'use strict';

  app.controller('UserReviewsController', UserReviewsController);

  UserReviewsController.$inject = ['$scope', 'ALERT', 'API', 'Notify', 'Modal', 'QINGSTOR', '$stateParams', '$translate'];

  function UserReviewsController($scope, ALERT, API, Notify, Modal, QINGSTOR, $stateParams, $translate) {
    var vm = this;

    vm.isForProduct = false;
    vm.productId = $stateParams.productID;
    vm.productMIID = $stateParams.productMIID;
    if(vm.productId && vm.productMIID) {
      vm.isForProduct = true;
    }

    vm.head = [
      {
        title: 'Rating',
        query: 'rating'
      },
      {
        title: 'Date',
        query: 'date'
      },
      {
        title: 'Status',
        query: 'status'
      }
    ];
    vm.statusClass = {
      APPROVED: 'approved',
      PENDING: 'review-pending',
      REJECTED: 'rejected',
      ARCHIVED: 'archived'
    };

    vm.status = [
      {
        name: $translate.instant('filters.status.label') || 'Status',
        value: 'status',
        disabled: true
      },
      {
        name: 'Approved',
        value: 'APPROVED'
      },
      {
        name: 'Pending',
        value: 'PENDING',
        disabled: true
      },
      {
        name: 'Rejected',
        value: 'REJECTED'
      },
      {
        name: 'Archived',
        value: 'ARCHIVED'
      }
    ];

    vm.brands = [{
      name: $translate.instant('filters.brand.label') || 'Brand',
      id: -1
    }];

    vm.filters = {
      status: vm.status[0].value,
      brand: vm.brands[0],
      from: moment((new Date("1/1/" + moment().year())).valueOf()),
      to: moment(),
      text: ""
    };

    vm.assetURL = QINGSTOR.base_url;
    vm.loading = true;
    vm.reviews = [];
    vm.totalItems = 0;
    vm.itemsGot = 0;
    vm.previousPage = 0;
    vm.currentPage = 1;
    vm.itemsLimit = 10;
    vm.loadCached = false; //boolean to load cached result

    // get all brands
    vm.listBrands = function () {
      API
        .Brands
        .getAll(null, null, 'LIST_VALUES')
        .then(function (brands) {
          if (brands.data && brands.data.length) {

            brands.data.forEach(function (brand) {
              vm.brands.push({id: brand._id, name: brand.name});
            })
          }
        }).catch(function (err) {
        vm.brands = [{name: $translate.instant('errors.errorLoadingBrand') || 'Error loading brands', id: -1}]
      });
    };

    //Get all reviews
    vm.listReviews = function () {
      vm.sortOptions.count = 0;
      vm.sortOptions.listBox = false;
      vm.loading = true;

      API
        .UserReviews
        .getAll(vm.itemsLimit
          , vm.currentPage - 1
          , vm.sortOptions.order
          , vm.sortOptions.category
          , (vm.filters.status != 'status') ? vm.filters.status : null
          , (vm.filters.brand.id != -1) ? vm.filters.brand.id : null
          , vm.filters.from.valueOf()
          , vm.filters.to.valueOf()
          , (vm.filters.text && vm.filters.text.trim()) ? vm.filters.text.trim() : null
          , (vm.isForProduct) ? vm.productId : null
        ).then(function (re) {
        angular.forEach(re.data, function (obj) {
          //for checkbox
          obj.checked = false;
        });
        vm.loading = false;
        vm.reviews = re.data;
        // set display flag for client side text filtering
        _.each(vm.reviews, function (item) {
          item.display = true;
          var totalPics = item.pictures.length;
          for(var i=0; i<(4 - totalPics); i++) {
            item.pictures.push(null);
          }
        });
        vm.itemsGot = vm.reviews.length;
        vm.totalItems = re.paging.total;
      })
        .catch(function (err) {
          ALERT.confirm(vm.listReviews());
          vm.loading = false;
        });
    };


    //check all checkboxes
    vm.toggleCheckbox = function (checkAll) {
      angular.forEach(vm.reviews, function (data) {
        data.checked = checkAll;
      });
      if (checkAll)
        vm.sortOptions.count = vm.reviews.length; //as this checks all the current page reviews
      else
        vm.sortOptions.count = 0;
    };

    //mark checkbox

    vm.markCheckBox = function (status) {
      vm.sortOptions.count = 0;
      var alreadyChecked = false;
      angular.forEach(vm.reviews, function (data) {
        if (data.checked) {
          alreadyChecked = true;//just need one checked box
          ++vm.sortOptions.count; //inc counter
        }
      });

      if (status) {
        //mark checkbox as true
        vm.sortOptions.listBox = true;
      } else {
        //check if checkbox is already true
        //check if none boxes are checked
        if (!alreadyChecked) {
          vm.sortOptions.listBox = false;
        }
      }

    };

    //change page

    vm.changePage = function () {
      vm.previousPage = (vm.currentPage - 1);
      vm.loading = true;
      vm.listReviews();
    };

    //change single status
    vm.changeSingleStatus = function (item, status) {

      item.changingStatus = true;

      if (item.status === status) {
        item.changingStatus = false;
        return;
      }
      API.UserReviews
        .updateStatus(item._id, status)
        .then(function (result) {
          item.status = status;
          Notify.infoSaved();
          item.changingStatus = false;
        })
        .catch(function (err) {
          Notify.errorSaving();
          item.changingStatus = false;
        });
    };

    //Change status of the selected reviews
    vm.changeStatus = function () {
      Modal.open(
        'CMSUserReviewsChangeStatusModalController',
        'modal',
        'app/modules/cms/user_reviews/change_status_modal/cms.userReviews.changeStatusModal.html',
        {
          ReviewsList: function () {
            return vm.reviews;
          },
          Status: function () {
            return vm.status;
          },
          Classes: function () {
            return vm.statusClass;
          }
        }
      );
    }; // end change status function

    //sorting table header options
    vm.sortOptions = {
      templateUrl: 'cms/user_reviews/templates/tableHead.html',
      order: 'desc',
      category: 'date', //first initialize
      handler: function () {
        //start loading so that user can't make more request.
        vm.loading = true;
        vm.currentPage = 1; //set page to 0 again.
        vm.listReviews();
      },
      toggleCheckbox: vm.toggleCheckbox,
      listBox: false, //initial
      count: 0,
      changeStatus: vm.changeStatus
    };

    //to clear product filter and list all review
    vm.clearProdAndList = function() {
      delete vm.productId;
      delete vm.productMIID;
      vm.isForProduct = false;
      vm.listReviews();
    };

    // to populate Brands drop down
    vm.listBrands();

    // get list of reviews
    vm.listReviews();

    // work around for date picker bug, that is not except variables in vm but except scope variable and functions
    $scope.dateChanged = function () {
      vm.listReviews();
    };


    $scope.$watch("vm.filters.from", function () {
      if (vm.filters.from.valueOf() > vm.filters.to.valueOf()) {
        vm.filters.from = moment(vm.filters.to);
      }
    });

    $scope.$watch("vm.filters.to", function () {
      if (vm.filters.to.valueOf() < vm.filters.from.valueOf()) {
        vm.filters.to = moment(vm.filters.from);
      }
    });

  }


})(angular.module('adminPanel.cms.user_reviews.controller', []));
