(function(app) {

  app.config(['$stateProvider', function($stateProvider) {

    $stateProvider
      .state('admin-panel.cms', {
        url: '/cms',
        abstract: true,
        template: '<ui-view></ui-view>'
      });

  }]);

})(angular.module('adminPanel.cms.routes',[]));
