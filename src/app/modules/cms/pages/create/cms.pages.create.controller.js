(function (app) {

  app.controller('CreatePageController', CreatePageController);

  CreatePageController.$inject = ['Modal','$stateParams', '$state', 'Slug', 'API', 'ALERT', 'Notify', 'Upload','QINGSTOR', 'MIID_API', '$translate'];

  function CreatePageController(Modal, $stateParams, $state, Slug, API, ALERT, Notify, Upload, QINGSTOR, MIID_API, $translate) {

    var vm = this;

    // Variables

    vm.baseUrl = QINGSTOR.base_url;
    vm.secOne = true;
    vm.secTwo = true;
    vm.secThree = true;
    vm.secFour = true;
    vm.loading = false;
    vm.slugExist = false;
    vm.origPage = {};
    vm.oldMainBanner = {};
    vm.oldSections = {};
    vm.selectedPane = '';
    vm.page = {
      title: {
        en: '',
        ch: ''
      },
      slug: '',
      type: 'HOME_PAGE',
      status: 'INACTIVE', //default is inactive
      seo: {
        title: '',
        description: '',
        currentKeyword: '',
        keywords: []
      },
      sections: [],
      main_banner: []
    };
    vm.pageMainBanner = [
       {
        name: 'homePageCreatePage.mainBanner.mobile1',
        path: '',
        images: []
      },
       {
        name: 'homePageCreatePage.mainBanner.mobile2',
        path: '',
        images: []
      },
       {
        name: 'homePageCreatePage.mainBanner.mobile3',
        path: '',
        images: []
      },
       {
        name: 'homePageCreatePage.mainBanner.mobile4',
        path: '',
        images: []
      },
       {
        name: 'homePageCreatePage.mainBanner.desktop1',
        path: '',
        images: []
      },
       {
        name: 'homePageCreatePage.mainBanner.desktop2',
        path: '',
        images: []
      },
       {
        name: 'homePageCreatePage.mainBanner.desktop3',
        path: '',
        images: []
      },
       {
        name: 'homePageCreatePage.mainBanner.desktop4',
        path: '',
        images: []
      }
    ];
    vm.gridByTwo = getGrids(4);
    vm.gridByThree = getGrids(9);
    vm.mainBannerVariable = getGrids(8);

    // Functions

    vm.enableButton = enableButton;
    vm.saveChangings = saveChangings;
    vm.fillSlugTitle = fillSlugTitle;
    vm.setSlug = setSlug;
    vm.slugify = slugify;
    vm.addTag = addTag;
    vm.removeTag = removeTag;
    vm.isEdit = isEdit;
    vm.loadPageData = loadPageData;
    vm.uploadMainBanner = uploadMainBanner;
    vm.removeMainBanner = removeMainBanner;
    vm.addCollections = addCollections;
    vm.addProducts = addProducts;
    vm.saveGridByTwoImage = saveGridByTwoImage;
    vm.removeGridByTwoImage = removeGridByTwoImage;
    vm.confirmDeleteCollection = confirmDeleteCollection;
    vm.confirmDeleteProduct = confirmDeleteProduct;
    vm.deleteCollection = deleteCollection;
    vm.deleteProduct = deleteProduct;
    vm.saveLink = saveLink;
    vm.enableSaveLink = enableSaveLink;
    vm.saveGridByThreeImage = saveGridByThreeImage;
    vm.removeGridByThreeImage = removeGridByThreeImage;
    vm.update = update;

    // Function Implementation

    function enableButton() {
      return vm.isEdit() ?
        (vm.page.title.en == '' || vm.page.slug == '' ? true :
        (vm.page.title.en == vm.origPage.title.en &&
        vm.page.seo.title == vm.origPage.seo.page_title  &&
        vm.page.seo.description == vm.origPage.seo.meta_desc &&
        vm.page.slug == vm.origPage.slug &&
        vm.page.seo.keywords.join(',') == vm.origPage.seo.meta_kws) &&
        angular.equals(vm.oldSections, vm.page.sections) &&
        angular.equals(vm.oldMainBanner, vm.page.main_banner))
        : vm.page.title.en == '' || vm.page.slug == '';
    }

    function getGrids(howMany) {
      var temp = [];
      for(var i = 0; i< howMany; i++) {
        temp.push({uploading: false, uploadProgress: 0});
      }
      return temp;
    }

    function saveChangings() {
      vm.loading = true;
      if(vm.isEdit()) { // means we are in edit mode
        var params = setEditParams();
        if(vm.origPage.slug != vm.page.slug) {
          API
            .Pages
            .getBySlug(vm.page.slug)
            .then(function (obj) {
              if(obj) {
                Notify.errorSlugSaving();
                vm.slugExist = true;
              }
            })
            .catch(function (err) {
              vm.slugExist = false;
              if(err.status == 404 || (err.data && err.data.statusCode == 404)) {
                vm.update(params);
                return;
              }
              Notify.errorSaving();
            }).finally(function () {
              vm.loading = false;
          });
          return;
        }
        vm.update(params);
        return;
      }

      // means we are in creating mode.
      var params = setCreateParams();
      API
        .Pages
        .getBySlug(vm.page.slug)
        .then(function (obj) {
          if(obj) {
            Notify.errorSlugSaving();
            vm.slugExist = true;
            vm.loading = false;
          }
        })
        .catch(function (err) {
          vm.slugExist = false;
          if(err.status == 404 || (err.data && err.data.statusCode == 404)) {
            API
              .Pages
              .create(params)
              .then(function (data) {
                $state.go('admin-panel.cms.pages.create', {pageID: data._id});
                setPageData(data);
                Notify.infoSaved();
              })
              .catch(function (err) {
                Notify.errorSaving();
                vm.loading = false;
              });
            return;
          }
          Notify.errorSaving();
          vm.loading = false;
        });
    }

    function update(parameter) {
      API
        .Pages
        .edit(vm.origPage._id, parameter)
        .then(function () {
          Notify.infoSaved();
          vm.loadPageData();
        })
        .catch(function () {
          Notify.errorSaving();
        }).finally(function () {
          vm.loading = false;
      });
    }

    function fillSlugTitle() {
      if (vm.isEdit()) return;
      vm.page.seo.title = vm.page.title.en;
      vm.page.slug = Slug.slugify(vm.page.title.en);
    }

    function setSlug() {
      vm.page.slug = Slug.slugify(vm.page.seo.title);
    }

    function slugify(input) {
      return Slug.slugify(input);
    }

    function addTag() {
      if (vm.page.seo.currentKeyword &&
        vm.page.seo.keywords.indexOf(vm.page.seo.currentKeyword) < 0) // avoid duplicate in repeat directive
        vm.page.seo.keywords.push(vm.page.seo.currentKeyword);
      vm.page.seo.currentKeyword = "";
    }

    function removeTag(tag) {
      vm.page.seo.keywords = _.without(vm.page.seo.keywords, tag);
    }

    function isEdit() {
      vm.pageID = $stateParams.pageID && $stateParams.pageID != '' && $stateParams.pageID != 'new' ? $stateParams.pageID : null;
      return vm.pageID != null;
    }

    function loadPageData() {
      vm.loading = true;
      API
        .Pages
        .get(vm.pageID)
        .then(function (data) {
          setPageData(data);
      })
        .catch(function (err) {
        ALERT.confirm(vm.loadPageData);
      })
        .finally(function () {
        vm.loading = false;
      });
    }

    function uploadMainBanner(file, index) {
      if(!vm.pageID){
        Notify.errorSaving();
        return;
      }
       vm.mainBannerVariable[index].uploading = true;
       var uploadUrl = MIID_API.base_url + '/media/upload/page/' + vm.pageID + '?page_banner=' + vm.page.main_banner[index]._id;
      Upload.upload({
        url: uploadUrl,
        data: {'file' : file}
      }).then(function (resp) {
        vm.mainBannerVariable[index].uploading = false;
        if(resp.data.path) {
          var d = new Date();
            vm.pageMainBanner[index].path = resp.data.path;
            vm.pageMainBanner[index].images = vm.baseUrl +  resp.data.path + '?' + d.getTime();
          Notify.infoImageSaved();
          vm.loadPageData();
          return;
        }
        Notify.errorSaving();
      }, function (err) {
        vm.mainBannerVariable[index].uploading = false;
        Notify.errorSaving();
      }, function (evt) {
        vm.mainBannerVariable[index].uploadProgress = parseInt(100.0 * evt.loaded / evt.total);
      });
    }

    function removeMainBanner(file, index) {
      var type = 'page';
      var  path = vm.pageMainBanner[index].path;
      var id = vm.pageID + '?page_banner=' + vm.page.main_banner[index]._id ;
      API
        .Media
        .removeImage(id,type,path)
        .then(function (resp) {
          Notify.infoDeleted();
          vm.loadPageData();
        }).catch(function (err) {
        Notify.errorSaving();
      });
    }

    function addCollections() {
      Modal.open(
        'PagesAddCollectionModalController',
        'vm',
        'app/modules/cms/pages/create/addCollection/cms.pages.create.addCollection.addCollectionModal.html',
        {
          PAGE: function () {
            return vm.origPage;
          }
        },
        'lg'
      )
        .result.then(function () {
        vm.loadPageData();
      });
    }

    function addProducts() {
      Modal
        .open(
        'PagesAddProductModalController',
        'vm',
        'app/modules/cms/pages/create/addProduct/cms.pages.create.addProduct.addProductModal.html',
        {
          PAGE: function () {
            return vm.origPage;
          }
        },
        'lg'
      )
        .result
        .then(function () {
        vm.loadPageData();
      });
    }

    function saveGridByTwoImage(file, params) {
      if(!vm.pageID){
        Notify.errorSaving();
        return;
      }
      vm.gridByTwo[params.uploadingId].uploading = true;
      var uploadUrl = MIID_API.base_url + '/media/upload/page/' + vm.pageID + '?page_section=' + vm.page.sections[0]._id + '&page_grid=' + params.gridId;
      Upload.upload({
        url: uploadUrl,
        data: {'file' : file}
      }).then(function (resp) {
        vm.gridByTwo[params.uploadingId].uploading = false;
        Notify.infoImageSaved();
        vm.loadPageData();
      }, function (err) {
        vm.gridByTwo[params.uploadingId].uploading = false;
        Notify.errorSaving();
      }, function (evt) {
        vm.gridByTwo[params.uploadingId].uploadProgress = parseInt(100.0 * evt.loaded / evt.total);
      });
    }

    function removeGridByTwoImage(index,gridByTwo) {
      var id = vm.pageID + '?page_section=' + vm.page.sections[0]._id + '&page_grid=' + gridByTwo._id;
      var type = 'page';
      var path = vm.baseUrl + gridByTwo.thumb;
      API
        .Media
        .removeImage(id, type, path)
        .then(function (data) {
          Notify.infoDeleted();
          vm.loadPageData();
      })
        .catch(function (err) {
          Notify.errorSaving();
      })
        .finally(function () {
      });
    }

    function saveGridByThreeImage(file, params) {
      if(!vm.pageID){
        Notify.errorSaving();
        return;
      }
      vm.gridByThree[params.uploadingId].uploading = true;
      var uploadUrl = MIID_API.base_url + '/media/upload/page/' + vm.pageID + '?page_section=' + vm.page.sections[1]._id + '&page_grid=' + params.gridId;
      Upload.upload({
        url: uploadUrl,
        data: {'file' : file}
      }).then(function (resp) {
        vm.gridByThree[params.uploadingId].uploading = false;
        Notify.infoImageSaved();
        vm.loadPageData();
      }, function (err) {
        vm.gridByThree[params.uploadingId].uploading = false;
        Notify.errorSaving();
      }, function (evt) {
        vm.gridByThree[params.uploadingId].uploadProgress = parseInt(100.0 * evt.loaded / evt.total);
      });
    }

    function removeGridByThreeImage(index,gridByThree) {
      var id = vm.pageID + '?page_section=' + vm.page.sections[1]._id + '&page_grid=' + gridByThree._id;
      var type = 'page';
      var path = vm.baseUrl + gridByThree.thumb;
      API
        .Media
        .removeImage(id, type, path)
        .then(function (data) {
          Notify.infoDeleted();
          vm.loadPageData();
        })
        .catch(function (err) {
          Notify.errorSaving();
        })
        .finally(function () {
        });
    }

    function confirmDeleteCollection(collectionId) {
      ALERT.confirmWarning(function () {
        return vm.deleteCollection(collectionId);
      });
    }

    function deleteCollection(collectionId) {
      vm.loading = true;
      var collectionIDs = _.pluck(vm.origPage.sections[2].collections, '_id');
      collectionIDs = _.without(collectionIDs, collectionId);
      var parameters = {};
      parameters.sections = vm.origPage.sections;
      delete parameters.sections[2].grid;
      delete parameters.sections[2].products;
      delete parameters.sections[3].grid;
      delete parameters.sections[3].collections;
      parameters.sections.forEach(function (section) {
        if(section.grid) {
          section.grid.forEach(function (grid) {
            if(grid.thumb === null) {
              grid.thumb = "";
            }
          });
        }

      });
      if(parameters.sections[3].products.length > 0){
        parameters.sections[3].products = _.pluck(parameters.sections[3].products, '_id');
      } else {
        delete parameters.sections[3].products;
      }
      if(collectionIDs.length > 0) {
        parameters.sections[2].collections = collectionIDs;
      } else {
        delete parameters.sections[2].collections;
        delete parameters.sections[2]._id;
      }
      API
        .Pages
        .edit(vm.origPage._id, parameters)
        .then(function (data) {
        Notify.infoDeleted();
      })
        .catch(function (err) {
        Notify.errorSaving();
      })
        .finally(function () {
        vm.loading = false;
        setTimeout(function () {
          vm.loadPageData();
        }, 2000);
      });
    }

    function confirmDeleteProduct(productId) {
      ALERT.confirmWarning(function () {
        return vm.deleteProduct(productId);
      });
    }

    function deleteProduct(productId) {
      vm.loading = true;
      var productIDs = _.pluck(vm.origPage.sections[3].products, '_id');
      productIDs = _.without(productIDs, productId);
      var parameters = {};
      parameters.sections = vm.origPage.sections;
      delete parameters.sections[2].grid;
      delete parameters.sections[2].products;
      delete parameters.sections[3].grid;
      delete parameters.sections[3].collections;
      parameters.sections.forEach(function (section) {
        if(section.grid) {
          section.grid.forEach(function (grid) {
            if(grid.thumb === null) {
              grid.thumb = "";
            }
          });
        }
      });
      if(parameters.sections[2].collections.length > 0){
        parameters.sections[2].collections = _.pluck(parameters.sections[2].collections, '_id');
      } else {
        delete parameters.sections[2].collections;
      }
      if(productIDs.length > 0) {
        parameters.sections[3].products = productIDs;
      } else {
        delete parameters.sections[3].products;
        delete parameters.sections[3]._id;
      }
      API
        .Pages
        .edit(vm.origPage._id, parameters)
        .then(function (data) {
        Notify.infoDeleted();
      })
        .catch(function (err) {
        Notify.errorSaving();
      })
        .finally(function () {
        vm.loading = false;
        setTimeout(function () {
          vm.loadPageData();
        }, 2000);
      });
    }

    function saveLink(index) {
      vm.loading = true;
      var params = {
        main_banner: []
      };
      var link = vm.page.main_banner[index].link;
      var id = vm.page.main_banner[index]._id;
      vm.origPage.main_banner.forEach(function (banner) {
        if(banner._id === id){
          if( banner && banner.img && banner.img !== '') {
            params.main_banner.push({ img: banner.img , link: link, type: banner.type });
          } else {
            params.main_banner.push({ img: '' , link: link, type: banner.type });
          }
        } else {
          params.main_banner.push({ img: banner.img, link: banner.link, type: banner.type   });
        }
      });
      API
        .Pages
        .edit(vm.origPage._id, params)
        .then(function (data) {
          Notify.infoSaved();
          vm.loadPageData();
        })
        .catch(function (err) {
          Notify.errorSaving();
        })
        .finally(function () {
          vm.loading = false;
      });
    }

    function enableSaveLink() {
      return angular.equals(vm.page.main_banner, vm.oldMainBanner) ? true : false;
    }

    function setPageData(data) {
      vm.page.type = data.type;
      vm.page.title.en = data.title.en;
      vm.page.status = data.status;
      vm.page.slug = data.slug;
      vm.page.sections = data.sections;
      vm.page.main_banner = data.main_banner;
      vm.oldMainBanner = angular.copy(vm.page.main_banner);
      vm.oldSections = angular.copy(vm.page.sections);
      vm.origPage = angular.copy(data);
      if(data.seo && data.seo.meta_desc && data.seo.meta_desc != ''){
        vm.page.seo.description = data.seo.meta_desc;
      } else {
        vm.origPage.seo.meta_desc = '';
      }
      if(data.seo && data.seo.page_title && data.seo.page_title != '') {
        vm.page.seo.title = data.seo.page_title;
      } else {
        vm.origPage.seo.page_title = '';
      }
      if(data.seo && data.seo.meta_kws && data.seo.meta_kws != '') {
        vm.page.seo.keywords = data.seo.meta_kws.split(',');
      } else {
        vm.origPage.seo.meta_kws = '';
      }
      for(var j=0; j<=7; j++){
        vm.pageMainBanner[j].images = [];
      }
      for(var i=0; i<=7; i++){
        if(data.main_banner && data.main_banner[i].img && data.main_banner[i].img !== '') {
          vm.pageMainBanner[i].path = data.main_banner[i].img;
          vm.pageMainBanner[i].images.push( vm.baseUrl + data.main_banner[i].img);
        } else {
          vm.pageMainBanner[i].path = null;
        }
      }
    }

    function setEditParams() {
      var params = {
        title: {
          en: '',
          ch: ''
        },
        slug: '',
        status: 'ACTIVE',
        main_banner: [],
        sections: [],
        seo: {}
      };
      params.title.en = vm.page.title.en;
      params.title.ch = vm.page.title.en;
      if (!_.isEmpty(vm.page.seo.title))
        params.seo.page_title = vm.page.seo.title;
      if (!_.isEmpty(vm.page.seo.description))
        params.seo.meta_desc = vm.page.seo.description;
      if (vm.page.seo.keywords.length > 0)
        params.seo.meta_kws = vm.page.seo.keywords.join(',');
      if (!_.isEmpty(vm.page.slug))
        params.slug = vm.page.slug;
      if (vm.origPage.status != vm.page.status)
        params.status = vm.page.status;
      if (_.isEmpty(params.seo))
        delete params.seo;
      if(!angular.equals(vm.oldMainBanner, vm.page.main_banner)){
        params.main_banner = vm.page.main_banner;
      } else {
        delete params.main_banner;
      }
      if(!angular.equals(vm.oldSections, vm.page.sections)) {
        params.sections = vm.page.sections;
        delete params.sections[2].products;
        delete params.sections[3].collections;
        for(var i = 0; i<2; i++) {
          delete params.sections[i].products;
          delete params.sections[i].collections;
          delete params.sections[i+2].grid;
        }
        params.sections.forEach(function (section) {
          if(section.grid){
            section.grid.forEach(function (grid) {
              if(grid.thumb == null) {
                grid.thumb = "";
              }
            });
          }
        });
        if(params.sections[2].collections.length <=0) {
          delete params.sections[2].collections;
        } else {
          params.sections[2].collections = _.pluck(params.sections[2].collections, '_id');
        }
        if(params.sections[3].products.length <= 0){
          delete params.sections[3].products;
        } else {
          params.sections[3].products = _.pluck(params.sections[3].products, '_id');
        }
      } else {
        delete params.sections;
      }
      return params;
    }

    function setCreateParams() {
      var keywords = vm.page.seo.keywords.length > 0 && vm.page.seo.keywords.join(',') || '';
      var type = '';
      var gridByTwoSection = {
        title: {
          en: 'Default Grid by Two',
          ch: 'Default Grid by Two'
        },
        has_link: false,
        link: '',
        display_options: {
          title: true,
          image: false
        },
        type: 'GRID_BY_2',
        grid: []
      };
      var gridByThreeSection = {
        title: {
          en: 'Default Grid by Three',
          ch: 'Default Grid by Three'
        },
        has_link: false,
        link: '',
        display_options: {
          title: true,
          image: false
        },
        type: 'GRID_BY_3',
        grid: []
      };
      var listByCollectionSection = {
        title: {
          en: 'Default List Collection',
          ch: 'Default List Collection'
        },
        has_link: false,
        link: '',
        display_options: {
          title: true,
          image: false
        },
        type: 'LIST_BY_COLLECTIONS'
      };
      var listByProductSection = {
        title: {
          en: 'Default List Product',
          ch: 'Default List Product'
        },
        has_link: false,
        link: '',
        display_options: {
          title: true,
          image: false
        },
        type: 'LIST_BY_PRODUCTS'
      };
      var params = {
        title: {
          en: vm.page.title.en,
          ch: vm.page.title.en
        },
        slug: vm.page.slug,
        type: 'HOME_PAGE', // Default is home page
        seo: {},
        main_banner: [],
        sections: []
      };
      gridByTwoSection.grid = setGridData(4);
      gridByThreeSection.grid = setGridData(9);
      for(var i =0; i<=7; i++){
        if(i <=3) {
          type = 'M';
        } else {
          type = 'D';
        }
        params.main_banner.push({ img: "", link: '', type: type });
      }
      params.sections.push(gridByTwoSection);
      params.sections.push(gridByThreeSection);
      params.sections.push(listByCollectionSection);
      params.sections.push(listByProductSection);
      if (vm.page.seo.title != '')
        params.seo.page_title = vm.page.seo.title;
      if (vm.page.seo.description != '')
        params.seo.meta_desc = vm.page.seo.description;
      if (keywords != '')
        params.seo.meta_kws = keywords;
      return params;
    }

    function setGridData(howMany) {
      var temp = [];
      for(var i = 0; i< howMany; i++) {
        temp.push({
          title: {
            en: 'Default',
              ch: 'Default'
          },
          thumb: '',
            link: '',
            has_label: false,
            label: {
            en: '',
              ch: ''
          }
        });
      }
      return temp;
    }

    if(isEdit()){
      vm.loadPageData();
    }

  }

})(angular.module('adminPanel.cms.pages.create.controller',[]));
