'use strict';

describe('Controllers', function () {
  describe('PagesAddProductModalController', function () {

    var page = {
      "title": {
        "en": "test page",
        "ch": "test page"
      },
      "slug": "test-page",
      "type": "HOME_PAGE",
      "status": "ACTIVE",
      "main_banner": [],
      "sections": [],
      "seo": {}
    };

    var $scope,
      $uibModalInstance,
      API,
      QINGSTOR,
      Notify,
      $httpBackend,
      $rootScope,
      Controller;

    beforeEach(module('ui.bootstrap'));
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.services.notifier'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.api'));
    beforeEach(module('adminPanel.cms.pages.create.addProduct.controller'));

    beforeEach(inject(function ($injector) {
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      var $controller = $injector.get('$controller');
      API = $injector.get('API');
      Notify = $injector.get('Notify');
      $httpBackend = $injector.get('$httpBackend');
      QINGSTOR = $injector.get('QINGSTOR');
      $uibModalInstance = {
        close: jasmine.createSpy('$uibModalInstance')
      };
      Controller = function () {
        return $controller('PagesAddProductModalController', {
          '$scope': $scope,
          'API': API,
          'QINGSTOR': QINGSTOR,
          '$uibModalInstance': $uibModalInstance,
          'Notify': Notify,
          'PAGE': page
        });
      };

    }));

    it('should set the baseUrl', function () {
      var controller = Controller();
      expect(controller.baseUrl).toBe(QINGSTOR.base_url);
    });

    it('should set the left list bulk item to Add to listing', function () {
      var controller = Controller();
      expect(controller.leftListBulk[0].title).toBe('Add to Listing');
    });

    it('should set the loading variable to true', function () {
      var controller = Controller();
      expect(controller.loading).toBeTruthy();
    });

    it('should close the modal on clicking on close button', function () {
      var controller = Controller();
      controller.close();
      expect($uibModalInstance.close).toHaveBeenCalled();
    });


  });
});
