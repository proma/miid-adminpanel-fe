(function (app) {

  app.controller('PagesAddProductModalController', PagesAddProductModalController);

  PagesAddProductModalController.$inject = ['$uibModalInstance', 'PAGE', 'API', 'QINGSTOR', 'Notify', '$translate'];

  function PagesAddProductModalController($uibModalInstance, PAGE, API, QINGSTOR, Notify, $translate) {

    // Variables
    var vm = this;
    vm.baseUrl = QINGSTOR.base_url;
    vm.loading = false;
    vm.loadProductData = loadProductData;
    vm.selectorForProducts = {};
    var origPage = PAGE;
    var origProducts = [];
    vm.products = [];
    vm.leftListBulk = [
      {
        title: $translate.instant('modals.product.add') || 'Add to Listing',
        action: function () {
          vm.submit();
        }
      }
    ];

    // Functions

    vm.close = close;
    vm.submit = submit;


    // Functions Implementation

    function close() {
      $uibModalInstance.close();
    }

    function submit() {
      vm.loading = true;
      var params = {
        sections: origPage.sections
      };
      params.sections[0].grid.forEach(function (grid) {
        if(grid.thumb === null) {
          grid.thumb = "";
        }
      });
      params.sections[1].grid.forEach(function (grid) {
        if(grid.thumb === null) {
          grid.thumb = "";
        }
      });
      if(params.sections[2].collections.length > 0) {
        params.sections[2].collections = _.pluck(params.sections[2].collections, '_id');
      } else {
        delete params.sections[2].collections;
      }
      delete params.sections[2].grid;
      delete params.sections[2].products;
      var productIDs = [];
      delete params.sections[3].collections;
      delete params.sections[3].grid;
      delete params.sections[3].products;
      vm.products.forEach(function (data) {
        if (data.checked) {
          productIDs.push(data.prod._id);
        }
      });
      if (productIDs.length > 0) {
        params.sections[3].products = productIDs;
      }
      API
        .Pages
        .edit(origPage._id, params)
        .then(function (data) {
          vm.close();
          Notify.infoSaved();
        })
        .catch(function (err) {
          Notify.errorSaving();
        })
        .finally(function () {
          vm.loading = false;
        });
    }

    function loadProductData() {
      vm.loading = true;
      var params = {basic_info: true};
      API
        .Products
        .getList(params)
        .then(function (data) {
          origProducts = data;
          var sectionProductIDs;
          if ( origPage.sections.length > 3 && origPage.sections[3].products.length <= 0) {
            data.forEach(function (product) {
              vm.products.push({checked: false, prod: product});
            });
          } else {
            sectionProductIDs = _.intersection(_.pluck(origProducts, '_id'), _.pluck(origPage.sections[3].products, '_id'));
            data.forEach(function (product) {
              var filter = true;
              sectionProductIDs.forEach(function (id) {
                if (id == product._id) {
                  filter = false;
                  vm.products.push({checked: true, prod: product});
                }
              });
              if (filter) {
                vm.products.push({checked: false, prod: product});
              }
            });
          }
        })
        .catch(function (err) {

        })
        .finally(function () {
          vm.loading = false;
        });
    }
    vm.loadProductData();
  }

})(angular.module('adminPanel.cms.pages.create.addProduct.controller',[]));
