(function (app) {

})(angular.module('adminPanel.cms.pages.create',
  ['adminPanel.cms.pages.create.routes',
  'adminPanel.cms.pages.create.controller',
  'adminPanel.cms.pages.create.addCollection',
  'adminPanel.cms.pages.create.addProduct']));
