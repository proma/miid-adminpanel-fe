(function (app) {

  app.controller('PagesAddCollectionModalController', PagesAddCollectionModalController);

  PagesAddCollectionModalController.$inject = ['$uibModalInstance', 'API', 'QINGSTOR', 'PAGE', 'Notify', '$translate'];

  function PagesAddCollectionModalController($uibModalInstance, API ,QINGSTOR, PAGE, Notify, $translate) {

    // Variables

    var vm = this;
    vm.baseUrl = QINGSTOR.base_url;
    vm.loading = false;
    vm.loadCollectionData = loadCollectionData;
    vm.selectorForCollections = {};
    var origPage = PAGE;
    var origCollections = [];
    vm.collections = [];
    vm.leftListBulk = [
      {
        title: $translate.instant('modals.product.add') || 'Add to Listing',
        action: function () {
          vm.submit();
        }
      }
    ];

    // Functions

    vm.close = close;
    vm.submit = submit;

    // Functions Implementation

    function close() {
      $uibModalInstance.close();
    }

    function submit() {
      vm.loading = true;
      var params = {
        sections: origPage.sections
      };
      params.sections.forEach(function (section) {
        section.grid.forEach(function (grid) {
          if(grid.thumb === null) {
            grid.thumb = "";
          }
        });
      });
      if(params.sections[3].products.length > 0){
        params.sections[3].products = _.pluck(params.sections[3].products, '_id');
      } else {
        delete params.sections[3].products;
      }
      delete params.sections[3].collections;
      delete params.sections[3].grid;
      var collectionIDs = [];
      delete params.sections[2].collections;
      delete params.sections[2].grid;
      delete params.sections[2].products;
      vm.collections.forEach(function (data) {
        if(data.checked) {
          collectionIDs.push(data.coll._id);
        }
      });
      if(collectionIDs.length > 0) {
        params.sections[2].collections = collectionIDs;
      }
      API
        .Pages
        .edit(origPage._id, params)
        .then(function (data) {
          vm.close();
          Notify.infoSaved();
        })
        .catch(function (err) {
          Notify.errorSaving();
        })
        .finally(function () {
          vm.loading = false;
        });
    }

    function loadCollectionData() {
      vm.loading = true;
      var params = {};
      API
        .Collections
        .getAll(params)
        .then(function (data) {
        origCollections = data.data;
        var sectionCollectionIDs;
        if(origPage.sections[2].collections.length <=0 ) {
          data.data.forEach(function (collection) {
            vm.collections.push({ checked: false, coll: collection });
          });
        } else {
          sectionCollectionIDs = _.intersection(_.pluck(origCollections, '_id'), _.pluck(origPage.sections[2].collections, '_id'));
          data.data.forEach(function (collection) {
            var filter = true;
            sectionCollectionIDs.forEach(function (id) {
              if(id == collection._id){
                filter = false;
                vm.collections.push({ checked: true, coll: collection });
              }
            });
            if(filter) {
              vm.collections.push({ checked: false, coll: collection  });
            }
          });
        }
      })
        .catch(function (err) {
      })
        .finally(function () {
        vm.loading = false;
      });
    }
    vm.loadCollectionData();
  }

})(angular.module('adminPanel.cms.pages.create.addCollection.controller',[]));
