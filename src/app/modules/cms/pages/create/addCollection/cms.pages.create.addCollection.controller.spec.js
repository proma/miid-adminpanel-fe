'use strict';
describe('Controllers',function () {
  describe('PagesAddCollectionModalController', function () {

    var page = {
      "title": {
        "en": "test page",
        "ch": "test page"
      },
      "slug": "test-page",
      "type": "HOME_PAGE",
      "status": "ACTIVE",
      "main_banner": [],
      "sections": [],
      "seo": {}
    };

    var collection = {
      "_id": '571f51c7d5569128031875bsd',
      "title": "test collection",
      "slug": "test-collection",
      "type": "MIID_SITES",
      "status": "ACTIVE",
      "sections": [{
         "title": "test section"
      }]
    };
    collection.sections[0].products = [
      {_id: '571f51c7d5569128031875b6'},
      {_id: '571f53c7d5569128031875b6'},
      {_id: '571f57c7d5569128031874b6'}
    ];


    var Controller,
      $rootScope,
      $httpBackend,
      URL,
      $uibModalInstance,
      QINGSTOR,
      $scope,
      API,
      Notify;

    beforeEach(module('ui.bootstrap'));
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.services.notifier'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.api'));
    beforeEach(module('adminPanel.cms.pages.create.addCollection.controller'));

    beforeEach(inject(function ($injector) {
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      $httpBackend = $injector.get('$httpBackend');
      URL = $injector.get('URL');
      API = $injector.get('API');
      var $controller = $injector.get('$controller');
      QINGSTOR = $injector.get('QINGSTOR');
      Notify = $injector.get('Notify');
      $uibModalInstance = {
        close: jasmine.createSpy('$uibModalInstance')
      };
      $httpBackend.when('GET', URL.COLLECTIONS + '?limit=10&type=MIID_SITES&status=ACTIVE').respond(collection);
      Controller = function () {
        return $controller('PagesAddCollectionModalController', {
          '$scope': $rootScope,
          'QINGSTOR': QINGSTOR,
          'Notify': Notify,
          'PAGE': page,
          'API': API,
          '$uibModalInstance': $uibModalInstance
        });
      };
    }));

    it('should set the baseUrl', function () {
      var controller = Controller();
      expect(controller.baseUrl).toBe(QINGSTOR.base_url);
    });

    it('should set the left list bulk item to Add to listing', function () {
      var controller = Controller();
      expect(controller.leftListBulk[0].title).toBe('Add to Listing');
    });

    it('should set the loading variable to true', function () {
      var controller = Controller();
      expect(controller.loading).toBeTruthy();
    });

    it('should close the modal on clicking on close button', function () {
      var controller = Controller();
      controller.close();
      expect($uibModalInstance.close).toHaveBeenCalled();
    });

    it('should set the origCollection', function () {
      $httpBackend.expectGET('https://api-dev.miid.com/collections?limit=10&type=MIID_SITES&status=ACTIVE');
      var controller = Controller();
      controller.origCollections = collection;
      expect(controller.origCollections).toBe(collection);
    });

  });
});
