(function (app) {

  app.config(['$stateProvider','URLProvider', function ($stateProvider) {

    $stateProvider
      .state('admin-panel.cms.pages.create', {
        url: '/create/:pageID',
        templateUrl: 'app/modules/cms/pages/create/cms.pages.create.html',
        controller: 'CreatePageController',
        controllerAs: 'vm'
      });

  }]);


})(angular.module('adminPanel.cms.pages.create.routes', []));
