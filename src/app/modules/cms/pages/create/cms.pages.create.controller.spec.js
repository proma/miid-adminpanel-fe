'use strict';
describe('Controllers', function () {
  describe('CreatePageController', function () {

    var page = {
      _id: 'dksks',
      title: {
        en: 'test page',
        ch: 'test page'
      },
      type: 'HOME_PAGE',
      status: 'ACTIVE',
      slug: '',
      main_banner: [],
      sections: [],
      seo: {}
    };

    var
      Controller,
      $rootScope,
      URL,
      $httpBackend,
      ALERT,
      $stateParams,
      API,
      $state,
      $scope,
      Notify,
      Modal;

    // load the dependencies module
    beforeEach(module('ui.bootstrap'));
    beforeEach(module('ngResource'));
    beforeEach(module('oitozero.ngSweetAlert'));
    beforeEach(module('slugifier'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('ngFileUpload'));
    beforeEach(module('adminPanel.services.notifier'));
    beforeEach(module('adminPanel.services.modal'));
    beforeEach(module('ui.router'));
    beforeEach(module('adminPanel.services.alert'));
    beforeEach(module('adminPanel.api'));
    beforeEach(module('adminPanel.cms.pages.create.controller'));


    beforeEach(inject(function ($injector) {
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      $httpBackend = $injector.get('$httpBackend');
      URL = $injector.get('URL');
      var $controller = $injector.get('$controller');
      $stateParams = {};
      $state = $injector.get('$state');
      API = $injector.get('API');
      Notify = $injector.get('Notify');
      Modal = $injector.get('Modal');
      ALERT = $injector.get('ALERT');
      Controller = $controller('CreatePageController', {
        '$scope': $scope,
        'API': API,
        'Modal': Modal,
        '$stateParams': $stateParams,
        '$state': $state,
        'Slug': $injector.get('Slug'),
        'ALERT': ALERT,
        'Notify': Notify,
        'MIID_API': $injector.get('MIID_API'),
        'Upload': $injector.get('Upload')
      });
      Controller.origPage = page;
      $httpBackend.when('GET', URL.PAGES ).respond(200, page);
    }));
    it('should be in edit mode when there is pageID', function () {
      $stateParams.pageID = '12345';
      expect(Controller.isEdit()).toBeTruthy();
    });
    it('should be in create mode when there is no pageID', function () {
      expect(Controller.isEdit()).toBeFalsy();
    });
    it('should have default status of INACTIVE', function () {
      expect(Controller.page.status).toEqual('INACTIVE');
    });
    it('should set slug form seo title', function () {
      Controller.page.seo.title = 'this is test';
      Controller.setSlug();
      expect(Controller.page.slug).toEqual('this-is-test');
    });
    it('should return slugified string', function () {
      expect(Controller.slugify('this is test')).toEqual('this-is-test');
    });
    it('should set seo title and slug from page title when not in edit mode', function () {
      Controller.page.title.en = 'this is test';
      Controller.fillSlugTitle();
      expect(Controller.page.seo.title).toEqual('this is test');
      expect(Controller.page.slug).toEqual('this-is-test');
    });
    it('shouldnt set seo title and slug from page title while in edit mode', function () {
      Controller.page.title.en = 'this is test';
      $stateParams.pageID = 'bla bla';
      Controller.fillSlugTitle();
      expect(Controller.page.seo.title).not.toEqual('this is test');
      expect(Controller.page.slug).not.toEqual('this-is-test');
    });
    it('should add keyword in seo', function () {
      Controller.page.seo.currentKeyword = 'this is test';
      Controller.addTag();
      expect(Controller.page.seo.keywords.length).toEqual(1);
      expect(Controller.page.seo.currentKeyword).toEqual('');
      expect(Controller.page.seo.keywords[0]).toEqual('this is test');
    });
    it('should remove keyword in seo', function () {
      Controller.page.seo.currentKeyword = 'this is test';
      Controller.addTag();
      expect(Controller.page.seo.keywords.length).toEqual(1);
      Controller.removeTag('this is test');
      expect(Controller.page.seo.keywords.length).toEqual(0);
    });
  });
});
