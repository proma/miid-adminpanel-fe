(function (app) {

})(angular.module('adminPanel.cms',
  ['adminPanel.cms.routes',
    'adminPanel.cms.navigation',
    'adminPanel.cms.collections',
    'adminPanel.cms.user_reviews',
    'adminPanel.cms.pages']));
