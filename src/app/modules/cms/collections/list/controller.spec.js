'use strict';

describe('Controllers', function () {
  describe('CollectionsListController', function () {

    var data = {
      "paging": {"total": 3},
      "data": [{
        "_id": "571f50d9d5569128031874b2",
        "title": "Collection 1",
        "slug": "collection-1",
        "status": "ACTIVE",
        "type": "MIID_SITES"
      }, {
        "_id": "571f51b6d5569128031874b4",
        "title": "Collection 2",
        "slug": "collection-2",
        "status": "ACTIVE",
        "type": "BRANDS"
      }, {
        "_id": "571f51c7d5569128031874b6",
        "title": "Collection 3",
        "slug": "collection-3",
        "status": "INACTIVE",
        "type": "MIID_SITES"
      }]
    };

    var
      CollectionsListController,
      $rootScope,
      URL,
      $httpBackend,
      $timeout,
      QINGSTOR,
      ALERT,
      $scope,
      $uibModal,
      Notify;


    // load the dependencies module
    beforeEach(module('adminPanel.services.notifier'));
    beforeEach(module('adminPanel.services.modal'));
    beforeEach(module('ui.bootstrap'));
    beforeEach(module('ngResource'));
    beforeEach(module('oitozero.ngSweetAlert'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.services.alert'));
    beforeEach(module('adminPanel.api'));
    beforeEach(module('adminPanel.cms.collections.list.controller'));

    beforeEach(inject(function ($injector) {
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      $httpBackend = $injector.get('$httpBackend');
      URL = $injector.get('URL');
      var $controller = $injector.get('$controller');
      QINGSTOR = $injector.get('QINGSTOR');
      Notify = $injector.get('Notify');
      $uibModal = $injector.get('Modal');
      ALERT = $injector.get('ALERT');
      $timeout = $injector.get('$timeout');

      CollectionsListController = $controller('CollectionsListController', {
        'Notify': Notify,
        '$scope': $scope,
        'Modal': $uibModal,
        'API': $injector.get('API'),
        'QINGSTOR': QINGSTOR,
        'ALERT': $injector.get('ALERT')
      });

      CollectionsListController.listSelectorOptions.reset = function() {}; //fake coz of directive
      data.url = URL.COLLECTIONS + '?limit=10&page=0';
      data.url2 = URL.COLLECTIONS + '?limit=10&page=1';
      data.delURL = URL.COLLECTIONS + '/571f50d9d5569128031874b2';
      CollectionsListController.listCollections();
    }));

    describe('MA-306: List Collections and change status', function () {

      it('should be empty first', function () {
        expect(CollectionsListController.collections.length).toBe(0);
      });

      it('should be loading first', function () {
        expect(CollectionsListController.loading).toBeTruthy();
      });

      it('should stop loading after load', function () {
        $httpBackend.expect('GET', data.url).respond(200, data);
        $httpBackend.flush();
        expect(CollectionsListController.loading).toBeFalsy();
      });

      it('should have set the base url', function () {
        expect(CollectionsListController.baseURL).toBe(QINGSTOR.base_url);
      });

      it('should set items limit to 10', function () {
        expect(CollectionsListController.itemsLimit).toBe(10);
      });

      it('should call alert when error', function () {
        spyOn(ALERT, 'confirm');
        $httpBackend.expect('GET', data.url).respond(500, data);
        $httpBackend.flush();
        CollectionsListController.listCollections();
        expect(ALERT.confirm).toHaveBeenCalled();
      });

      it('should populate collections', function () {
        $httpBackend.expect('GET', data.url).respond(200, data);
        $httpBackend.flush();
        expect(CollectionsListController.collections.length).toEqual(3);
      });

      it('should set totalItems to 3', function () {
        $httpBackend.expect('GET', data.url).respond(200, data);
        $httpBackend.flush();
        expect(CollectionsListController.totalItems).toBe(3);
      });

      it('should set itemsGot to 2', function () {
        $httpBackend.expect('GET', data.url).respond(200, data);
        $httpBackend.flush();
        expect(CollectionsListController.itemsGot).toBe(3);
      });

      it('should call listSelectorOptions.reset() when called changePage()', function () {
        spyOn(CollectionsListController.listSelectorOptions, 'reset');
        CollectionsListController.currentPage = 2; // page is index based so in query page 1 will be called
        $httpBackend.when('GET', data.url).respond(200, data);
        $httpBackend.when('GET', data.url2).respond(200, data);

        CollectionsListController.changePage();

        $httpBackend.flush();

        expect(CollectionsListController.listSelectorOptions.reset).toHaveBeenCalled();
      });


      it('should call delete api and alert when called deleteCollection', function () {
        $httpBackend.when('GET', data.url).respond(200, data);
        $httpBackend.flush();
        CollectionsListController.collections[0].checked = true; //manually checking
        spyOn(ALERT, 'confirm');
        CollectionsListController.deleteCollections();
        expect(ALERT.confirm).toHaveBeenCalled();
      });

    });

  });

});
