(function (app) {

  app.controller('CMSCollectionsListStatusModalController', CMSCollectionsListStatusModalController);

  CMSCollectionsListStatusModalController.$inject = ['$uibModalInstance'];

  function CMSCollectionsListStatusModalController($uibModalInstance) {
    var modal = this;

    modal.selectedStatus = 'ACTIVE';

    modal.close = function () {
      $uibModalInstance.dismiss('cancel');
    };

    modal.submit = function () {
      $uibModalInstance.close({
        status: modal.selectedStatus
      });
    }
  }

})(angular.module('adminPanel.cms.collections.list.changeStatusModal.controller', []));
