(function(app) {

  app.config(['$stateProvider', function($stateProvider) {

    $stateProvider
      .state('admin-panel.cms.collections.list', {
        url: '/list',
        templateUrl: 'app/modules/cms/collections/list/list.html',
        controller: 'CollectionsListController',
        controllerAs: 'vm'
      });

  }]);

})(angular.module('adminPanel.cms.collections.list.routes',[]));
