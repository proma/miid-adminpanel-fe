(function (app) {

  app.controller('CollectionsListController', CollectionsListController);

  CollectionsListController.$inject = [ 'API', 'QINGSTOR', 'ALERT', 'Notify', 'Modal' ];

  function CollectionsListController(API, QINGSTOR, ALERT, Notify, Modal) {
    var vm = this;

    vm.baseURL = QINGSTOR.base_url; //for logo

    vm.loading = true;
    vm.listSelectorOptions = {}; //for list selector that directive will bind some methods to this object.
    vm.collections = [];
    vm.currentTab = 'ALL';
    vm.filterByTab = ''; //defaults to 'ALL COLLECTIONS'

    vm.totalItems = 0; //TODO: need to make directive for these
    vm.itemsGot = 0;
    vm.previousPage = 0;
    vm.currentPage = 1;
    vm.itemsLimit = 10;

    vm.bulkActions = [
      {
        title: 'Change Status',
        action: function () {
          vm.changeStatus();
        },
        divider: true
      },
      {
        title: 'Delete Collections',
        action: function () {
          vm.deleteCollections();
        }
      }
    ];

    vm.listCollections = function (isFilter) {
      vm.loading = true;

      if(isFilter) { //reset if filter applied.
        vm.currentPage = 1;
        vm.previousPage = 0;
      }

      var dataPromise =
        API
        .Collections
        .getAll({
          page: (vm.currentPage-1),
          limit: vm.itemsLimit,
          type: vm.filterByTab
        });

      dataPromise
        .then(function (data) {
          vm.listSelectorOptions.reset();
          vm.loading = false;

          vm.totalItems = data.paging.total;
          vm.collections = data.data;
          vm.itemsGot = vm.collections.length;

      }).catch(function (err) {
          vm.loading = false;
          ALERT.confirm(function () {
            return vm.listCollections(isFilter);
          });
      });

      return dataPromise;
    };

    vm.changePage = function () {
      vm.previousPage = (vm.currentPage - 1);
      vm.loading = true;
      vm.listSelectorOptions.reset(); //reset the selected items
      vm.listCollections();
    };

    vm.changeTab = function (type) {
      if(vm.loading) //load the previous requests first.
        return;

      vm.listSelectorOptions.reset();

      vm.currentTab = type;
      if(type === 'ALL')
        vm.filterByTab = ''; //reset filter
      else
        vm.filterByTab = vm.currentTab;

      vm.listCollections(true);
    };

    vm.changeSingleStatus = function(collection, status) {
      if(collection.status === status) {
        Notify.infoSameItemStatus();
        return;
      }

      collection.performingOperation = true;
      var obj = {
        status: status
      };

      API
        .Collections
        .edit(collection._id, obj)
        .then(function (data) {
          collection.status = status;
          collection.performingOperation = false;
          Notify.infoChangeStatus(status);
        })
        .catch(function (err) {
          Notify.errorItemStatus();
          collection.performingOperation = false;
        });
    };

    vm.changeStatus = function(){
      var modal = {};

      modal = Modal.open(
        'CMSCollectionsListStatusModalController',
        'modal',
        'app/modules/cms/collections/list/change_status_modal/cms.collections.list.changeStatusModal.html');

      modal.result.then(function (result) {
        ALERT.confirm(function () {
          // change status of all the objects.
          var completedReq = 0;
          var noOfErrors = 0;
          var noOfCalls = 0; //no of http requests to call

          //checking if user has selected collections?

          angular.forEach(vm.collections,function(data){
            if (data.checked){
              if (result.status != data.status)
                ++noOfCalls;
            }
          });

          //if noOfCalls 0 then return

          if(noOfCalls === 0){
            Notify.infoSameItemStatus();
            return;
          }

          angular.forEach(vm.collections, function(data){
            //if status of selected collection == selected status, we save http request.
            if(data.checked)
              if(result.status != data.status){
                //we send request to change status.
                data.performingOperation = true;
                var obj = {
                  status: result.status
                };
                API.
                  Collections
                  .edit(data._id, obj)
                  .then(function(res){
                    data.status = result.status;
                    data.performingOperation = false;
                    callBack();
                  })
                  .catch(function(err){
                    data.performingOperation = false;
                    Notify.errorItemStatus();
                    callBack(err);
                  });
              }
          });

          function callBack(err){
            if(!err){
              ++completedReq;
            }
            if(err){
              ++noOfErrors;
            }
            if(noOfCalls == (completedReq+noOfErrors)){
              //all calls have finished, let's call Notify
              Notify.infoChangeItemsStatus(noOfCalls,completedReq,result.status);
            }
          }

        }, 'Are you sure?', 'This will change the status of selected collections to ' + result.status, 'warning', 'Confirm');

      });

    }; // end change status function

    vm.deleteCollections = function() {

      ALERT.confirm(function () {

        var completedReq = 0;
        var noOfErrors = 0;
        var noOfCalls = 0; //no of http requests to call

        //checking if user has selected collections?

        angular.forEach(vm.collections,function(data){
          if (data.checked){
            ++noOfCalls;
          }
        });

        angular.forEach(vm.collections, function (data) {
          if(data.checked) {
            data.performingOperation = true;
            API.Collections.delete(data._id)
              .then(function() {
                data.performingOperation = false;
                callBack();
              })
              .catch(function (err) {
                data.performingOperation = false;
                callBack(err);
              });
          }
        });

        function callBack(err){
          if(!err){
            ++completedReq;
          }
          if(err){
            ++noOfErrors;
          }
          if(noOfCalls == (completedReq+noOfErrors)){
            vm.listCollections(true); //reload collections
            Notify.infoItemsDeleted(noOfCalls,completedReq);
          }
        }

      }, 'Are you sure?', 'This will delete the selected collections permanently.', 'warning','Confirm');
    };

  }

})(angular.module('adminPanel.cms.collections.list.controller', []));
