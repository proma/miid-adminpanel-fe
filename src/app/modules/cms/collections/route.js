(function(app) {

  app.config(['$stateProvider', 'URLProvider', function($stateProvider, URLProvider) {

    $stateProvider
      .state('admin-panel.cms.collections', {
        url: '/collections',
        abstract: true,
        template: '<ui-view></ui-view>',
        data: {
          permissions: {
            only: 'canSee.CMS.Collections.Listing',
            redirectTo: URLProvider.$get().RedirectURL
          }
        }
      });

  }]);

})(angular.module('adminPanel.cms.collections.routes',[]));
