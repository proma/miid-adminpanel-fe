(function (app) {

  app.controller('CollectionsCreateController', CollectionsCreateController);

  CollectionsCreateController.$inject = ['Modal', '$stateParams', '$state', 'Slug', 'API', 'ALERT', 'QINGSTOR', 'Upload', 'MIID_API', 'Notify'];

  function CollectionsCreateController(Modal, $stateParams, $state, Slug, API, ALERT, QINGSTOR, Upload, MIID_API, Notify) {
    var vm = this;

    vm.isEdit = function () {
      vm.collectionID = $stateParams.collectionID && $stateParams.collectionID != '' && $stateParams.collectionID != 'new' ? $stateParams.collectionID : null;
      return vm.collectionID != null;
    };

    vm.baseURL = QINGSTOR.base_url;
    vm.editorOptions = {
      toolbar: [
        // [groupName, [list of button]]
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['fontname']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']]
      ],
      placeholder: 'Write description here..',
      disableDragAndDrop: true,
      shortcuts: false,
      disableResizeEditor: true,
      editable: false, //TODO: implement the editor for description
      contenteditable: false
    };


    vm.selectedPane = '';
    vm.loading = false;
    vm.slugExist = false;
    vm.origCollection = {};
    vm.currentSection = {};
    vm.oldCurrentSection = {};
    vm.collectionImages = {
      logo: {
        path: '',
        images: []
      },
      mobile: {
        path: '',
        images: []
      },
      desktop: {
        path: '',
        images: []
      }
    };
    vm.collection = {
      title: '',
      slug: '',
      status: 'INACTIVE', //default is inactive
      visibility: {
        IOS: true,
        ANDROID: true,
        WEB_MOBILE: true,
        WEB_DESKTOP: true
      },
      seo: {
        title: '',
        description: '',
        currentKeyword: '',
        keywords: []
      },
      sections: []
    };

    vm.enableButton = function () {
      return vm.isEdit ?
        (vm.collection.title == '' || vm.collection.slug == '' ? true :
          (vm.collection.title == vm.origCollection.title &&
          vm.collection.seo.title == vm.origCollection.seo.page_title &&
          vm.collection.seo.description == vm.origCollection.seo.meta_desc &&
          vm.collection.slug ==  vm.origCollection.slug &&
          vm.collection.status == vm.origCollection.status &&
          vm.collection.seo.keywords.join(',') == vm.origCollection.seo.meta_kws) &&
          angular.equals(vm.oldCurrentSection, vm.currentSection) &&
          angular.equals(vm.collection.visibility, vm.origCollection.visibility))
        : vm.collection.title == '' || vm.collection.slug == '';
    };

    vm.setSlug = function () {
      vm.collection.slug = Slug.slugify(vm.collection.seo.title);
    };

    vm.slugify = function (input) {
      return Slug.slugify(input);
    };

    vm.fillSlugTitle = function () {
      if (vm.isEdit()) return;
      vm.collection.seo.title = vm.collection.title;
      vm.collection.slug = Slug.slugify(vm.collection.title);
    };

    vm.addTag = function () {
      if (vm.collection.seo.currentKeyword &&
        vm.collection.seo.keywords.indexOf(vm.collection.seo.currentKeyword) < 0) // avoid duplicate in repeat directive
        vm.collection.seo.keywords.push(vm.collection.seo.currentKeyword);
      vm.collection.seo.currentKeyword = "";
    };

    vm.removeTag = function (tag) {
      vm.collection.seo.keywords = _.without(vm.collection.seo.keywords, tag);
    };

    vm.changeSection = function (section) {
      if (!angular.equals(vm.oldCurrentSection, vm.currentSection)) {
        Notify.infoSaveChanges();
        return;
      }
      vm.currentSection = section;
      vm.oldCurrentSection = angular.copy(vm.currentSection);
    };

    vm.removeImage = function (file) {
      var path = '';
      var type = '';

      if(vm.selectedPane === 'Mobile') {
        type = 'col_m_top';
        path = vm.collectionImages.mobile.path;
      } else if(vm.selectedPane === 'Logo') {
        type = 'col_logo';
        path = vm.collectionImages.logo.path;
      } else {
        type = 'col_d_top';
        path = vm.collectionImages.desktop.path;
      }

      API
        .Collections
        .removeImage(vm.collectionID,type,path)
        .then(function (resp) {
          vm.loadCollectionData();
          Notify.infoDeleted();
        }).catch(function (err) {
          Notify.errorSaving();
      })
    };

    vm.uploadImage = function (file) {
      if(!vm.collectionID){
        Notify.errorSaving();
        return;
      }
      vm.uploading = true;
      vm.uploadProgress = 0;
      var uploadUrl = '';
      var type = vm.selectedPane;

      if(vm.selectedPane === 'Mobile')
        var uploadUrl = MIID_API.base_url + '/media/upload/col_m_top/' + vm.collectionID;
      else if(vm.selectedPane === 'Logo')
        var uploadUrl = MIID_API.base_url + '/media/upload/col_logo/' + vm.collectionID;
      else
        var uploadUrl = MIID_API.base_url + '/media/upload/col_d_top/' + vm.collectionID;

      Upload.upload({
        url: uploadUrl,
        data: {'file' : file}
      }).then(function (resp) {
        vm.uploading = false;
        if(resp.data.path) {
          var d = new Date();
          if(type === 'Mobile') {
            vm.collectionImages.mobile.path = resp.data.path;
            vm.collectionImages.mobile.images[0] = vm.baseURL + resp.data.path + '?' + d.getTime();
          }else if(type === 'Web') {
            vm.collectionImages.desktop.path = resp.data.path;
            vm.collectionImages.desktop.images[0] = vm.baseURL + resp.data.path + '?' + d.getTime();
          }
          if(type === 'Logo') {
            vm.collectionImages.logo.path = resp.data.path;
            vm.collectionImages.logo.images[0] = vm.baseURL + resp.data.path + '?' + d.getTime();
          }
          Notify.infoImageSaved();
          return;
        }
        Notify.errorSaving();

      }, function (err) {
        vm.uploading = false;
        Notify.errorSaving();

      }, function (evt) {
        vm.uploadProgress = parseInt(100.0 * evt.loaded / evt.total);
      });
    };

    vm.saveChangings = function () {

      vm.loading = true;

      if (vm.isEdit()) { // means we are in editing mode.

        var params = {
          seo: {},
          visibility: vm.collection.visibility
        };

        if (!_.isEmpty(vm.collection.title) && vm.origCollection.title != vm.collection.title)
          params.title = vm.collection.title;
        if (!_.isEmpty(vm.collection.seo.title))
          params.seo.page_title = vm.collection.seo.title;
        if (!_.isEmpty(vm.collection.seo.description))
          params.seo.meta_desc = vm.collection.seo.description;
        if (vm.collection.seo.keywords.length > 0)
          params.seo.meta_kws = vm.collection.seo.keywords.join(',');
        if (!_.isEmpty(vm.collection.slug) && vm.origCollection.slug != vm.collection.slug)
          params.slug = vm.collection.slug;
        if (vm.origCollection.status != vm.collection.status)
          params.status = vm.collection.status;
        if (_.isEmpty(params.seo))
          delete params.seo;
        if (!angular.equals(vm.currentSection, vm.oldCurrentSection)) {
          params.sections = vm.getSections().map(function (section) {
            delete section['products_designs'];
            if (vm.currentSection.title == section.title) {
              section.products = _.pluck(vm.currentSection.products, '_id');
            }
            return section;
          });
        }

        if(vm.origCollection.slug != vm.collection.slug) {
          API
            .Collections
            .getBySlug(vm.collection.slug)
            .then(function (obj) {
              if(obj) {
                Notify.errorSlugSaving();
                vm.slugExist = true;
                vm.loading = false;
              }
            })
            .catch(function (err) {
              vm.slugExist = false;
              if(err.status == 404 || (err.data && err.data.statusCode == 404)) {
                vm.update(params);
                return;
              }
              vm.loading = false;
              Notify.errorSaving();
            });
          return;
        }

        vm.update(params);

        return;
      }

      // means we are in creating mode.
      var keywords = vm.collection.seo.keywords.length > 0 && vm.collection.seo.keywords.join(',') || '';
      var params = {
        title: vm.collection.title,
        slug: vm.collection.slug,
        status: vm.collection.status,
        visibility: vm.collection.visibility,
        type: 'MIID_SITES',
        seo: {},
        sections: [
          {title: 'Default'} // default sub collection.
        ]
      };
      if (vm.collection.seo.title != '')
        params.seo.page_title = vm.collection.seo.title;
      if (vm.collection.seo.description != '')
        params.seo.meta_desc = vm.collection.seo.description;
      if (keywords != '' )
        params.seo.meta_kws = keywords;

      API
        .Collections
        .getBySlug(vm.collection.slug)
        .then(function (obj) {
          if(obj) {
            Notify.errorSlugSaving();
            vm.slugExist = true;
            vm.loading = false;
          }
        })
        .catch(function (err) {
          vm.slugExist = false;
          if(err.status == 404 || (err.data && err.data.statusCode == 404)) {
            API
              .Collections
              .create(params)
              .then(function (result) {
                $state.go('admin-panel.cms.collections.create', {collectionID: result._id});
                vm.collection.sections = result.sections;
                vm.currentSection = vm.collection.sections[0];
              })
              .catch(function (err) {
                Notify.errorSaving();
                vm.loading = false;
              });
            return;
          }
          Notify.errorSaving();
          vm.loading = false;
        });

    };


    vm.update = function (params) {
      API
        .Collections
        .edit(vm.origCollection._id, params)
        .then(function () {
          Notify.infoSaved();
          vm.oldCurrentSection = angular.copy(vm.currentSection);
          vm.loadCollectionData();
        })
        .catch(function () {
          vm.loading = false;
          Notify.errorSaving();
        });
    };

    vm.saveNewSectionTitle = function () {
      vm.loading = true;
      var obj = {
        sections: []
      };
      obj.sections = vm.getSections();
      obj.sections.forEach(function (data) {
        delete data['products_designs'];
        if(data._id == vm.currentSection._id) {
          data.title = vm.currentSection.title;
        }
      });
      API
        .Collections
        .edit(vm.origCollection._id, obj)
        .then(function (data) {
          vm.loading = false;
          vm.sectionExist = false;
          vm.oldCurrentSection.title = vm.currentSection.title;
          vm.loadCollectionData();
        })
        .catch(function (err) {
          vm.loading = false;
          if(err.data.statusCode == 400){
            Notify.errorObjectExist();
            vm.sectionExist = true;
            return;
          }
          Notify.errorSaving();
        });
    };

    vm.confirmDeleteSection = function (section) {
      ALERT.confirm(function () {
        return vm.deleteSubCollection(section);
      }, 'Are you sure?', 'This will delete the sub collection with title \'' + section.title + '\'', 'warning', 'Confirm');
    };

    vm.deleteSubCollection = function (section) {
      vm.loading = true;

      var sections = vm.getSections();

      if (sections.length < 2) {
        Notify.errorOperation(); // can't delete if there is only 1 sub collection.
        vm.loading = false;
        return;
      }
      sections = _.reject(sections, function (sec) { return sec.title == section.title });
      sections.forEach(function (data) {
        delete data._id;
      });

      var params = {
        'sections': sections
      };

      API
        .Collections
        .edit(vm.origCollection._id, params)
        .then(function (data) {
          vm.loading = false;
          vm.loadCollectionData();
        })
        .catch(function (err) {
          vm.loading = false;
        });
    };

    vm.confirmDeleteProduct = function (productID) {
      ALERT.confirm(function () {
        return vm.deleteProductFromSection(productID);
      }, 'Are you sure?', 'This will delete the selected product from Sub Collection \'' + vm.currentSection.title + '\'', 'warning', 'Confirm');
    };

    vm.deleteProductFromSection = function (productID) {

      vm.loading = true;

      var productIDs = _.pluck(vm.currentSection.products, '_id');
      productIDs = _.without(productIDs, productID);

      var params = {};
      params.sections = vm.getSections();

      params.sections.forEach(function (data) {
        if (data._id == vm.currentSection._id)
          if (productIDs.length > 0)
            data.products = productIDs;
          else //means there are no products
            delete data.products;
        delete data._id;
      });

      API
        .Collections
        .edit(vm.origCollection._id, params)
        .then(function () {
          Notify.infoDeleted();
          vm.loadCollectionData();
        })
        .catch(function () {
          vm.loading = false;
          Notify.errorSaving();
        });
    };

    vm.addProducts = function () {
        Modal.open(
          'CollectionsCreateProductsModalController',
          'prod',
          'app/modules/cms/collections/create/add_products/productsModal.html',
          {
            SECTION: function () {
              return vm.currentSection;
            },
            COLLECTION: function () {
              return vm.origCollection;
            }
          },
          'prod-listing-custom'
        )
        .result
        .finally(function () {
          vm.loadCollectionData();
        });
    };

    vm.addSubCollection = function () {
        Modal.open(
          'CollectionsCreateSubCollectionController',
          'sub',
          'app/modules/cms/collections/create/sub_collection/subCollectionModal.html',
          {
            COLLECTION: function () {
              return vm.origCollection;
            }
          },
          'sub-collection'
        )
        .result
        .finally(function () {
          vm.loadCollectionData();
        });
    };

    vm.loadCollectionData = function () {
      vm.loading = true;
      API
        .Collections
        .get(vm.collectionID)
        .then(function (data) {
          vm.loading = false;
          vm.collection.type = data.type;
          vm.collection.title = data.title;
          vm.collection.status = data.status;
          vm.collection.visibility = angular.copy(data.visibility);
          vm.collection.slug = data.slug;
          vm.collection.sections = data.sections;
          vm.collection.seo.title = data.seo && data.seo.page_title;
          vm.collection.seo.description = data.seo && data.seo.meta_desc;
          if (data.seo && data.seo.meta_kws && data.seo.meta_kws != '')
            vm.collection.seo.keywords = data.seo.meta_kws.split(',');
          vm.currentSection = data.sections[0];
          vm.oldCurrentSection = angular.copy(vm.currentSection);
          vm.origCollection = data;
          //images
          vm.collectionImages.desktop.images = [];
          vm.collectionImages.mobile.images = [];
          vm.collectionImages.logo.images = [];

          if(data.images && data.images.d_top_banner && data.images.d_top_banner !== '') {
            vm.collectionImages.desktop.path = data.images.d_top_banner;
            vm.collectionImages.desktop.images.push(vm.baseURL + data.images.d_top_banner);
          }else {
            vm.collectionImages.desktop.path = null;
          }
          if(data.images && data.images.logo && data.images.logo !== '') {
            vm.collectionImages.logo.path = data.images.logo;
            vm.collectionImages.logo.images.push(vm.baseURL + data.images.logo);
          }else {
            vm.collectionImages.logo.path = null;
          }
          if(data.images && data.images.m_top_banner && data.images.m_top_banner !== '') {
            vm.collectionImages.mobile.path = data.images.m_top_banner;
            vm.collectionImages.mobile.images.push(vm.baseURL + data.images.m_top_banner);
          }else {
            vm.collectionImages.mobile.path = null;
          }

        })
        .catch(function () {
          vm.loading = false;
          ALERT.confirm(vm.loadCollectionData);
        });
    };


    vm.getSections = function () {
      var sects = angular.copy(vm.origCollection.sections);
      sects.forEach(function (data) {
        if (data.products && data.products.length <= 0)
          delete data.products;
        else
          data.products = _.pluck(data.products, '_id');
      });
      return sects;
    };

    if (vm.isEdit()) {
      vm.loadCollectionData();
    }
  }

})(angular.module('adminPanel.cms.collections.create.controller', []));
