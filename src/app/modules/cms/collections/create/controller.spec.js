'use strict';

describe('Controllers', function () {
  describe('CollectionsCreateController', function () {
    var collection = {
      _id: '571f51c7d5569128031875bsd',
      sections: [{
        title: 'cool title'
      }]
    };
    var
      Controller,
      $rootScope,
      URL,
      $httpBackend,
      ALERT,
      $stateParams,
      API,
      $state,
      $scope,
      Notify,
      Modal;


    // load the dependencies module
    beforeEach(module('ui.bootstrap'));
    beforeEach(module('ngResource'));
    beforeEach(module('oitozero.ngSweetAlert'));
    beforeEach(module('slugifier'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('ngFileUpload'));
    beforeEach(module('adminPanel.services.notifier'));
    beforeEach(module('adminPanel.services.modal'));
    beforeEach(module('ui.router'));
    beforeEach(module('adminPanel.services.alert'));
    beforeEach(module('adminPanel.api'));
    beforeEach(module('adminPanel.cms.collections.create.controller'));

    beforeEach(inject(function ($injector) {
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      $httpBackend = $injector.get('$httpBackend');
      URL = $injector.get('URL');
      var $controller = $injector.get('$controller');
      $stateParams = {};
      $state = $injector.get('$state');
      API = $injector.get('API');
      Notify = $injector.get('Notify');
      Modal = $injector.get('Modal');
      ALERT = $injector.get('ALERT');
      Controller = $controller('CollectionsCreateController', {
        '$scope': $scope,
        'API': API,
        'Modal': Modal,
        '$stateParams': $stateParams,
        '$state': $state,
        'Slug': $injector.get('Slug'),
        'ALERT': ALERT,
        'Notify': Notify,
        'MIID_API': $injector.get('MIID_API'),
        'Upload': $injector.get('Upload')
      });

      collection.sections[0].products = [
        {_id: '571f51c7d5569128031875b6'},
        {_id: '571f53c7d5569128031875b6'},
        {_id: '571f57c7d5569128031874b6'}
      ];
      Controller.origCollection = collection;
      $httpBackend.when('GET', URL.COLLECTIONS ).respond(200, collection);
    }));


    describe('MBA-158: Create/Edit Collection', function () {

      it('should be in edit mode when there is collectionID', function () {
        $stateParams.collectionID = '12345';
        expect(Controller.isEdit()).toBeTruthy();
      });

      it('should be in create mode when there is no collectionID', function () {
        expect(Controller.isEdit()).toBeFalsy();
      });

      it('should have default status of INACTIVE', function () {
        expect(Controller.collection.status).toEqual('INACTIVE');
      });

      it('should set slug form seo title', function () {
        Controller.collection.seo.title = 'this is test';
        Controller.setSlug();
        expect(Controller.collection.slug).toEqual('this-is-test');
      });

      it('should return slugified string', function () {
        expect(Controller.slugify('this is test')).toEqual('this-is-test');
      });

      it('should set seo title and slug from collection title when not in edit mode', function () {
        Controller.collection.title = 'this is test';
        Controller.fillSlugTitle();
        expect(Controller.collection.seo.title).toEqual('this is test');
        expect(Controller.collection.slug).toEqual('this-is-test');
      });

      it('shouldnt set seo title and slug from collection title while in edit mode', function () {
        Controller.collection.title = 'this is test';
        $stateParams.collectionID = 'bla bla';
        Controller.fillSlugTitle();
        expect(Controller.collection.seo.title).not.toEqual('this is test');
        expect(Controller.collection.slug).not.toEqual('this-is-test');
      });

      it('should add keyword in seo', function () {
        Controller.collection.seo.currentKeyword = 'this is test';
        Controller.addTag();
        expect(Controller.collection.seo.keywords.length).toEqual(1);
        expect(Controller.collection.seo.currentKeyword).toEqual('');
        expect(Controller.collection.seo.keywords[0]).toEqual('this is test');
      });

      it('should remove keyword in seo', function () {
        Controller.collection.seo.currentKeyword = 'this is test';
        Controller.addTag();
        expect(Controller.collection.seo.keywords.length).toEqual(1);
        Controller.removeTag('this is test');
        expect(Controller.collection.seo.keywords.length).toEqual(0);
      });

      it('it should add section and call loadCollectionData', function () {
        $stateParams.collectionID = '123456';
        spyOn(Controller,'loadCollectionData');
        $httpBackend.expect('PUT', URL.COLLECTIONS + '/'+collection._id).respond(200, true);
        Controller.saveNewSectionTitle();
        $httpBackend.flush();
        expect(Controller.loadCollectionData).toHaveBeenCalled();
      });

      it('should call Notify if there is less than 1 section before deleting', function () {
        spyOn(Notify, 'errorOperation');
        Controller.deleteSubCollection(collection.sections[0]);
        expect(Notify.errorOperation).toHaveBeenCalled();
        expect(Controller.origCollection.sections.length).toEqual(1);
      });


      it('should delete product and call Notify on success', function () {
        spyOn(Notify, 'infoDeleted');
        $httpBackend.expect('PUT', URL.COLLECTIONS +'/'+collection._id).respond(200, true);
        Controller.deleteProductFromSection();
        $httpBackend.flush();
        expect(Notify.infoDeleted).toHaveBeenCalled();
      });

      it('should return sections with clean keys', function () {
        Controller.origCollection = collection;
        //see method converted object of products to only ids.
        expect(Controller.getSections()[0].products[0]).toEqual('571f51c7d5569128031875b6');
      });

      it('should hit PUT while in edit mode', function () {
        spyOn(Controller,'update');

        Controller.origCollection.slug = Controller.collection.slug;
        $stateParams.collectionID = "asdfs";
        Controller.saveChangings();

        expect(Controller.update).toHaveBeenCalled();
      });

      it('should hit POST while not in edit mode', function () {
        spyOn($state, 'go');
        $httpBackend.expect('GET', URL.COLLECTIONS +'/slug' ).respond(404, {statusCode: 404});
        $httpBackend.expect('POST', URL.COLLECTIONS).respond(200, {sections: [{test: 's'}]});
        Controller.saveChangings();

        $httpBackend.flush();

        expect($state.go).toHaveBeenCalled();
      });

    });

  });

});
