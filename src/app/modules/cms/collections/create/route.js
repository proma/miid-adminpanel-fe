(function(app) {

  app.config(['$stateProvider', function($stateProvider) {

    $stateProvider
      .state('admin-panel.cms.collections.create', {
        url: '/create/:collectionID',
        templateUrl: 'app/modules/cms/collections/create/create-collection.html',
        controller: 'CollectionsCreateController',
        controllerAs: 'vm'
      });

  }]);

})(angular.module('adminPanel.cms.collections.create.route',[]));
