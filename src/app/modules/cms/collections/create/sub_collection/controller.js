(function (app) {

  app.controller('CollectionsCreateSubCollectionController', CollectionsCreateSubCollectionController);

  CollectionsCreateSubCollectionController.$inject = ['$uibModalInstance', 'COLLECTION', 'API'];

  function CollectionsCreateSubCollectionController($uibModalInstance, COLLECTION, API) {
    var sub = this;

    sub.collectionCreated = false;
    sub.collectionExist = false;
    sub.loading = false;

    sub.close = function () {
      $uibModalInstance.close();
    };

    sub.submit = function () {
      sub.loading = true;

      var params = {
        sections: []
      };

      COLLECTION.sections.forEach(function (data) {
        var section = {title: data.title, _id: data._id};
        if(data.products && data.products.length > 0)
          section.products = _.pluck(data.products, '_id');
        params.sections.push(section);
      });

      params.sections.push({title: sub.name});

      API
        .Collections
        .edit(COLLECTION._id, params)
        .then(function (data) {
          sub.collectionCreated = true;
          sub.collectionExist = false;
          sub.loading = false;
        })
        .catch(function (err) {
          sub.loading = false;
          sub.collectionExist = true;
          sub.collectionCreated = false;
        });


    };
  }

})(angular.module('adminPanel.cms.collections.create.subCollection.controller', []));
