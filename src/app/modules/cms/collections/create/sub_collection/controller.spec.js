'use strict';

describe('Controllers', function () {
  describe('CollectionsCreateSubCollectionController', function () {

    var
      Controller,
      $rootScope,
      URL,
      $httpBackend,
      $scope,
      $uibModalInstance;


    // load the dependencies module
    beforeEach(module('toastr'));
    beforeEach(module('ui.bootstrap'));
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.api'));
    beforeEach(module('adminPanel.cms.collections.create.subCollection.controller'));

    beforeEach(inject(function ($injector) {
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      $httpBackend = $injector.get('$httpBackend');
      URL = $injector.get('URL');
      var $controller = $injector.get('$controller');
      $uibModalInstance =  {
        // create a mock object using spies
        close: jasmine.createSpy('$uibModalInstance.close'),
      };

      Controller = $controller('CollectionsCreateSubCollectionController', {
        '$scope': $scope,
        '$uibModalInstance': $uibModalInstance,
        'API': $injector.get('API'),
        'COLLECTION': {_id: '123456', sections: []}
      });

    }));


    it('should close the modal', function () {
      Controller.close();
      expect($uibModalInstance.close).toHaveBeenCalled();
    });

    it('should initialize some variables', function () {
      expect(Controller.collectionCreated).toBeFalsy();
      expect(Controller.collectionExist).toBeFalsy();
      expect(Controller.loading).toBeFalsy();
    });

    it('should start loading after clicking submit', function () {
      Controller.submit();
      expect(Controller.loading).toBeTruthy();
    });

    it('should call api and save the sub collection', function () {
      $httpBackend.expect('PUT', URL.COLLECTIONS + '/123456').respond(200, {success: true});
      Controller.submit();
      $httpBackend.flush();
      expect(Controller.loading).toBeFalsy();
      expect(Controller.collectionCreated).toBeTruthy();
      expect(Controller.collectionExist).toBeFalsy();
    });

    it('should make collectionExist true if error', function () {
      $httpBackend.expect('PUT', URL.COLLECTIONS + '/123456').respond(500, {success: true});
      Controller.submit();
      $httpBackend.flush();
      expect(Controller.loading).toBeFalsy();
      expect(Controller.collectionCreated).toBeFalsy();
      expect(Controller.collectionExist).toBeTruthy();
    });

  });

});
