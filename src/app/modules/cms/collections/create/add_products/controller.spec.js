'use strict';

describe('Controllers', function () {
  describe('CollectionsCreateProductsModalController', function () {
    var products = {
      paging: {total: 5},
      data: [
        {
          _id: '571f51c7d5569128031875b6',
          brand: {_id:124}
        },
        {
          _id: '571f51c745569128031875b6',
          brand: {_id:124}
        },
        {
          _id: '571f51c745569128031875b13',
          brand: {_id:124}
        },
        {
          _id: '571f51c745569128031875b14',
          brand: {_id:124}
        }
      ]
    };
    var collection = {
      _id: '571f51c7d5569128031875bsd',
      "sections": [{
        title: 'cool title',
        products: [
          {
            _id: '571f51c7d5569128031875b6',
            brand: {_id:124}
          },
          {
            _id: '571f51c7d5569128031874b6',
            brand: {_id:124}
          }
        ]
      }]
    };

    var
      Controller,
      $rootScope,
      URL,
      $httpBackend,
      $timeout,
      ALERT,
      $scope,
      $uibModalInstance,
      Notify;


    // load the dependencies module

    beforeEach(module('ui.bootstrap'));
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.services.notifier'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.api'));
    beforeEach(module('adminPanel.services.alert'));
    beforeEach(module('adminPanel.cms.collections.create.addProducts.controller'));

    beforeEach(inject(function ($injector) {
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      $httpBackend = $injector.get('$httpBackend');
      URL = $injector.get('URL');
      var $controller = $injector.get('$controller');
      Notify = $injector.get('Notify');
      ALERT = $injector.get('ALERT');
      $uibModalInstance =  {
        // create a mock object using spies
        close: jasmine.createSpy('$uibModalInstance.close'),
      };

      Controller = $controller('CollectionsCreateProductsModalController', {
        'Notify': Notify,
        '$scope': $scope,
        '$uibModalInstance': $uibModalInstance,
        'API': $injector.get('API'),
        'COLLECTION': collection,
        'SECTION': collection.sections[0],
        'ALERT': ALERT
      });

      Controller.selectorForProducts.reset = function() {}; //fake coz of directive
      Controller.selectorForSectionProducts.reset = function() {}; //fake coz of directive
      Controller.products = products.data;

      $httpBackend.when('GET', URL.BRANDS + '?list_type=LIST_VALUES').respond({data:[]}, 200);
      $httpBackend.when('GET', URL.PRODUCTS + '/list_values?basic_info=true').respond(products.data, 200);
      $httpBackend.flush();
    }));


    it('products array should be initialized', function () {
      expect(Controller.products.length).toEqual(3);
    });

    it('sectionProducts array should be initialized', function () {
      expect(Controller.sectionProducts.length).toEqual(2);
    });

    it('it should add checked products from product array to sectionProducts array', function () {
      Controller.products[0].checked = true;
      Controller.addToList();
      expect(Controller.sectionProducts.length).toEqual(3);
    });

    it('it should remove checked products from product array', function () {
      Controller.products[0].checked = true;
      Controller.addToList();
      expect(Controller.products.length).toEqual(2);
    });

    it('it should add checked products from sectionProducts array to products array', function () {
      Controller.sectionProducts[1].checked = true;
      Controller.removeFromList();
      expect(Controller.products.length).toEqual(4);
    });

    it('it should remove checked products from sectionProducts array', function () {
      Controller.sectionProducts[1].checked = true;
      Controller.removeFromList();
      expect(Controller.sectionProducts.length).toEqual(1);
    });

    it('it should remove products from all products that exist in section already', function () {
      $httpBackend.expect('GET',URL.PRODUCTS + '/list_values?basic_info=true').respond(200, products.data);
      expect(Controller.products.length).toEqual(4);
    });

    it('it should call api and Notify when clicked submit', function () {

      spyOn(Notify, 'infoSaved');
      $httpBackend.expect('PUT', URL.COLLECTIONS + '/571f51c7d5569128031875bsd').respond(200, {success: true});
      Controller.submit();
      $httpBackend.flush();
      expect(Notify.infoSaved).toHaveBeenCalled();
    });

    it('it should call error Notify when server error.', function () {

      spyOn(Notify, 'errorSaving');
      $httpBackend.expect('PUT', URL.COLLECTIONS + '/571f51c7d5569128031875bsd').respond(500,{success: true});
      Controller.submit();
      $httpBackend.flush();
      expect(Notify.errorSaving).toHaveBeenCalled();
    });

  });

});
