(function (app) {

  app.controller('CollectionsCreateProductsModalController', CollectionsCreateProductsModalController);

  CollectionsCreateProductsModalController.$inject = ['$scope', 'API', '$uibModalInstance', 'COLLECTION', 'SECTION', 'Notify', 'QINGSTOR', 'ALERT', '$translate'];

  /**
   * @param $scope
   * @param API
   * @param $uibModalInstance
   * @param COLLECTION
   * @param SECTION
   * @param Notify
   * @param QINGSTOR
   * @param ALERT
   */
  function CollectionsCreateProductsModalController($scope, API, $uibModalInstance, COLLECTION, SECTION, Notify, QINGSTOR, ALERT, $translate) {
    var prod = this;

    prod.baseURL = QINGSTOR.base_url;
    prod.section = SECTION;
    prod.products = [];
    prod.selectorForProducts = {};
    prod.selectorForSectionProducts = {};
    prod.sectionProducts = [];
    prod.cachedSectionProducts = {deleted: [], current: []}; //we have to keep track of these as admin can apply filters and we can loose previous products
    prod.loadingProducts = true;
    prod.brands = [{name: $translate.instant('filters.brand.label') || 'Brands', _id: -1}];
    prod.status = {
      'STATUS': $translate.instant('filters.status.label') || 'Status',
      'ACTIVE': $translate.instant('constants.status.active') || 'Active',
      'IN_ACTIVE': $translate.instant('constants.status.in_active') || 'Inactive'
    };

    prod.selectedBrandForProducts = prod.brands[0];
    prod.selectedBrandForSectionProducts = prod.brands[0];
    prod.selectedStatusForProducts = 'STATUS';
    prod.selectedStatusForSectionProducts = 'STATUS';
    prod.searchedTextForProducts = '';
    prod.searchedTextForSectionProducts = '';

    prod.leftListBulk = [
      {
        title: 'Add to Listing',
        action: function () {
          prod.addToList();
        }
      }
    ];

    prod.rightListBulk = [
      {
        title: 'Remove from Listing',
        action: function () {
          prod.removeFromList();
        }
      }
    ];


    /**
     * This function acts as generic function to add or remove items from both listings
     * @param {Array} source
     * @param {Array} dest
     * @param {Function} cb has argument 'selected' which are selected items
     * @returns {*[]}
     */
    prod.addRemoveFromList = function (source, dest, cb) {

      var selected = _.filter(source, function (item) { //find selected items
        var isChecked;
        if (item.checked) {
          item.changed = true;
        }
        isChecked = item.checked || false; //two in one processing ;)
        item.checked = false;
        return isChecked;
      });
      source = _.difference(source, selected); //remove those items from source
      dest = _.union(dest, selected); //add those to destination
      if (cb) cb(selected);

      prod.selectorForProducts.reset();
      prod.selectorForSectionProducts.reset();
      return [source, dest];
    };

    /**
     * Adds the selected items from the list and removes it from the other list. The added
     * item will have little border at left to show what's been changed.
     */
    prod.addToList = function () {

      var result;
      result = prod.addRemoveFromList(prod.products, prod.sectionProducts, null); //source is products, dest is sectionProducts
      prod.products = result[0];
      prod.sectionProducts = result[1];
    };

    /**
     * Removes the selected items from the list and adds it to the other list. The removed
     * item will have little border at left to show what's been changed.
     */
    prod.removeFromList = function () {

      var result;

      result = prod.addRemoveFromList(prod.sectionProducts, prod.products, function (selected) {
        selected = _.filter(prod.cachedSectionProducts.current, function (item) { return _.find(selected, {_id: item._id}) });
        // selected = selected.map(function (item) { item.brandID = item.brand._id; return item });
        prod.cachedSectionProducts.current = _.difference(prod.cachedSectionProducts.current, selected);
        prod.cachedSectionProducts.deleted = _.union(prod.cachedSectionProducts.deleted, selected);
      });
      prod.sectionProducts = result[0];
      prod.products = result[1];
    };

    /**
     * Submits the form and post the data to the API we only sends IDs
     * If user has applied some filtering then we have lost our all sectionProducts so we
     * use cachedSectionProducts with union so that we don't miss any section product.
     * We close the modal after saving
     */
    prod.submit = function () {
      prod.loadingProducts = true;

      var productIDs =
          _.union(_.pluck(prod.sectionProducts, '_id'), _.pluck(prod.cachedSectionProducts.current, '_id'));

      var params = {};
      params.sections = angular.copy(COLLECTION.sections);

      params.sections.forEach(function (data) {

        if(data._id == SECTION._id)
          data.products = productIDs;
        else
          data.products = _.pluck(data.products, '_id');

        if (data.products && data.products.length <= 0)
          delete data.products;

        delete data._id;
      });

      API
        .Collections
        .edit(COLLECTION._id, params)
        .then(function (data) {
          prod.loadingProducts = false;
          Notify.infoSaved();
          prod.close();
        })
        .catch(function (err) {
          prod.loadingProducts = false;
          Notify.errorSaving();
        });
    };

    /**
     * Loads all brands in the Brands drop down for filtering
     */
    prod.loadBrands = function () {
      API
          .Brands
          .getAll(null, null, 'LIST_VALUES')
          .then(function (data) {
            prod.brands = data.data;
            prod.brands.unshift({name: $translate.instant('filters.brand.label') || 'Brands', _id: -1});
            prod.selectedBrandForProducts = prod.brands[0];
          })
          .catch(function () {
            prod.brands[0] = {name: 'Error Loading...', _id: -1};
            ALERT.confirm(prod.loadBrands);
          });
    };

    /**
     * Loads all the products on the left table in UI. We filter this list with section products
     * so that we don't get what's in the section products ;)
     */
    prod.loadProductsData = function () {
      prod.loadingProducts = true;
      var params;
      params = {basic_info: true};

      if (prod.selectedBrandForProducts._id != -1)
        params.brand = prod.selectedBrandForProducts._id;
      if (prod.selectedStatusForProducts.toLowerCase() != 'status')
        params.status = prod.selectedStatusForProducts;
      if (prod.searchedTextForProducts !== '')
        params.text = prod.searchedTextForProducts;

      API
          .Products
          .getList(params) //TODO: find a way to add pagination to that table
          .then(function (data) {
            prod.loadingProducts = false;
            prod.products = data;

            prod.removeDuplicates();
          })
          .catch(function () {
            prod.loadingProducts = false;
            ALERT.confirm(prod.loadProductsData);
          });
    };

    /**
     * Checks the status of the submit button to mark it disabled or not. This will get updated
     * after every digest cycle of angular
     */
    prod.checkStatus = function () {
      //look for 'changed' property on the sectionProducts
      var changed = true;
      prod.sectionProducts.forEach(function (item) {
        if (item.changed) {
          return changed = false;
        }
      });
      if (changed)
        prod.products.forEach(function (item) {
          if (item.changed) {
            return changed = false;
          }
        });
      return changed;
    };


    /**
     * This function is used to load the functions i-e loadProductsData
     * based on type showing the ALERT to confirm if there are unsaved changes
     * @param {string} type 'sectionProducts' | 'products'
     */
    prod.triggerLoad = function (type) {
      var fn;
      fn = type == 'products' ? prod.loadProductsData : prod.filterSectionProducts;

      if (!prod.checkStatus())
        ALERT.confirmUnsavedChanges(function () {
          //resets the cache
          prod.cachedSectionProducts.current = _.union(prod.cachedSectionProducts.current, prod.cachedSectionProducts.deleted);
          prod.products = _.union(prod.products, prod.cachedSectionProducts.deleted);
          prod.cachedSectionProducts.deleted = [];
          prod.sectionProducts = prod.cachedSectionProducts.current;
          fn();
        });
      else
        fn();
    };

    /**
     * Loads section Products.
     */
    prod.loadSectionProducts = function () {

      prod.sectionProducts = _.map(SECTION.products, function (item){ item.brandID = item.brand._id; return item});//workaround for filtetring by brand
      prod.cachedSectionProducts.current = angular.copy(prod.sectionProducts);

    };

    /**
     * Remove the duplicates i-e section products which should not be in the left hand side listing
     */
    prod.removeDuplicates = function () {
      var section;
      section = _.union(_.pluck(prod.cachedSectionProducts.current, '_id'), _.pluck(prod.sectionProducts, '_id'));
      prod.products = _.reject(prod.products, function(item) {
        return _.find(section, function (prod){return prod == item._id});
      });
    };

    /**
     * Filters the section products by search text, brand and status. Because this list is not too long
     * so it make sense to do this on client side.
     */
    prod.filterSectionProducts = function () {
      var findObj = {};

      if (prod.selectedBrandForSectionProducts._id != -1)
        findObj.brandID = prod.selectedBrandForSectionProducts._id;
      if (prod.selectedStatusForSectionProducts.toLowerCase() != 'status')
        findObj.status = prod.selectedStatusForSectionProducts;

      prod.sectionProducts = _.unique(_.where(prod.cachedSectionProducts.current, findObj), function(prod){return prod._id});
    };

    prod.close = function () {
      $uibModalInstance.close();
    };

    prod.loadBrands();
    prod.loadSectionProducts();
    prod.triggerLoad('products');
  }

})(angular.module('adminPanel.cms.collections.create.addProducts.controller', []));
