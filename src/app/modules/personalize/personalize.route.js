(function (app) {

  app.config(['$stateProvider', function ($stateProvider) {

    $stateProvider
      .state('admin-panel.personalize', {
        url: '/personalize',
        abstract: true,
        template: '<div class="animated fadeInRight" ui-view></div>'
      });

  }]);

})(angular.module('adminPanel.personalize.route', []));
