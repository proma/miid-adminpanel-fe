(function () {

})(angular.module('adminPanel.personalize', [
  'adminPanel.personalize.route',
  'adminPanel.personalize.list',
  'adminPanel.personalize.galleries',
  'adminPanel.personalize.artworks'
]));
