(function (app) {

  app.controller('PersonalizeGalleriesListAddGalleryModalController', PersonalizeGalleriesListAddGalleryModalController);

  PersonalizeGalleriesListAddGalleryModalController.$inject = ['$uibModalInstance', 'API', 'Notify', '$timeout', 'ALERT', 'QINGSTOR', 'BRAND', 'RolesService', '$translate'];

  function PersonalizeGalleriesListAddGalleryModalController($uibModalInstance, API, Notify, $timeout, ALERT, QINGSTOR, BRAND, RolesService, $translate) {
    var modal = this;

    modal.tags = [];
    modal.artworks = [];
    modal.galleries = [{en: {name: 'Loading..'}, _id: -1}];

    modal.baseURL = QINGSTOR.base_url;
    modal.processing = false;
    modal.toggled = false;
    modal.loadingArtworks = false;
    modal.loadingTags = false;

    modal.name = '';
    modal.selectedGallery = modal.galleries[0];
    modal.selected = 0;
    modal.artworksPage = 0;
    modal.totalArtworks = 1;

    modal.createTag = function () {
      var hasOne = _.filter(modal.tags, function (tag) { return tag.new;} ).length > 0;
      if (!hasOne)
        modal.tags.unshift({name: '', new: true, selected: false});
    };

    modal.saveTag = function (tag) {
      tag.name = tag.name.toLowerCase();
      if (_.where(modal.tags, {name: tag.name}).length > 1)
        tag.error = true;
      else
        tag.new = false;
    };

    modal.delTag = function (tag) {
      modal.tags = _.without(modal.tags, tag);
    };


    modal.toggle = function (state) {
      modal.artworks.forEach(function (item) {
        item.selected = state;
      });
      modal.selected = state ? modal.artworks.length : 0;
    };

    modal.selectCheckBox = function (state) {
      modal.selected = state ? ++modal.selected : --modal.selected;
      if (modal.selected === 1)
        modal.toggled = true;
      if (modal.selected === 0)
        modal.toggled = false;
    };

    modal.loadArtworks = function (filterByGallery) {
      var param = {brand: BRAND, public_items: !BRAND};

      if (filterByGallery) {
        modal.selected = 0;
        modal.toggled = false;
        modal.artworksPage = 0;
        modal.artworks = [];
        if (filterByGallery != -1) {
          param.gallery = filterByGallery;
        }
      }

      if (modal.totalArtworks <= modal.artworks.length || modal.loadingArtworks)
        return;

      modal.loadingArtworks = true;
      param.page = modal.artworksPage;
      param.limit = 10;
      API
        .Artworks
        .query(param)
        .then(function (obj) {
          obj.data = obj.data.map(function (item) {
            item.selected = false;
            return item;
          });
          modal.artworks = modal.artworks.concat(obj.data);
          modal.totalArtworks = obj.paging.total > 0? obj.paging.total: 1;
          modal.artworksPage++;
        })
        .catch(function () {
          ALERT.confirm(modal.loadArtworks);
        })
        .finally(function () {
          modal.loadingArtworks = false;
        });
    };

    modal.loadTags = function () {
      modal.loadingTags = true;
      API
        .Galleries
        .getTags()
        .then(function (data) {
          modal.tags = data.map(function (tag) {
            return {name: tag, selected: false};
          });
        })
        .catch(function () {
          ALERT.confirm(modal.loadTags);
        })
        .finally(function () {
          modal.loadingTags = false;
        });
    };

    modal.loadGalleries = function () {
      API
        .Galleries
        .query({list_type: 'LIST_VALUES', brand: BRAND, public_items: !BRAND})
        .then(function (obj) {
          modal.galleries = obj.data;
          modal.galleries.unshift({en: {name: $translate.instant('words.gallery') || 'Gallery'}, _id: -1});
          modal.selectedGallery = modal.galleries[0];
        })
        .catch(function () {
          modal.galleries = [{en: {name: 'Error Loading..'}, _id: -1}];
          modal.selectedGallery = modal.galleries[0];
        });
    };

    modal.getSelectedTags = function () {
      return  _.chain(modal.tags).where({selected:true}).pluck('name').value();
    };

    modal.getSelectedArtworks = function () {
      return _.chain(modal.artworks).where({selected: true}).pluck('_id').value();
    };

    modal.saveGallery = function () {
      modal.processing = true;
      var tags = modal.getSelectedTags(), artworks = modal.getSelectedArtworks(), gallery = {};

      gallery = {
        en: {
          name: modal.name
        },
        status: 'ENABLE'
      };

      if (tags.length > 0)
        gallery.en.tags = tags;
      if (artworks.length > 0)
        gallery.artworks = artworks;
      if (BRAND) {
        gallery.brand = BRAND;
        gallery.type = 'PRIVATE';
      }
      else if (RolesService.isUser('BRAND_ADMIN'))
        gallery.type = 'PRIVATE';
      else
        gallery.type = 'PUBLIC';

      gallery.ch = gallery.en;

      API
        .Galleries
        .create(gallery)
        .then(function () {
          Notify.infoSaved();
          $timeout(function () {
            modal.close(true);
          }, 150);
        })
        .catch(function () {
          Notify.errorSaving();
        })
        .finally(function () {
          modal.processing = false;
        });
    };

    modal.close = function (close) {
      if (close)
        $uibModalInstance.close();
      else
        $uibModalInstance.dismiss();
    };

    modal.loadArtworks();
    modal.loadGalleries();
    modal.loadTags();
  }

})(angular.module('adminPanel.personalize.galleries.list.addGalleryModal.controller', []));
