(function (app) {
  app.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('admin-panel.personalize.galleries.list', {
        url: '/list?brand&id&{public:bool}',
        controller: 'PersonalizeGalleriesListController',
        controllerAs: 'vm',
        templateUrl: 'app/modules/personalize/galleries/list/personalize.galleries.list.html'
      });
  }]);
})(angular.module('adminPanel.personalize.galleries.list.route', []));
