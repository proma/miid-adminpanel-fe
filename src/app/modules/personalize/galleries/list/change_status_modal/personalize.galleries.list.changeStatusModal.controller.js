(function (app) {

  app.controller('PersonalizeGalleriesListChangeStatusModalController', PersonalizeGalleriesListChangeStatusModalController);

  PersonalizeGalleriesListChangeStatusModalController.$inject = ['$uibModalInstance', 'LIST', 'FN', 'Notify'];

  function PersonalizeGalleriesListChangeStatusModalController($uibModalInstance, LIST, FN, Notify) {
    var modal = this;

    modal.selectedStatus = 'ENABLE';

    modal.submit = function () {
      modal.processing = true;
      var completedReq = 0;
      var noOfErrors = 0;
      var noOfCalls = 0; //no of http requests to call

      LIST.forEach(function(data){
        if (data.checked){
          ++noOfCalls;
        }
      });

      LIST.forEach(function (data) {
        if(data.checked) {
          FN(data, modal.selectedStatus, function (err, obj) {
            if (err)
              callBack(err);
            else
              callBack();
          });
        }
      });

      function callBack(err){
        if(!err){
          ++completedReq;
        }
        if(err){
          ++noOfErrors;
        }
        if(noOfCalls == (completedReq + noOfErrors)){
          modal.close(true);
          modal.processing = false;
          Notify.infoChangeItemsStatus(noOfCalls, completedReq, modal.selectedStatus);
        }
      }
    };

    modal.close = function (close) {
      if (close)
        $uibModalInstance.close();
      else
        $uibModalInstance.dismiss();
    }

  }

})(angular.module('adminPanel.personalize.galleries.list.changeStatusModal.controller', []));
