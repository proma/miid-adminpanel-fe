(function (app) {

  app.controller('PersonalizeGalleriesListTagsModalController', PersonalizeGalleriesListTagsModalController);

  PersonalizeGalleriesListTagsModalController.$inject = ['$uibModalInstance', 'Single', 'Galleries', 'API', 'ALERT', 'Notify'];

  function PersonalizeGalleriesListTagsModalController($uibModalInstance, Single, Galleries, API, ALERT, Notify) {
    var modal = this;

    modal.tags = [];

    modal.loadingTags = false;

    modal.createTag = function () {
      var hasOne = _.filter(modal.tags, function (tag) { return tag.new;} ).length > 0;
      if (!hasOne)
        modal.tags.unshift({name: '', new: true});
    };

    modal.saveTag = function (tag) {
      tag.name = tag.name.toLowerCase();
      if (_.where(modal.tags, {name: tag.name}).length > 1)
        tag.error = true;
      else
        tag.new = false;
    };

    modal.delTag = function (tag) {
      modal.tags = _.without(modal.tags, tag);
    };

    modal.loadTags = function () {
      modal.loadingTags = true;
      API
        .Galleries
        .getTags()
        .then(function (data) {
          modal.tags = modal.filterTags(data);
        })
        .catch(function () {
          ALERT.confirm(modal.loadTags);
        })
        .finally(function () {
          modal.loadingTags = false;
        });
    };

    modal.filterTags = function (tagsList) {
      var tags = [], uniqueTags = [], tagsLeft = [], exist;

      Galleries.forEach(function (item) {
        if (item.en && item.en.tags)
          tags.push(item.en.tags);
      });

      uniqueTags = _.intersection.apply(_, tags);
      tags = _.flatten(tags);
      tags = _.unique(tags);
      tagsLeft = _.difference(tagsList, tags);

      tags = tags.map(function (item) {
        exist = _.filter(uniqueTags, function (tg) { return tg == item;}).length > 0;
        return {name: item, disabled: !exist && !Single, selected: true, included: true};
      });
      tagsLeft = tagsLeft.map(function (tag) {
        return {name: tag, selected: false, disabled: false};
      });

      return _.union(tags, tagsLeft);
    };

    modal.saveTags = function () {
      modal.processing = true;
      var tags = [], allTags = [], delTags = [], galleries = [], completedReq = 0, noOfErrors = 0, noOfCalls = 0;

      allTags  = _.chain(modal.tags).where({selected: true}).value();
      delTags = _.difference(_.chain(modal.tags).where({included: true}).pluck('name').value(), _.pluck(allTags, 'name'));
      tags = _.chain(allTags).where({disabled: false}).pluck('name').value();

      galleries = _.map(Galleries, function (gl) {
        return {
          _id: gl._id,
          en: {
            name: gl.en.name,
            tags: _.unique(_.difference(_.union(gl.en.tags, tags), delTags))
          }
        }
      });

      noOfCalls = galleries.length;

      galleries.forEach(function (gallery) {
        API
          .Galleries
          .edit(gallery._id, {en: gallery.en})
          .then(function () {
            callBack();
          })
          .then(function (err) {
            callBack(err);
          });
      });

      function callBack(err){
        if(!err){
          ++completedReq;
        }
        if(err){
          ++noOfErrors;
        }
        if(noOfCalls == (completedReq + noOfErrors)){
          modal.close(true);
          modal.processing = false;
          galleries.length > 0 ? Notify.infoSavedItems(noOfCalls, completedReq) : Notify.infoSaved();
        }
      }
    };

    modal.close = function (close) {
      if (close)
        $uibModalInstance.close();
      else
        $uibModalInstance.dismiss();
    }


    modal.loadTags();
  }

})(angular.module('adminPanel.personalize.galleries.list.addTagsModal.controller', []));
