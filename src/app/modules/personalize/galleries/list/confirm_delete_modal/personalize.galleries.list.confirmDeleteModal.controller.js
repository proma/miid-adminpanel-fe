(function (app) {

  app.controller('PersonalizeGalleriesListDeleteModalController', PersonalizeGalleriesListDeleteModalController);

  PersonalizeGalleriesListDeleteModalController.$inject = ['$uibModalInstance', 'MULTIPLE'];

  function PersonalizeGalleriesListDeleteModalController($uibModalInstance, MULTIPLE) {
    var modal = this;

    modal.multiple = MULTIPLE;

    modal.close = function () {
      $uibModalInstance.dismiss();
    };

    modal.confirm = function () {
      $uibModalInstance.close();
    };

  }

})(angular.module('adminPanel.personalize.galleries.list.confirmDeleteModal.controller', []));
