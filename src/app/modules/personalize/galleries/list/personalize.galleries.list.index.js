(function () {
})(angular.module('adminPanel.personalize.galleries.list', [
  'adminPanel.personalize.galleries.list.controller',
  'adminPanel.personalize.galleries.list.route',
  'adminPanel.personalize.galleries.list.addTagsModal',
  'adminPanel.personalize.galleries.list.addGalleryModal',
  'adminPanel.personalize.galleries.list.changeStatusModal',
  'adminPanel.personalize.galleries.list.confirmDeleteModal'
]));
