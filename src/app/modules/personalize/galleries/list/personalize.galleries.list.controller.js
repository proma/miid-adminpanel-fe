(function (app) {

  app.controller('PersonalizeGalleriesListController', PersonalizeGalleriesListController);

  PersonalizeGalleriesListController.$inject = ['$scope', 'Modal', 'API', 'ALERT', 'Notify', '$stateParams', 'QINGSTOR', 'RolesService', '$localStorage'];

  function PersonalizeGalleriesListController($scope, Modal, API, ALERT, Notify, $stateParams, QINGSTOR, RolesService, $localStorage) {
    var vm = this;

    vm.loading = false;
    vm.processing = false;
    vm.galleries = [];
    if (RolesService.isUser('BRAND_ADMIN')) {
      vm.brand = $localStorage['userData']['brand']._id;
      vm.brandID = $localStorage['userData']['brand'].brand_id;
    } else {
      vm.brand = $stateParams.brand || null;
      vm.public = $stateParams.public || null;
      vm.brandID = $stateParams.id || null;
    }
    vm.artworksCount = 0;

    vm.totalItems = 0; //TODO: need to make directive for these
    vm.itemsGot = 0;
    vm.previousPage = 0;
    vm.currentPage = 1;
    vm.itemsLimit = 10;

    vm.filters = {
      status: '0',
      from: moment((new Date("1/1/" + moment().year())).valueOf()),
      to: moment(),
      search: ''
    };

    vm.bulkActions =
      [
        {title: 'Change Status', action: function () { return vm.changeStatusModal();}, divider: true},
        {title: 'Add Tags', action: function () { return vm.addTags(_.where(vm.galleries, {checked: true}), 'multiple') }, divider: true},
        {title: 'Delete Galleries', action: function () { return vm.confirmDelete('bulk');}}
      ];
    vm.listSelectorOptions = {};

    vm.loadList = function (currDate) {
      if (currDate)
        vm.filters.to = moment(currDate);

      vm.loading = true;
      API
        .Galleries
        .query({
          public_items: !!vm.public,
          page: vm.currentPage-1,
          limit: vm.itemsLimit,
          brand: vm.brand,
          text: vm.filters.search,
          from: vm.filters.from.valueOf(),
          to: currDate || vm.filters.to.valueOf(),
          status: vm.filters.status == 0 ? null: vm.filters.status
        })
        .then(function (items) {
          items.data.map(function (item) {
            item.created_at = new Date(item.created_at.substr(0,item.created_at.indexOf('T')));
            item.images = [];

            item.artworks.forEach(function (aw) {
              item.images.push(aw.image && aw.image.path ? QINGSTOR.base_url + aw.image.path: '/img/icons/img-placeholder.png')
            });
            for(var i = 4-item.images.length; i > 0; i--)
              item.images.push('/img/icons/img-placeholder.png');

            return item;
          });
          vm.galleries = items.data;
          vm.totalItems = items.paging.total; //pagination data
          vm.itemsGot = vm.galleries.length;
        })
        .catch(function () {
          ALERT.confirm(vm.loadList);
        })
        .finally(function () {
          vm.loading = false;
          vm.listSelectorOptions.reset();
        });
    };

    vm.changePage = function () {
      vm.previousPage = (vm.currentPage - 1);
      vm.listSelectorOptions.reset(); //reset the selected items
      vm.loadList();
    };

    vm.getArtworksCount = function () {
      API
        .Artworks
        .query({page: 0, limit: 5, brand: vm.brand, public_items: !!vm.public})
        .then(function (response) {
          vm.artworksCount = response.paging.total;
        });
    };

    vm.editGalleryName = function (gallery) {
      gallery.editing = false;
      gallery.processing = true;

      API
        .Galleries
        .edit(gallery._id, {en: {name: gallery.newName}, ch: {name: gallery.newName}})
        .then(function () {
          Notify.infoSaved();
          gallery.en.name = gallery.newName;
        })
        .catch(function () {
          Notify.errorSaving();
        })
        .finally(function () {
          gallery.processing = false;
        });
    };

    vm.bulkDelete = function () {
      var completedReq = 0;
      var noOfErrors = 0;
      var noOfCalls = 0; //no of http requests to call

      vm.galleries.forEach(function(data){
        if (data.checked){
          ++noOfCalls;
        }
      });

      vm.galleries.forEach(function (data) {
        if(data.checked) {
          vm.deleteGallery(data, function (err, obj) {
            if (err)
              callBack(err);
            else
              callBack();
          });
        }
      });

      function callBack(err){
        if(!err){
          ++completedReq;
        }
        if(err){
          ++noOfErrors;
        }
        if(noOfCalls == (completedReq+noOfErrors)){
          vm.loadList();
          Notify.infoItemsDeleted(noOfCalls,completedReq);
        }
      }
    };

    vm.confirmDelete = function (type, gl) {
      Modal.open(
        'PersonalizeGalleriesListDeleteModalController',
        'modal',
        'app/modules/personalize/galleries/list/confirm_delete_modal/personalize.galleries.list.confirmDeleteModal.html',
        {
          MULTIPLE: function () {
            return type === 'bulk';
          }
        }
      )
        .result
        .then(function () {
          type == 'bulk' ? vm.bulkDelete(): vm.deleteGallery(gl);
        });
    };

    vm.changeStatus = function (gallery, status, cb) {
      gallery.processing = true;
      API
        .Galleries
        .edit(gallery._id, {'status': status})
        .then(function (obj) {
          gallery.status = status;
          if (!cb)
            Notify.infoSaved();
          else
            cb(null, obj);
        })
        .catch(function (err) {
          if (!cb)
            Notify.errorSaving();
          else
            cb(err, null);
        })
        .finally(function () {
          gallery.processing = false;
        });
    };

    vm.deleteGallery = function (gallery, cb) {
      gallery.processing = true;
      API
        .Galleries
        .delete(gallery._id)
        .then(function (obj) {
          vm.loadList();
          if (!cb) {
            Notify.infoDeleted();
          }
          else
            cb(null, obj);
        })
        .catch(function (err) {
          if (!cb)
            Notify.errorSaving();
          else
            cb(err, null)
        })
        .finally(function () {
          gallery.processing = false;
        });
    };

    vm.addTags = function (galleries, type) {
      Modal.open(
        'PersonalizeGalleriesListTagsModalController',
        'modal',
        'app/modules/personalize/galleries/list/add_tags_modal/personalize.galleries.list.addTagsModal.html',
        {
          Galleries: function () {
            return type == 'single' ? [galleries] : galleries;
          },
          Single: function () {
            return type == 'single';
          }
        }
      )
        .result
        .then(function () {
          vm.loadList();
        });
    };

    vm.createGallery = function () {
      Modal.open(
        'PersonalizeGalleriesListAddGalleryModalController',
        'modal',
        'app/modules/personalize/galleries/list/add_gallery_modal/personalize.galleries.list.addGalleryModal.html',
        {
          BRAND: function () { return vm.brand }
        },
        'md'
      )
        .result
        .then(function () {
          vm.loadList(Date.now());
        });
    };

    vm.changeStatusModal = function () {
      Modal.open(
        'PersonalizeGalleriesListChangeStatusModalController',
        'modal',
        'app/modules/personalize/galleries/list/change_status_modal/personalize.galleries.list.changeStatusModal.html',
        {
          LIST: function () {
            return vm.galleries;
          },
          FN: function () {
            return vm.changeStatus;
          }
        }
      )
        .result
        .then(function () {
          vm.loadList();
        });
    };

    $scope.dateChanged = function ( ){
      if (vm.filters.from.valueOf() < vm.filters.to.valueOf())
        vm.loadList();
    };

    vm.loadList();
    vm.getArtworksCount();
  }

})(angular.module('adminPanel.personalize.galleries.list.controller', []));
