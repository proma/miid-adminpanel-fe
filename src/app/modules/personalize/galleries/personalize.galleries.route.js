(function (app) {

  app.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('admin-panel.personalize.galleries', {
        url: '/galleries',
        abstract: true,
        template: '<div ui-view></div>'
      });
  }]);

})(angular.module('adminPanel.personalize.galleries.route', []));
