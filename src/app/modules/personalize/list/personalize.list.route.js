(function (app) {

  app.config(['$stateProvider', 'URLProvider', function ($stateProvider, URLProvider) {
    $stateProvider
      .state('admin-panel.personalize.list', {
        url: '/list?for',
        templateUrl: 'app/modules/personalize/list/personalize.list.html',
        controller: 'PersonalizeListController',
        controllerAs: 'vm',
        data: {
          permissions: {
            only: 'canSee.Personalize.BrandsListing',
            redirectTo: URLProvider.$get().RedirectURL
          }
        }
      });
  }]);

})(angular.module('adminPanel.personalize.list.route', []));
