'use strict';

fdescribe('Controllers', function () {
  fdescribe('PersonalizeListController', function () {

    var data = {
      data: [
        {
          name: 'SOFLO',
          id: 'BRAND1',
          status: 'ACTIVE'
        },
        {
          name: 'Brand2',
          id: 'testbrand',
          status: 'IN_ACTIVE'
        }
      ]
    };

    var
      Controller,
      $rootScope,
      ALERT,
      URL,
      $httpBackend,
      QINGSTOR,
      $scope;

    // load the dependencies module
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.services.alert'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.api'));
    beforeEach(module('adminPanel.personalize.list.controller'));

    beforeEach(inject(function ($injector) {
      URL = $injector.get('URL');
      $httpBackend = $injector.get('$httpBackend');

      var $controller = $injector.get('$controller');

      ALERT = $injector.get('ALERT');
      Controller = $controller('PersonalizeListController', {
        'API': $injector.get('API'),
        'QINGSTOR': $injector.get('QINGSTOR'),
        'ALERT': ALERT
      });
    }));

    fdescribe('MAD-54: Personalize Server: List Brand Galleries', function () {

      fit('should make brands empty first', function () {
        expect(Controller.brands.length).toBe(0);
      });

      fit('should be loading first', function () {
        expect(Controller.loading).toBeTruthy();
      });

      fit('should stop loading after load', function () {
        $httpBackend.expect('GET', URL.BRANDS + '?list_type=LIST_VALUES').respond(200, data);
        $httpBackend.flush();
        expect(Controller.loading).toBeFalsy();
      });

      fit('should call alert when error', function () {
        spyOn(ALERT, 'confirm');
        $httpBackend.expect('GET', URL.BRANDS + '?list_type=LIST_VALUES').respond(500, data);
        $httpBackend.flush();
        expect(ALERT.confirm).toHaveBeenCalled();
      });

      fit('should populate brands', function () {
        $httpBackend.expect('GET', URL.BRANDS + '?list_type=LIST_VALUES').respond(200, data);
        $httpBackend.flush();
        expect(Controller.brands.length).toBeGreaterThan(0);
      });

    });

  });

});
