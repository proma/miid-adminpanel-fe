(function (app) {

  app.controller('PersonalizeListController', PersonalizeListController);

  PersonalizeListController.$inject = ['API', 'ALERT', 'QINGSTOR', '$state', '$stateParams'];

  function PersonalizeListController (API, ALERT, QINGSTOR, $state, $stateParams) {
    var vm = this;

    vm.loading = false;
    vm.brands = [];
    vm.baseURL = QINGSTOR.base_url;
    vm.search = '';
    vm.status = '0';

    if (!$stateParams.for) {
      $state.go('admin-panel.not-found');
      return;
    } else {
      vm.for = $stateParams.for;
    }

    vm.loadList = function () {
      vm.loading = true;

      API
        .Brands
        .getAll(null, null, 'LIST_VALUES', vm.status === '0'? null: vm.status, vm.search)
        .then(function (data) {
          vm.brands = data.data;
          vm.brands.unshift({
            brand_id: 'MIID',
            name: 'MIID Public ' + vm.for,
            logo: '/img/icons/miid-logo.png',
            status: 'active',
            public: true
          });
        })
        .catch(function () {
          ALERT.confirm(vm.loadList);
        })
        .finally(function () {
          vm.loading = false;
        });
    };

    vm.loadList();
  }

})(angular.module('adminPanel.personalize.list.controller', []));
