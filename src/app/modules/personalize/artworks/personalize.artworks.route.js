(function (app) {

  app.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('admin-panel.personalize.artworks', {
        url: '/artworks',
        abstract: true,
        template: '<div ui-view></div>'
      });
  }]);

})(angular.module('adminPanel.personalize.artworks.route', []));
