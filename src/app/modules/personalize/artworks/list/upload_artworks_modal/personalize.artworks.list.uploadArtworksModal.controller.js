(function (app) {

  app.controller('PersonalizeArtworksListUploadArtworksModalController', PersonalizeArtworksListUploadArtworksModalController);

  PersonalizeArtworksListUploadArtworksModalController.$inject = ['$uibModalInstance', 'Upload', 'MIID_API', 'QINGSTOR', 'Notify', 'API', 'ALERT', 'BRAND', 'RolesService'];

  function PersonalizeArtworksListUploadArtworksModalController($uibModalInstance, Upload, MIID_API, QINGSTOR, Notify, API, ALERT, BRAND, RolesService) {
    var modal = this;

    modal.baseURL = QINGSTOR.base_url;

    modal.galleries = [];
    modal.tags = [];

    modal.creatingGallery = false;
    modal.loadingGalleries = false;
    modal.creatingArtwork = false;
    modal.enabled = false;

    modal.artwork = {
      id: null,
      name: '',
      altImage: '',
      price: 0
    };

    modal.loadGalleries = function () {
      modal.loadingGalleries = true;

      API
        .Galleries
        .query({list_type:'LIST_VALUES', brand: BRAND, public_items: !BRAND})
        .then(function (list) {
          modal.galleries = list.data;
        })
        .catch(function () {
          ALERT.confirm(modal.loadGalleries);
        })
        .finally(function () {
          modal.loadingGalleries = false;
        });
    };

    modal.createGallery = function () {
      var hasOne = _.filter(modal.galleries, function (gallery) { return gallery.new;} ).length > 0;
      if (!hasOne)
        modal.galleries.unshift({name: '', new: true});
    };

    modal.addTag = function () {
      if (modal.tags.indexOf(modal.tag.toLowerCase()) == -1) {
        modal.tags.push(modal.tag.toLowerCase());
        modal.tag = '';
      }
    };

    modal.delGallery = function (gallery) {
      modal.galleries = _.without(modal.galleries, gallery);
    };

    modal.saveGallery = function (gallery) {
      modal.creatingGallery = true;
      var postData = {en: {name: gallery.name}, ch: {name: gallery.name}};

      if (BRAND) {
        postData.brand = BRAND;
        postData.type = 'PRIVATE';
      }
      else if (RolesService.isUser('BRAND_ADMIN'))
        postData.type = 'PRIVATE';
      else
        postData.type = 'PUBLIC';
      API
        .Galleries
        .create(postData)
        .then(function () {
          Notify.infoSaved();
          modal.galleries = [];
          modal.loadGalleries();
        })
        .catch(function () {
          Notify.errorSaving();
        })
        .finally(function () {
          modal.creatingGallery = false;
        });
    };

    modal.createArtwork = function (file, type) {
      if (!file && !type) //bug in uploader
        return;

      modal.creatingArtwork = true;
      modal.uploading = true;
      modal.enabled = false;
      var name = '';

      var payload = {en: {}, ch: {}}, Factory = {}, tags = [], galleries = [];

      if (file && modal.artwork.id) { //enables re-upload if failed
        modal.uploadImage(file, modal.artwork.id);
        return;
      }

      if (file) { // if there is file, save as file name
        name = file.name.length > 20 ? file.name.substr(0, 20): file.name;
        payload.en.name = name ;
        payload.ch.name = name;
        Factory = function () { return API.Artworks.create(payload)};
      } else {
        //else get name from artwork obj
        galleries = _.chain(modal.galleries).where({selected: true}).pluck('_id').value();
        payload.en.name = modal.artwork.name;
        payload.ch.name = modal.artwork.name;

        if (modal.artwork.altImage != '') {
          payload.en.image_alt = modal.artwork.altImage;
          payload.ch.image_alt = modal.artwork.altImage;
        }
        if (modal.tags.length > 0) {
          payload.en.tags = modal.tags;
          payload.ch.tags = modal.tags;
        }
        if (galleries.length > 0)
          payload.galleries = galleries;

        Factory = function () { return API.Artworks.edit(modal.artwork.id, payload)};
      }

      if (BRAND){
        payload.brand = BRAND; //if super admin creating artworks
        payload.type = 'PRIVATE';
      }
      else if (RolesService.isUser('BRAND_ADMIN'))
        payload.type = 'PRIVATE';
      else
        payload.type = 'PUBLIC';

      Factory()
        .then(function (obj) {
          if (!file) {
            Notify.infoSaved();
            modal.enabled = true;
            modal.close(true);
          } else {
            modal.artwork.id = obj._id;
            modal.artwork.name = payload.en.name;
            modal.uploadImage(file, obj._id);
          }
        })
        .catch(function () {
          Notify.errorSaving();
          modal.uploading = false;
        })
        .finally(function () {
          modal.creatingArtwork = false;
        });
    };

    modal.uploadImage = function (file, id) {
      modal.uploading = true;
      modal.uploadProgress = 0;
      Upload.upload({
        url: MIID_API.base_url + '/media/upload/aw_bg/'+ id,
        data: {file: file}
      })
        .then(function (resp) {
          modal.enabled = true;

          Notify.infoImageSaved();
          modal.imgSrc = resp.data.path + '?' + Date.now();
          modal.imgArr = [modal.baseURL + modal.imgSrc];
        }, function (err) {
          Notify.errorSaving();
        }, function (evt) {
          modal.uploadProgress = parseInt(100.0 * evt.loaded / evt.total);
        })
        .finally(function () {
          modal.uploading = false;
          modal.loading = false;
          modal.creatingArtwork = false;
        });
    };

    modal.removeImage = function () {
      modal.imgSrc = null;
    };

    modal.removeTag = function (tag) {
      modal.tags = _.without(modal.tags, tag);
    };

    modal.close = function (close) {
      if (close)
        $uibModalInstance.close();
      else
        $uibModalInstance.dismiss();
    };

    modal.loadGalleries();
  }

})(angular.module('adminPanel.personalize.artworks.list.uploadArtworksModal.controller', []));
