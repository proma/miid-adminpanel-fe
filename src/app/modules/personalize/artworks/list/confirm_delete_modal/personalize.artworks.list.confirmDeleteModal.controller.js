(function (app) {

  app.controller('PersonalizeArtworksListDeleteModalController', PersonalizeArtworksListDeleteModalController);

  PersonalizeArtworksListDeleteModalController.$inject = ['$uibModalInstance', 'MULTIPLE'];

  function PersonalizeArtworksListDeleteModalController($uibModalInstance, MULTIPLE) {
    var modal = this;

    modal.multiple = MULTIPLE;

    modal.close = function () {
      $uibModalInstance.dismiss();
    };

    modal.confirm = function () {
      $uibModalInstance.close();
    };

  }

})(angular.module('adminPanel.personalize.artworks.list.confirmDeleteModal.controller', []));
