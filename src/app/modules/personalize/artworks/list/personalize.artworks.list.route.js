(function (app) {
  app.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('admin-panel.personalize.artworks.list', {
        url: '/list?gallery&brand&name&id&{public:bool}&total',
        controller: 'PersonalizeArtworksListController',
        controllerAs: 'vm',
        templateUrl: 'app/modules/personalize/artworks/list/personalize.artworks.list.html'
      });
  }]);
})(angular.module('adminPanel.personalize.artworks.list.route', []));
