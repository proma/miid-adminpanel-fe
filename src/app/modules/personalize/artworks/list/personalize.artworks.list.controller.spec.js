'use strict';

fdescribe('Controllers', function () {
  fdescribe('PersonalizeArtworksListController', function () {

    var data = {
      data: [
        {
          name: 'SOFLO',
          id: 'BRAND1',
          status: 'ACTIVE'
        },
        {
          name: 'Brand2',
          id: 'testbrand',
          status: 'IN_ACTIVE'
        }
      ]
    };

    var
      Controller,
      Modal,
      ALERT,
      Notify,
      URL,
      $httpBackend,
      QINGSTOR,
      $scope;

    // load the dependencies module
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.services'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.api'));
    beforeEach(module('ui.router'));
    beforeEach(module('adminPanel.personalize.artworks.list.controller'));

    beforeEach(inject(function ($injector) {
      URL = $injector.get('URL');
      $httpBackend = $injector.get('$httpBackend');

      var $controller = $injector.get('$controller');

      Modal = $injector.get('Modal');
      ALERT = $injector.get('ALERT');
      Notify = $injector.get('Notify');
      Controller = $controller('PersonalizeArtworksListController', {
        'Modal': Modal,
        'API': $injector.get('API'),
        'QINGSTOR': $injector.get('QINGSTOR'),
        'ALERT': ALERT,
        'Notify': Notify,
        '$stateParams': $injector.get('$stateParams'),
        '$state': $injector.get('$state'),
        '$scope': $injector.get('$rootScope').$new()
      });
    }));

  });

});
