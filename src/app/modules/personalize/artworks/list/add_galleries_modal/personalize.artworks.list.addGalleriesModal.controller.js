(function (app) {

  app.controller('PersonalizeArtworksListGalleriesModalController', PersonalizeArtworksListGalleriesModalController);

  PersonalizeArtworksListGalleriesModalController.$inject = ['$uibModalInstance', 'API', 'ALERT', 'Single', 'Artworks', 'Notify', 'BRAND', 'RolesService'];

  function PersonalizeArtworksListGalleriesModalController($uibModalInstance, API, ALERT, Single, Artworks, Notify, BRAND, RolesService) {
    var modal = this;

    modal.galleries = [];

    modal.loadingGalleries = false;
    modal.creatingGallery = false;
    modal.processing = false;

    modal.createGallery = function () {
      var hasOne = _.filter(modal.galleries, function (gallery) { return gallery.new;} ).length > 0;
      if (!hasOne)
        modal.galleries.unshift({name: '', new: true});
    };

    modal.saveGallery = function (gl) {
      modal.creatingGallery = true;
      var postData = {en: {name: gl.name}, ch: {name: gl.name}};
      if (BRAND) {
        postData.brand = BRAND;
        postData.type = 'PRIVATE';
      }
      else if (RolesService.isUser('BRAND_ADMIN'))
        postData.type = 'PRIVATE';
      else
        postData.type = 'PUBLIC';

      API
      .Galleries
      .create(postData)
      .then(function (data) {
        gl._id = data._id;
        delete gl.new;
        Notify.infoSaved();
      })
      .catch(function () {
        Notify.errorSaving();
      })
      .finally(function () {
        modal.creatingGallery = false;
      });
    };

    modal.delGallery = function (gl) {
      modal.galleries = _.without(modal.galleries, gl);
    };

    modal.loadGalleries = function () {
      modal.loadingGalleries = true;
      API
        .Galleries
        .query({list_type: 'LIST_VALUES', brand: BRAND, public_items: !BRAND})
        .then(function (data) {
          data = data.data.map(function (gl) {
            return {_id: gl._id, name: gl.en.name};
          });
          modal.galleries = modal.filterGalleries(data);
        })
        .catch(function () {
          ALERT.confirm(modal.loadGalleries);
        })
        .finally(function () {
          modal.loadingGalleries = false;
        });
    };

    modal.filterGalleries = function (galList) {
      var galleries = [], uniqueGls = [], galsLeft = [], exist;

      Artworks.forEach(function (aw) {
        galleries.push(_.map(aw.galleries, function (gl) { return {name: gl.en.name, _id: gl._id}}));
      });

      uniqueGls = _.intersectionObjects.apply(_, galleries);
      galleries = _.flatten(galleries);
      galleries = _.unique(galleries, function (item) { return item._id; });
      galsLeft = _.filter(galList, function(obj){ return !_.findWhere(galleries, obj); });

      galleries = galleries.map(function (item) {
        exist = _.filter(uniqueGls, function (gl) { return gl._id == item._id;}).length > 0;
        item.disabled = !exist && !Single;
        item.selected = true;
        item.included = true;
        return item;
      });

      return _.union(galleries, galsLeft);
    };

    modal.saveGalleries = function () {
      modal.processing = true;
      var artworks = [], galleries = [], allGalleries = [], delGal = [], completedReq = 0, noOfErrors = 0, noOfCalls = 0;

      allGalleries  = _.chain(modal.galleries).where({selected: true}).value();
      delGal = _.difference(_.pluck(_.where(modal.galleries, {included: true}), '_id'), _.pluck(allGalleries, '_id'));
      galleries = _.chain(allGalleries).where({disabled: false}).pluck('_id').value();

      artworks = _.map(Artworks, function (aw) {
        return {
          _id: aw._id,
          galleries: _.unique(_.difference(_.union(_.pluck(aw.galleries, '_id'), galleries), delGal))
        }
      });

      noOfCalls = artworks.length;
      artworks.forEach(function (aw) {
        API
          .Artworks
          .edit(aw._id, {galleries: aw.galleries})
          .then(function (d) {
            callBack();
            console.log(d);
          })
          .catch(function (err) {
            callBack(err);
          });
      });

      function callBack(err){
        if(!err){
          ++completedReq;
          console.log('completed', completedReq);
        }
        if(err){
          ++noOfErrors;
          console.log('errors', noOfErrors);
        }
        if(noOfCalls == (completedReq + noOfErrors)){
          console.log(noOfCalls, completedReq, noOfErrors);
          modal.close(true);
          modal.processing = false;
          artworks.length > 0 ? Notify.infoSavedItems(noOfCalls, completedReq) : Notify.infoSaved();
        }
      }
    };



    modal.close = function (close) {
      if (close)
        $uibModalInstance.close();
      else
        $uibModalInstance.dismiss();
    };

    modal.loadGalleries();
  }

})(angular.module('adminPanel.personalize.artworks.list.addGalleriesModal.controller', []));
