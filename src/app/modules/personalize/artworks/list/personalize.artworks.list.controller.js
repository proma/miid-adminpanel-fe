(function (app) {

  app.controller('PersonalizeArtworksListController', PersonalizeArtworksListController);

  PersonalizeArtworksListController.$inject = ['Modal', 'API', 'ALERT', 'Notify', '$stateParams', '$state', '$scope', 'QINGSTOR', '$localStorage', 'RolesService', '$translate'];

  function PersonalizeArtworksListController(Modal, API, ALERT, Notify, $stateParams, $state, $scope, QINGSTOR, $localStorage, RolesService, $translate) {
    var vm = this;

    vm.artworks = [];
    vm.loading = false;
    vm.galleryID = $stateParams.gallery;
    if (RolesService.isUser('BRAND_ADMIN')) {
      vm.brandID = $localStorage['userData']['brand']._id;
      vm.brandName = $localStorage['userData']['brand'].brand_id;
    } else {
      console.log($stateParams.public, !!$stateParams.public);
      vm.public = $stateParams.public || null;
      vm.brandID = $stateParams.brand || null;
      vm.brandName = $stateParams.id || null;
    }
    vm.totalArtworks = $stateParams.total || null;
    vm.galleryName = $stateParams.name;
    vm.galleries = [{_id: '-1', en: {name: 'Loading..'}}];
    vm.baseURL = QINGSTOR.base_url;

    vm.totalItems = 0; //TODO: need to make directive for these
    vm.itemsGot = 0;
    vm.previousPage = 0;
    vm.currentPage = 1;
    vm.itemsLimit = 10;
    vm.filters = {
      search: '',
      gallery: '-1',
      status: '0',
      from: moment((new Date("1/1/" + moment().year())).valueOf()),
      to: moment()
    };

    vm.bulkActions =
      [
        {title: 'Change Status', action: function () { return vm.changeStatusModal();}, divider: true},
        {title: 'Add to Galleries', action: function () { return vm.addGalleries(_.where(vm.artworks, {checked: true}, 'multiple'))}, divider: true},
        {title: 'Delete Artworks', action: function () { return vm.confirmDelete('bulk');}}
      ];
    vm.listSelectorOptions = {};


    vm.loadList = function (currentDate) {
      if (currentDate)
        vm.filters.to = moment(currentDate);
      vm.loading = true;
      API
        .Artworks
        .query({
          public_items: !!vm.public,
          page: vm.currentPage-1,
          limit: vm.itemsLimit,
          text: vm.filters.search,
          from: vm.filters.from.valueOf(),
          to: currentDate || vm.filters.to.valueOf(),
          gallery: vm.galleryID || (vm.filters.gallery != '-1' ? vm.filters.gallery : null),
          brand: vm.brandID,
          status: vm.filters.status == 0 ? null: vm.filters.status
        })
        .then(function (list) {
          vm.artworks = list.data.map(function (item) {
            item.created_at = new Date(item.created_at.substr(0, item.created_at.indexOf('T')));
            return item;
          });
          vm.totalItems = list.paging.total; //pagination data
          vm.itemsGot = vm.artworks.length;
        })
        .catch(function () {
          ALERT.confirm(vm.loadList);
        })
        .finally(function () {
          vm.loading = false;
          vm.listSelectorOptions.reset();
        });
    };

    vm.loadGalleries = function () {
      API
        .Galleries
        .query({
          list_type: 'LIST_VALUES',
          brand: vm.brandID,
          sort_by: 'name',
          sort_order: 'asc',
          public_items: !!vm.public
        })
        .then(function (list) {
          vm.galleries = list.data;
          vm.galleries.unshift({_id: '-1', en: {name: $translate.instant('words.gallery')||'Gallery'}});
          if (vm.galleryID){
            vm.filters.gallery = vm.galleryID;
            vm.selectedGalleryName = vm.galleryName;
          }
        })
        .catch(function () {
          vm.galleries = [{_id: '-1', en: {name: 'Loading Error..'}}];
          ALERT.confirm(vm.loadGalleries);
        });
    };

    vm.changeStatus = function (artwork, status, cb) {
      artwork.processing = true;
      API
        .Artworks
        .edit(artwork._id, {'status': status})
        .then(function (obj) {
          artwork.status = status;
          if (!cb)
            Notify.infoSaved();
          else
            cb(null, obj);
        })
        .catch(function (err) {
          if (!cb)
            Notify.errorSaving();
          else
            cb(err, null);
        })
        .finally(function () {
          artwork.processing = false;
        });
    };

    vm.changeStatusModal = function () {
      Modal
        .open(
          'PersonalizeArtworksListChangeStatusModalController',
          'modal',
          'app/modules/personalize/artworks/list/change_status_modal/personalize.artworks.list.changeStatusModal.html',
          {
            LIST: function () {
              return vm.artworks;
            },
            FN: function () {
              return vm.changeStatus;
            }
          }
        )
        .result
        .then(function () {
          console.log('asdfsadf');
          vm.loadList();
        });
    };

    vm.galleryFilter = function () {
      if (vm.galleryID && vm.filters.gallery == -1) {
        $state.go('admin-panel.personalize.artworks.list', {gallery: null, name: null});
        vm.selectedGalleryName = '';
      }
      else{
        vm.loadList();
        vm.selectedGalleryName = _.where(vm.galleries, {_id: vm.filters.gallery})[0].en.name;
      }
    };

    vm.editArtworkName = function (artwork) {
      artwork.editing = false;
      artwork.processing = true;

      var payload = {
        en: {name: artwork.newName, image_alt: artwork.newName},
        ch: {name: artwork.newName, image_alt: artwork.newName}
      };
      API
        .Artworks
        .edit(artwork._id, payload)
        .then(function () {
          Notify.infoSaved();
          artwork.en.name = artwork.newName;
        })
        .catch(function () {
          Notify.errorSaving();
        })
        .finally(function () {
          artwork.processing = false;
        });
    };

    vm.changePage = function () {
      vm.previousPage = (vm.currentPage - 1);
      vm.listSelectorOptions.reset(); //reset the selected items
      vm.loadList();
    };

    vm.bulkDelete = function () {
      var completedReq = 0;
      var noOfErrors = 0;
      var noOfCalls = 0; //no of http requests to call

      vm.artworks.forEach(function(data){
        if (data.checked){
          ++noOfCalls;
        }
      });

      vm.artworks.forEach(function (data) {
        if(data.checked) {
          vm.deleteArtwork(data, function (err, obj) {
            if (err)
              callBack(err);
            else
              callBack();
          });
        }
      });

      function callBack(err){
        if(!err){
          ++completedReq;
        }
        if(err){
          ++noOfErrors;
        }
        if(noOfCalls == (completedReq+noOfErrors)){
          vm.loadList();
          Notify.infoItemsDeleted(noOfCalls,completedReq);
        }
      }
    };

    vm.confirmDelete = function (type, aw) {
      Modal.open(
        'PersonalizeArtworksListDeleteModalController',
        'modal',
        'app/modules/personalize/artworks/list/confirm_delete_modal/personalize.artworks.list.confirmDeleteModal.html',
        {
          MULTIPLE: function () {
            return type === 'bulk';
          }
        }
      )
        .result
        .then(function () {
          type == 'bulk' ? vm.bulkDelete(): vm.deleteArtwork(aw);
        });
    };

    vm.deleteArtwork = function (artwork, cb) {
      artwork.processing = true;
      API
        .Artworks
        .delete(artwork._id)
        .then(function (obj) {
          vm.loadList();
          if (!cb) {
            Notify.infoDeleted();
          }
          else
            cb(null, obj);
        })
        .catch(function (err) {
          if (!cb)
            Notify.errorSaving();
          else
            cb(err, null)
        })
        .finally(function () {
          artwork.processing = false;
        });
    };

    vm.addGalleries = function (aw, type) {
      Modal.open(
        'PersonalizeArtworksListGalleriesModalController',
        'modal',
        'app/modules/personalize/artworks/list/add_galleries_modal/personalize.artworks.list.addGalleriesModal.html',
        {
          Single: function () {
            return type == 'single';
          },
          Artworks: function () {
            return type == 'single' ? [aw]: aw;
          },
          BRAND: function () { return vm.brandID }
        }
      )
        .result
        .then(function () {
          vm.loadList();
        });
    };

    vm.uploadArtworks = function () {
      Modal.open(
        'PersonalizeArtworksListUploadArtworksModalController',
        'modal',
        'app/modules/personalize/artworks/list/upload_artworks_modal/personalize.artworks.list.uploadArtworksModal.html',
        {
          BRAND: function () { return vm.brandID; }
        },
        'lg'
      )
        .result
        .then(function () {
          vm.loadList(Date.now());
        });
    };

    $scope.dateChanged = function ( ){
      if (vm.filters.from.valueOf() < vm.filters.to.valueOf())
        vm.loadList();
    };

    vm.loadList();
    vm.loadGalleries();
  }

})(angular.module('adminPanel.personalize.artworks.list.controller', []));
