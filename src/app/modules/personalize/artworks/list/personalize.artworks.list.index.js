(function () {
})(angular.module('adminPanel.personalize.artworks.list', [
  'adminPanel.personalize.artworks.list.controller',
  'adminPanel.personalize.artworks.list.route',
  'adminPanel.personalize.artworks.list.addGalleriesModal',
  'adminPanel.personalize.artworks.list.uploadArtworksModal',
  'adminPanel.personalize.artworks.list.changeStatusModal',
  'adminPanel.personalize.artworks.list.confirmDeleteModal'
]));
