(function(app){

  app.config(function($stateProvider){
    $stateProvider
      .state('admin-panel.support', {
        url: '/support',
        templateUrl: 'app/modules/support/support.html'
      });
  });


})(angular.module('adminPanel.support.route',[]));
