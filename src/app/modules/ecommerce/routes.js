(function(app) {

  app.config(['$stateProvider', function($stateProvider) {

    $stateProvider
      .state('admin-panel.ecommerce', {
        url: '/ecommerce',
        abstract: true,
        template: '<ui-view></ui-view>'
      });

  }]);

})(angular.module('adminPanel.ecommerce.routes',[]));
