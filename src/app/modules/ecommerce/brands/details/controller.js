(function (app) {
  'use strict';

  app.controller('BrandsDetailsController', BrandsDetailsController);

  BrandsDetailsController.$inject = ['$scope', 'Notify', 'API', 'Upload', 'QINGSTOR', '$stateParams', 'ALERT', 'Slug', 'MIID_API', 'MERCHANT_TYPE'];

  function BrandsDetailsController($scope, Notify, API, Upload, QINGSTOR, $stateParams, ALERT, Slug, MIID_API, MERCHANT_TYPE) {
    var vm = this;

    vm.oldBrand = vm.newBrand = {};
    vm.brandID = $stateParams.brandID;
    vm.logo = '';
    vm.loadingBrand = true;
    vm.dataInValid = true;
    vm.howManyReq = 0;
    vm.uploading = false;
    vm.uploadProgress = 0;
    vm.contactExist = {
      found: false,
      id: null
    };
    vm.assetURL = QINGSTOR.base_url;
    vm.imageArray = [ [] ];
    vm.merchantTypeList = [MERCHANT_TYPE.IP, MERCHANT_TYPE.BRAND, MERCHANT_TYPE.ARTIST, MERCHANT_TYPE.MULTIBRAND];

    vm.slugify = function () {
      vm.newBrand.basic.slug = Slug.slugify(vm.newBrand.basic.slug);
      vm.enableButton(); //check for button
    };

    vm.save = function () {
      vm.loadingBrand = true;

      //it means changes have been made and we need to save this data
      if(!angular.equals(vm.oldBrand.basic, vm.newBrand.basic)) {
        var payload = angular.copy(vm.newBrand.basic);
        delete payload.logo;
        vm.howManyReq++;
        API
          .Brands
          .edit(vm.brandID, payload)
          .then(function (obj) {
            if(obj.result === 'success') {
              vm.oldBrand.basic = angular.copy(vm.newBrand.basic);
              vm.enableButton();
              vm.showSuccess();
            }
          })
          .catch(function (err) {
            vm.howManyReq--;
            vm.loadingBrand = false;

            if (err.status === 400) {
              if (err.data && err.data.message) {
                Notify.error(err.data.message);
              } else {
                Notify.errorSaving();
              }
            } else {
              Notify.errorSaving();
            }
          });
      }

      //it means changes have been made and we need to save this data
      if(!angular.equals(vm.oldBrand.contact, vm.newBrand.contact)) {
        var payload = angular.copy(vm.newBrand);
        delete payload.basic;
        payload.contact = _.pick(payload.contact, _.identity);
        payload.contact.type = 'CUSTOMER_SERVICE';
        if(!payload.contact.person)
          payload.contact.person = 'default'; //so it's not required in GUI.
        vm.howManyReq++;

        if(vm.contactExist.found) {
          //it exist and we need to update it.
          API
            .Brands
            .editContact(vm.brandID, vm.contactExist.id, payload)
            .then(function (obj) {
              vm.oldBrand.contact = angular.copy(vm.newBrand.contact);
              vm.enableButton();
              vm.showSuccess();
            })
            .catch(function (err) {
              vm.howManyReq--;
              vm.loadingBrand = false;
              Notify.errorSaving();
            });
        }else {
          API
            .Brands
            .createContact(vm.brandID, payload)
            .then(function (obj) {
              vm.oldBrand.contact = angular.copy(vm.newBrand.contact);
              vm.contactExist.id = obj._id;
              //need to add obj.id to contact.id
              vm.enableButton();
              vm.showSuccess();
            })
            .catch(function () {
              vm.howManyReq--;
              vm.loadingBrand = false;
              Notify.errorSaving();
            });

        }

      }
    };

    vm.uploadImage = function (file) {
      vm.uploading = true;
      vm.uploadProgress = 0;
      var uploadUrl = MIID_API.base_url + '/media/upload/br_logo/' + vm.brandID;
      Upload.upload({
        url: uploadUrl,
        data: {'file' : file}
      }).then(function (resp) {
        vm.uploading = false;
        if(resp.data.path) {
          vm.logo = resp.data.path;
          vm.imageArray[0][0] = vm.assetURL + vm.logo;
          Notify.infoImageSaved();
          return;
        }
        Notify.errorSaving();

      }, function (err) {
        vm.uploading = false;
        Notify.errorSaving();

      }, function (evt) {
        vm.uploadProgress = parseInt(100.0 * evt.loaded / evt.total);
      });
    };

    vm.showSuccess = function () {
      vm.howManyReq--;
      if(vm.howManyReq === 0){
        Notify.infoSaved();
        vm.loadingBrand = false;
      }
    };

    vm.loadBrand = function () {
      vm.loadingBrand = true;

      API
        .Brands
        .get(vm.brandID)
        .then(function (obj) {
          vm.loadingBrand = false;

          vm.oldBrand = {
            basic: {
              brand_id: obj.brand_id,
              name: obj.name,
              slug: obj.slug,
              merchant_type: obj.merchant_type,
              commission: obj.commission
            },
            contact: {
              person: '',
              email: '',
              wechat: '',
              phone: '',
              qq: ''
            }
          };
          vm.logo = obj.logo;

          if(obj && obj.contacts.length > 0) { //safe check
            var contact = _.find(obj.contacts, function (contact) { return contact.type === 'CUSTOMER_SERVICE';});
            if(contact) {
              vm.contactExist.found = true; //enable's PUT instead of POST
              vm.contactExist.id = contact._id;

              vm.oldBrand.contact.email = contact.email;
              vm.oldBrand.contact.wechat = contact.wechat;
              vm.oldBrand.contact.phone = contact.phone;
              vm.oldBrand.contact.qq = contact.qq;
              vm.oldBrand.contact.person = contact.person;
            }
          }
          vm.newBrand = angular.copy(vm.oldBrand);
          vm.imageArray[0].push(vm.assetURL + vm.logo);
        })
        .catch(function (err) {
          vm.loadingBrand = false;
          ALERT.confirm(vm.loadBrand);
        });
    };

    vm.removeImage = function () {
      vm.logo = null;
    };

    vm.enableButton = function () {
      vm.dataInValid = angular.equals(vm.oldBrand, vm.newBrand);
    };

    //call loadBrand to populate

    vm.loadBrand();
  }

})(angular.module('adminPanel.ecommerce.brands.details.controller', []));
