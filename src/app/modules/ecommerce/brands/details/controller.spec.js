'use strict';

describe('Controllers', function () {
  describe('BrandsDetailsController', function () {

    var data = {
      name: 'BRAND',
      logo: 'some/url/to/somewhere',
      brand_id: 'BRAND_ID',
      slug: 'this-looks-like-slug',
      merchant_type: 'BRAND',
      contacts: [
        {
          _id: 'contact_id',
          person: 'name',
          qq: 'someqq',
          wechat: 'somewechat',
          email: 'this@test.com',
          phone: '12345',
          type: 'CUSTOMER_SERVICE'
        }
      ]
    };

    var
      Controller,
      $rootScope,
      URL,
      $httpBackend,
      $scope,
      $stateParams,
      ALERT,
      MIID,
      Upload,
      Notify,
      MERCHANT_TYPE;


    // load the dependencies module
    beforeEach(module('ui.bootstrap'));
    beforeEach(module('slugifier'));
    beforeEach(module('ngFileUpload'));
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.services.alert'));
    beforeEach(module('adminPanel.services.notifier'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.api'));
    beforeEach(module('adminPanel.ecommerce.brands.details.controller'));
    beforeEach(module('adminPanel.ecommerce.brands.constants'));

    beforeEach(inject(function ($injector) {
      $rootScope = $injector.get('$rootScope');
      URL = $injector.get('URL');

      $scope = $rootScope.$new();
      $httpBackend = $injector.get('$httpBackend');
      $stateParams = {
        brandID: '12345'
      };
      ALERT = $injector.get('ALERT');
      Notify = $injector.get('Notify');
      MIID = $injector.get('MIID_API');
      Upload = $injector.get('Upload');
      MERCHANT_TYPE = $injector.get('MERCHANT_TYPE');

      var $controller = $injector.get('$controller');

      Controller = $controller('BrandsDetailsController', {
        '$scope': $scope,
        'Notify': Notify,
        'API': $injector.get('API'),
        'Upload': Upload,
        'QINGSTOR': $injector.get('QINGSTOR'),
        '$stateParams': $stateParams,
        'ALERT': ALERT,
        'Slug': $injector.get('Slug'),
        'MIID_API': MIID,
        'MERCHANT_TYPE': MERCHANT_TYPE
      });

      data.url = URL.BRANDS + '/admin/12345';
      $httpBackend.when('GET', data.url).respond(200, data);
      Controller.loadBrand();
      $httpBackend.flush();
    }));

    describe('MBA-154: Brand details', function () {

      it('should get details ', function () {
        expect(Controller.brandID).toEqual('12345');
      });

      it('should have set the variables', function () {
        expect(Controller.newBrand).toEqual(Controller.oldBrand);
      });

      it('should slugify the brand slug', function () {
        Controller.newBrand.basic.slug = 'this should be slugified';
        Controller.slugify();
        expect(Controller.newBrand.basic.slug).toEqual('this-should-be-slugified');
      });

      it('it should call Upload api and set loading to true', function () {
        spyOn(Notify, 'infoImageSaved');
        $httpBackend.expect('POST',MIID.base_url + '/media/upload/br_logo/12345').respond(200, {path: 'some/path'});
        Controller.uploadImage('asdf');
        expect(Controller.uploading).toBeTruthy();//starts uploading
        $httpBackend.flush();
        expect(Controller.uploading).toBeFalsy(); //successfully uploads;
        expect(Notify.infoImageSaved).toHaveBeenCalled();
      });

      it('it should show error toastr on error uploading', function () {
        spyOn(Notify, 'errorSaving');
        $httpBackend.expect('POST',MIID.base_url + '/media/upload/br_logo/12345').respond(500, {path: 'some/path'});
        Controller.uploadImage('asdf'); //faking file :P
        expect(Controller.uploading).toBeTruthy();//starts uploading
        $httpBackend.flush();
        expect(Controller.uploading).toBeFalsy(); //fails to uploads;
        expect(Notify.errorSaving).toHaveBeenCalled();
      });

      it('should show success message when all requests have been flushed', function () {
        Controller.howManyReq = 1;
        spyOn(Notify, 'infoSaved');
        Controller.showSuccess();
        expect(Notify.infoSaved).toHaveBeenCalled();
        expect(Controller.loadingBrand).toBeFalsy();
      });

      it('should remove image', function () {
        Controller.removeImage();
        expect(Controller.logo).toEqual(null);
      });

      it('should enable the button if changings have been made', function () {
        Controller.newBrand.basic.name = 'changed';
        expect(Controller.dataInValid).toBeTruthy();
        Controller.enableButton();
        expect(Controller.dataInValid).toBeFalsy();
      });

      it('should send request to PUT brand when brand info is edited', function () {
        $httpBackend.expect('PUT', URL.BRANDS + '/12345').respond(200, {result: 'success'});
        Controller.newBrand.basic.name = 'changed to trigger';
        Controller.save();
        $httpBackend.flush();
        expect(Controller.newBrand.basic).toEqual(Controller.oldBrand.basic);
      });

      it('should send request to PUT contact/brand if contact exist', function () {
        $httpBackend.expect('PUT', URL.BRANDS + '/12345/contacts/contact_id').respond(200, {result: 'success'});
        Controller.newBrand.contact.person = 'changed to trigger';
        Controller.save();
        $httpBackend.flush();
        expect(Controller.newBrand.contact).toEqual(Controller.oldBrand.contact);
      });

      it('should send request to POST contact/brand if contact doesnt exist', function () {
        $httpBackend.expect('POST', URL.BRANDS + '/12345/contacts').respond(200, {_id: 'id_got'});
        Controller.contactExist.found = false;
        Controller.newBrand.contact.person = 'changed to trigger';
        Controller.save();
        $httpBackend.flush();
        expect(Controller.contactExist.id).toEqual('id_got');
      });

    });


  });

});
