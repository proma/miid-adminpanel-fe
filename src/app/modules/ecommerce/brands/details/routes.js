(function(app){

  app.config(['$stateProvider', 'URLProvider', function($stateProvider, URLProvider){

    $stateProvider
      .state('admin-panel.ecommerce.brands.details',{
        url: '/details/:brandID',
        templateUrl: 'app/modules/ecommerce/brands/details/details.html',
        controller: 'BrandsDetailsController',
        controllerAs: 'vm',
        data: {
          permissions: {
            only: 'canSee.Brands.Listing',
            redirectTo: URLProvider.$get().RedirectURL 
          }
        }
      });

  }]);

})(angular.module('adminPanel.ecommerce.brands.details.routes',[]));
