angular.module("adminPanel.ecommerce.brands.constants", [])

.constant("MERCHANT_TYPE", {
    IP: 'IP',
    BRAND: 'BRAND',
    ARTIST: 'ARTIST',
    MULTIBRAND: 'MULTIBRAND'
  })
;
