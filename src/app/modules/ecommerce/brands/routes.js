(function(app) {

  app.config(['$stateProvider', 'URLProvider', function($stateProvider, URLProvider) {

    $stateProvider
      .state('admin-panel.ecommerce.brands', {
        url: '/brand',
        abstract: true,
        template: '<ui-view></ui-view>',
        data: {
          permissions: {
            only: 'canSee.Brands.Listing',
            redirectTo: URLProvider.$get().RedirectURL
          }
        }
      });

  }]);

})(angular.module('adminPanel.ecommerce.brands.routes',[]));
