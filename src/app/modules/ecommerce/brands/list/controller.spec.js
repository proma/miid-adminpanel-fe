'use strict';

describe('Controllers', function () {
  describe('BrandsController', function () {

    var data = {
      data: [
        {
          name: 'SOFLO',
          id: 'BRAND1',
          status: 'ACTIVE'
        },
        {
          name: 'Brand2',
          id: 'testbrand',
          status: 'IN_ACTIVE'
        }
      ],
      paging: {
        total: 5
      }
    };

    var
      BrandsController,
      $rootScope,
      URL,
      $httpBackend,
      QINGSTOR,
      $scope,
      ALERT,
      Modal,
      Notify,
      Spy = {test: function () {}};


    // load the dependencies module
    beforeEach(module('adminPanel.services.modal'));
    beforeEach(module('adminPanel.services.notifier'));
    beforeEach(module('ui.bootstrap'));
    beforeEach(module('slugifier'));
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.services.alert'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.api'));
    beforeEach(module('ui.router'));
    beforeEach(module('adminPanel.ecommerce.brands.list.controller'));

    beforeEach(inject(function ($injector) {
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      $httpBackend = $injector.get('$httpBackend');
      URL = $injector.get('URL');
      var $controller = $injector.get('$controller');
      QINGSTOR = $injector.get('QINGSTOR');
      Notify = $injector.get('Notify');
      ALERT = $injector.get('ALERT');
      Modal = {open: function () { return {result: { finally: function (){ Spy.test()}}}}};
      BrandsController = $controller('BrandsController', {
        '$scope': $scope,
        'Notify': Notify,
        'Modal': Modal,
        'API': $injector.get('API'),
        'ALERT': ALERT,
        'QINGSTOR': QINGSTOR,
        '$timeout': $injector.get('$timeout'),
        'Slug': $injector.get('Slug'),
        'MIID_API':$injector.get('MIID_API'),
        '$state': $injector.get('$state'),
      });
    }));

    describe('MA-117: List Brands & Pagination', function () {

      it('should make brands empty first', function () {
        expect(BrandsController.brands.length).toBe(0);
      });

      it('should be loading first', function () {
        expect(BrandsController.loading).toBeTruthy();
      });

      it('should stop loading after load', function () {
        $httpBackend.expect('GET', URL.BRANDS + '?limit=10&page=0').respond(200, data);
        $httpBackend.flush();
        expect(BrandsController.loading).toBeFalsy();
      });

      it('should have set the base url', function () {
        expect(BrandsController.base_url).toBe(QINGSTOR.base_url);
      });

      it('should set items limit to 10', function () {
        expect(BrandsController.itemsLimit).toBe(10);
      });

      it('should call alert when error', function () {
        spyOn(ALERT, 'confirm');
        $httpBackend.expect('GET', URL.BRANDS + '?limit=10&page=0').respond(500, data);
        $httpBackend.flush();
        BrandsController.listBrands();

        expect(ALERT.confirm).toHaveBeenCalled();
      });

      it('should populate brands', function () {
        $httpBackend.expect('GET', URL.BRANDS + '?limit=10&page=0').respond(200, data);
        $httpBackend.flush();
        expect(BrandsController.brands.length).toBeGreaterThan(0);
      });

      it('should set totalItems to 5', function () {
        $httpBackend.expect('GET', URL.BRANDS + '?limit=10&page=0').respond(200, data);
        $httpBackend.flush();
        expect(BrandsController.totalItems).toBe(5);
      });

      it('should set itemsGot to 2', function () {
        $httpBackend.expect('GET', URL.BRANDS + '?limit=10&page=0').respond(200, data);
        $httpBackend.flush();
        expect(BrandsController.itemsGot).toBe(2);
      });

      it('should query page 2 when called changePage()', function () {
        spyOn(BrandsController, 'listBrands');
        BrandsController.currentPage = 2; // page is index based so in query page 1 will be called
        $httpBackend.when('GET', URL.BRANDS + '?limit=10&page=0').respond(200, data);
        $httpBackend.when('GET', URL.BRANDS + '?limit=10&page=1').respond(200, data);

        BrandsController.changePage();

        $httpBackend.flush();

        expect(BrandsController.listBrands).toHaveBeenCalled();
      });

    });

    describe('MBA-192: Update brand status', function () {

      it("Should open update status modal", function () {
        spyOn(Spy, 'test');

        BrandsController.changeStatus();

        expect(Spy.test).toHaveBeenCalled();

      });

    });

    describe('MBA-191: Add brand ', function () {

      it("Should open add brand modal", function () {
        spyOn(Spy, 'test');

        BrandsController.addBrand();

        expect(Spy.test).toHaveBeenCalled();

      });

    });

  });

});
