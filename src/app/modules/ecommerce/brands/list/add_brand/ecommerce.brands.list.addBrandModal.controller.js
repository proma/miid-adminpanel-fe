(function (app) {

  app.controller('BrandsListAddBrandModalController', BrandsListAddBrandModalController);

  BrandsListAddBrandModalController.$inject = ['Controller', 'Slug', 'MIID_API', 'API', '$uibModalInstance', '$timeout', 'MERCHANT_TYPE'];

  function BrandsListAddBrandModalController (Controller, Slug, MIID_API, API, $uibModalInstance, $timeout, MERCHANT_TYPE) {
    var modal = this;

    modal.title = 'Change Brand Status';
    modal.Controller = Controller;
    modal.Slug = Slug;
    modal.success = false;
    modal.error = false;
    modal.message = '';
    modal.addingBrand = false;
    modal.timeOut = null;
    modal.brandIdMessage = "Brand ID cannot be changed once products are added to this brand";
    modal.brand = {name: '', brand_id:'', slug:'', email:'', password:'', merchant_type: MERCHANT_TYPE.BRAND};
    modal.brandBaseUrl = MIID_API.base_url + "/brands/";
    modal.merchantTypeList = [MERCHANT_TYPE.IP, MERCHANT_TYPE.BRAND, MERCHANT_TYPE.ARTIST, MERCHANT_TYPE.MULTIBRAND];

    modal.setSlug = function () {
      modal.brand.slug = Slug.slugify(modal.brand.name);
    };

    modal.Slugify = function (input) {
      return Slug.slugify(input);
    };

    modal.close = function () {
      $uibModalInstance.dismiss('cancel');
      // clear timeout
      if(modal.timeOut != null){
        $timeout.cancel(modal.timeOut);
      }
    };

    modal.submit = function (valid) {

      function callBack(message, isError){
        if(isError === true){
          modal.error = true;
          modal.success = false;
        } else {
          modal.success = true;
          modal.error = false;
          modal.timeOut = $timeout(modal.close,2000);
        }

        modal.message = message;
        modal.addingBrand = false;
      }

      if(valid) {
        modal.addingBrand = true;

        API.
        Brands
          .save(modal.brand)
          .then(function (response) {
            callBack("New Merchant with Merchant ID (" + modal.brand.brand_id + ") is added successfully", false);
          })
          .catch(function (response) {
            if (response.status === 400) {
              if (response.data && response.data.message) {
                callBack(response.data.message, true);
              } else {
                callBack("Something goes wrong while creating new merchant", true);
              }
            } else {
              callBack("Something goes wrong while creating new merchant", true);
            }
          });
      }
    }

  }

})(angular.module('adminPanel.ecommerce.brands.list.addBrandModal.controller', []));
