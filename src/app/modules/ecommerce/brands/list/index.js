(function () {

})(angular.module('adminPanel.ecommerce.brands.list',[
  'adminPanel.ecommerce.brands.constants',
  'adminPanel.ecommerce.brands.list.routes',
  'adminPanel.ecommerce.brands.list.controller',
  'adminPanel.ecommerce.brands.list.addBrandModal',
  'adminPanel.ecommerce.brands.list.changeStatusModal'
]));
