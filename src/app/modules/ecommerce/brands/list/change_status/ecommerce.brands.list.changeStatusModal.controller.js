(function (app) {

  app.controller('BrandsListChangeStatusModalController', BrandsListChangeStatusModalController);

  BrandsListChangeStatusModalController.$inject = ['Status', 'CurrentStatus', 'BrandID', 'API', 'Notify', '$uibModalInstance'];

  function BrandsListChangeStatusModalController (Status, CurrentStatus, BrandID, API, Notify, $uibModalInstance) {
    var modal = this;

    modal.title = 'Change Brand Status';
    modal.statuses = Status;
    modal.selectedStatus = CurrentStatus;
    modal.changingStatus = false;

    modal.close = function () {
      $uibModalInstance.dismiss('cancel');
    };

    modal.submit = function () {
      modal.changingStatus = true; //disables button
      API.Brands.updateStatus(BrandID, modal.selectedStatus)
        .then(function () {
          callBack();
        })
        .catch(function (err) {
          callBack(err);
        });

      function callBack(err) {
        modal.changingStatus = false;
        if (err) {
          Notify.errorItemStatus();
        } else {
          Notify.infoChangeStatus(modal.selectedStatus);
        }
        $uibModalInstance.close({
          success: true
        });
      }

    }
  }

})(angular.module('adminPanel.ecommerce.brands.list.changeStatusModal.controller', []));
