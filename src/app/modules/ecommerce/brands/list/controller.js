(function (app) {
  'use strict';

  app.controller('BrandsController', BrandsController);

  BrandsController.$inject = ['$scope', 'Notify', 'Modal', 'API', 'ALERT', 'QINGSTOR', '$timeout', 'Slug', 'MIID_API', '$state'];

  function BrandsController($scope, Notify, Modal, API, ALERT, QINGSTOR, $timeout, Slug, MIID_API, $state) {

    var vm = this;
    vm.status = [
      {
        name: 'Active',
        value: 'ACTIVE',
        class: 'label-production'
      },
      {
        name: 'Inactive',
        value: 'IN_ACTIVE',
        class: 'label-error'
      }
    ];
    vm.loading = true;
    vm.brands = [];
    vm.totalItems = 0;
    vm.itemsGot = 0;
    vm.previousPage = 0;
    vm.currentPage = 1;
    vm.itemsLimit = 10;
    vm.base_url = QINGSTOR.base_url;

    vm.listBrands = function () {
      vm.loading = true;
      API.Brands
        .getAll(vm.itemsLimit, vm.currentPage - 1)
        .then(function (data) {
          vm.brands = data.data;
          vm.itemsGot = vm.brands.length;
          vm.totalItems = data.paging.total;
        })
        .catch(function (err) {
          ALERT.confirm(vm.listBrands);
        })
        .finally(function () {
          vm.loading = false;
        });
    };

    //change page
    vm.changePage = function () {
      vm.previousPage = (vm.currentPage - 1);
      vm.loading = true;
      vm.listBrands();
    };


    // Change brand status modal
    vm.changeStatus = function (brandId, currentStatus) {
      Modal.open(
          'BrandsListChangeStatusModalController',
          'modal',
          'app/modules/ecommerce/brands/list/change_status/ecommerce.brands.list.changeStatusModal.html',
          {
            Status: function () {
              return vm.status;
            },
            CurrentStatus: function () {
              return currentStatus;
            },
            BrandID: function () {
              return brandId;
            }
          }
        )
        .result.finally(function () {
          vm.listBrands();
        });
    }; // end change status function

    // Add brand status modal
    vm.addBrand = function () {
      Modal.open(
        'BrandsListAddBrandModalController',
        'modal',
        'app/modules/ecommerce/brands/list/add_brand/ecommerce.brands.list.addBrandModal.html',
        {
          Controller: function () {
            return vm;
          },
          Slug : function () {
            return Slug;
          },
          MIID_API : function () {
            return MIID_API;
          }
        },
        'add-brand-modal'
      )
        .result.finally(function () {
          vm.listBrands();
        });
    }; // end add brand function

    //navigate to details
    vm.navigateToDetails = function(brand) {
      $state.go('admin-panel.ecommerce.brands.details', {'brandID': brand._id});
    };

    vm.listBrands();
  }
})(angular.module('adminPanel.ecommerce.brands.list.controller', []));
