(function(app) {

  app.config(['$stateProvider', 'URLProvider', function($stateProvider, URLProvider) {

    $stateProvider
      .state('admin-panel.ecommerce.brands.list', {
        url: '/list',
        templateUrl: 'app/modules/ecommerce/brands/list/brands.html',
        controller: 'BrandsController',
        controllerAs: 'vm',
        data: {
          permissions: {
            only: 'canSee.Brands.Listing',
            redirectTo: URLProvider.$get().RedirectURL
          }
        }
      });

  }]);

})(angular.module('adminPanel.ecommerce.brands.list.routes',[]));
