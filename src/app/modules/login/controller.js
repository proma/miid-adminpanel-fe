(function (app) {
  'use strict';

  app.controller('LoginController', ['$scope', '$state', '$localStorage', 'APIService', 'RolesService', '$translate', LoginController]);

  /** @ngInject */
  function LoginController($scope, $state, $localStorage, APIService, RolesService, $translate) {
    var vm = this;
    vm.email = "";
    vm.password = "";

    vm.changeLang = function (lang) {
      $translate.use(lang);
    };

    vm.doLogin = function () {

      var data = {
        username: $scope.email,
        password: $scope.password,
        type: "ADMIN",
        id_type: "email"
      };
      var $loader = angular.element('.loader');
      var $loginError = angular.element('.login-error');
      $loader.removeClass('hidden');
      $loginError.addClass('hidden');

      var requestOptions = {
        method: 'POST',
        path: '/auth/login',
        data: data,
        skipAuth: true
      };
      APIService.sendRequest(requestOptions)
        .then(function (data) {
          $loader.addClass('hidden');
          $localStorage.jwt = data['miidapi-jwt'];
          $localStorage.refreshToken = data['refresh-jwt'];
          $localStorage.userData = data['userData'];
          RolesService.flush(); //removes all permissions
          RolesService.createPermissions(data.userData.type);

          $state.go('admin-panel.cust_mgmt.orders.list');
        })
        .catch(function (error) {
          var status = error.statusCode;
          if (status == 404) {
            vm.errorMessage = "We couldn't find any user with that email";
          } else if (status == 401) {
            vm.errorMessage = "Username or password are incorrect. Contact MIID support if need help to reset it";
          } else if (status == 500) {
            vm.errorMessage = "There was an error while processing your request, please try again later or contact admin";
          }
          $loader.addClass('hidden');
          $loginError.removeClass('hidden');
        });
    }
  }
})(angular.module('adminPanel.login.controller',[]));
