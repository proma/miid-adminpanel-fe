(function(app){

  app.config(function($stateProvider){
    $stateProvider
      .state('login',{
        url: '/login',
        templateUrl: 'app/modules/login/login.html',
        controller: 'LoginController',
        controllerAs: 'login'
      });
  });


})(angular.module('adminPanel.login.route',[]));
