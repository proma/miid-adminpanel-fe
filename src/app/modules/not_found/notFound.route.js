(function(app){

  app.config(function($stateProvider){
    $stateProvider
      .state('admin-panel.not-found', {
        url: '/not-found',
        templateUrl: 'app/modules/not_found/notFound.html'
      });
  });


})(angular.module('adminPanel.notFound.route',[]));
