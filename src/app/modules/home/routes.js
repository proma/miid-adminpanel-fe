(function(app){

  app.config(function($stateProvider){
    $stateProvider
      .state('admin-panel.home',{
        url: '/',
        templateUrl: 'app/modules/home/main.html',
        controller: 'HomeController',
        controllerAs: 'vm'
      });
  });


})(angular.module('adminPanel.home.route',[]));
