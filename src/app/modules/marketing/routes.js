(function(app){
  app.config(function($stateProvider){

    $stateProvider
      .state('admin-panel.marketing',{
        url: '/marketing',
        abstract: true,
        template: '<ui-view/>'
      });

  });

})(angular.module('adminPanel.marketing.routes',[]));
