(function(app){

  app.config(function($stateProvider){

    $stateProvider
      .state('admin-panel.marketing.coupons.list',{
        url: '/list',
        templateUrl: 'app/modules/marketing/coupons/list/list.html',
        controller: 'CouponsListController',
        controllerAs: 'vm'
      });

  });

})(angular.module('adminPanel.marketing.coupons.list.route',[]));
