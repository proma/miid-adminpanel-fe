(function (app) {

  app.controller('CouponListChangeStatusModalController', CouponListChangeStatusModalController);

  CouponListChangeStatusModalController.$inject = ['$uibModalInstance'];

  function CouponListChangeStatusModalController ($uibModalInstance) {
    var modal = this;

    modal.selectedStatus = 'ACTIVE';

    modal.close = function () {
      $uibModalInstance.dismiss('cancel');
    };

    modal.submit = function () {
      $uibModalInstance.close({
        status: modal.selectedStatus
      });
    }
  }

})(angular.module('adminPanel.marketing.coupons.list.changeStatusModal.controller', []));
