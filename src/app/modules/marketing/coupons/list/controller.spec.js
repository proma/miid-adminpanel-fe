'use strict';

describe('Controllers', function () {
  describe('CouponsListController', function () {

    var data = {
      "paging": {"total": 3},
      "data": [{
        "_id": "571f50d9d5569128031874b2",
        "name": "coupon 1",
        "code": "123456",
        "discount": {
          apply_on: 'SPECIFIC_PRODUCT',
          value: 10,
          type: 'PERCENTAGE'
        },
        "status": "ACTIVE",
      }, {
        "_id": "571f51b6d5569128031874b4",
        "name": "coupon 2",
        "code": "223456",
        "discount": {
          apply_on: 'SPECIFIC_PRODUCT',
          value: 20,
          type: 'PERCENTAGE'
        },
        "status": "ACTIVE",
      }]
    };

    var
      CouponsListController,
      $rootScope,
      URL,
      $httpBackend,
      RolesService,
      ALERT,
      $scope,
      Modal,
      Notify;


    // load the dependencies module
    beforeEach(module('adminPanel.services.notifier'));
    beforeEach(module('adminPanel.services.roles'));
    beforeEach(module('adminPanel.services.modal'));
    beforeEach(module('ui.bootstrap'));
    beforeEach(module('ngResource'));
    beforeEach(module('oitozero.ngSweetAlert'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.services.alert'));
    beforeEach(module('adminPanel.api'));
    beforeEach(module('adminPanel.marketing.coupons.list.controller'));

    beforeEach(inject(function ($injector) {
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      $httpBackend = $injector.get('$httpBackend');
      URL = $injector.get('URL');
      var $controller = $injector.get('$controller');
      Notify = $injector.get('Notify');
      RolesService = $injector.get('RolesService');
      spyOn(RolesService, 'checkPermission');
      Modal = $injector.get('$uibModal');
      ALERT = $injector.get('ALERT');

      CouponsListController = $controller('CouponsListController', {
        'Notify': Notify,
        '$scope': $scope,
        'Modal': Modal,
        'API': $injector.get('API'),
        'ALERT': $injector.get('ALERT'),
        'RoleService': RolesService
      });

      CouponsListController.listSelectorOptions.reset = function() {}; //fake coz of directive
      data.url = URL.COUPONS + '?limit=10&page=0';
      data.url2 = URL.COUPONS + '?limit=10&page=1';
      data.delURL = URL.COUPONS + '/571f50d9d5569128031874b2';
    }));

    describe('MA-317: List Coupons and change status', function () {

      it('should be empty first', function () {
        expect(CouponsListController.coupons.length).toBe(0);
      });

      it('should be loading first', function () {
        expect(CouponsListController.loadingCoupons).toBeTruthy();
      });

      it('should stop loading after load', function () {
        $httpBackend.expect('GET', data.url).respond(200, data);
        $httpBackend.flush();
        expect(CouponsListController.loadingCoupons).toBeFalsy();
      });

      it('should set items limit to 10', function () {
        expect(CouponsListController.itemsLimit).toBe(10);
      });

      it('should call listCoupons() and populate the chartData', function () {
        $httpBackend.expect('GET', data.url).respond(200, data);
        $httpBackend.flush();
        $scope.$digest();
        expect(CouponsListController.coupons[0].chartData).toBeDefined();
        expect(CouponsListController.coupons[1].chartData).toBeDefined();
      });

      it('should call alert when error', function () {
        spyOn(ALERT, 'confirm');
        $httpBackend.expect('GET', data.url).respond(500, data);
        $httpBackend.flush();
        CouponsListController.listCoupons();
        expect(ALERT.confirm).toHaveBeenCalled();
      });

      it('should populate coupons', function () {
        $httpBackend.expect('GET', data.url).respond(200, data);
        $httpBackend.flush();
        expect(CouponsListController.coupons.length).toEqual(2);
      });

      it('should set totalItems to 2', function () {
        $httpBackend.expect('GET', data.url).respond(200, data);
        $httpBackend.flush();
        expect(CouponsListController.totalItems).toBe(3);
      });

      it('should set itemsGot to 2', function () {
        $httpBackend.expect('GET', data.url).respond(200, data);
        $httpBackend.flush();
        expect(CouponsListController.itemsGot).toBe(2);
      });

      it('should call listSelectorOptions.reset() when called changePage()', function () {
        spyOn(CouponsListController.listSelectorOptions, 'reset');
        CouponsListController.currentPage = 2; // page is index based so in query page 1 will be called
        $httpBackend.when('GET', data.url).respond(200, data);
        $httpBackend.when('GET', data.url2).respond(200, data);

        CouponsListController.changePage();

        $httpBackend.flush();

        expect(CouponsListController.listSelectorOptions.reset).toHaveBeenCalled();
      });


      it('should call delete api and alert when called deleteCoupon', function () {
        $httpBackend.expect('GET', data.url).respond(200, data);
        $httpBackend.flush();
        CouponsListController.coupons[0].checked = true; //manually checking
        spyOn(ALERT, 'confirm');
        CouponsListController.deleteCoupon();
        expect(ALERT.confirm).toHaveBeenCalled();
      });

      it('should set chartData on coupons', function () {
        $httpBackend.expect('GET', data.url).respond(200, data);
        $httpBackend.flush();
        expect(CouponsListController.coupons[0].chartData[0]).toBe(10);
        expect(CouponsListController.coupons[1].chartData[0]).toBe(20);
      });

    });

  });

});
