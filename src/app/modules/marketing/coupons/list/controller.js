(function(app){

  app.controller('CouponsListController', CouponsListController);

  CouponsListController.$inject = ['API', 'ALERT', 'Notify', 'Modal', 'RolesService'];

  function CouponsListController(API, ALERT, Notify, Modal, RolesService){
    var vm = this;

    vm.listSelectorOptions = {};

    vm.coupons = [];
    vm.loadingCoupons = true;

    vm.totalItems = 0; //TODO: need to make directive for these
    vm.itemsGot = 0;
    vm.previousPage = 0;
    vm.currentPage = 1;
    vm.itemsLimit = 10;

    vm.bulkActions = [
      {
        title: 'Change Status',
        action: function () {
          vm.changeStatus();
        },
        divider: true
      },
      {
        title: 'Delete coupons',
        action: function () {
          vm.deleteCoupon();
        }
      }
    ];

    if (!RolesService.checkPermission('canSee.Coupons.DeleteButton')) {
      //we delete the delete button configuration from array
      vm.bulkActions = [vm.bulkActions[0]];
      vm.bulkActions[0].divider = false;
    }

    vm.listCoupons = function (isFilter) {
      vm.loadingCoupons = true;

      if(isFilter) { //reset if filter applied.
        vm.currentPage = 1;
        vm.previousPage = 0;
      }


      API
        .Coupons
        .query({
          page: (vm.currentPage-1),
          limit: vm.itemsLimit
        })
        .then(function (data) {
          vm.listSelectorOptions.reset();
          vm.loadingCoupons = false;

          vm.coupons = data.data; //coupons array
          vm.coupons.forEach(function (coupon) {
            coupon.chartData = [];
            coupon.chartData.push(coupon.discount.value);

            if( coupon.discount.type === 'PERCENTAGE')
              coupon.chartData.push(100-coupon.discount.value);
            else
              coupon.chartData.push(coupon.discount.value + 100);
          });

          vm.totalItems = data.paging.total; //pagination data
          vm.itemsGot = vm.coupons.length;
        })
        .catch(function (err) {
          vm.loadingCoupons = false;
          ALERT.confirm(function () {
            return vm.listCoupons(isFilter);
          });
        });

    };


    vm.changeStatus = function(){
      Modal.open(
        'CouponListChangeStatusModalController',
        'modal',
        'app/modules/marketing/coupons/list/change_status/marketing.coupons.list.changeStatusModal.html'
      )
      .result.then(function (result) {
        ALERT.confirm(function () {
          // change status of all the objects.
          var completedReq = 0;
          var noOfErrors = 0;
          var noOfCalls = 0; //no of http requests to call

          //checking if user has selected coupons?

          vm.coupons.forEach(function(data){
            if (data.checked){
              if (result.status != data.status)
                ++noOfCalls;
            }
          });

          //if noOfCalls 0 then return

          if(noOfCalls === 0){
            Notify.infoSameItemStatus();
            return;
          }

          vm.coupons.forEach(function(data){
            //if status of selected coupon == selected status, we save http request.
            if(data.checked)
              if(result.status != data.status){
                //we send request to change status.
                data.performingOperation = true;
                var obj = {
                  status: (result.status === 'INACTIVE' ? 'DISABLED': result.status)
                };
                API.
                  Coupons
                  .edit(data._id, obj)
                  .then(function(res){
                    data.status = (result.status === 'INACTIVE' ? 'DISABLED': result.status);
                    data.performingOperation = false;
                    callBack();
                  })
                  .catch(function(err){
                    data.performingOperation = false;
                    Notify.errorItemStatus();
                    callBack(err);
                  });
              }
          });

          function callBack(err){
            if(!err){
              ++completedReq;
            }
            if(err){
              ++noOfErrors;
            }
            if(noOfCalls == (completedReq+noOfErrors)){
              Notify.infoChangeItemsStatus(noOfCalls,completedReq,result.status);
            }
          }

        }, result.status);

      });

    }; // end change status function

    vm.changeSingleStatus = function(coupon, status) {
      if(coupon.status === status) {
        Notify.infoSameItemStatus();
        return;
      }

      coupon.performingOperation = true;
      var obj = {
        status: (status === 'INACTIVE' ? 'DISABLED': status)
      };

      API
        .Coupons
        .edit(coupon._id, obj)
        .then(function (data) {
          coupon.status = (status === 'INACTIVE' ? 'DISABLED': status);
          coupon.performingOperation = false;
          Notify.infoChangeStatus(status);

        })
        .catch(function (err) {
          Notify.errorItemStatus();
          coupon.performingOperation = false;
        });
    };

    vm.deleteCoupon = function() {

      ALERT.confirmGroupWarning(function () {

        var completedReq = 0;
        var noOfErrors = 0;
        var noOfCalls = 0; //no of http requests to call

        //checking if user has selected coupons?

        vm.coupons.forEach(function(data){
          if (data.checked){
            ++noOfCalls;
          }
        });

        vm.coupons.forEach(function (data) {
          if(data.checked) {
            data.performingOperation = true;
            API.Coupons.delete(data._id)
              .then(function() {
                data.performingOperation = false;
                callBack();
              })
              .catch(function (err) {
                data.performingOperation = false;
                callBack(err);
              });
          }
        });

        function callBack(err){
          if(!err){
            ++completedReq;
          }
          if(err){
            ++noOfErrors;
          }
          if(noOfCalls == (completedReq+noOfErrors)){
            //all calls have finished, let's call Notify
            vm.listCoupons(true); //reload coupons
            Notify.infoItemsDeleted(noOfCalls,completedReq);
          }
        }

      });
    };

    vm.changePage = function () {
      vm.previousPage = (vm.currentPage - 1);
      vm.listSelectorOptions.reset(); //reset the selected items
      vm.listCoupons();
    };

    vm.listCoupons();

  }


})(angular.module('adminPanel.marketing.coupons.list.controller',[]));
