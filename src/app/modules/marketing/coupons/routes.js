(function(app){
  app.config(['$stateProvider', 'URLProvider', function($stateProvider, URLProvider){

    $stateProvider
      .state('admin-panel.marketing.coupons',{
        url: '/coupons',
        abstract: true,
        template: '<ui-view/>',
        data: {
          permissions: {
            only: 'canSee.Coupons.Listing',
            redirectTo: URLProvider.$get().RedirectURL
          }
        }
      });

  }]);

})(angular.module('adminPanel.marketing.coupons.routes',[]));
