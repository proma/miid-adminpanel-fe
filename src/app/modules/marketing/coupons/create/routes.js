(function(app){

  app.config(function($stateProvider){

    $stateProvider
      .state('admin-panel.marketing.coupons.create',{
        url: '/create',
        templateUrl: 'app/modules/marketing/coupons/create/create.html',
        controller: 'CouponsCreateController',
        controllerAs: 'vm'
      });

  });

})(angular.module('adminPanel.marketing.coupons.create.route',[]));
