'use strict';

describe('Controllers', function () {
  describe('CouponsCreateController', function () {

    var data = {
      "_id": "571f50d9d5569128031874b2",
      "title": "coupon 1",
      "status": "ACTIVE",
      "brand": [{"name": "test", "_id": "232323"}]
    };

    var
      CouponsCreateController,
      $rootScope,
      URL,
      $httpBackend,
      ALERT,
      $scope,
      $state,
      RolesService,
      Notify;


    // load the dependencies module
    beforeEach(module('adminPanel.services.notifier'));
    beforeEach(module('adminPanel.services.roles'));
    beforeEach(module('ngResource'));
    beforeEach(module('ui.router'));
    beforeEach(module('oitozero.ngSweetAlert'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('adminPanel.services.alert'));
    beforeEach(module('adminPanel.api'));
    beforeEach(module('adminPanel.marketing.coupons.create.controller'));

    beforeEach(inject(function ($injector) {
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      $httpBackend = $injector.get('$httpBackend');
      URL = $injector.get('URL');
      var $controller = $injector.get('$controller');
      Notify = $injector.get('Notify');
      $state = $injector.get('$state');
      ALERT = $injector.get('ALERT');
      RolesService = $injector.get('RolesService');
      spyOn(RolesService, 'checkPermission');

      CouponsCreateController = $controller('CouponsCreateController', {
        'Notify': Notify,
        '$scope': $scope,
        '$state': $state,
        'API': $injector.get('API'),
        'ALERT': $injector.get('ALERT'),
        'RolesService': $injector.get('RolesService')
      });

      data.productURL = URL.PRODUCTS + '/list_values';
      data.brandURL = URL.BRANDS + '?list_type=LIST_VALUES';
      data.couponURL = URL.COUPONS;
    }));


    describe('MBA-151: Coupons Create', function () {
      it('should call listProducts() when selected SPECIFIC_PRODUCT', function () {
        spyOn(CouponsCreateController, 'listProducts');

        CouponsCreateController.coupon.condition.applicationType = 'SPECIFIC_PRODUCT';
        $scope.$digest();

        expect(CouponsCreateController.listProducts).toHaveBeenCalled();
      });

      it('should call alert when error in listing Brands', function () {
        spyOn(ALERT, 'confirm');

        $httpBackend.when('GET', data.brandURL).respond(500, {data: data.brand});
        CouponsCreateController.listBrands();
        $httpBackend.flush();

        expect(ALERT.confirm).toHaveBeenCalled();
      });

      it('should call alert when error in listing products', function () {
        spyOn(ALERT, 'confirm');

        $httpBackend.expect('GET', data.productURL).respond(500, data);
        $httpBackend.expect('GET', data.brandURL).respond(200, {data: data.brand});
        CouponsCreateController.listProducts();
        $httpBackend.flush();

        expect(ALERT.confirm).toHaveBeenCalled();
      });


      it('should create a random code', function () {
        CouponsCreateController.generateCode();
        expect(CouponsCreateController.coupon.code.length).toBe(20);
      });

      it('should call listBrands() when selected ALL_BRAND_PRODUCTS', function () {
        spyOn(CouponsCreateController, 'listBrands');

        CouponsCreateController.coupon.condition.applicationType = 'ALL_BRAND_PRODUCTS';
        $scope.$digest();

        expect(CouponsCreateController.listBrands).toHaveBeenCalled();
      });

      it('should call Notify if PERCENTAGE is greater than 100 or less than 0', function () {

        spyOn(Notify, 'errorPercentageValue');
        CouponsCreateController.coupon.condition.discountType = 'PERCENTAGE';
        CouponsCreateController.coupon.condition.discountValue = 101; //fake value
        CouponsCreateController.processForm(true);


        expect(Notify.errorPercentageValue).toHaveBeenCalled();

      });


      it('should call Notify if ALL_BRAND_PRODUCTS selected and selected brand value is -1', function () {

        spyOn(Notify, 'errorItemSelection');
        CouponsCreateController.coupon.condition.applicationType = 'ALL_BRAND_PRODUCTS';
        //it's already -1
        CouponsCreateController.processForm(true);

        expect(Notify.errorItemSelection).toHaveBeenCalled();

      });

      it('should call Notify if SPECIFIC_PRODUCT selected and selected product value is -1', function () {

        spyOn(Notify, 'errorItemSelection');
        CouponsCreateController.coupon.condition.applicationType = 'SPECIFIC_PRODUCT';
        //it's already -1
        CouponsCreateController.processForm(true);

        expect(Notify.errorItemSelection).toHaveBeenCalled();

      });

      it('should transition to list page when successfully posted', function () {
        spyOn($state,'go');

        $httpBackend.expect('POST', URL.COUPONS).respond(200, data);
        $httpBackend.expect('GET', data.brandURL).respond(200, {data: data.brand});
        CouponsCreateController.sendRequest({},true);
        $httpBackend.flush();

        expect($state.go).toHaveBeenCalled();

      });

      it('should show error if coupon code exist', function () {
        spyOn(Notify,'errorCodeExist');
        CouponsCreateController.coupon.code = '123456';
        $httpBackend.when('GET', data.brandURL).respond(200, {data: data.brand});
        $httpBackend.when('GET', URL.COUPONS + '/code/123456').respond(200,data);
        CouponsCreateController.coupon.condition.applicationType = 'PRODUCTS_OVER_PRICE';
        CouponsCreateController.processForm(true);
        $httpBackend.flush();

        expect(Notify.errorCodeExist).toHaveBeenCalled();
        expect(CouponsCreateController.error).toBeTruthy();
      });

      it('should show alert on server error', function () {
        spyOn(ALERT,'confirm');

        $httpBackend.expect('POST', URL.COUPONS).respond(500, {message: 'server error'});
        $httpBackend.expect('GET', data.brandURL).respond(200, {data: data.brand});
        CouponsCreateController.sendRequest({}, true);

        $httpBackend.flush();



        expect(ALERT.confirm).toHaveBeenCalled();
      });


    });


  });

});
