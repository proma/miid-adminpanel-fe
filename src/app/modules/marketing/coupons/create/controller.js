(function(app){

  app.controller('CouponsCreateController', CouponsCreateController);

  CouponsCreateController.$inject = ['$scope', '$state', 'API', 'ALERT', 'Notify', 'RolesService'];

  function CouponsCreateController($scope, $state, API, ALERT, Notify, RolesService){
    var vm = this;

    vm.regExpForPercentage = /^(?:100|\d{1,2})(?:\.\d{1,2})?$/;
    vm.regExpForAmount = /^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/;
    vm.savingCoupon = false;
    vm.dateStart = true;
    vm.dateEnd = true;

    vm.brands = [{ name: 'Loading brands...', value: "-1" }];
    vm.products = [{ name: 'Loading products...', value: "-1" }];

    vm.selectedBrands = [vm.brands[0].value];
    vm.selectedProduct = vm.products[0].value;

    vm.applicationTypes = [
      {
        title: 'All Products',
        value: 'ALL_PRODUCTS'
      },
      {
        title: 'All Brand Products',
        value: 'ALL_BRAND_PRODUCTS'
      },
      {
        title: 'Specific Product',
        value: 'SPECIFIC_PRODUCT'
      },
      // {
      //   title: 'Order Over',
      //   value: 'ORDER_OVER' //TODO: will uncomment this in other version.
      // },
      {
        title: 'Products Over Price',
        value: 'PRODUCTS_OVER_PRICE'
      }
    ];

    //Roles based access for options
    if (!RolesService.checkPermission('canSee.Coupons.Create.AllProductsOption'))
      vm.applicationTypes = _.reject(vm.applicationTypes, function (item) { return item.value == 'ALL_PRODUCTS' });

    if (!RolesService.checkPermission('canSee.Coupons.Create.ProductsOverPriceOption'))
      vm.applicationTypes = _.reject(vm.applicationTypes, function (item) { return item.value == 'PRODUCTS_OVER_PRICE' });


    vm.coupon = {
      name: '',
      code: '',
      condition: {
        discountType: 'PERCENTAGE',
        discountValue: 10,
        applicationType: vm.applicationTypes[0].value,
        minAmount: 10
      },
      expiry: {
        canExpire: false,
        expiryStart: moment(),
        expiryEnd: moment().add(1, 'week')
      },
      limit: {
        limited: false,
        limitedTo: 100
      },
      limitPerCust: false
    };


    vm.generateCode = function () {
      var couponCode = '';
      var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

      for( var i=0; i <= 19; i++ )
        couponCode += possible.charAt(Math.floor(Math.random() * possible.length));

      vm.coupon.code = couponCode;
    };


    vm.listBrands = function () {
      API
        .Brands
        .getAll(null, null, 'LIST_VALUES')
        .then(function (brands) {
          vm.brands = []; //reset array
          vm.selectedBrands = [];

          angular.forEach(brands.data, function (brand) {
            vm.brands.push({
              name: brand.name,
              value: brand._id
            });
          });

          vm.selectedBrands.push(vm.brands[0].value);
        })
        .catch(function (err) {
          vm.brands = [];
          vm.brands = [{ name: 'Error Loading brands...', value: "-1" }];
          ALERT.confirm(vm.listBrands);
        });
    };

    vm.listProducts = function () {
      API
        .Products
        .getList() // get all ACTIVE products
        .then(function (products) {
          vm.products = []; //reset array

          angular.forEach(products, function (product) {
            vm.products.push({
              name: product.miid_id,
              value: product._id
            });
          });

          if (vm.products.length < 1)
            vm.products = [{ name: 'No Products...', value: "-1" }];

          vm.selectedProduct = vm.products[0].value;
        })
        .catch(function (err) {
          vm.products = [{ name: 'Error Loading products...', value: "-1" }];
          ALERT.confirm(vm.listProducts);
        });
    };

    vm.processForm = function (valid) {
      if(!valid) return;
      vm.savingCoupon = true;

      var postObj = {};
      //sub objects
      postObj.user_usage_limit = {};
      postObj.discount = {};
      postObj.expires = {};

      //General info
      postObj.name = vm.coupon.name;
      postObj.code = vm.coupon.code;
      postObj.status = 'ACTIVE'; //by default

      //User usage limits ( according to UI )
      postObj.validity_times = 0; //for unlimited it's 0

      if(vm.coupon.limit.limited) //apply if coupon is limited
        postObj.validity_times = vm.coupon.limit.limitedTo;
      postObj.user_usage_limit.active = vm.coupon.limitPerCust;

      //Coupon expiry
      postObj.expires.never = !vm.coupon.expiry.canExpire;
      if(vm.coupon.expiry.canExpire) {
        postObj.expires.start = vm.coupon.expiry.expiryStart.valueOf();
        postObj.expires.end = vm.coupon.expiry.expiryEnd.valueOf();
      }
      //Discount options
      postObj.discount.type = vm.coupon.condition.discountType;
      if(vm.coupon.condition.discountType === 'PERCENTAGE')
        if(vm.coupon.condition.discountValue > 100 || vm.coupon.condition.discountValue < 0) {
          Notify.errorPercentageValue();
          vm.savingCoupon = false;
          return;
        }

      postObj.discount.value = vm.coupon.condition.discountValue;
      postObj.discount.apply_on = vm.coupon.condition.applicationType;
      // in case of ALL_BRAND_PRODUCTS
      if(vm.coupon.condition.applicationType === 'ALL_BRAND_PRODUCTS') {
        if(vm.selectedBrands.length > 0 && vm.selectedBrands[0] != -1){
          postObj.discount.brands = vm.selectedBrands;}
        else {
          Notify.errorItemSelection(1);
          vm.savingCoupon = false;
          return;
        }
      }

      // in case of SPECIFIC_PRODUCT
      if(vm.coupon.condition.applicationType === 'SPECIFIC_PRODUCT') {
        if(vm.selectedProduct != -1)
          postObj.discount.specific_product  = vm.selectedProduct;
        else {
          Notify.errorItemSelection();
          vm.savingCoupon = false;
          return;
        }
      }

      // in case of ORDER_OVER and PRODUCTS_OVER_PRICE
      if(vm.coupon.condition.applicationType === 'PRODUCTS_OVER_PRICE' || vm.coupon.condition.applicationType === 'ORDER_OVER')
        postObj.discount.min_amount = vm.coupon.condition.minAmount;


      //first call getByCode to see if there's any coupon
      API
        .Coupons
        .getByCode(vm.coupon.code)
        .then(function (data) {
          vm.savingCoupon = false;
          if(data) { //means coupon exist
            vm.error = true;
            Notify.errorCodeExist();
            return;
          }
        })
        .catch(function (err) {
          if(err.statusCode == 404 || (err.data && err.data.statusCode == 404)) {
            vm.sendRequest(postObj, valid);
          }
        });
    };


    vm.sendRequest = function (postObj, valid) {
      API
        .Coupons
        .save(postObj)
        .then(function (result) {
          $state.go('admin-panel.marketing.coupons.list');
          vm.savingCoupon = false;
        })
        .catch(function (err) {
          vm.savingCoupon = false;
          ALERT.errorDataSaving(function () {
            return vm.processForm(valid);
          });
        });
    };

    vm.checkDate = function () {
      if(moment().diff(vm.coupon.expiry.expiryStart, 'day') <=0) {
        vm.dateStart = true;
      }else {
        vm.dateStart = false;
      }

      if(vm.coupon.expiry.expiryEnd.diff(vm.coupon.expiry.expiryStart, 'minutes') > 0 ) {
        vm.dateEnd = true;
      }else {
        vm.dateEnd = false;
      }
      $scope.createCoupon.couponExpiryEnd.$setValidity('dateInvalid', vm.dateEnd);
      $scope.createCoupon.couponExpiryStart.$setValidity('dateInvalid', vm.dateStart);
    };

    $scope.$watch('vm.coupon.condition.applicationType', function () {
      if(vm.coupon.condition.applicationType === 'ALL_BRAND_PRODUCTS')
        vm.listBrands();
      else if(vm.coupon.condition.applicationType === 'SPECIFIC_PRODUCT')
        vm.listProducts();
    });
  }


})(angular.module('adminPanel.marketing.coupons.create.controller',[]));
