'use strict';

describe('Controllers', function () {

  var data = {
    customers: {
      paging: {
        total: 2
      },
      data: [
        {
          "_id": "56b8918c936e8b0600231ed5",
          "username": "roman",
          "name": "Roman",
          "password": "$2a$10$.YlrIEkPngeJ11P3qzaAF.Qqn5U4zNj5vNIll1U8JeYz2nC3Y/XJC",
          "followings": [],
          "media": [],
          "__v": 0,
          "updated_at": "2016-02-08T13:01:00.516Z",
          "created_at": "2016-02-08T13:01:00.503Z",
          "status": "ACTIVE",
          "following": [],
          "followers": [],
          "addresses": [],
          "background_img": null,
          "thumb": null,
          "gender": "M",
          "designs_collection": [],
          "login_with": {
            "qq": {
              "provider_id": null,
              "refresh_token": null,
              "access_token": null
            },
            "weibo": {
              "provider_id": null,
              "refresh_token": null,
              "access_token": null
            },
            "wechat": {
              "provider_id": null,
              "refresh_token": null,
              "access_token": null
            }
          },
          "contact": {
            "address": null,
            "email": "some@miid.com",
            "weibo": "weiboid",
            "wechat": "wechatid",
            "qq": "qqid",
            "phone": "+86135445784283",
          }
        },
        {
          "_id": "56c6acc364c4760600b4e9bc",
          "username": "sheldon2",
          "name": "sheldon22",
          "password": "$2a$10$8kKTxUgyFzkdMqiypSuFpu/yawekJgYDlcYgK149XPpICqOfPwsk.",
          "followings": [],
          "__v": 0,
          "refreshToken": "338965ed-f00a-43a3-bab3-42810785d472",
          "updated_at": "2016-02-19T05:48:51.697Z",
          "created_at": "2016-02-19T05:48:51.685Z",
          "status": "ACTIVE",
          "following": [],
          "followers": [],
          "addresses": [],
          "background_img": null,
          "thumb": null,
          "gender": "M",
          "designs_collection": [],
          "login_with": {
            "qq": {
              "provider_id": null,
              "refresh_token": null,
              "access_token": null
            },
            "weibo": {
              "provider_id": null,
              "refresh_token": null,
              "access_token": null
            },
            "wechat": {
              "provider_id": null,
              "refresh_token": null,
              "access_token": null
            }
          },
          "contact": {
            "address": null,
            "email": "some@miid.com",
            "weibo": "weiboid",
            "wechat": "wechatid",
            "qq": "qqid",
            "phone": "+86135445784283",
          }
        }
      ]
    }
  };

  var
    UsersController,
    $rootScope,
    URL,
    $httpBackend,
    $scope;

  // load the controller's module
  beforeEach(module('ngResource'));
  beforeEach(module('toastr'));
  beforeEach(module('adminPanel.constants'));
  beforeEach(module('ui.bootstrap'));
  beforeEach(module('adminPanel.services.alert'));
  beforeEach(module('ui.bootstrap'));
  beforeEach(module('adminPanel.api'));
  beforeEach(module('ui.router'));
  beforeEach(module('adminPanel.customer_management.users.controller'));

  beforeEach(inject(function ($injector) {
    $rootScope = $injector.get('$rootScope');
    $scope = $rootScope.$new();
    URL = $injector.get('URL');
    $httpBackend = $injector.get('$httpBackend');
    var $controller = $injector.get('$controller');
    UsersController = $controller('UsersController', {
      '$scope': $scope,
      'ALERT': $injector.get('ALERT'),
      'API': $injector.get('API')
    });

    // list users query
    var listQuery = '?from=' + UsersController.filters.from.valueOf() + '&limit=10&page=0';
    listQuery += '&to=' + UsersController.filters.to.valueOf();
    $httpBackend.expect('GET', URL.CUSTOMERS + listQuery).respond(200, data.customers);

    // get users registered today
    var todayQuery = '?from=' + UsersController.todayFilter.from.valueOf() + '&limit=10&page=0';
    todayQuery += '&to=' + UsersController.todayFilter.to.valueOf();
    $httpBackend.expect('GET', URL.CUSTOMERS + todayQuery).respond(200, data.customers);

    // get users registered last week
    var lastWeekQuery = '?from=' + UsersController.lastWeekFilter.from.valueOf() + '&limit=10&page=0';
    lastWeekQuery += '&to=' + UsersController.lastWeekFilter.to.valueOf();
    $httpBackend.expect('GET', URL.CUSTOMERS + lastWeekQuery).respond(200, data.customers);

    // get users registered last month
    var lastMonthQuery = '?from=' + UsersController.lastMonthFilter.from.valueOf() + '&limit=10&page=0';
    lastMonthQuery += '&to=' + UsersController.lastMonthFilter.to.valueOf();
    $httpBackend.expect('GET', URL.CUSTOMERS + lastMonthQuery).respond(200, data.customers);

    // get users registered total
    var totalQuery = '?from=' + UsersController.totalFilter.from.valueOf() + '&limit=10&page=0';
    totalQuery += '&to=' + UsersController.totalFilter.to.valueOf();
    $httpBackend.expect('GET', URL.CUSTOMERS + totalQuery).respond(200, data.customers);

  }));

  describe('UsersController', function () {

    describe('MBA-38: List Users', function () {
      it('should have empty users in start', function () {
        expect(UsersController.users.length).toEqual(0);
      });

      it('should be loading first', function () {
        expect(UsersController.loading).toBeTruthy();
        expect(UsersController.loadingToday).toBeTruthy();
        expect(UsersController.loadingLastWeek).toBeTruthy();
        expect(UsersController.loadingLastMonth).toBeTruthy();
        expect(UsersController.loadingTotal).toBeTruthy();
      });

      it('should stop loading after load', function () {
        $httpBackend.flush();
        expect(UsersController.loading).toBeFalsy();
        expect(UsersController.loadingToday).toBeFalsy();
        expect(UsersController.loadingLastWeek).toBeFalsy();
        expect(UsersController.loadingLastMonth).toBeFalsy();
        expect(UsersController.loadingTotal).toBeFalsy();
      });

      it('should populate the user registered stats', function () {
        $httpBackend.flush();
        expect(UsersController.stats.today).toBe(2);
        expect(UsersController.stats.lastWeek).toBe(2);
        expect(UsersController.stats.lastMonth).toBe(2);
        expect(UsersController.stats.total).toBe(2);
      });

      it('should reset page to first one when filtering is triggered', function () {
        $httpBackend.flush();
        UsersController.currentPage = 10;
        UsersController.filtersChanged();
        expect(UsersController.currentPage).toBe(1);

        UsersController.currentPage = 10;
        $scope.dateChanged();
        expect(UsersController.currentPage).toBe(1);
      });

    });


  });
});
