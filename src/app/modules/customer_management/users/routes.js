(function(app){

  app.config(['$stateProvider', 'URLProvider', function($stateProvider, URLProvider){

    $stateProvider
      .state('admin-panel.cust_mgmt.users',{
        url: '/users',
        templateUrl: "app/modules/customer_management/users/users.html",
        controller: 'UsersController',
        controllerAs: 'vm',
        data: {
          permissions: {
            only: 'canSee.Users.Listing',
            redirectTo: URLProvider.$get().RedirectURL
          }
        }
      })

  }]);

})(angular.module('adminPanel.customer_management.users.route',[]));
