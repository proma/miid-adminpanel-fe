(function (app) {

  app.controller('UsersController', UsersController);

  UsersController.$inject = ['$scope', 'ALERT', 'API'];

  function UsersController($scope, ALERT,  API) {
    var vm = this;

    vm.loading = true;
    vm.loadingToday = true;
    vm.loadingLastWeek = true;
    vm.loadingLastMonth = true;
    vm.loadingTotal = true;
    vm.users = [];
    vm.totalItems = 0;
    vm.itemsGot = 0;
    vm.previousPage = 0;
    vm.currentPage = 1;
    vm.itemsLimit = 10;
    vm.filters = {
      text: "",
      from: moment((new Date("1/1/" + moment().year())).valueOf()),
      to: moment()
    };
    vm.stats = {
      today: 0,
      lastWeek: 0,
      lastMonth: 0,
      total: 0
    };

    vm.listUsers = function () {
      vm.loading = true;
      API.Customers.getAll(vm.itemsLimit, vm.currentPage - 1, vm.filters)
        .then(function (customers) {
          vm.loading = false;
          angular.forEach(customers.data, function(obj){
            var d = new Date(obj.created_at);
            obj.created_at = d.getDate() + ' / ' + (d.getMonth()+1) + ' / ' + d.getFullYear();
          });
          vm.users = customers.data;
          vm.itemsGot = vm.users.length;
          vm.totalItems = customers.paging.total;
        })
        .catch(function () {
          vm.loading = false;
         ALERT.confirm(vm.listUsers);
        });
    };


    vm.todayFilter = {
      from: moment().startOf('day'),
      to: moment()
    };
    vm.lastWeekFilter = {
      from: moment().subtract(7, 'days'),
      to: moment()
    };
    vm.lastMonthFilter = {
      from: moment().subtract(1, 'months'),
      to: moment()
    };
    vm.totalFilter = {
      from: moment().subtract(5, 'years'),
      to: moment()
    };
    vm.loadStats = function() {
      vm.loadingToday = true;
      vm.loadingLastWeek = true;
      vm.loadingLastMonth = true;
      vm.loadingTotal = true;

      // load today
      API.Customers.getAll(vm.itemsLimit, vm.currentPage - 1, vm.todayFilter)
        .then(function (resp) {
          vm.loadingToday = false;
          vm.stats.today = resp.paging.total;
        })
        .catch(function (err) {
          vm.loadingToday = false;
          ALERT.confirm(vm.loadStats);
        });

      // load last week
      API.Customers.getAll(vm.itemsLimit, vm.currentPage - 1, vm.lastWeekFilter)
        .then(function (resp) {
          vm.loadingLastWeek = false;
          vm.stats.lastWeek = resp.paging.total;
        })
        .catch(function (err) {
          vm.loadingLastWeek = false;
          ALERT.confirm(vm.loadStats);
        });

      // load last month
      API.Customers.getAll(vm.itemsLimit, vm.currentPage - 1, vm.lastMonthFilter)
        .then(function (resp) {
          vm.loadingLastMonth = false;
          vm.stats.lastMonth = resp.paging.total;
        })
        .catch(function (err) {
          vm.loadingLastMonth = false;
          ALERT.confirm(vm.loadStats);
        });

      // load all time
      API.Customers.getAll(vm.itemsLimit, vm.currentPage - 1, vm.totalFilter)
        .then(function (resp) {
          vm.loadingTotal = false;
          vm.stats.total = resp.paging.total;
        })
        .catch(function (err) {
          vm.loadingTotal = false;
          ALERT.confirm(vm.loadStats);
        });
    };

    vm.changePage = function(){
      vm.previousPage = (vm.currentPage-1);
      vm.loading = true;
      vm.listUsers();
    };

    $scope.dateChanged = function () {
      vm.previousPage = 0;
      vm.currentPage = 1;
      vm.listUsers();
    };

    vm.filtersChanged = function () {
      vm.previousPage = 0;
      vm.currentPage = 1;
      vm.listUsers();
    };

    vm.listUsers();
    vm.loadStats();
  }

})(angular.module('adminPanel.customer_management.users.controller', []));
