(function(app){

})(angular.module('adminPanel.customer_management.orders.details',
  [
    'adminPanel.customer_management.orders.details.route',
    'adminPanel.customer_management.orders.details.controller',
    'adminPanel.customer_management.orders.details.filter',
    'adminPanel.customer_management.orders.details.trackingModal'
  ]
));
