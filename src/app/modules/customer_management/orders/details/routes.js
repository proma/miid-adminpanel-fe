(function(app){

  app.config(['$stateProvider', 'URLProvider', function($stateProvider, URLProvider){

    $stateProvider
      .state('admin-panel.cust_mgmt.orders.details',{
        url: '/details/:orderID',
        templateUrl: 'app/modules/customer_management/orders/details/details.html',
        controller: 'OrdersDetailsController',
        controllerAs: 'vm',
        data: {
          permissions: {
            only: 'canSee.Orders.Details',
            redirectTo: URLProvider.$get().RedirectURL
          }
        }
      });

  }]);

})(angular.module('adminPanel.customer_management.orders.details.route',[]));
