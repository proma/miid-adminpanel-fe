(function (app) {

  app
    .filter('TrackingDesignNumbers', function () {

      return function (trackingDesigns, orderDesigns) {

        var design_ids = [];
        _.each(trackingDesigns, function(trackingDesign){
          var foundDesign = _.find(orderDesigns, function(orderDesign){
            return (orderDesign.design && (orderDesign.design._id == trackingDesign));
          });
          if(foundDesign) {
            design_ids.push('#' + foundDesign.design.design_id);
          }
        });

        if(design_ids.length > 0) {
          return 'Designs ' + design_ids.join();
        } else {
          return '';
        }
      };
    });

})(angular.module('adminPanel.customer_management.orders.details.filter', []));
