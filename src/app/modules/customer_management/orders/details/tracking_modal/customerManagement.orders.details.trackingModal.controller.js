(function (app) {

  app.controller('OrderDetailsTrackingModalController', OrderDetailsTrackingModalController);

  OrderDetailsTrackingModalController.$inject = ['Order', 'Mode', 'TrackingIndex', 'ShippingOptions', 'API', 'Notify', '$uibModalInstance', 'QINGSTOR'];

  function OrderDetailsTrackingModalController(Order, Mode, TrackingIndex, ShippingOptions, API, Notify, $uibModalInstance, QINGSTOR) {

    var modal = this;

    modal.order = Order;
    modal.mode = Mode;
    modal.trackingIndex = TrackingIndex;
    modal.shippingOptions = ShippingOptions;
    modal.disabled = false;
    modal.isDesignView = false;
    modal.designURL = QINGSTOR.design_url;

    modal.close = function(){
      $uibModalInstance.dismiss('cancel');
    };

    modal.submit = function(){

      modal.disabled = true; //disable input

      if(!modal.tracking.charges) {
        modal.tracking.charges = 0;
      }

      if(modal.mode == 'ADD') {

        API.Orders
          .addTracking(modal.order._id, modal.tracking)
          .then(function(result){
            Notify.infoSaved();
            modal.disabled = false;
            $uibModalInstance.close({
              result: result
            });
          })
          .catch(function(err){
            Notify.errorSaving();
            modal.disabled = false;
            $uibModalInstance.close({
              result: null
            });
          });

      } else if(modal.mode == 'EDIT') {

        var trackingId = modal.tracking._id;
        delete modal.tracking._id;
        delete modal.tracking.created_at;

        API.Orders
          .editTracking(modal.order._id, trackingId, modal.tracking)
          .then(function(result){
            Notify.infoSaved();
            modal.disabled = false;
             $uibModalInstance.close({
              result: result
             });
          })
          .catch(function(err){
            Notify.errorSaving();
            modal.disabled = false;
            $uibModalInstance.close({
              result:null
            });
          });

      }

    };

    modal.showDesignView = function() {

      //Set checked
      _.each(modal.remainingDesigns, function(remainingDesign){
        var index = _.findIndex(modal.tracking.designs, function(trackingDesignID){
          return remainingDesign._id == trackingDesignID;
        });
        if(index != -1) remainingDesign.isChosen = true;
        else remainingDesign.isChosen = false;
      });

      modal.choosingDesigns = angular.copy(modal.remainingDesigns);

      modal.isDesignView = true;
    };

    modal.saveCheckedDesigns = function() {

      modal.tracking.designs = [];

      _.each(modal.choosingDesigns, function(design){

        if(design.isChosen) {

          modal.tracking.designs.push(design._id);
        }

      });

      modal.isDesignView = false;
    };

    modal.cancelCheckedDesigns = function() {
      modal.isDesignView = false;
    };

    modal.hasChangedEditView = function() {

      if(angular.equals(modal.tracking, Order.tracking_details[TrackingIndex])) return false;

      return true;

    };

    modal.hasChangedDesignView = function() {

      //Check changes
      if(angular.equals(modal.choosingDesigns, modal.remainingDesigns)) return false;

      //Check one of designs is checked at least
      var index = _.findIndex(modal.choosingDesigns, function(design){
        return design.isChosen == true;
      });
      if(index == -1) return false;

      return true;
    };

    modal.setTrackingCompany = function() {

      var shippingOption = _.find(modal.shippingOptions, function(option) {
        return option._id == modal.tracking.delivery_id;
      });

      if(shippingOption) {
        modal.tracking.company = shippingOption.company;
      }
    };

    //Private functions

    function _initRemainingDesigns() {

      var allTrackingDesignIDs = [];

      if(Mode == 'ADD') {

        _.each(modal.order.tracking_details, function (tracking) {
          allTrackingDesignIDs = _.union(allTrackingDesignIDs, tracking.designs);
        });

      } else if(Mode == 'EDIT') {

        _.each(modal.order.tracking_details, function(tracking, index){
          if(index != modal.trackingIndex) {
            allTrackingDesignIDs = _.union(allTrackingDesignIDs, tracking.designs);
          }
        });
      }

      modal.remainingDesigns = [];
      modal.remainingDesignIDs = [];

      _.each(modal.order.designs, function(orderDesign){
        if(orderDesign.design) {
          var foundIndex = _.findIndex(allTrackingDesignIDs, function (trackingDesignID) {
            return orderDesign.design._id == trackingDesignID;
          });
          if (foundIndex == -1) {
            modal.remainingDesigns.push(orderDesign.design);
            modal.remainingDesignIDs.push(orderDesign.design._id);
          }
        }
      });
    }

    function _init() {

      _initRemainingDesigns();

      if(Mode == 'ADD') {

        //Init tracking model
        modal.tracking = {
          delivery_id: Order.delivery.id,
          company: Order.delivery.company,
          designs: modal.remainingDesignIDs,
          charges: 0
        };

      } else if(Mode == 'EDIT') {

        //Init tracking model
        modal.tracking = angular.copy(Order.tracking_details[TrackingIndex]);

      }

    }

    _init();
  }

})(angular.module('adminPanel.customer_management.orders.details.trackingModal.controller', []));
