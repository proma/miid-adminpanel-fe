(function(app) {
  'use strict';

  app.controller('OrdersDetailsController', OrdersDetailsController);

  OrdersDetailsController.$inject = [ 'API', 'Notify', 'ALERT', 'Modal', '$stateParams', '$state', 'QINGSTOR' ];

  function OrdersDetailsController(API, Notify, ALERT, Modal, $stateParams, $state, QINGSTOR) {
    var vm = this;

    vm.order = {};
    vm.orderID = $stateParams.orderID;
    vm.assetURL = QINGSTOR.base_url;
    vm.designURL = QINGSTOR.design_url;

    vm.statusConfig = {
      'PAYMENT_ERROR': {
        name: 'Payment Error',
        class: 'error',
        weight: 0 // for status steps progress bar
      },
      'PAID': {
        name: 'Paid',
        class: 'paid',
        weight: 1
      },
      'IN_PRODUCTION': {
        name: 'In Production',
        class: 'production',
        weight: 2
      },
      'SENT': {
        name: 'Sent',
        class: 'sent',
        weight: 3
      },
      'RECEIVED': {
        name: 'Received',
        class: 'received',
        weight: 4
      },
      'FINALIZED': {
        name: 'Finalized',
        class: 'finalized',
        weight: 5
      },
      'PENDING': {
        name: 'Pending',
        class: 'pending'
      }
    };

    vm.changingStatus = false;
    vm.orderStatus = 'IN_PRODUCTION';
    vm.orderNote = {
      value: '',
      changing: false
    };


    vm.refreshPO = function() {
      vm.order.refreshingPO = true;
      API
        .Orders
        .genPO(vm.order._id)
        .then(function() {
          vm.order.refreshingPO = false;
          Notify.infoPOFiles();
        })
        .catch(function() {
          vm.order.refreshingPO = false;
          Notify.errorPOFiles();
        })
    };

    vm.getBackImage = function(imgURL) {
      return imgURL.replace('0.jpg', '4.jpg');
    };

    vm.getTrackingStatusByDesign = function(orderDesign) {

      var foundTracking = null;

      _.each(vm.order.tracking_details, function(tracking) {

        var index = _.findIndex(tracking.designs, function(trackingDesign){
          return trackingDesign == orderDesign;
        });

        if(index != -1) {
          foundTracking = tracking;
        }

      });

      if(foundTracking) {
        if(vm.order.status == 'IN_PRODUCTION') return 'Sent';
        else return vm.order.status;
      } else {
        if(vm.order.status == 'IN_PRODUCTION') return 'IN_PRODUCTION';
        else return '';
      }

    };

    vm.getTrackingNumberByDesign = function(orderDesign) {

      var foundTracking = null;

      _.each(vm.order.tracking_details, function(tracking) {

        var index = _.findIndex(tracking.designs, function(trackingDesign){
          return trackingDesign == orderDesign;
        });

        if(index != -1) {
          foundTracking = tracking;
        }

      });

      if(foundTracking) {
        return foundTracking.company+' #'+ foundTracking.tracking_number;
      } else {
        return '';
      }

    };

    //navigate

    vm.navigate = function(direction) {
      //check if cache available if not ask user to go back to User list
      if(!API.Orders.cache.available()) {
        Notify.infoNoItems();
        return;
      }
      var orders = API.Orders.cache.getOrdersList();
      var index = _.findIndex(orders, { '_id': vm.orderID });
      if(direction === 'left') {
        index--;
      }else {
        index++;
      }
      if(typeof(orders[index]) != 'undefined') {
        $state.go('admin-panel.cust_mgmt.orders.details', {'orderID': orders[index]._id});
      }else{
        //if there are no orders in the list for navigation. ask user to go back to change pagination.
        Notify.infoNoItems();
      }
    };

    //load shipping_options

    vm.loadShippingOptions = function() {

      if(vm.order.brand) {

        API
          .Brands
          .getShippingOptions(vm.order.brand._id)
          .then(function(data){
            vm.shippingOptions = data;
          })
          .catch(function(err) {
            vm.shippingOptions = [];
          });
      } else if(vm.order.delivery) {

        vm.shippingOptions = [{
          _id: vm.order.delivery.delivery_id,
          company: vm.order.delivery.company
        }];
      } else {
        vm.shippingOptions = [];
      }

    };

    //load order

    vm.loadOrder = function () {
      API
        .Orders
        .getOne(vm.orderID)
        .then(function(data) {
          vm.order = data;
          vm.order.sub_total = vm.order.total_amount;

          if (vm.order.delivery && vm.order.delivery.charges)
            vm.order.total_amount += vm.order.delivery.charges;
          if (vm.order.coupon.id)
            vm.order.sub_total += vm.order.coupon.total_discount;

          vm.orderNote.value = vm.order.note || '';
          vm.order.created_at = new Date(vm.order.created_at);

          //load shipping options
          vm.loadShippingOptions();

          vm.order.tracking_details = vm.order.tracking_details || [];console.log(vm.order);

          //Merge Customization & Customization_3d to show all ui, inputs, selectable, custom images
          vm.order.designs.forEach(function (design) {
            var duplicateStepsInCustomization = [];

            if (!design.design)
              return;

            design.design.customization_3d.steps.forEach(function (cus_3d_step) {

              if(design.design.customization.steps != undefined) {

                var customization_steps = _.filter(design.design.customization.steps, function (cus_step) {
                  return cus_step.id == cus_3d_step.id;
                });

                if(customization_steps.length > 0) {
                  cus_3d_step.groups = cus_3d_step.groups.concat(customization_steps[0].groups);
                }

                duplicateStepsInCustomization = _.union(duplicateStepsInCustomization, customization_steps);
              }
            });
            //get the difference of left out steps
            var left_customization_steps = _.difference(design.design.customization.steps, duplicateStepsInCustomization);
            //just append it to the customization 3d
            design.design.customization_3d.steps = _.union(design.design.customization_3d.steps, left_customization_steps);
          });

          if (API.Orders.cache.available()) {
            var orders = API.Orders.cache.getOrdersList();
            var index = _.findIndex(orders, { '_id': vm.orderID });
            vm.order.next = orders[index+1]? orders[index+1]._id: null;
            vm.order.previous = orders[index-1]? orders[index-1]._id: null;
          }
        })
        .catch(function(err) {
          ALERT.confirm(vm.loadOrder)
        });
    };

     //change status
    vm.changeStatus = function(status){

      vm.changingStatus = true;

      if(vm.order.status === status){
        Notify.infoSameItemStatus();
        vm.changingStatus = false;
        return;
      }

      if(status === 'SENT'){

        //Check there are still remaining designs to send
        var allTrackingDesignIDs = [];
        _.each(vm.order.tracking_details, function (tracking) {
          allTrackingDesignIDs = _.union(allTrackingDesignIDs, tracking.designs);
        });

        if(allTrackingDesignIDs.length < vm.order.designs.length) {
          vm.showTrackingModal('ADD',0);
          return;
        }
      }

      API.
        Orders
        .edit(vm.order._id, {'status': status})
        .then(function(result){
          vm.order.status = result.status;
          Notify.infoChangeStatus(status);
          vm.changingStatus = false;
        })
        .catch(function(err){
          if(err.data && err.data.message) {
            Notify.error(err.data.message);
          } else {
            Notify.errorItemStatus();
          }
          vm.changingStatus = false;
        });
    };

    vm.deleteTracking = function(trackingIndex) {

      ALERT.confirmDeleting(function(){

        vm.changingStatus = true;

        API.Orders
          .deleteTracking(vm.order._id, vm.order.tracking_details[trackingIndex]._id)
          .then(function(result){
            Notify.infoSaved();
            vm.changingStatus = false;
            vm.order.tracking_details.splice(trackingIndex,1);
          })
          .catch(function(err){
            Notify.errorSaving();
            vm.changingStatus = false;
          });
      });

    };

    vm.showTrackingModal = function(mode, trackingIndex) {
      Modal
        .open(
          'OrderDetailsTrackingModalController',
          'modal',
          'app/modules/customer_management/orders/details/tracking_modal/customerManagement.orders.details.trackingModal.html',
          {
            Order : function(){
              return vm.order;
            },
            Mode: function() {
              return mode;
            },
            TrackingIndex: function() {
              return trackingIndex;
            },
            ShippingOptions: function() {
              return vm.shippingOptions;
            }
          },
          'lg')
        .result.then(function (res) {
            if(res.result) {
              vm.loadOrder();
            }
            vm.changingStatus = false;
          }, function(){
            vm.changingStatus = false;
          });
    };


    vm.submitNote = function() {
      vm.orderNote.changing = true;
      API
        .Orders
        .saveNote(vm.orderID, vm.orderNote.value)
        .then(function(data) {
          vm.orderNote.changing = false;
          vm.order.note = vm.orderNote.value;
          Notify.infoSaved();
          API
            .Orders
            .genPO(vm.order._id)
            .then(function() {
            })
            .catch(function(err) {
              Notify.errorPOFiles();
            });
        })
        .catch(function() {
          vm.orderNote.changing = false;
          Notify.errorSaving();
        });
    };

    //call loadOrder to populate
    vm.loadOrder();
  }

})(angular.module('adminPanel.customer_management.orders.details.controller',[]));
