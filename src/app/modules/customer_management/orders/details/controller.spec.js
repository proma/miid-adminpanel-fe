'use strict';

describe('Controllers', function () {
  describe('OrdersDetailsController', function () {
    var data = {
        "_id": "570657405587fac809d50c5b",
        "order_id": 10001,
        "comment": "my comment for first order opalis with 2 products in it",
        "paid_amount": 0,
        "total_amount": 598,
        "platform": "DESKTOP",
        "payment_method": "WECHAT",
        "status_num": 2,
        "status": "PAYMENT_ERROR",
        "note": "working fine",
        "coupon": { id: null },
        "designs": []
      };
    var list = {
      data: [
        {_id: '570657405587fac809d50c5b'},
        {_id: '570657405587fac809d50c5c'}
      ],
      paging: {
        total: 2
      }
    };
    var
      OrdersDetailsController,
      $rootScope,
      Notify,
      URL,
      $httpBackend,
      $state,
      API,
      $scope;

    // load the controller's module
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.services.notifier'));
    beforeEach(module('adminPanel.services.modal'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('ui.bootstrap'));
    beforeEach(module('ui.router'));
    beforeEach(module('adminPanel.services.alert'));
    beforeEach(module('adminPanel.api'));
    beforeEach(module('adminPanel.customer_management.orders.details.controller'));

    beforeEach(inject(function ($injector) {
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      URL = $injector.get('URL');
      $httpBackend = $injector.get('$httpBackend');
      Notify = $injector.get('Notify');
      API = $injector.get('API');
      $state = $injector.get('$state');
      var $controller = $injector.get('$controller');
      OrdersDetailsController = $controller('OrdersDetailsController', {
        '$scope': $scope,
        'API': API,
        'ALERT': $injector.get('ALERT'),
        'Notify': Notify,
        '$stateParams': $injector.get('$stateParams'),
        '$state': $state,
        'QINGSTOR': $injector.get('QINGSTOR'),
        'Modal': $injector.get('Modal')
      });
      OrdersDetailsController.orderID = data._id;
      OrdersDetailsController.orderNote = {};
      $httpBackend.expect('GET', URL.ORDERS ).respond(200, data);

    }));


    describe('MA-189: Order Details Page', function () {
      it('should have empty order in start', function () {
        expect(OrdersDetailsController.order).toEqual({});
      });

      it('should populate order', function () {
        $httpBackend.flush();
        expect(OrdersDetailsController.order._id).toEqual(data._id);
      });

      it('should save note and call genPO on success', function () {
        $httpBackend.expect('PUT', URL.ORDERS +'/'+ data._id + '/note').respond(200, {success: true});
        $httpBackend.expect('GET', URL.ORDERS +'/generate_po/'+ data._id).respond(200, {success: true});

        OrdersDetailsController.orderNote.value = 'test';
        OrdersDetailsController.submitNote();

        $httpBackend.flush();
        expect(OrdersDetailsController.order.note).toEqual(OrdersDetailsController.orderNote.value);
      });

      it('should not save note and not reset variable on server error', function () {
        $httpBackend.expect('PUT', URL.ORDERS +'/'+ data._id + '/note').respond(500, {success: false});

        OrdersDetailsController.orderNote.value = 'test';
        OrdersDetailsController.submitNote();

        $httpBackend.flush();
        expect(OrdersDetailsController.orderNote.value).toEqual(OrdersDetailsController.orderNote.value);
      });

      it('should change status to PAID', function () {
        OrdersDetailsController.changeStatus('PAID');
        $httpBackend.expect('PUT', URL.ORDERS).respond(200, {status: 'PAID'});
        $httpBackend.flush();
        expect(OrdersDetailsController.order.status).toEqual('PAID');
      });

      it('should call modal for SENT status', function () {
        spyOn(OrdersDetailsController,'showModal');
        OrdersDetailsController.changeStatus('SENT');
        expect(OrdersDetailsController.showModal).toHaveBeenCalled();
      });

      it('should call Notify if there is no cache in API for navigation', function () {
        spyOn(Notify,'infoNoItems');
        OrdersDetailsController.navigate('left');
        expect(Notify.infoNoItems).toHaveBeenCalled();
      });


      it('should create PO files and call Notify on success', function () {
        $httpBackend.expect('GET', URL.ORDERS +'/generate_po/'+ data._id).respond(200, {success: true});
        spyOn(Notify,'infoPOFiles');

        OrdersDetailsController.order = { _id: data._id}; //setting fake order
        OrdersDetailsController.refreshPO();

        $httpBackend.flush();
        expect(Notify.infoPOFiles).toHaveBeenCalled();
      });

      it('should load next order in list if called navigate()', function () {
        spyOn($state,'go');
        $httpBackend.expect('GET', URL.ORDERS).respond(200, list);

        API.Orders.getAll().then(function(data){
          OrdersDetailsController.navigate('right');
          // expect $state to have been called with next order in list
          expect($state.go).toHaveBeenCalledWith('admin-panel.cust_mgmt.orders.details', {'orderID': list.data[1]._id});
        });
        $httpBackend.flush();
      });

      it('should call Notify on no order left in list if called navigate()', function () {
        spyOn(Notify,'infoNoItems');
        $httpBackend.expect('GET', URL.ORDERS).respond(200, list);

        API.Orders.getAll().then(function(data){
          OrdersDetailsController.navigate('left');
          // expect Notify to be called as there is no order in left of list.
          expect(Notify.infoNoItems).toHaveBeenCalled();
        });
        $httpBackend.flush();
      });

    });
  });

});
