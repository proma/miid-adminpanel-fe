(function(app){

  app.config(function($stateProvider){

    $stateProvider
      .state('admin-panel.cust_mgmt.orders',{
        url: '/orders',
        abstract: true,
        template: '<div ui-view></div>'
      });

  });

})(angular.module('adminPanel.customer_management.orders.route',[]));
