(function (app) {

  app.controller('OrdersListChangeStatusModalController', OrdersListChangeStatusModalController);

  OrdersListChangeStatusModalController.$inject = ['OrdersList', 'Status', 'Classes', 'API', 'ALERT', 'Notify', '$uibModalInstance'];

  function OrdersListChangeStatusModalController(OrdersList, Status, Classes, API, ALERT, Notify, $uibModalInstance) {
    var modal = this;

    modal.title = 'status'; //translate id
    modal.statuses = Status;
    modal.selectedStatus = 'PAYMENT_ERROR';
    modal.changingStatus = false;
    modal.statusClass = Classes;
    modal.trackingOrders = [];
    modal.isTrackingView = false; //for tracking details

    modal.setTrackingCompany = function(order) {

      var shippingOption = _.find(order.shippingOptions, function(option) {
        return option._id == order.tracking.delivery_id;
      });

      if(shippingOption) {
        order.tracking.company = shippingOption.company;
      }
    };

    modal.hasDesignsToSend = function() {

      _.each(modal.trackingOrders, function(order) {
        if(order.tracking.designs.length > 0) {
          return true;
        }
      });

      return false;
    };

    modal.close = function(){
      $uibModalInstance.dismiss('cancel');
    };

    modal.submit = function(){

      if(modal.selectedStatus === 'SENT'){

        if(modal.isTrackingView) {

          angular.forEach(modal.trackingOrders, function(data){
            data.disabled = true;
          });
          change(true);

        } else {

          modal.title = 'Shipment details';
          modal.trackingOrders = []; //reset the array

          angular.forEach(OrdersList, function(data){
            if(data.checked){
              data.disabled = false;
              modal.trackingOrders.push(data);
            }
          });

          _initTrackingDetails();
          _loadShippingOptions();

          modal.isTrackingView = true;

        }

      }
      else
        change(false);
    };


    // Private functions

    function _initTrackingDetails() {

      _.each(modal.trackingOrders, function(order) {

        //Get remaining designs to send
        var allTrackingDesigns = [];
        var remainingDesigns = [];

        _.each(order.tracking_details, function (tracking) {
          allTrackingDesigns = _.union(allTrackingDesigns, tracking.designs);
        });

        _.each(order.designs, function(orderDesign){
          if(orderDesign.design) {
            var foundIndex = _.findIndex(allTrackingDesigns, function (trackingDesign) {
              return orderDesign.design == trackingDesign;
            });
            if (foundIndex == -1) {
              remainingDesigns.push(orderDesign.design);
            }
          }
        });

        //Init tracking model
        order.tracking = {
          delivery_id: order.delivery.id,
          company: order.delivery.company,
          designs: remainingDesigns,
          charges: 0
        };
      });

      console.log(modal.trackingOrders);
    }

    function _loadShippingOptions() {

      modal.changingStatus = true;

      async.each(modal.trackingOrders, function(order, callback) {

        if(order.brand && order.brand.length > 0) {

          API
              .Brands
              .getShippingOptions(order.brand[0]._id)
              .then(function (data) {
                order.shippingOptions = data;
                callback();
              })
              .catch(function (err) {
                callback({type:'api', err:err, order:order});
              });

        } else if(order.delivery) {

          order.shippingOptions = [{
            _id: order.delivery.delivery_id,
            company: order.delivery.company
          }];

          callback();

        } else {
          callback({type:'brand', err:{msg:'Brand Not Found'}, order:order});
        }

      }, function(err) {

        modal.changingStatus = false;

        if( err ) {
          ALERT.confirm(_loadShippingOptions);
        }
      });

    }

    function change(isSent){

      modal.changingStatus = true; //disables button

      var noOfCalls = 0; //no of http requests to call

      //checking if user has selected orders?

      angular.forEach(OrdersList,function(data){
        if (data.checked){
          if (modal.selectedStatus === 'SENT' || modal.selectedStatus != data.status)
            ++noOfCalls;
        }
      });

      //if noOfCalls 0 then return

      if(noOfCalls === 0 && modal.selectedStatus != 'SENT'){
        modal.changingStatus = false;
        Noify.infoSameItemStatus();

        return;
      }

      // change status of all the objects.
      var completedReq = 0;
      var noOfErrors = 0;

      function callBack(err){
        if(!err){
          ++completedReq;
        }
        if(err){
          ++noOfErrors;
        }
        if(noOfCalls == (completedReq+noOfErrors)){
          modal.changingStatus = false;

          //all calls have finished, let's call Notify

          Notify.infoChangeItemsStatus(noOfCalls,completedReq,modal.selectedStatus);

          // hide the modal

          $uibModalInstance.close({
            success: true
          });
        }
      }

      if(isSent){

        //validate cost fields
        _.each(modal.trackingOrders, function(order){
          if(!order.tracking.charges) {
            order.tracking.charges = 0;
          }
        });

        angular.forEach(modal.trackingOrders, function(order){

          if(order.tracking.designs.length > 0) {

            //Add tracking

            API.Orders
                .addTracking(order._id, order.tracking)
                .then(function (result) {

                  order.tracking_details = order.tracking_details || [];
                  order.tracking_details.push(result);

                  order.status = 'SENT';

                  callBack();

                })
                .catch(function (err) {
                  Notify.errorItemStatus();
                  callBack(err);
                });
          } else {

            //Update status only

            API.
                Orders
                .edit(order._id, {status:'SENT'})
                .then(function(result){
                  order.status = result.status;
                  callBack();
                })
                .catch(function(err){
                  if(err.data && err.data.message) {
                    Notify.error(err.data.message);
                  } else {
                    Notify.errorItemStatus();
                  }
                  callBack(err);
                });
          }

        });

      }else{
        angular.forEach(OrdersList, function(data){
          //if status of selected order == selected status, we save http request.
          if(data.checked)
            if(modal.selectedStatus != data.status){
              //we send request to change status.
              var obj = {
                status: modal.selectedStatus
              };
              API.
              Orders
                .edit(data._id, obj)
                .then(function(result){
                  data.status = result.status;
                  callBack();
                })
                .catch(function(err){
                  if(err.data && err.data.message) {
                    Notify.error(err.data.message);
                  } else {
                    Notify.errorItemStatus();
                  }
                  callBack(err);
                });
            }
        });
      }
    }
  }

})(angular.module('adminPanel.customer_management.orders.list.changeStatusModal.controller', []));
