'use strict';

describe('Controllers', function () {
  describe('OrdersListExportModalController', function(){
    var data = {
      templates: [
        {
          _id: 'FAKE_TEMPLATE1',
          title: 'FAKE TEMPLATE'
        },
        {
          _id: 'FAKE_TEMPLATE2',
          title: 'FAKE TEMPLATE'
        }
      ]
    };


    var
      Controller,
      $uibModalInstance,
      Notify,
      URL,
      ALERT,
      $httpBackend,
      Modal;

    // load the controller's module
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.services.notifier'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('ui.bootstrap'));
    beforeEach(module('adminPanel.services.alert'));
    beforeEach(module('ui.bootstrap'));
    beforeEach(module('adminPanel.api'));
    beforeEach(module('adminPanel.customer_management.orders.list.ordersExportModal.controller'));

    beforeEach(inject(function($injector){
      var $controller;
      $uibModalInstance = {dismiss: function () {}};
      URL = $injector.get('URL');
      $httpBackend = $injector.get('$httpBackend');
      Notify = $injector.get('Notify');
      ALERT = $injector.get('ALERT');
      $controller = $injector.get('$controller');
      Modal = {
        open: function () {
        }
      };

      spyOn(Modal, 'open').and.returnValue({result: {then: function(){{}}}});
      spyOn($uibModalInstance, 'dismiss');

      Controller = $controller('OrdersListExportModalController',{
        'Search': {},
        'SelectedOrders': 'fake',
        'API': $injector.get('API'),
        'ALERT': ALERT,
        'Notify': Notify,
        'Modal': Modal,
        '$uibModalInstance': $uibModalInstance
      });
      data.templateURL = URL.ORDERS + '/po/templates';
      data.exportURL = URL.ORDERS + '/export_po';
    }));


    it('should close the modal', function () {
      Controller.close();
      expect($uibModalInstance.dismiss).toHaveBeenCalled();
    });

    it('should set the type as SELECTED', function () {
      expect(Controller.export.selectedOrdersType).toEqual('SELECTED');
    });

    it('should set disableSelected to false', function () {
      expect(Controller.export.disableSelected).toBeFalsy();
    });

    it('should have set the templates to empty', function () {
      expect(Controller.templates.length).toBe(0);
    });

    it('should send request to get templates', function () {
      $httpBackend.expect('GET' ,data.templateURL).respond(200, data.templates);
      expect(Controller.loadingTemplates).toBeTruthy();
      $httpBackend.flush();
      expect(Controller.templates.length).toBe(2);
      expect(Controller.templates[0]._id).toBe('FAKE_TEMPLATE1');
      expect(Controller.loadingTemplates).toBeFalsy();
    });

    it('should send request to get templates and set it to first template', function () {
      $httpBackend.expect('GET' ,data.templateURL).respond(200, data.templates);
      expect(Controller.loadingTemplates).toBeTruthy();
      $httpBackend.flush();
      expect(Controller.templates.length).toBe(2);
      expect(Controller.templates[0]._id).toBe('FAKE_TEMPLATE1');
      expect(Controller.loadingTemplates).toBeFalsy();
      expect(Controller.export.selectedTemplate).toBe(Controller.templates[0]._id);
    });

    it('should set selectedTemplate to null if no templates', function () {
      $httpBackend.expect('GET' ,data.templateURL).respond(200, []);
      expect(Controller.loadingTemplates).toBeTruthy();
      $httpBackend.flush();
      expect(Controller.templates.length).toBe(0);
      expect(Controller.loadingTemplates).toBeFalsy();
      expect(Controller.export.selectedTemplate).toBeNull();
    });

    it('should show alert if request did\'nt get well for GET templates', function () {
      spyOn(ALERT, 'confirm');
      $httpBackend.expect('GET' ,data.templateURL).respond(500, {});
      expect(Controller.loadingTemplates).toBeTruthy();
      $httpBackend.flush();
      expect(Controller.templates.length).toBe(0);
      expect(ALERT.confirm).toHaveBeenCalled();
      expect(Controller.loadingTemplates).toBeFalsy();
    });

    it('should open modal when clicked on create template', function () {
      Controller.createTemplate();
      expect(Modal.open).toHaveBeenCalled();
    });

    it('should delete a template', function () {
      $httpBackend.expect('GET', data.templateURL).respond(200, data.templates);
      $httpBackend.flush();

      $httpBackend.expect('DELETE', data.templateURL + '/FAKE_TEMPLATE1').respond(200, {});

      Controller.deleteTemplate(Controller.templates[0]);
      $httpBackend.flush();

      expect(Controller.templates.length).toEqual(1);
    });

    it('should show Notify if error in deleting a template', function () {
      spyOn(Notify, 'errorSaving');

      $httpBackend.expect('GET', data.templateURL).respond(200, data.templates);
      $httpBackend.flush();

      $httpBackend.expect('DELETE', data.templateURL + '/FAKE_TEMPLATE1').respond(500, {});

      Controller.deleteTemplate(Controller.templates[0]);
      $httpBackend.flush();

      expect(Notify.errorSaving).toHaveBeenCalled();
    });

    it('should show error if error submitting the form', function () {
      spyOn(Notify, 'errorSaving');

      $httpBackend.expect('GET', data.templateURL).respond(200, data.templates);
      $httpBackend.flush();

      $httpBackend.expect('POST', data.exportURL).respond(500, {});

      Controller.submit();
      $httpBackend.flush();

      expect(Notify.errorSaving).toHaveBeenCalled();
    });

  });

});
