(function (app) {

  app.controller('OrdersListExportModalController', OrdersListExportModalController);

  OrdersListExportModalController.$inject = ['SelectedOrders', 'Search', 'API', 'Notify', 'ALERT', 'Modal', 'URL', '$localStorage', '$uibModalInstance'];

  function OrdersListExportModalController(SelectedOrders, Search, API, Notify, ALERT, Modal, URL, $localStorage, $uibModalInstance) {
    var modal = this;

    modal.templates = [];
    modal.loadingTemplates = false;
    modal.disableSelected = !SelectedOrders.length;
    modal.export = {
      from: moment().subtract(1, 'week'),
      to: moment(),
      selectedOrdersType: SelectedOrders.length? 'SELECTED': 'ALL',
      selectedLanguage: 'EN',
      selectedFormat: 'PDF',
      selectedTemplate: 'DEFAULT',
      separateOrders: {
        enable: false,
        type: 'BRAND'
      },
      attachPictures: {
        enable: false,
        type: 'PDF'
      }
    };

    modal.getTemplates = function () {
      modal.loadingTemplates = true;
      API
        .Orders
        .Templates
        .query()
        .then(function (templates) {
          modal.templates = templates;
          modal.export.selectedTemplate = modal.templates.length? modal.templates[0]._id: null;
        })
        .catch(function (err) {
          console.error('There was an error loading templates at ' + new Date(), err);
          ALERT.confirm(modal.getTemplates);
        })
        .finally(function () {
          modal.loadingTemplates = false;
        });
    };

    modal.deleteTemplate = function (template) {
      template.processing = true;
      API
        .Orders
        .Templates
        .delete(template._id)
        .then(function () {
          modal.templates = _.without(modal.templates, template);
          Notify.infoSaved();
        })
        .catch(function () {
          Notify.errorSaving();
        })
        .finally(function () {
          template.processing = false;
        })
    };

    modal.createTemplate = function () {
      Modal.open(
        'OrdersListTemplateCreationModalController',
        'template',
        'app/modules/customer_management/orders/list/orders-export-modal/template-creation-modal/customerManagement.orders.list.templateCreationModal.html',
        null,
        'lg',
        null, null, 'template-modal'
      )
        .result
        .then(function (template) {
          modal.templates.push({_id: template.templateID, title: template.templateName});
        });
    };


    modal.submit = function () {
      var postData = {
        language: modal.export.selectedLanguage,
        format: modal.export.selectedFormat,
        template: modal.export.selectedTemplate
      };
      postData.is_separate = modal.export.separateOrders.enable;
      postData.is_attachment = modal.export.attachPictures.enable;
      postData.orders = {filter_type: modal.export.selectedOrdersType};
      if (modal.export.separateOrders.enable) {
        postData.separate_by = modal.export.separateOrders.type;
      }
      if (modal.export.attachPictures.enable) {
        postData.attachment_in = modal.export.attachPictures.type;
      }
      if (modal.export.selectedOrdersType === 'ALL')
        postData.orders.all = true;
      else if (modal.export.selectedOrdersType === 'RANGE') {
        postData.orders.from = modal.export.from.valueOf();
        postData.orders.to = modal.export.to.valueOf();
      } else if (modal.export.selectedOrdersType === 'SELECTED')
        postData.orders.selected = SelectedOrders;
      else if (modal.export.selectedOrdersType === 'CURRENT_SEARCH') {
        postData.orders.current_search = Search;
      }
      modal.createAndSubmitForm(postData);
      modal.close();
    };

    modal.createElement = function (name, val) {
      var elm = document.createElement('input');
      elm.setAttribute('type', 'text');
      elm.setAttribute('hidden', 'hidden');
      elm.setAttribute('value', val);
      elm.setAttribute('name', name);
      return elm;
    };

    //making HTML form because downloads doesn't work with AJAX
    modal.createAndSubmitForm = function (obj) {
      var f = document.createElement("form"), s = document.createElement("input");

      f.setAttribute('method',"post");
      f.setAttribute('name',"ordersExportForm");
      f.setAttribute('target',"_blank");
      f.setAttribute('action', URL.EXPORT);

      angular.forEach(obj, function (grandParentVal, grandParentKey) {
        if (_.keys(obj[grandParentKey]).length) {
          angular.forEach(obj[grandParentKey], function (parentVal, parentKey) {
            if (_.keys(obj[grandParentKey][parentKey]).length) {
              angular.forEach(obj[grandParentKey][parentKey], function (val, key) {
                f.appendChild(modal.createElement(grandParentKey + '[' + parentKey + '][' + key + ']', val));
              })
            } else {
              f.appendChild(modal.createElement(grandParentKey + '[' + parentKey + ']', parentVal));
            }
          })
        } else {
          f.appendChild(modal.createElement(grandParentKey, grandParentVal));
        }
      });

      f.appendChild(modal.createElement('token', $localStorage['jwt']));
       //input element, Submit button
      s.setAttribute('type',"submit");
      s.setAttribute('hidden',"hidden");
      s.setAttribute('value',"Submit");

      f.appendChild(s);

      document.getElementsByTagName('body')[0].appendChild(f);
      document['ordersExportForm'].submit();
      f.remove();
    };

    modal.close = function(){
      $uibModalInstance.dismiss('cancel');
    };

    modal.getTemplates();
  }

})(angular.module('adminPanel.customer_management.orders.list.ordersExportModal.controller', []));
