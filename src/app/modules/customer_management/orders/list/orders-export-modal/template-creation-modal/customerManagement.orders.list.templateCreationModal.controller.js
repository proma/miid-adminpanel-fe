(function (app) {

  app.controller('OrdersListTemplateCreationModalController', OrdersListTemplateCreationModalController);

  OrdersListTemplateCreationModalController.$inject = ['API', 'Notify', '$timeout', '$uibModalInstance', '$translate'];

  function OrdersListTemplateCreationModalController(API, Notify, $timeout, $uibModalInstance, $translate) {
    var template = this;
    console.log($translate.use());
    template.lang = $translate.use().toLowerCase() == 'en_us'? 'en': 'ch';

    template.close = function(success, data){
      if (success)
        $uibModalInstance.close(data);
      else
        $uibModalInstance.dismiss('cancel');
    };

    template.name = '';

    template.column1 = {
      MERCHANT: {
        title: {
          en: 'Merchant',
          ch: '商家'
        },
        checked: true,
        tip: {
          en: 'Merchant name',
          ch: '商家'
        }
      },
      MERCHANT_TYPE: {
        title: {
          en: 'Merchant type',
          ch: '商家类型'
        },
        checked: false,
        tip: {
          en: 'Merchant type',
          ch: '商家类型'
        }
      },
      ORDER_NO: {
        title: {
          en: 'Order #',
          ch: '订单#'
        },
        checked: false,
        tip: {
          en: 'Order#',
          ch: '订单#'
        }
      },
      ORDER_ID: {
        title: {
          en: 'Order ID',
          ch: '订单 ID'
        },
        checked: false,
        tip: {
          en: 'Order ID (correspondent to payment)',
          ch: '订单 ID '
        }
      },
      ORDER_DATE: {
        title: {
          en: 'Order date',
          ch: '下单日子'
        },
        checked: false,
        tip: {
          en: 'Date in which the order was done',
          ch: '下单日子'
        }
      },
      USER_ID: {
        title: {
          en: 'User ID',
          ch: '用户 ID'
        },
        checked: true,
        tip: {
          en: 'User ID',
          ch: '用户 ID'
        }
      },
      ORDER_STATUS: {
        title: {
          en: 'Order status',
          ch: '订单状态'
        },
        checked: true,
        tip: {
          en: 'Status of the order in the moment of export',
          ch: '订单状态'
        }
      },
      SALES_CHANNEL: {
        title: {
          en: 'Sales channel',
          ch: '渠道'
        },
        checked: true,
        tip: {
          en: 'Channel where the sell took place',
          ch: '下单渠道'
        }
      },
      PAYMENT_METHOD: {
        title: {
          en: 'Payment method',
          ch: '支付方式'
        },
        checked: false,
        tip: {
          en: 'Payment method used by the user',
          ch: '用户使用的支付方式'
        }
      },
      ORDER_SUBTOTAL: {
        title: {
          en: 'Order subtotal',
          ch: '订单小计'
        },
        checked: true,
        tip: {
          en: 'Order subtotal amount (Order total - Shipment total)',
          ch: '订单小计 （订单总额 － 快递小计）'
        }
      },
      SHIPMENT_PRICE: {
        title: {
          en: 'Shipment price',
          ch: '快递小计'
        },
        checked: true,
        tip: {
          en: 'Shipment fee paid by the user',
          ch: '用户支付的快递费用'
        }
      },
      ORDER_TOTAL_AMOUNT: {
        title: {
          en: 'Order total amount',
          ch: '订单总额'
        },
        checked: false,
        tip: {
          en: 'Order total amount (sum of all items + shipment fee)',
          ch: '订单总额 （订单小计 ＋ 快递小计）'
        }
      },
      DISCOUNT_NAME: {
        title: {
          en: 'Discount/promo name',
          ch: '优惠券名称'
        },
        checked: true,
        tip: {
          en: 'Name of coupon used',
          ch: '优惠券名称 '
        }
      },
      DISCOUNT_AMOUNT: {
        title: {
          en: 'Discount/promo amount',
          ch: '活动／优惠券金额'
        },
        checked: true,
        tip: {
          en: 'Actual amount discounted from the original prices thanks to the use of the discount or promotion',
          ch: '活动／优惠劵实际扣掉的金额， 按照原价'
        }
      },
      BANK_FEE: {
        title: {
          en: 'Bank fee',
          ch: '支付方式手续费'
        },
        checked: true,
        tip: {
          en: 'Amount discounted from this order from the payment gateway as a fee ( Order total x gateway fee %)',
          ch: '支付方式手续费'
        }
      }
    };

    template.column2 = {
      SALES_CHANNEL_SHARE: {
        title: {
          en: 'Sales channel share',
          ch: '渠道分成金额'
        },
        checked: true,
        tip: {
          en: 'Amount that belongs to the sales channel ( Order total x Channel %) ',
          ch: '渠道分成金额 （订单总额 X 渠道比例）'
        }
      },
      IP_SHARE: {
        title: {
          en: 'IP share',
          ch: 'IP分成金额'
        },
        checked: true,
        tip: {
          en: 'Amount that belongs to the IP merchant ( Order total x IP %) ',
          ch: 'IP分成金额 （订单总额 X IP比例）'
        }
      },
      BRAND_SHARE: {
        title: {
          en: 'Brand share',
          ch: '品牌分成金额'
        },
        checked: false,
        tip: {
          en: 'Amount that belongs to the brand merchant ( Order total x Brand %) ',
          ch: '品牌分成金额 （订单总额 X 品牌比例）'
        }
      },
      MIID_SHARE: {
        title: {
          en: 'MIID share',
          ch: 'MIID分成金额'
        },
        checked: false,
        tip: {
          en: 'Amount that belongs to MiID ( Order total x MiID %) ',
          ch: 'MIID分成金额 （订单总额 X MiID比例）'
        }
      },
      ORDER_SHIPMENT_COST: {
        title: {
          en: 'Order shipment costs',
          ch: '快递总成本'
        },
        checked: false,
        tip: {
          en: 'The actual total cost of the shipment of the order, as input by admin when sent ',
          ch: '订单快递总实际成本 '
        }
      },
      ORDER_PRODUCTION_COST: {
        title: {
          en: 'Order production costs',
          ch: '生产总成本金额'
        },
        checked: false,
        tip: {
          en: 'The total cost of the production of items in this order',
          ch: '生产实际总成本金额'
        }
      },
      DELIVERY_ADDRESS: {
        title: {
          en: 'Delivery address (full)',
          ch: '收获地址 （全）'
        },
        checked: true,
        tip: {
          en: 'The full delivery address in one single field',
          ch: '全收获地址'
        }
      },
      ADDRESS_CONSIGNEE: {
        title: {
          en: '(Address) Consignee',
          ch: '（地址） 收获人名'
        },
        checked: false,
        tip: {
          en: 'The name of the consignee for delivery',
          ch: '（地址） 收获人名'
        }
      },
      ADDRESS_PHONE_NUMBER: {
        title: {
          en: '(Address) Phone number',
          ch: '（地址） 电话'
        },
        checked: false,
        tip: {
          en: 'Phone number of consignee for delivery',
          ch: '（地址） 电话'
        }
      },
      ADDRESS_PROVINCE: {
        title: {
          en: '(Address) Province/City/District',
          ch: '（地址）区域'
        },
        checked: false,
        tip: {
          en: 'The Province/City/Region of the consignee for delivery',
          ch: '（地址）区域'
        }
      },
      SPECIFIC_ADDRESS: {
        title: {
          en: '(Address) Specific address',
          ch: '（地址） 具体地址'
        },
        checked: true,
        tip: {
          en: 'The specific address of the consignee for delivery',
          ch: '（地址） 具体地址'
        }
      },
      SHIPMENT_COMPANY: {
        title: {
          en: 'Shipment company',
          ch: '快递公司'
        },
        checked: true,
        tip: {
          en: 'Name of the actual shipment company used to deliver the products',
          ch: '快递公司'
        }
      },
      USER_COMMENTS: {
        title: {
          en: 'User comments',
          ch: '用户注释'
        },
        checked: true,
        tip: {
          en: 'Comments made by the user to the order',
          ch: '用户注释'
        }
      },
      CS_COMMENTS: {
        title: {
          en: 'CS comments',
          ch: '客服注释'
        },
        checked: true,
        tip: {
          en: 'Comments made by the customer service to the order',
          ch: '客服注释'
        }
      },
      PRODUCT_MIID_ID: {
        title: {
          en: 'Product MIID ID',
          ch: '产品 MIID ID'
        },
        checked: false,
        tip: {
          en: 'Product MIID ID',
          ch: '产品 MIID ID'
        }
      }
    };

    template.column3 = {
      PRODUCT_SKU: {
        title: {
          en: 'Product SKU',
          ch: '产品 SKU'
        },
        checked: false,
        tip: {
          en: 'Product SKU',
          ch: '产品 SKU'
        }
      },
      PRODUCT_NAME: {
        title: {
          en: 'Product Name',
          ch: '产品名'
        },
        checked: false,
        tip: {
          en: 'Product name',
          ch: '产品名'
        }
      },
      REF_PIC_FRONT: {
        title: {
          en: 'Reference pic front (PDF)',
          ch: '参考图 （前）'
        },
        checked: false,
        tip: {
          en: 'Reference picture front (only available in PDF)',
          ch: '参考图 （前）只能在PDF格式实现'
        }
      },
      REF_PIC_BACK: {
        title: {
          en: 'Reference pic back (PDF)',
          ch: '参考图 （后）'
        },
        checked: false,
        tip: {
          en: 'Reference picture back (only available in PDF)',
          ch: '参考图 （后）只能在PDF格式实现'
        }
      },
      DESIGN_ID: {
        title: {
          en: 'Design ID',
          ch: '设计 ID'
        },
        checked: false,
        tip: {
          en: 'Design ID',
          ch: '设计 ID'
        }
      },
      DESIGN_LINK: {
        title: {
          en: 'Design link',
          ch: '设计链接'
        },
        checked: false,
        tip: {
          en: 'Design link',
          ch: '设计链接'
        }
      },
      PRODUCT_MANUFACTURER: {
        title: {
          en: 'Product Manufacturer',
          ch: '生产家'
        },
        checked: true,
        tip: {
          en: 'Name of the product manufacturer',
          ch: '生产家名称'
        }
      },
      ITEM_QUANTITY: {
        title: {
          en: 'Product item Quantity',
          ch: '数量'
        },
        checked: false,
        tip: {
          en: 'Quantity for this item',
          ch: '产品数量'
        }
      },
      PRODUCT_NOTES: {
        title: {
          en: 'Product production notes',
          ch: '产品生产注释'
        },
        checked: false,
        tip: {
          en: 'Product production notes',
          ch: '产品生产注释'
        }
      },
      PRODUCT_ESTIMATE_SHIPPING_DATE: {
        title: {
          en: 'Product Estimate shipping date',
          ch: '要出货时间'
        },
        checked: false,
        tip: {
          en: 'The estimated shipping date (date of order + production time)',
          ch: '要出货时间'
        }
      },
      PRODUCT_ACTUAL_SHIPPING_DATE: {
        title: {
          en: 'Product actual shipping date',
          ch: '实际出货时间'
        },
        checked: true,
        tip: {
          en: 'The date in which the product was actually shipped, as input by the administrator',
          ch: '实际出货时间'
        }
      },
      PRODUCT_SHIPPING_TRACKING_NO: {
        title: {
          en: 'Product Shipping tracking #',
          ch: '快递跟单号'
        },
        checked: false,
        tip: {
          en: 'Shipment tracking number',
          ch: '快递跟单号'
        }
      },
      PRODUCT_ESTIMATED_RECEIVING_DATE: {
        title: {
          en: 'Estimates receiving date',
          ch: '预计收获日期'
        },
        checked: false,
        tip: {
          en: 'Estimated receiving date according to the third party delivery system',
          ch: '预计收获日期'
        }
      },
      PRODUCT_SELLING_PRICE_BASIC: {
        title: {
          en: 'Product Selling price (basic)',
          ch: '产品售价 （基本）'
        },
        checked: false,
        tip: {
          en: 'The basic price of the product (price without add-ons)',
          ch: '产品售价 （基本售价， 不带具体定制部件的成本）'
        }
      },
      PRODUCT_SELLING_PRICE_TOTAL: {
        title: {
          en: 'Product Selling price (total)',
          ch: '产品售价 （总额）'
        },
        checked: false,
        tip: {
          en: 'The total price of the product (Product basic price + sum of all selected add-ons)',
          ch: '产品售价（总额售价， 带着所有具体定制部件的成本）'
        }
      }
    };

    template.column4 = {
      PRODUCT_PROD_COST_BASIC: {
        title: {
          en: 'Product Prod. Costs (basic)',
          ch: '产品生产成本 （基本）'
        },
        checked: true,
        tip: {
          en: 'The basic cost of producing the product (cost without add-ons)',
          ch: '产品生产成本（基本售价， 不带具体定制部件的成本）'
        }
      },
      PRODUCT_PROD_COST_TOTAL: {
        title: {
          en: 'Product Prod. Costs (total)',
          ch: '产品生产成本 （总额）'
        },
        checked: true,
        tip: {
          en: 'The total cost of producing the product (cost including the add-ons costs)',
          ch: '产品生产成本 （总额售价， 带着所有具体定制部件的成本）'
        }
      },
      PRODUCT_SHIPMENT_COST: {
        title: {
          en: 'Product shipment costs',
          ch: '产品快递成本'
        },
        checked: true,
        tip: {
          en: 'Actual shipping costs of shipping this product (as input by the admin)',
          ch: '产品快递成本'
        }
      },
      CUSTOMISATION_STEPS: {
        title: {
          en: 'Customisation steps',
          ch: '定制步骤'
        },
        checked: false,
        tip: {
          en: 'Customisation step tech#',
          ch: '定制步骤技术名称'
        }
      },
      CUSTOMISATION_GROUPS: {
        title: {
          en: 'Customisation groups',
          ch: '定制部件'
        },
        checked: false,
        tip: {
          en: 'Button group tech#',
          ch: '定制部件技术名称'
        }
      },
      CUSTOMISATION_TECH1: {
        title: {
          en: 'Customisation option tech#1',
          ch: '定制选项技术号1'
        },
        checked: true,
        tip: {
          en: 'Customisation option Tech#1',
          ch: '定制选项技术名称＃1'
        }
      },
      CUSTOMISATION_TECH2: {
        title: {
          en: 'Customisation option tech#2',
          ch: '定制选项技术号2'
        },
        checked: true,
        tip: {
          en: 'Customisation option Tech#2*',
          ch: '定制选项技术名称＃2'
        }
      },
      CUSTOMISATION_TECH3: {
        title: {
          en: 'Customisation option tech#3',
          ch: '定制选项技术号3'
        },
        checked: true,
        tip: {
          en: 'Customisation option Tech#3*',
          ch: '定制选项技术名称＃3'
        }
      },
      OPTION_SELLING_PRICE: {
        title: {
          en: 'Option selling price',
          ch: '定制选项售价'
        },
        checked: false,
        tip: {
          en: 'Price paid by the user for the customisation option',
          ch: '定制选项售价'
        }
      },
      OPTION_IP_COST: {
        title: {
          en: 'Option IP costs',
          ch: '定制选项IP成本'
        },
        checked: false,
        tip: {
          en: 'IP cost of the customisation option',
          ch: '定制选项IP成本'
        }
      },
      OPTION_PRODUCTION_COST: {
        title: {
          en: 'Option Production costs',
          ch: '定制选项生产成本'
        },
        checked: true,
        tip: {
          en: 'Production cost of the customisation option',
          ch: '定制选项生产成本'
        }
      }
    };

    template.submit = function () {
      template.processing = true;

      var postData = {};
      postData.title = template.name;
      postData.fields = template.getFieldsArray();

      API
        .Orders
        .Templates
        .create(postData)
        .then(function (data) {
          Notify.infoSaved();
          $timeout(function () {
            template.close(true, {templateID: data._id, templateName: template.name});
          }, 200);
        })
        .catch(function (err) {
          console.error('Error while saving Template PO at ' + new Date(), err );
          Notify.errorSaving();
        })
        .finally(function () {
          template.processing = false;
        })
    };

    template.getFieldsArray = function () {
      var allFields = [];
      allFields = allFields.concat(template.getKeys(template.column1));
      allFields = allFields.concat(template.getKeys(template.column2));
      allFields = allFields.concat(template.getKeys(template.column3));
      allFields = allFields.concat(template.getKeys(template.column4));
      return allFields;
    };

    template.getKeys = function (obj) {
      var temp = [];
      _.forEach(obj, function (val, key) {
        if (val.checked)
          temp.push(key);
      });
      return temp;
    }
  }

})(angular.module('adminPanel.customer_management.orders.list.ordersExportModal.templateCreationModal.controller', []));
