'use strict';

describe('Controllers', function () {
  describe('OrdersListTemplateCreationModalController', function(){
    var data = {};

    var
      Controller,
      $uibModalInstance,
      Notify,
      URL,
      $timeout,
      $httpBackend;

    // load the controller's module
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.services.notifier'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('ui.bootstrap'));
    beforeEach(module('ui.bootstrap'));
    beforeEach(module('adminPanel.api'));
    beforeEach(module('adminPanel.customer_management.orders.list.ordersExportModal.templateCreationModal.controller'));

    beforeEach(inject(function($injector){
      var $controller;
      $uibModalInstance = {dismiss: function () {}, close: function(){}};
      URL = $injector.get('URL');
      $httpBackend = $injector.get('$httpBackend');
      Notify = $injector.get('Notify');
      $timeout = $injector.get('$timeout');
      $controller = $injector.get('$controller');

      spyOn($uibModalInstance, 'dismiss');
      spyOn($uibModalInstance, 'close');

      Controller = $controller('OrdersListTemplateCreationModalController',{
        'API': $injector.get('API'),
        'Notify': Notify,
        '$timeout': $timeout,
        '$uibModalInstance': $uibModalInstance
      });
      data.templateURL = URL.ORDERS + '/po/templates';
    }));


    it('should call close if passed with param', function () {
      Controller.close(true);
      expect($uibModalInstance.close).toHaveBeenCalled();
    });

    it('should call dismiss if passed without param', function () {
      Controller.close();
      expect($uibModalInstance.dismiss).toHaveBeenCalled();
    });

    it('should initialize the columns', function () {
      expect(Controller.column1).toBeDefined();
      expect(Controller.column2).toBeDefined();
      expect(Controller.column3).toBeDefined();
      expect(Controller.column4).toBeDefined();
    });

    it('should get fields correctly', function () {
      Controller.column1 = {test1: {checked: true}};
      Controller.column2 = {test2: {checked: true}};
      Controller.column3 = {test3: {checked: false}};
      Controller.column4 = {test4: {checked: true}};
      expect(Controller.getFieldsArray().length).toBe(3);
      expect(Controller.getFieldsArray()[0]).toBe('test1');
    });

    it('should get keys of objects with checked true', function () {
      expect(Controller.getKeys({test: {checked: true}}).length).toBe(1);
      expect(Controller.getKeys({test: {checked: true}})[0]).toBe('test');
    });

    it('should get empty array if no keys', function () {
      expect(Controller.getKeys({test: {checked: false}}).length).toBe(0);
    });

    it('should close the modal after creation of template', function () {
      spyOn(Controller, 'close');
      $httpBackend.expect('POST', data.templateURL).respond(200, {});
      Controller.submit();
      $httpBackend.flush();
      $timeout.flush();

      expect(Controller.close).toHaveBeenCalled();
    });

    it('should not close the modal after creation of template instead show error', function () {
      spyOn(Controller, 'close');
      spyOn(Notify, 'errorSaving');
      $httpBackend.expect('POST', data.templateURL).respond(500, {});
      Controller.submit();
      $httpBackend.flush();
      $timeout.flush();

      expect(Controller.close).not.toHaveBeenCalled();
      expect(Notify.errorSaving).toHaveBeenCalled();
    });

  });

});
