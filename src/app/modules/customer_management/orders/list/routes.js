(function(app){

  app.config(['$stateProvider', 'URLProvider', function($stateProvider, URLProvider){
    $stateProvider
      .state('admin-panel.cust_mgmt.orders.list',{
        url: '/list',
        templateUrl: 'app/modules/customer_management/orders/list/orders.html',
        controller: 'OrdersController',
        controllerAs: 'vm',
        data: {
          permissions: {
            only: 'canSee.Orders.Listing',
            redirectTo: URLProvider.$get().RedirectURL //hackish way to inject service in config :D
          }
        }
      });

  }]);

})(angular.module('adminPanel.customer_management.orders.list.route',[]));
