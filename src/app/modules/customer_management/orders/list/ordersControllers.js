(function(app){
  'use strict';

  app.controller('OrdersController',OrdersController);

  OrdersController.$inject = ['$scope', 'ALERT', 'API', 'Notify', 'Modal', 'QINGSTOR', '$state', '$translate'];

  function OrdersController($scope, ALERT, API, Notify, Modal, QINGSTOR, $state, $translate){
    var vm = this;

    //Statuses for select box

    vm.status = [
      {
        name: $translate.instant('filters.status.label') || 'Status',
        disabled: true,
        value: 'status'
      },
      {
        name: 'Payment Error',
        value: 'PAYMENT_ERROR'
      },
      {
        name: 'Paid',
        value: 'PAID'
      },
      {
        name: 'In Production',
        value: 'IN_PRODUCTION'
      },
      {
        name: 'Sent',
        value: 'SENT'
      },
      {
        name: 'Received',
        value: 'RECEIVED'
      },
      {
        name: 'Finalized',
        value: 'FINALIZED'
      }
    ];

    vm.head = [
      {
        title: 'Orders',
        query: 'order'
      },
      {
        title: 'Brand',
        query: 'brand'
      },
      {
        title: 'Qty',
        query: 'qty'
      },
      {
        title: 'Date',
        query: 'date'
      },
      {
        title: 'Amount',
        query: 'amount'
      },
      {
        title: 'Customer',
        query: 'customer'
      },
      {
        title: 'Phone',
        query: 'phone'
      },
      {
        title: 'Status',
        query: 'status'
      }
    ];
    vm.statusClass = {
      IN_PRODUCTION: 'production',
      SENT: 'sent',
      RECEIVED: 'received',
      PENDING: 'pending',
      PAYMENT_ERROR: 'error',
      PAID: 'paid',
      FINALIZED: 'finalized'
    };

    vm.brands =[{
      name: $translate.instant('filters.brand.label') || 'Brand',
      id: -1
    }];

    vm.filters = {
      status: vm.status[0].value,
      brand: vm.brands[0],
      from: moment((new Date("1/1/" + moment().year())).valueOf()),
      to: moment(),
      text: "",
      paymentError: false
    };
    vm.keyword ='';


    vm.assetURL = QINGSTOR.base_url + 'po/';
    vm.loading = true;
    vm.orders = [];
    vm.totalItems = 0;
    vm.itemsGot = 0;
    vm.previousPage = 0;
    vm.currentPage = 1;
    vm.itemsLimit = 10;
    vm.loadCached = false; //boolean to load cached result

    // get all brands
    vm.listBrands = function() {
      API
        .Brands
        .getAll(null,null,'LIST_VALUES')
        .then(function (brands) {
        if (brands.data && brands.data.length) {

          brands.data.forEach(function (brand) {
            vm.brands.push({id: brand._id, name: brand.name});
          })
        }
      }).catch(function (err) {
        vm.brands = [{name: $translate.instant('errors.errorLoadingBrand') || 'Error loading brands', id: -1}]
      });
    };

    //Get all orders
    vm.listOrders = function() {
      //reset select list
      vm.sortOptions.count = 0;
      vm.sortOptions.listBox = false;
      //start loading so that user can't make more request.
      vm.loading = true;

      API
        .Orders
        .getAll(vm.itemsLimit
          , vm.currentPage-1
          , vm.sortOptions.order
          , vm.sortOptions.category
          , (vm.filters.status != 'status')? vm.filters.status : null
          , (vm.filters.brand.id != -1)? vm.filters.brand.id : null
          , vm.filters.from.valueOf()
          , vm.filters.to.valueOf()
          , vm.filters.paymentError
          , vm.filters.text
        ).then(function(re){
          angular.forEach(re.data, function(obj){
            var d = new Date(obj.created_at);
            obj.created_at = d.getDate() + ' / ' + (d.getMonth()+1) + ' / ' + d.getFullYear();
            //for checkbox
            obj.checked = false
          });
          vm.loading = false;
          vm.orders =  re.data;
          // set display flag for client side text filtering
          _.each(vm.orders, function(order) {
            order.display = true;
          });
          vm.itemsGot = vm.orders.length;
          vm.totalItems = re.paging.total;
        })
        .catch(function(err){
          ALERT.confirm(vm.listOrders);
          vm.loading = false;
        });
    };

    //check all checkboxes
    vm.toggleCheckbox = function(checkAll){
      angular.forEach(vm.orders,function(data){
        data.checked = checkAll;
      });
      if(checkAll)
        vm.sortOptions.count = vm.orders.length; //as this checks all the current page orders
      else
        vm.sortOptions.count = 0;
      return false;
    };

    //mark checkbox
    vm.markCheckBox = function(status){
      vm.sortOptions.count = 0;
      var alreadyChecked = false;
      angular.forEach(vm.orders, function (data) {
        if (data.checked) {
          alreadyChecked = true;//just need one checked box
          ++vm.sortOptions.count; //inc counter
        }
      });

        if (status) {
          //mark checkbox as true
          vm.sortOptions.listBox = true;
        } else {
          //check if checkbox is already true
          //check if none boxes are checked
          if (!alreadyChecked) {
            vm.sortOptions.listBox = false;
          }
        }

    };

    //change page
    vm.changePage = function(){
      vm.previousPage = (vm.currentPage-1);
      vm.loading = true;
      vm.listOrders();
    };

    //change single status
    vm.changeSingleStatus = function(order, status){

      if(order.status === status){
        Notify.infoSameItemStatus();
        return;
      }

      if(status == 'SENT') {

        //Check there are still remaining designs to send
        var allTrackingDesignIDs = [];
        _.each(order.tracking_details, function (tracking) {
          allTrackingDesignIDs = _.union(allTrackingDesignIDs, tracking.designs);
        });

        if(allTrackingDesignIDs.length < order.designs.length) {
          vm.showTrackingModal(order);
          return;
        }
      }

      order.changingStatus = true;

      API.
        Orders
        .edit(order._id, {'status': status})
        .then(function(result){
          order.status = result.status;
          Notify.infoChangeStatus(status);
          order.changingStatus = false;
        })
        .catch(function(err){
          if(err.data && err.data.message) {
            Notify.error(err.data.message);
          } else {
            Notify.errorItemStatus();
          }
          order.changingStatus = false;
        });
    };

    //tracking modal for single status : for SENT status
    vm.showTrackingModal = function(order){

      Modal.open(
        'OrdersListCTrackingModalController',
        'modal',
        'app/modules/customer_management/orders/list/tracking/customerManagement.orders.list.trackingModal.html',
        {
          Order : function(){
            return order;
          }
        },
        'lg'
      );
    }; // end change status function

    //Change status of the selected orders
    vm.changeStatus = function(){
      Modal.open(
        'OrdersListChangeStatusModalController',
        'modal',
        'app/modules/customer_management/orders/list/change_status/customerManagement.orders.list.changeStatusModal.html',
        {
          OrdersList : function(){
            return vm.orders;
          },
          Status: function() {
            return vm.status;
          },
          Classes: function() {
            return vm.statusClass;
          }
        },
        'md'
      );
    }; // end change status function

    vm.exportOrders = function () {
      Modal.open(
        'OrdersListExportModalController',
        'modal',
        'app/modules/customer_management/orders/list/orders-export-modal/customerManagement.orders.list.ordersExportModal.html',
        {
          SelectedOrders : function(){
            return _.chain(vm.orders).where({checked: true}).pluck('_id').value();
          },
          Search: function () {
            var searchObj =
            {
              status: vm.filters.status.value != 'status'? vm.filters.status.value: null,
              brand: vm.filters.brand.id != -1? vm.filters.brand.id : null,
              from: vm.filters.from.valueOf(),
              to: vm.filters.to.valueOf(),
              search: vm.filters.text
            };
            searchObj = _.pick(searchObj, _.identity);
            searchObj.payment_error = vm.filters.paymentError;
            return searchObj;
          }
        },
        'export',
        null, null, 'export-orders', null
      );
    };

    //sorting options
    vm.sortOptions = {
      templateUrl : 'customer_management/orders/list/templates/sortTemplate.html',
      order: 'desc',
      category: 'date', //first initialize
      handler: function(){
        //start loading so that user can't make more request.
        vm.loading = true;
        vm.currentPage = 1; //set page to 0 again.
        vm.listOrders();
      },
      toggleCheckbox: vm.toggleCheckbox,
      listBox: false, //initial
      count: 0,
      changeStatus: vm.changeStatus,
      exportOrders: function () {
        return vm.exportOrders();
      }
    };

    vm.refreshPO = function(order) {
      order.refreshingPO = true;
      API
        .Orders
        .genPO(order._id)
        .then(function() {
          order.refreshingPO = false;
          Notify.infoPOFiles();
        })
        .catch(function() {
          order.refreshingPO = false;
          Notify.errorPOFiles();
        })
    };

    // to populate Brands drop down
    vm.listBrands();

    //navigate to details and uncheck all orders
    vm.navigateToDetails = function(orderID) {
      vm.orders.forEach(function(order) {
        order.checked = false;
      });
      $state.go('admin-panel.cust_mgmt.orders.details', {'orderID': orderID});
    };

    // work around for date picker bug, that is not except variables in vm but except scope variable and functions
    $scope.dateChanged = function(){
      vm.listOrders();
    };

    $scope.$watch("vm.filters.from",function(){
        if(vm.filters.from.valueOf() > vm.filters.to.valueOf()){
          vm.filters.from = moment(vm.filters.to);
        }
    });

    $scope.$watch("vm.filters.to",function(){
        if (vm.filters.to.valueOf() < vm.filters.from.valueOf()) {
          vm.filters.to = moment(vm.filters.from);
        }
    });
  }
})(angular.module('adminPanel.customer_management.orders.list.controller',[]));
