(function(app){

  app.controller('OrdersChartController', OrdersChartController);

  function OrdersChartController(){
    var vm = this;
    vm.lineData = {
      labels: ["Nov 4", "Nov 8", "Nov 12", "Nov 16", "Nov 20", "Nov 24", "Nov 28"],
      datasets: [
        {
          label: "Example dataset",
          fillColor: "rgba(220,220,220,0.5)",
          strokeColor: "rgba(220,220,220,0.9)",
          pointColor: "#dcdcdc",
          pointStrokeColor: "#caf3f4",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: [65, 59, 80, 81, 56, 55, 40]
        },
        {
          label: "Example dataset",
          fillColor: "rgba(26,216,219,0.5)",
          strokeColor: "rgba(26,216, 219,0.7)",
          pointColor: "#1ab092",
          pointStrokeColor: "#caf3f4",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(26,179,148,1)",
          data: [28, 48, 40, 19, 86, 27, 90]
        }
      ]
    };
    vm.lineOptions = {
      scaleFontSize: 12,
      scaleBeginAtZero: true,
      scaleLabel: function(object) {
        return "      " + object.value;
      },
      scaleLineWidth: 1,
      scaleOverride: true,
      scaleSteps: 5,
      scaleStepWidth: 20,
      scaleFontStyle: "light",
      scaleFontColor: "#818385",
      scaleShowGridLines : true,
      scaleGridLineColor : "rgba(0,0,0,.04)",
      scaleGridLineWidth : 1,
      bezierCurve : true,
      bezierCurveTension : 0.4,
      pointDot : true,
      pointDotRadius : 4,
      pointDotStrokeWidth : 1,
      pointHitDetectionRadius : 20,
      datasetStroke : true,
      datasetStrokeWidth : 2,
      datasetFill : true
    };

  }

})(angular.module('adminPanel.customer_management.orders.list.controller'));

