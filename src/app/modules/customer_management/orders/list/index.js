(function(app){

})(angular.module('adminPanel.customer_management.orders.list',
  [
    'adminPanel.customer_management.orders.list.route',
    'adminPanel.customer_management.orders.list.controller',
    'adminPanel.customer_management.orders.list.changeStatusModal',
    'adminPanel.customer_management.orders.list.trackingModal',
    'adminPanel.customer_management.orders.list.ordersExportModal'
  ]
));
