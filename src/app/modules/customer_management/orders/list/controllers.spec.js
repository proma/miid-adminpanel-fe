'use strict';

describe('Controllers', function () {
  describe('OrdersController', function(){
    var data = {
      data: [
        {
          _id: '152a7a8eb85e3064',
          created_at: "2016-01-15T20:57:27.268Z",
          status: 'FINALIZED',
          brand: [
            { name: "Brand 1" }
          ],
          customer: [
            { name: "Customer 1", phone: "+8618611111111"}
          ],
          designs: [
            {
              quantity: 2
            },
            {
              quantity: 2
            }
          ]
        },
        {
          _id: '152a312c162592f6',
          created_at: "2015-01-15T20:57:27.268Z",
          status: 'PENDING',
          brand: [
            { name: "Brand 2" }
          ],
          customer: [
            { name: "Customer 2", phone: "+8618622222222"}
          ],
          designs: [
            {
              quantity: 4
            },
            {
              quantity: 2
            }
          ]
        }
      ],
      paging: {
        total: 5
      },
      url: '',
      brands: {
        data: [
          {
            name: 'SOFLO',
            id: 'BRAND1'
          },
          {
            name: 'Brand2',
            id: 'testbrand'
          }
        ],
        paging: {
          total: 5
        }
      }
    };


    var
      OrdersController,
      $rootScope,
      Notify,
      URL,
      ALERT,
      $httpBackend,
      Modal,
      $scope;

    // load the controller's module
    beforeEach(module('ngResource'));
    beforeEach(module('adminPanel.services.notifier'));
    beforeEach(module('adminPanel.services.modal'));
    beforeEach(module('adminPanel.constants'));
    beforeEach(module('ui.bootstrap'));
    beforeEach(module('adminPanel.services.alert'));
    beforeEach(module('ui.bootstrap'));
    beforeEach(module('adminPanel.api'));
    beforeEach(module('ui.router'));
    beforeEach(module('adminPanel.customer_management.orders.list.controller'));

    beforeEach(inject(function($injector){
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      URL = $injector.get('URL');
      $httpBackend = $injector.get('$httpBackend');
      Notify = $injector.get('Notify');
      ALERT = $injector.get('ALERT');
      Modal = $injector.get('Modal');
      var $controller = $injector.get('$controller');
      OrdersController = $controller('OrdersController',{
        '$scope': $scope,
        'API': $injector.get('API'),
        'ALERT': ALERT,
        'Notify': Notify,
        'Modal': Modal,
        '$state': $injector.get('$state'),
        'QINGSTOR': $injector.get('QINGSTOR')
      });
      data.url = '?from=' + OrdersController.filters.from.valueOf() + '&limit=10&page=0&payment_error=false&sort_by=date&sort_order=desc';
      data.url += '&to=' +  OrdersController.filters.to.valueOf();
      $httpBackend.expect('GET',URL.BRANDS + '?list_type=LIST_VALUES').respond(200, data.brands);
      OrdersController.listOrders();
    }));


    describe('MA-29: List Orders',function(){
      it('should have empty orders in start',function(){
        expect(OrdersController.orders.length).toEqual(0);
      });

      it('should get date in this 16 / 1 / 2016 format',function(){
        $httpBackend.expect('GET',URL.ORDERS+data.url).respond(200, data);
        $httpBackend.flush();
        expect(OrdersController.orders[0].created_at).toBe("16 / 1 / 2016");
      });

      it('should get error network error',function(){
        $httpBackend.expect('GET',URL.ORDERS+data.url).respond(500, data);

        spyOn(ALERT,'confirm');
        $httpBackend.flush();

        expect(ALERT.confirm).toHaveBeenCalled();
      });

      it('should initialise the display flag to true for all orders',function(){
        $httpBackend.expect('GET',URL.ORDERS+data.url).respond(200, data);
        $httpBackend.flush();
        OrdersController.listOrders();
        _.each(OrdersController.orders, function(order) {
          expect(order.display).toBe(true);
        });
      });

    });

    describe('MA-32: Perform Operations on many Orders',function(){

      it('should select disabled status by default',function(){
        expect(OrdersController.filters.status.value).toBe('status');
      });

      it('should uncheck checkboxes by default',function(){
        $httpBackend.expect('GET',URL.ORDERS+data.url).respond(200, data);
        $httpBackend.flush();
        OrdersController.listOrders();
        expect(OrdersController.orders[0].checked).toBeFalsy();
      });

      it('should check all checkboxes',function() {
        $httpBackend.expect('GET', URL.ORDERS + data.url).respond(200, data);
        $httpBackend.flush();
        OrdersController.listOrders();
        OrdersController.toggleCheckbox(true);
        expect(OrdersController.orders[0].checked).toBeTruthy();
      });

      it('should uncheck all checkboxes',function(){
        $httpBackend.expect('GET', URL.ORDERS + data.url).respond(200, data);
        $httpBackend.flush();
        OrdersController.listOrders();
        //set first order to true
        OrdersController.orders[0].checked = true;

        OrdersController.toggleCheckbox(false);
        expect(OrdersController.orders[0].checked).toBeFalsy();
      });

      it("should open modal for changing status",function(){
        spyOn(Modal, 'open');
        OrdersController.changeStatus();
        expect(Modal.open).toHaveBeenCalled();
      });

    });

    describe('MA-34: Pagination', function() {

      it('should set items limit to 10', function() {
        expect(OrdersController.itemsLimit).toBe(10);
      });

      it('should set totalItems to 5', function() {
        $httpBackend.expect('GET',URL.ORDERS + data.url).respond(200, data);
        $httpBackend.flush();
        expect(OrdersController.totalItems).toBe(5);
      });

      it('should set itemsGot to 2', function() {
        $httpBackend.expect('GET',URL.ORDERS + data.url).respond(200, data);
        $httpBackend.flush();
        expect(OrdersController.itemsGot).toBe(2);
      });

      it('should query page 2 when called changePage()',function(){
        spyOn(OrdersController,'listOrders');
        OrdersController.currentPage = 2; // page is index based so in query page 1 will be called
        $httpBackend.when('GET',URL.ORDERS+data.url).respond(200, data);
        $httpBackend.when('GET',URL.ORDERS+'?limit=10&page=1&sort_order=asc&sort_by=date').respond(200, data);

        OrdersController.changePage();

        $httpBackend.flush();

        expect(OrdersController.listOrders).toHaveBeenCalled();
      });
    });

    describe('MBA-35: Downlaod PO files', function () {
      it('should call Notify and refresh po files by calling api', function () {
        spyOn(Notify,'infoPOFiles');
        OrdersController.currentPage = 1; // page is index based so in query page 1 will be called
        $httpBackend.when('GET',URL.ORDERS+'/generate_po/' + data.data[0]._id).respond(200, data);
        $httpBackend.when('GET',URL.ORDERS+data.url).respond(200, data);
        OrdersController.refreshPO(data.data[0]);

        $httpBackend.flush();

        expect(Notify.infoPOFiles).toHaveBeenCalled();

      });

      it('should define a add refreshingPO property to the order', function () {
        OrdersController.currentPage = 1; // page is index based so in query page 1 will be called
        $httpBackend.when('GET',URL.ORDERS+'/generate_po/' + data.data[0]._id).respond(200, {success: true});
        $httpBackend.when('GET',URL.ORDERS+data.url).respond(200, data);
        OrdersController.refreshPO(data.data[0]);

        $httpBackend.flush();

        expect(data.data[0].refreshingPO).toBeDefined();
      });

      it('should call error Notify if there is error calling refresh PO api', function () {
        spyOn(Notify,'errorPOFiles');
        OrdersController.currentPage = 1; // page is index based so in query page 1 will be called
        $httpBackend.when('GET',URL.ORDERS+'/generate_po/' + data.data[0]._id).respond(500, {success: false});
        $httpBackend.when('GET',URL.ORDERS+data.url).respond(200, data);
        OrdersController.refreshPO(data.data[0]);

        $httpBackend.flush();

        expect(Notify.errorPOFiles).toHaveBeenCalled();

      });
    });

  });

});
