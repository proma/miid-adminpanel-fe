(function (app) {

  app.controller('OrdersListCTrackingModalController', OrdersListCTrackingModalController);

  OrdersListCTrackingModalController.$inject = ['Order', 'API', 'Notify', '$uibModalInstance', 'QINGSTOR'];

  function OrdersListCTrackingModalController(Order, API, Notify, $uibModalInstance, QINGSTOR) {
    var modal = this;

    modal.changingStatus = false;
    modal.order = Order;
    modal.disabled = false;
    modal.isDesignView = false;
    modal.designURL = QINGSTOR.design_url;


    modal.close = function(){
      $uibModalInstance.dismiss('cancel');
    };

    modal.submit = function(){

      modal.disabled = true; //diable input

      if(!modal.tracking.charges) {
        modal.tracking.charges = 0;
      }

      API.Orders
        .addTracking(modal.order._id, modal.tracking)
        .then(function(result){
          Notify.infoSaved();
          modal.disabled = false;

          Order.tracking_details = Order.tracking_details || [];
          Order.tracking_details.push(result);

          //Check all the tracking and Update order status
          var allTrackingDesignIDs = [];
          _.each(Order.tracking_details, function (tracking) {
            allTrackingDesignIDs = _.union(allTrackingDesignIDs, tracking.designs);
          });

          if(Order.designs.length == allTrackingDesignIDs.length) {
            Order.status = 'SENT';
          }

          $uibModalInstance.close();
        })
        .catch(function(err){
          Notify.errorSaving();
          modal.disabled = false;
          $uibModalInstance.close();
        });
    };

    modal.showDesignView = function() {

      //Set checked
      _.each(modal.remainingDesigns, function(remainingDesign){
        var index = _.findIndex(modal.tracking.designs, function(trackingDesignID){
          return remainingDesign._id == trackingDesignID;
        });
        if(index != -1) remainingDesign.isChosen = true;
        else remainingDesign.isChosen = false;
      });

      modal.choosingDesigns = angular.copy(modal.remainingDesigns);

      modal.isDesignView = true;
    };

    modal.saveCheckedDesigns = function() {

      modal.tracking.designs = [];

      _.each(modal.choosingDesigns, function(design){

        if(design.isChosen) {

          modal.tracking.designs.push(design._id);
        }

      });

      modal.isDesignView = false;
    };

    modal.cancelCheckedDesigns = function() {
      modal.isDesignView = false;
    };

    modal.hasChangedDesignView = function() {

      //Check changes
      if(angular.equals(modal.choosingDesigns, modal.remainingDesigns)) return false;

      //Check one of designs is checked at least
      var index = _.findIndex(modal.choosingDesigns, function(design){
        return design.isChosen == true;
      });
      if(index == -1) return false;

      return true;
    };

    modal.setTrackingCompany = function() {

      var shippingOption = _.find(modal.shippingOptions, function(option) {
        return option._id == modal.tracking.delivery_id;
      });

      if(shippingOption) {
        modal.tracking.company = shippingOption.company;
      }
    };

    //Private functions

    //load shipping_options

    function _loadShippingOptions() {

      if(modal.order.brand && modal.order.brand.length > 0) {

        modal.disabled = true;

        API
          .Brands
          .getShippingOptions(modal.order.brand[0]._id)
          .then(function (data) {
            modal.disabled = false;
            modal.shippingOptions = data;
          })
          .catch(function (err) {
            modal.disabled = false;
            modal.shippingOptions = [];
          });

      } else if(modal.order.delivery) {

        modal.shippingOptions = [{
          _id: modal.order.delivery.delivery_id,
          company: modal.order.delivery.company
        }];

      } else {

        modal.shippingOptions = [];

      }
    }

    function _loadOrder() {

      modal.disabled = true;
      API
        .Orders
        .getOne(modal.order._id)
        .then(function(data) {
          modal.order = data;

          _initRemainingDesigns();

          //Init tracking model
          modal.tracking = {
            delivery_id: Order.delivery.id,
            company: Order.delivery.company,
            designs: modal.remainingDesignIDs,
            charges: 0
          };

          modal.disabled = false;
        })
        .catch(function(err) {
          modal.disabled = false;
        });
    }

    function _initRemainingDesigns() {

      var allTrackingDesignIDs = [];

      modal.order.tracking_details = modal.order.tracking_details || [];

      _.each(modal.order.tracking_details, function (tracking) {
        allTrackingDesignIDs = _.union(allTrackingDesignIDs, tracking.designs);
      });

      modal.remainingDesigns = [];
      modal.remainingDesignIDs = [];

      _.each(modal.order.designs, function(orderDesign){
        if(orderDesign.design) {
          var foundIndex = _.findIndex(allTrackingDesignIDs, function (trackingDesignID) {
            return orderDesign.design._id == trackingDesignID;
          });
          if (foundIndex == -1) {
            modal.remainingDesigns.push(orderDesign.design);
            modal.remainingDesignIDs.push(orderDesign.design._id);
          }
        }
      });
    }

    function _init() {

      _loadShippingOptions();

      _loadOrder();

    }

    _init();

  }

})(angular.module('adminPanel.customer_management.orders.list.trackingModal.controller', []));
