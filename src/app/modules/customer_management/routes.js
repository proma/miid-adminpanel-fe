(function(app){
  app.config(function($stateProvider){

    $stateProvider
      .state('admin-panel.cust_mgmt',{
        url: '/customer-management',
        abstract: true,
        template: '<ui-view/>'
      });

  });

})(angular.module('adminPanel.customer_management.routes',[]));
