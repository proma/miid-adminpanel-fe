describe('Directives', function(){

  describe('SortingDirective', function() {
    var
      $scope,
      $compile,
      $httpBackend,
      $rootScope,
      data = {
        columns: [
          {
            title: 'First',
            query: 'frst'
          },
          {
            title: 'Second',
            query: 'scnd'
          }
        ],
        options: {
          order: 'asc',
          category: 'frst',
          handler: function(){
          }
        },
        loading: false
      };
    var template =
    "<th ng-click=\"handleClick(column.query)\" ng-repeat=\"column in columns\">"+
     " {{column.title}}"+
    "<span class=\"sort\">"+
      "<i class=\"fa fa-sort\" ng-class=\"sorting? ((options.category == column.query)? ( count == 0 ? 'fa-sort-up' : 'fa-sort-down'):''): ''\"></i>"+
      "</span>"+
      "</th>";

    beforeEach(module('adminPanel.directives.sort'));

    beforeEach(inject(function($injector) {
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();

      $scope.options = data.options;
      $scope.loading = data.loading;
      $scope.columns = data.columns;

      $compile = $injector.get('$compile');

      $httpBackend = $injector.get('$httpBackend');
      $httpBackend.expect('GET', 'app/directives/sort/template.html').respond(200, template);
    }));

    function getElement(){
      var element,compiledElm;
      element = angular.element('<tr sort options="options" loading="loading" columns="columns">');

      compiledElm = $compile(element)($scope);
      $httpBackend.flush();

      $scope.$apply();
      $scope.$digest();
      return compiledElm;
    }

    it('should populate the data', function() {
      var el = getElement();
      expect(el.find('th').length).toBe(2);
    });

    it('should change class to fa-sort-up and fa-sort-down when clicked 2 times', function() {
      var el = getElement(), head = angular.element(el.find('th')[0]);

      head.click();
      expect(el.isolateScope().options.order).toBe('asc');
      expect(el.isolateScope().options.category).toBe('frst');

      var i1 = angular.element(el.find('th')[0]).find('i');

      expect(angular.element(i1).hasClass('fa-sort-up')).toBeTruthy();
      head.click();
      expect(angular.element(el.find('th')[0]).find('i').hasClass('fa-sort-up')).toBeFalsy();
      expect(el.isolateScope().options.order).toBe('desc');
    });

    it('should call handler when clicked', function() {
      var el = angular.element(getElement().find('th')[0]);
      spyOn($scope.options, 'handler');
      el.click();
      expect($scope.options.handler).toHaveBeenCalled();
    });

  });

});
