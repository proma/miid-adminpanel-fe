(function(app){

  /*
  This directive let's u sort the table. Just give options, loading boolean and columns
  - Columns: will have object array composed of title to appear in table and query for the api
  - Loading: we set loading to true from start of request and set it to false when request completes
  give this to this directive so that there are no more pending requests
  -Options:
    -templateUrl: give custom template Url with original template in it
    -order: order should be asc ( ascending to start with)
    -category: give initial category to start with
    -handler: this will be invoked when user clicks on sorting heads.
    You can give other objects to have it in your template
   */

  app.directive('sort', [ '$compile', '$templateRequest', function($compile, $templateRequest) {
    return {
      restrict: 'A',
      scope: {
        columns: '=',
        loading: '=',
        options: '='
      },
      replace: false,
      link: function($scope, $elem){
        $scope.sorting = false;
        $scope.options.order = $scope.options.order || 'desc';
        $scope.previousCat = '';
        $scope.count = 0;

        if($scope.options.templateUrl){
          $templateRequest('app/modules/' + $scope.options.templateUrl).then(function(html){
            var template = angular.element(html);
            $elem.append(template);
            $compile(template)($scope);
          });
        }else{
          $templateRequest('app/directives/sort/template.html').then(function(html) {
            var template = angular.element(html);
            $elem.append(template);
            $compile(template)($scope);
          });
        }

        $scope.handleClick = function(cat){
          if(!$scope.loading){
            //only process when there is no pending request.
            $scope.options.category = cat;
            $scope.sorting = true;
            if($scope.previousCat === cat){
              if($scope.count === 0){
                $scope.count++;
                $scope.options.order = 'desc';
              }else if($scope.count === 1){
                $scope.count--;
                $scope.options.order = 'asc';
              }
            }else {
              $scope.previousCat = cat;
              $scope.count = 0;
              $scope.options.order = 'asc';
            }
            //invoke provided function.
            $scope.options.handler();
          }
        };
      }
    };
  }]);

})(angular.module('adminPanel.directives.sort',[]));
