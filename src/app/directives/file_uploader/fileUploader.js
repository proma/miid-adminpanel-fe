(function(app){

    app.directive('fileUploader', function() {
      return {
        restrict: 'E',
        templateUrl: 'app/directives/file_uploader/templates/fileUploader.html',
        transclude: true,
        scope: {
          mainClass: '@',
          title: '@',
          type: '@',
          loading: '=',
          selectedPane: '='
        },
        replace: true,
        controller: ['$scope', function($scope) {
          var panes = $scope.panes = [];
          $scope.select = function(pane) {
            angular.forEach(panes, function(pane) {
              pane.selected = false;
            });
            pane.selected = true;
            $scope.selectedPane = pane.title;
          };

          this.addPane = function(pane) {
            if (panes.length === 0) {
              $scope.select(pane);
            }
            panes.push(pane);
          };
        }]
      };
    });

})(angular.module('adminPanel.directives.fileUploader',[]));
