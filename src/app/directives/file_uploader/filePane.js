(function(app){

  app.directive('filePane', function() {
    return {
      require: '^^fileUploader',
      restrict: 'E',
      transclude: true,
      replace: true,
      scope: {
        title: '@',
        state: '@'
      },
      controller: ['$scope', function ($scope) {
      }],
      link: function(scope, element, attrs, fileUploader) {
        fileUploader.addPane(scope);
      },
      templateUrl: 'app/directives/file_uploader/templates/filePane.html'
    };
  });


})(angular.module('adminPanel.directives.filePane',[]));
