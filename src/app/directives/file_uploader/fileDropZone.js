(function(app){

  app.directive('fileDropZone', function() {
    return {
      require: '^^filePane',
      restrict: 'E',
      replace: true,
      scope: {
        mainClass: '@',
        loading: '=',
        uploadProgress: '=',
        dropMessage: '@',
        sizeMessage: '@',
        dropHead: '@',
        loadingMessage: '@',
        uploadImage: '=',
        uploadImageParam: '='
      },
      controller: ['$scope', '$element', '$attrs', 'Upload', function($scope, element, attrs, Upload) {
        // TODO: need to add any functionality here if required
        $scope.$watch('file', function () {
          if($scope.file){
            Upload.base64DataUrl($scope.file)
              .then(function(url){
                $scope.src = url;
              });
          }
        });
        $scope.$watch('loading', function () {
          if($scope.file && $scope.loading === false) {
            $scope.src = null;
          }
        });
      }],
      templateUrl: 'app/directives/file_uploader/templates/fileDropZone.html'
    };
  });


})(angular.module('adminPanel.directives.fileDropZone',[]));
