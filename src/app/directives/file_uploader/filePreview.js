(function(app){

  app.directive('filePreview', function() {
      return {
        require: '^^filePane',
        restrict: 'E',
        replace: true,
        scope: {
          imageArray: '=',
          mainClass: '@',
          loading: '=',
          deleting: '=',
          moveUp: '&moveUp',
          moveDown: '&moveDown',
          removeImage:'=',
          removeImageParam: '='
        },
        link: function(scope, element, attrs) {
          //TODO:  need to handle if there is any necessary functionality
        },
        templateUrl: 'app/directives/file_uploader/templates/filePreview.html'
      };
    });


})(angular.module('adminPanel.directives.filePreview',[]));
