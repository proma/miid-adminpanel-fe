(function (app) {

  app.directive('ngOnLoad', function() {
    return {
      restrict: 'A',
      scope: {
        ngOnLoad: '&'
      },
      link: function(scope, element, attrs) {
        element.bind('load', function() {
          scope.$apply(function () {
            scope.ngOnLoad();
          });
        });
        element.bind('error', function(){
          scope.$apply(function () {
            scope.ngOnLoad();
          });
        });
      }
    };
  });

})(angular.module('adminPanel.directives.ngOnLoad', []));
