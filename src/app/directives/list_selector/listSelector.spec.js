describe('Directives', function(){

  describe('ListSelectorDirective', function() {
    var element,
      $scope,
      $compile,
      $httpBackend,
      $rootScope;

    var template =
    '<div class="list-box" ng-class="listBox ? \'selected\': \'\'">'+
      '<div class="border">'+
        '<input icheck type="checkbox" ng-change="listOptions.toggleCheckbox(listBox)" ng-model="listBox">'+
        '<div ng-show="listBox" class="text">'+
          '{{selectedItemsCount}} {{name}} selected'+
        '</div>'+
        '<div ng-show="listBox" class="testShow">'+
          '<ul>'+
            '<li ng-repeat="item in actions" role="menuitem">'+
              '<a href="javascript:;" ng-click="item.action()">{{item.title}}</a>'+
              '<div class="divider" ng-if="item.divider"></div>'+
            '</li>' +
          '</ul>' +
       '</div>' +
      '</div>' +
    '</div>';



    beforeEach(module('adminPanel.directives.listSelector'));

    beforeEach(inject(function($injector) {
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();

      $scope.items = [
        {
          title: 'First'
        },
        {
          title: 'Second'
        }
      ];
      $scope.actions = [
        {
          title: 'first action',
          action: function () {
            //test
          }
        }
      ];
      $scope.name = 'test';
      $scope.listOptions = {};

      $compile = $injector.get('$compile');
      $httpBackend = $injector.get('$httpBackend');
      $httpBackend.expect('GET', 'app/directives/list_selector/template.html').respond(200, template);
    }));

    function getElement(){
      var element,compiledElm;
      element = angular.element(
        '<table>' +
          '<thead>' +
            '<tr>' +
              '<th>' +
                '<div list-selector items="items" list-options="listOptions" name="name" actions="actions"></div>' +
              '</th>' +
            '</tr>' +
          '</thead>' +
          '<tbody>' +
            '<tr ng-repeat="item in items">' +
              '<td>' +
                '<input type="checkbox" ng-model="item.checked">' +
              '</td>' +
            '</tr>' +
          '</tbody>' +
        '</table>');

      compiledElm = $compile(element)($scope);
      $httpBackend.flush();

      $scope.$apply();
      $scope.$digest();
      return compiledElm;
    }

    function getDirElement() {
      return angular.element(getElement().find('div')[0]);
    }

    it('should populate bulk actions from the array provided', function () {
      var el = getElement();
      expect(el.find('ul').length).toEqual(1);//as we have only 1 item in array
    });

    it('should call action when clicked on some of the bulk action menu', function() {
      var el = angular.element(getElement().find('a')[0]);
      spyOn($scope.actions[0], 'action');
      el.click();
      expect($scope.actions[0].action).toHaveBeenCalled();
    });

    it('should have bound a function to listOptions (public:markCheckBox)', function() {
      var el = getElement();
      expect($scope.listOptions.markCheckBox).toBeDefined();
    });

    it('should not show listBox at first', function() {
      var el = getDirElement();
      expect(el.isolateScope().listBox).toBeDefined();
    });

    it('should have not selected any items at start', function () {
      var el = getDirElement();
      expect(el.isolateScope().selectedItemsCount).toEqual(0);
    });

    it('should show listBox and check all items when checkbox is checked', function () {
      var el = getElement();

      angular.element(el.find('input')[0]).click();

      expect($scope.items[0].checked).toBeTruthy();
      expect($scope.items[1].checked).toBeTruthy();
      expect($scope.listOptions.selectedItemsCount).toEqual(2);
    });

    it('should show listBox when an item is selected.', function () {
      var el = getDirElement();
      $scope.items[0].checked = true; //manually selecting
      $scope.listOptions.markCheckBox(true);
      expect(el.isolateScope().selectedItemsCount).toEqual(1);
      expect(el.isolateScope().items[0].checked).toBeTruthy();
      $scope.$digest(); //apply the changes.
      expect(el.find('.testShow').hasClass('ng-hide')).toBeFalsy();
    });

    it('should reset the itemsCount and hide listBox', function () {
      var scope = getDirElement().isolateScope();
      scope.selectedItemsCount = 4; //fake count
      scope.listBox = true;//faking
      scope.listOptions.reset(); // calling reset.

      expect(scope.selectedItemsCount).toEqual(0);
      expect(scope.listBox).toBeFalsy();
    });

  });

});
