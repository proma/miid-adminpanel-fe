(function(app){

  /*
   This directive let's u make the table rows selectable. Just give the following options.
   - items: will have array to mark checked or not.
   - name: give name to appear when selected like "6 -name- selected".
   -list-options:
    -markCheckBox(bool): this function will be binded to list-options object automatically
   -actions: you can provide your actions in the dropdown by using this array. it's structure is.
    -title: title of the action in dropdown
    -divider: this is boolean if u want divider under that actions ( to group items )
    -action: this should be a function to trigger when someone clicks on that action.
   */

  app.directive('listSelector', function() {
    return {
      restrict: 'A',
      templateUrl: 'app/directives/list_selector/template.html',
      scope: {
        name: '@',
        items: '=',
        listOptions: '=',
        actions: '='
      },
      replace: true,
      link: function($scope, $elem, $attr){
        var $table = $elem.closest('table');
        $scope.selectedItemsCount = 0;
        $scope.listBox = false;
        $scope.listOptions = {};
        $scope.listOptions.selectedItemsCount = $scope.selectedItemsCount; //making this public.

        //toggle all checkboxes
        $scope.listOptions.toggleCheckbox = function(checkAll){
          var selectedCount = 0;
          $table.find('tr td:first-child input[type=checkbox]').each(function (key, data) {
            if (!data.disabled) {
              $scope.items[key].checked = checkAll;
              selectedCount++;
            }
          });

          // angular.forEach($scope.items,function(data){
          //   data.checked = checkAll;
          // });
          if(checkAll)
            $scope.selectedItemsCount = selectedCount ;
          else
            $scope.selectedItemsCount = 0;

          $scope.listOptions.selectedItemsCount = $scope.selectedItemsCount;
          return false;
        };

        $scope.listOptions.markCheckBox = function(status){
          $scope.selectedItemsCount = 0;
          var alreadyChecked = false;
          angular.forEach($scope.items, function (data) {
            if (data.checked) {
              alreadyChecked = true;//just need one checked box
              ++$scope.selectedItemsCount; //inc counter
            }
          });

          if (status) {
            //mark checkbox as true
            $scope.listBox = true;
          } else {
            //check if checkbox is already true
            //check if none boxes are checked
            if (!alreadyChecked) {
              $scope.listBox = false;
            }
          }
          $scope.listOptions.selectedItemsCount = $scope.selectedItemsCount;
        };

        $scope.listOptions.reset = function () {
          $scope.listBox = false;
          $scope.selectedItemsCount = 0;
          $scope.listOptions.selectedItemsCount = $scope.selectedItemsCount;
        };

      }
    };
  });

})(angular.module('adminPanel.directives.listSelector',[]));
