describe('Directives', function(){

  describe('PaginatorDirective', function() {
    var
      $scope,
      $rootScope,
      data = '';

    beforeEach(module('ui.bootstrap'));
    beforeEach(module('ngStorage'));
    beforeEach(module('templates'));
    beforeEach(module('adminPanel.directives.paginator'));

    beforeEach(inject(function($injector) {
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();

      $scope.onPageChange = function () {};
      $scope.itemsLimit = 10;
      $scope.currentPage = 1;
      $scope.loading = true;

      $compile = $injector.get('$compile');

      spyOn($scope,'onPageChange');
    }));

    function getElement(){
      var element,compiledElm;
      element = angular.element('<paginator on-page-change="onPageChange" items-limit="itemsLimit" items="20" total="40" loading="loading" current-page="currentPage"></paginator>');

      compiledElm = $compile(element)($scope);

      $scope.$apply();
      $scope.$digest();
      return compiledElm;
    }

    it('should set current page', function() {
      var el = getElement();

      expect(el.isolateScope().currentPage).toBe($scope.currentPage);
    });

    it('should set previousPage', function () {
      expect(getElement().isolateScope().previousPage).toBe(0);
    });

    it('should set previousPage when called onPageChange', function () {
      $scope.currentPage = 2;
      var scope = getElement().isolateScope();
      scope.changePage();
      expect(scope.previousPage).toBe(1);
      expect($scope.onPageChange).toHaveBeenCalled();
    });

  });

});
