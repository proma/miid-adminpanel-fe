(function(app){

  app.directive('paginator', ['$uibModal', '$localStorage', '$timeout', function($uibModal, $localStorage, $timeout) {
    return {
      priority: 0,
      restrict: 'AE',
      scope: {
        loading: '=',
        onPageChange: '=',
        itemsLimit: '=',
        currentPage: '=',
        items: '=',
        total: '='
      },
      templateUrl: 'app/directives/paginator/paginator.template.html',
      replace: false,
      link: function ($scope){
        $scope.pagination = {
          currentPage: $scope.currentPage
        };
        $scope.previousPage = $scope.currentPage-1;
        $scope.itemsLimit = $localStorage[('DEFAULT_LIST_PAGINATION_LIMIT')] || 10;

        $scope.changePage = function () {
          $scope.previousPage = ($scope.pagination.currentPage-1);
          $scope.currentPage = $scope.pagination.currentPage;
          $timeout($scope.onPageChange, 100);
        };

        $scope.changeOptions = function () {
          $uibModal.open({
            size: 'paginator',
            backdrop: 'static',
            controller: ['$uibModalInstance', function ($uibModalInstance) {
              var modal = this;
              modal.close = $uibModalInstance.dismiss;
              modal.itemsLimit = $localStorage['DEFAULT_LIST_PAGINATION_LIMIT'] || 10;
              modal.default = $localStorage['DEFAULT_LIST_PAGINATION_LIMIT'];

              modal.submit = function () {
                if (modal.setDefault)
                  $localStorage['DEFAULT_LIST_PAGINATION_LIMIT'] = modal.itemsLimit;

                $uibModalInstance.close(modal.itemsLimit);
              };

            }],
            windowTopClass: 'bounceIn animated',
            controllerAs: 'modal',
            templateUrl: 'app/directives/paginator/paginator.modal.template.html'
          })
            .result
            .then(function (limit) {
              $scope.itemsLimit = limit;
              $timeout($scope.onPageChange, 100);
            });
        };

        $timeout($scope.onPageChange, 100)
      }
    };
  }]);

})(angular.module('adminPanel.directives.paginator',[]));
