(function () {
  'use strict';

  angular
    .module('adminPanel')
    .config(translateBlock)
    .run(permissionRunBlock)
    .run(mainRunBlock);

  function mainRunBlock($log, $state, $rootScope) {
    $rootScope.$state = $state;
    $log.debug('APP running...');
  }

  function permissionRunBlock(RolesService, jwtHelper, $localStorage) {

    var currentToken = $localStorage.jwt;
    var tokenPayload = {};
    if (currentToken) {
      tokenPayload = jwtHelper.decodeToken(currentToken);
    }

    RolesService.flush(); //removes all previous permissions
    RolesService.createPermissions(tokenPayload.type);

  }

  function translateBlock($translateProvider, $localStorageProvider) {
    $translateProvider.useStaticFilesLoader({
      prefix: 'resources/locale-',
      suffix: '.json'
    });

    $translateProvider.preferredLanguage($localStorageProvider.get('lang') || 'en_US');
    $translateProvider.useMissingTranslationHandlerLog();
  }

})();
