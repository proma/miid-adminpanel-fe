(function() {
  'use strict';

  angular
    .module('adminPanel')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('admin-panel', {
        abstract: true,
        views: {
          'main': {
            templateUrl: 'app/views/main.html',
            controller: 'AppController'
          }
        },
        controllerAs: 'vm'
      });

    $urlRouterProvider.otherwise('/not-found');
  }

})();
