/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('adminPanel')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
