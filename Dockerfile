FROM registry.miid.com:5000/nodejs:v5.12.0

MAINTAINER Diego Rojas <diego@bithive.io>

EXPOSE 3000

# downloading app dependencies into temporal folder
ADD package.json /tmp/package.json
RUN cd /tmp/ && npm install
ADD bower.json /tmp/bower.json
RUN cd /tmp/ && bower install --allow-root

# moving app and modules into app folder
WORKDIR /app
ADD . /app
RUN mv /tmp/node_modules/ /app/
RUN mv /tmp/bower_components/ /app/

CMD ["gulp", "release"]
